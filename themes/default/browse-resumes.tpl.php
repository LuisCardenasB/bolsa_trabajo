<?php
if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php include("header.tpl.php");?>

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="ten columns">
			<span>Hemos encontrado los siguientes currículums:</span>
			<h2>Buscar currículums</h2>
		</div>

		<div class="six columns">
			<a href="<?php echo SITEURL . '/add-resume.php'; ?>" class="button">Publique un currículum, ¡es gratis!</a>
		</div>

	</div>
</div>

<!-- Content
================================================== -->
<div class="container">
	<!-- Recent Jobs -->
	<div class="eleven columns">
	<div class="padding-right">

		<form action="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" method="get" class="list-search">
			<button type="submit"><i class="fa fa-search"></i></button>
			<input id="keyword" type="text" name="search" placeholder="Buscar profesionales (por ejemplo, Ingeniero Civil)" value="<?php echo ( isset($_GET['search']) && $_GET['search'] != '' ) ? $_GET['search'] : '';?>"/>
			<div class="clearfix"></div>
		</form>

    <?php if($resumes): ?>
  		<ul class="resumes-list">
        <?php foreach ($resumes as $resume): ?>
    			<li><a href="resume.php?resumeid=<?php echo $resume->uid;?>">
    				<img src="<?php echo ($resume->avatar != '') ? UPLOADURL. 'avatars/' . $resume->avatar : THEMEURL . '/images/avatar-placeholder.png'; ?>" alt="<?php echo $resume->fullname;?>">
    				<div class="resumes-list-content">
    					<h4><?php echo $resume->fullname;?> <span><?php echo $resume->title;?></span></h4>
    					<span><i class="fa fa-map-marker"></i> <?php echo $resume->city;?></span>
    					<span><i class="fa fa-money"></i> $<?php echo $resume->hourly_rate;?> / hour</span>
              <p><?php echo substr(strip_tags($resume->objective),0,200); ?></p>

    					<div class="skills">
    						<?php $jobs->getJobSkillTags($resume->skills) ?>
    					</div>
    					<div class="clearfix"></div>

    				</div>
    				</a>
    				<div class="clearfix"></div>
    			</li>
        <?php endforeach; ?>

  		</ul>
    <?php endif; ?>
		<div class="clearfix"></div>

    <div class="pagination-container paginate">
      <p class="total-pages">
        <?php echo Lang::$word->TOTAL . ': ' . $pager->items_total;?> / <?php echo Lang::$word->CURPAGE . ': ' . $pager->current_page . ' ' . Lang::$word->OF . ' ' . $pager->num_pages;?>
      </p>
      <div class="pagination-new">
        <?php echo $pager->display_pages();?>
      </div>
    </div>

	</div>
	</div>


	<!-- Widgets -->
	<div class="five columns">

		<!-- Sort by -->
		<div class="widget">
			<h4>Ordenar por</h4>

			<!-- Select -->
  			<select id="orderfilter" data-placeholder="Choose Category" class="chosen-select-no-single">
  				<option value="DESC" <?php echo (isset($_GET['short']) && $_GET['short'] == 'DESC') ? 'selected="selected"' : ''; ?>>Sueldo deseado: El mas alto primero</option>
  				<option value="ASC" <?php echo (isset($_GET['short']) && $_GET['short'] == 'ASC') ? 'selected="selected"' : ''; ?>>Sueldo deseado: El mas bajo primero</option>
  			</select>

		</div>

		<!-- Skills -->
		<div class="widget">
			<h4>Habilidades</h4>

			<!-- Select -->
			<form action="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" method="get">
				<select name="skills[]" data-placeholder="Seleccionar habilidades" class="chosen-select" multiple>
					<?php $jobs->getJobSkillDropList($row->skills);?>
				</select>
				<div class="margin-top-15"></div>
				<button class="button">Filtrar</button>
			</form>
		</div>

		<!-- Location -->
		<div class="widget">
			<h4>Ubicación</h4>
			<form action="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" method="get">
				<input type="text" placeholder="Estado / Municipio" value=""/>
				<input type="text" placeholder="Ciudad" value=""/>

				<input type="text" class="miles" placeholder="Distancia" value=""/>
				<!--<label for="zip-code" class="from">desde</label>-->
				<input type="text" id="zip-code" class="zip-code" placeholder="Codigo postal" value=""/><br>

				<button class="button">Filtrar</button>
			</form>
		</div>

	</div>
	<!-- Widgets / End -->


</div>

<?php include("footer.tpl.php");?>
<script type="text/javascript">
  $(document).ready(function () {
      $('#orderfilter').change(function () {
			var order   = $("#orderfilter option:selected").val();
			var keyword = $("#keyword").val();
			var url = '<?php echo SITEURL . '/browse-resumes.php'?>';
			if(order != '' && keyword != ''){
				var url = url + '?search=' + keyword + '&short=' + order;
			} else if (order == '' && keyword != '') {
				var url = url + '?search=' + keyword;
			} else if (order != '' && keyword == '') {
				var url = url + '?short=' + order;
			}
			window.location.href = url;
      })
  });
</script>
