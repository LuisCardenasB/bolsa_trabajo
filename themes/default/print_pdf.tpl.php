<?php
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php if($row):?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo Lang::$word->INVOICE;?></title>
<style type="text/css">
body {
  background-color: #fff;
  color: #333;
  font-family: DejaVu Serif, Helvetica, Times-Roman;
  font-size: 1em;
  margin: 0;
  padding: 0
}
table {
  font-size: 75%;
  width: 100%;
  border-collapse: separate;
  border-spacing: 2px
}
th,
td {
  position: relative;
  text-align: left;
  border-radius: .25em;
  border-style: solid;
  border-width: 1px;
  padding: .5em
}
th {
  background: #EEE;
  border-color: #BBB
}
td {
  border-color: #DDD
}
h1 {
  font: bold 100% sans-serif;
  letter-spacing: .5em;
  text-align: center;
  text-transform: uppercase
}
table.inventory {
  clear: both;
  width: 100%
}
table.inventory th,
table.payments th {
  font-weight: 700;
  text-align: center
}
table.inventory td:nth-child(1) {
  width: 52%
}
table.payments {
  padding-top: 20px
}
table.balance th,
table.balance td {
  width: 50%
}
.green {
  background-color: #D5EEBE;
  color: #689340
}
.blue {
  background-color: #D0EBFB;
  color: #4995B1
}
.red {
  background-color: #FAD0D0;
  color: #AF4C4C
}
.yellow {
  background-color: #FFC;
  color: #BBB840
}
#aside {
  padding-top: 30px;
  font-size: 65%
}
small {
  font-size: 75%;
  line-height: 1.5em
}
table.inventory td.right {
  text-align: right;
  width: 12%
}
table.payments td.right,
table.balance td {
  text-align: right
}
#footer {
  position: fixed;
  bottom: 0px;
  left: 0px;
  right: 0px;
  height: 100px;
  text-align: center;
  border-top: 2px solid #eee;
  font-size: 85%;
  padding-top: 5px
}
</style>
</head>
<body>
<table border="0">
  <tr>
    <td style="width: 60%;" valign="top"><?php if (file_exists(UPLOADS . "print_logo.png")):?>
      <img alt="" src="<?php echo UPLOADS  . "print_logo.png";?>">
      <?php else:?>
      <?php echo Registry::get("Core")->site_name;?>
      <?php endif;?></td>
    <td valign="top" style="width:40%;text-align: right"><h4 style="margin:0px;padding:0px;font-size: 12px;">Factura: #<?php echo $row->invid;?></h4>
      <h4 style="margin:0px;padding:0px;font-size: 12px;"><?php echo Filter::dodate("short_date", $row->created);?></h4></td>
  </tr>
</table>
<div style="background-color:#ddd;height:1px">&nbsp;</div>
<table style="padding-top:25px">
  <tr>
    <td valign="top" style="width:60%">Pago Para</td>
    <td colspan="2" valign="top" style="width:40%">Cobrar a</td>
  </tr>
  <tr>
    <td valign="top" style="width:60%"><p> <?php echo cleanOut(Registry::get("Core")->inv_info);?> </p></td>
    <td colspan="2" valign="top" style="width:40%"><p><?php echo $usr->fname;?> <?php echo $usr->lname;?><br />
        <?php echo $usr->address;?><br />
        <?php echo $usr->city.', '.$usr->state.' '.$usr->zip;?><br />
        <?php echo $usr->country;?></p></td>
  </tr>
  <tr>
    <td valign="top" style="width:60%"><br /></td>
    <td valign="top" style="width:20%">Monto adeudado:<br />
      Fecha de vencimiento:</td>
    <td valign="top" style="width:20%"><?php echo Registry::get("Core")->formatMoney($row->totalprice);?><br />
      <?php echo Filter::dodate("short_date", $row->created);?></td>
  </tr>
</table>
<div style="height:20px"></div>
<?php $data = unserialize($row->items);?>
<table class="inventory">
  <thead>
    <tr>
      <th><span>artículos de factura</span></th>
      <th><span>Total</span></th>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($data as $price => $rows):?>
    <tr>
      <td><span><?php echo $rows;?></span></td>
      <td class="right"><span><?php echo $price;?></span></td>
    </tr>
    <?php endforeach;?>
    <tr>
      <td class="right">Discount/Coupon:</td>
      <td class="right">- <?php echo number_format($row->coupon, 2);?></td>
    </tr>
  </tbody>
</table>
<table class="balance">
  <tr>
    <th><span>Subtotal</span></th>
    <td><span><?php echo number_format($row->originalprice - $row->coupon, 2);?></span></td>
  </tr>
  <tr>
    <th><span>Impuestos</span></th>
    <td><span><?php echo $row->totaltax;?></span></td>
  </tr>
  <tr>
    <th><span>Gran Total</span></th>
    <td><span><?php echo number_format($row->totalprice, 2) . $row->currency;?></span></td>
  </tr>
  <tr>
    <th>Estado</th>
    <td class="green">Paid</td>
  </tr>
</table>
<div id="footer">
  <h1><span>Notas adicionales</span></h1>
  <div>
    <p><small class="extra"> <?php echo cleanOut(Registry::get("Core")->inv_note);?> </small></p>
  </div>
</div>
</body>
</html>
<?php else:?>
<?php die('<h1 style="text-align:center">You have selected invalid invoice</h1>');?>
<?php endif;?>