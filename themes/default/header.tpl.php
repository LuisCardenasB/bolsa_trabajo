<?php
if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');
?>
<?php include("lib/Mobile_Detect.php"); ?>
<!DOCTYPE html>
<!--[if IE 8 ]>
<html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <?php $row = isset($row) ? $row : null; ?>
    <?php echo $content->renderMetaData($row); ?>
    <script type="text/javascript">
        var SITEURL = "<?php echo SITEURL; ?>";
    </script>
    <meta charset="utf-8">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <?php $detect = new Mobile_Detect(); ?>
    <?php if ($detect->isMobile()) { ?>
        <link rel="stylesheet" href="<?php echo THEMEURL; ?>/css/style_menu.css">
        <link rel="stylesheet" href="<?php echo THEMEURL; ?>/css/base.css">
        <link rel="stylesheet" href="<?php echo THEMEURL; ?>/css/responsive.css">
        <link rel="stylesheet" href="<?php echo THEMEURL; ?>/css/style.css">
        <link rel="stylesheet" href="<?php echo THEMEURL; ?>/css/colors/default.css" id="colors">
        <script
                src="https://code.jquery.com/jquery-2.2.4.min.js"
                integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
                crossorigin="anonymous"></script>
        <script
                src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
                integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
                crossorigin="anonymous"></script>
    <?php } else { ?>
    <link rel="stylesheet" href="<?php echo THEMEURL; ?>/css/base.css">
    <link rel="stylesheet" href="<?php echo THEMEURL; ?>/css/responsive.css">
    <link rel="stylesheet" href="<?php echo THEMEURL; ?>/css/style.css">
    <link rel="stylesheet" href="<?php echo THEMEURL; ?>/css/colors/default.css" id="colors">
        <script
                src="https://code.jquery.com/jquery-2.2.4.min.js"
                integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
                crossorigin="anonymous"></script>
        <script
                src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
                integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
                crossorigin="anonymous"></script>
    <?php } ?>

    <!--script src="<?php echo THEMEURL;?>/scripts/jquery-2.1.3.min.js"></script>
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<?php $detect = new Mobile_Detect(); ?>
<?php if ($detect->isMobile()) { ?>

    <?php include("header_mobile.tpl.php"); ?>

<?php } else { ?>

    <?php include("header_desktop.tpl.php"); ?>

<?php } ?>
