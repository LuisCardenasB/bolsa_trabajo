<?php
if (!defined("_VALID_PHP"))
    die('Direct access to this location is not allowed.');
?>

<!DOCTYPE html>
<body>
<div id="wrapper">

    <!-- Header
    ================================================== -->
    <header>
        <div class="container">
            <div class="sixteen columns">

                <!-- Logo -->
                <div id="logo">
                    <h1>
                        <a href="<?php echo SITEURL; ?>/"><?php echo ($core->logo) ? '<img src="' . UPLOADURL . $core->logo . '" alt="' . $core->company . '" />' : $core->company; ?></a>
                    </h1>
                </div>
                <!-- Navigation -->
                <div id="mobile-navigation">
                    <a href="index.php" class="menu-trigger"><i class="fa fa-reorder"></i> Menú Principal</a>
                </div>
                <!-- Menu -->
                <input type="checkbox" class="checkbox" id="menu-toogle"/>
                <label  for="menu-toogle" class="menu-toogle"></label>
                <nav class="nav">
                	<span style="text-align:center;text-decoration:none;">Mi Menú</span>
                    <?php $menu = $content->getMenu();
                    $content->getMainMenus($menu, 0, "menu", "sm topmenu"); ?>
                    <?php if ($user->logged_in && $user->userlevel == 1) { ?>
                        <?php $countMsgs = $core->countMessages(); ?>
                        <ul class="nav-li">
                            <?php $userrow = $user->getUsers(); ?>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/account.php"><i
                                            class="fa fa-user"></i> <?php echo $user->fname; ?></a></li>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/messages.php">Mensajes
                                    <span><?php echo $countMsgs; ?></span></a></li>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/browse-jobs.php">Ver Ofertas</a></li>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/browse-categories.php">Ofertas por
                                    categoría</a></li>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/add-resume.php">Mi Curriculum</a></li>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/bookmarks.php">Marcadores</a></li>
                            <!--*<li><a href="<?php echo SITEURL; ?>/ajax/jobs.php?doresume=<?php echo $user->uid; ?>">Descargar Curriculm</a></li>-->
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/profile.php">Editar Perfil</a></li>
                            <!--<li><a href="<?php echo SITEURL; ?>/helpseeker.php">¿Necesitas ayuda?</a></li>-->
                        </ul>
                    <?php }
                    if ($user->logged_in && $user->userlevel == 2) { ?>
                        <?php $countMsgs = $core->countMessages(); ?>
                        <ul class="nav-li">
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/account.php"><i
                                            class="fa fa-user"></i> <?php echo $user->fname; ?></a></li>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/messages.php">Mensajes
                                    <span><?php echo $countMsgs; ?></span></a></li>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/add-job.php">Publicar Oferta</a></li>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/manage-jobs.php">Administrar
                                    ofertas</a></li>
                            <!--<li><a href="<?php echo SITEURL; ?>/manage-jobs.php">Solicitudes de empleo</a></li>-->
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/browse-resumes.php">Ver
                                    Curriculums</a></li>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/bookmarks.php">Marcadores</a></li>
                            <!--<li><a href="<?php //echo SITEURL; ?>/invoices.php">Invoices</a></li>-->
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/profile.php">Editar Perfil</a></li>
                            <li class="nav__item"><a href="<?php echo SITEURL; ?>/company-settings.php">Datos de la
                                    Empresa</a></li>
                            <!--<li><a href="<?php echo SITEURL; ?>/helpemployer.php">¿Necesitas ayuda?</a></li>-->
                        </ul>
                    <?php }
                    if ($user->logged_in) { ?>
                        <div style="text-align: center"><a href="<?php echo SITEURL; ?>/logout.php"><i
                                        class="fa fa-lock"></i>Cerrar sesión</a>
                        </div>
                    <?php } else { ?>
                        <div><a href="<?php echo SITEURL; ?>/register.php"><i class="fa fa-user"></i>Registro</a></div>
                        <div><a href="<?php echo SITEURL; ?>/login.php"><i class="fa fa-lock"></i>Acceder</a></div>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </header>
    <div class="clearfix"></div>
    <script>
        $(document).ready(function(){
            var cambio = false;
            $('.nav-li li a').each(function(index) {
                if(this.href.trim() == window.location){
                    $(this).parent().addClass("current");
                    cambio = true;
                }
            });
            if(!cambio){
                $('.nav li:first').addClass("current");
            }
        });
    </script>
