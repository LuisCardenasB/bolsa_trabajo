<div class="container">
	<!-- Recent Jobs -->
	<div class="eleven columns">
	<div class="padding-right">

		<form action="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" method="get" class="list-search">
			<button><i class="fa fa-search"></i></button>
			<input id="keyword" name="search" type="text" placeholder="título del trabajo, palabras clave" value="<?php echo ( isset($_GET['search']) && $_GET['search'] != '' ) ? $_GET['search'] : '';?>"/>
			<div class="clearfix"></div>
		</form>

		<ul class="job-list full">

			<?php if($alljobs): ?>
				<?php foreach ($alljobs as $job) { ?>
					<li><a href="<?php echo SITEURL . '/job.php?id=' . $job->id;?>">
						<img src="<?php echo UPLOADURL;?>avatars/<?php echo $job->avatar;?>" alt="">
						<div class="job-list-content">
							<h4><?php echo $job->title;?> <?php echo $jobs->jobType($job->type);?></h4>
							<div class="job-icons">
								<span><i class="fa fa-briefcase"></i> <?php echo $job->name; ?></span>
								<span><i class="fa fa-map-marker"></i> <?php echo $jobs->jobLocation($job->location); ?></span>
								<span><i class="fa fa-money"></i> <?php echo $job->salary;?></span>
							</div>
							<p><?php echo substr(cleanOut($job->description),0,150); ?></p>
						</div>
						</a>
						<div class="clearfix"></div>
					</li>
				<?php } ?>
			<?php else: ?>
				<li>Lo sentimos, no se encontró ningun trabajo. Por favor, inténtelo de nuevo más tarde.
				</li>
			<?php endif; ?>

		</ul>
		<div class="clearfix"></div>

		<div class="pagination-container paginate">
			<p class="total-pages">
				<?php echo Lang::$word->TOTAL . ': ' . $pager->items_total;?> / <?php echo Lang::$word->CURPAGE . ': ' . $pager->current_page . ' ' . Lang::$word->OF . ' ' . $pager->num_pages;?>
			</p>
			<div class="pagination-new">
				<?php echo $pager->display_pages();?>
			</div>
		</div>

	</div>
	</div>


	<!-- Widgets -->
	<div class="five columns">

		<!-- Sort by -->
		<div class="widget">
			<h4>Ordenar por</h4>

			<!-- Select -->
			<select id="orderfilter" data-placeholder="Choose Order" class="chosen-select-no-single">
				<option value="recent" <?php echo (isset($_GET['short']) && $_GET['short'] == 'recent') ? 'selected="selected"' : ''; ?>>El más nuevo</option>
				<option value="oldest" <?php echo (isset($_GET['short']) && $_GET['short'] == 'oldest') ? 'selected="selected"' : ''; ?>>Más antiguo</option>
				<option value="expiry" <?php echo (isset($_GET['short']) && $_GET['short'] == 'expiry') ? 'selected="selected"' : ''; ?>>Expirará pronto</option>
			</select>

		</div>

		<!-- Job Type -->
		<div class="widget">
			<h4>Tipo de empleo</h4>

			<ul class="checkboxes">
				<li>
					<input id="check-1" type="checkbox" name="check" value="check-1" checked>
					<label for="check-1">Cualquier tipo</label>
				</li>
				<li>
					<input id="check-2" type="checkbox" name="check" value="check-2">
					<label for="check-2">Tiempo completo <span>(312)</span></label>
				</li>
				<li>
					<input id="check-3" type="checkbox" name="check" value="check-3">
					<label for="check-3">Medio tiempo <span>(269)</span></label>
				</li>
				<li>
					<input id="check-4" type="checkbox" name="check" value="check-4">
					<label for="check-4">Interinato <span>(46)</span></label>
				</li>
				<li>
					<input id="check-5" type="checkbox" name="check" value="check-5">
					<label for="check-5">Freelance <span>(119)</span></label>
				</li>
			</ul>

		</div>

		<!-- Location -->
		<div class="widget">
			<h4>Ubicación</h4>
			<form action="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" method="get">
				<input type="text" placeholder="State / Province" value=""/>
				<input type="text" placeholder="City" value=""/>

				<input type="text" class="miles" placeholder="Miles" value=""/>
				<label for="zip-code" class="from">from</label>
				<input type="text" id="zip-code" class="zip-code" placeholder="Zip-Code" value=""/><br>

				<button class="button">Filtrar</button>
			</form>
		</div>

	</div>
	<!-- Widgets / End -->

</div>
<script type="text/javascript">
  $(document).ready(function () {
      $('#orderfilter').change(function () {
			var order = $("#orderfilter option:selected").val();
			var keyword = $("#keyword").val();
			var url = '<?php echo SITEURL . '/browse-jobs.php'?>';
			if(order != '' && keyword != ''){
				var url = url + '?search=' + keyword + '&short=' + order;
			} else if (order == '' && keyword != '') {
				var url = url + '?search=' + keyword;
			} else if (order != '' && keyword == '') {
				var url = url + '?short=' + order;
			}
			window.location.href = url;
      })
  });
</script>
