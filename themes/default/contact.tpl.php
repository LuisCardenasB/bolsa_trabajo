<?php
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

?>
<!-- Start Contact Form-->
<div class="container">
  <div style="margin-bottom:30px" id="msgholder"></div>
  <form id="wojo_form" name="wojo_form" method="post" autocomplete="off">
    <div class="two fields">
      <div class="field">
        <label><strong><?php echo Lang::$word->PLG_CT_NAME;?></strong></label>
        <label class="input"> <i class="icon-prepend icon user"></i><i class="icon-append icon asterisk"></i>
          <input type="text" name="name" value="<?php if ($user->logged_in) echo $user->name;?>" placeholder="Nombre completo">
        </label>
      </div>
      <div class="field">
        <label><strong><?php echo Lang::$word->EMAIL;?></strong></label>
        <label class="input"> <i class="icon-prepend icon mail"></i><i class="icon-append icon asterisk"></i>
          <input type="email" name="email_user" value="<?php if ($user->logged_in) echo $user->email;?>" placeholder="example@email.com">
        </label>
      </div>
      <!--
      <div class="field">
        <label><strong><?php echo Lang::$word->EMAIL;?></strong></label>
        <label class="input"> <i class="icon-prepend icon mail"></i><i class="icon-append icon asterisk"></i>
          <input type="text" name="email" value="<?php if ($user->logged_in) echo $user->email;?>" placeholder="example@email.com">
        </label>
      </div>
      -->
    </div>
    <div class="two fields">
      <div class="field">
        <p class="form-row form-row-wide">
          <label for="contact_subject"><strong><?php echo Lang::$word->PLG_CT_SUBJECT;?></strong></label>
          <select name="subject" id="contact_subject" class="chosen-select-no-single">
            <option value="">--- <?php echo Lang::$word->PLG_CT_SUBJECT_1;?> ---</option>
            <option value="<?php echo Lang::$word->PLG_CT_SUBJECT_8;?>"><?php echo Lang::$word->PLG_CT_SUBJECT_8;?></option>
            <option value="<?php echo Lang::$word->PLG_CT_SUBJECT_2;?>"><?php echo Lang::$word->PLG_CT_SUBJECT_2;?></option>
            <option value="<?php echo Lang::$word->PLG_CT_SUBJECT_3;?>"><?php echo Lang::$word->PLG_CT_SUBJECT_3;?></option>
            <option value="<?php echo Lang::$word->PLG_CT_SUBJECT_4;?>"><?php echo Lang::$word->PLG_CT_SUBJECT_4;?></option>
            <option value="<?php echo Lang::$word->PLG_CT_SUBJECT_5;?>"><?php echo Lang::$word->PLG_CT_SUBJECT_5;?></option>
            <option value="<?php echo Lang::$word->PLG_CT_SUBJECT_6;?>"><?php echo Lang::$word->PLG_CT_SUBJECT_6;?></option>
            <option value="<?php echo Lang::$word->PLG_CT_SUBJECT_7;?>"><?php echo Lang::$word->PLG_CT_SUBJECT_7;?></option>
          </select>
        </p>
      </div>
      <div class="field">
        <label><strong><?php echo Lang::$word->CAPTCHA;?></strong></label>
        <label class="input"> <img src="<?php echo SITEURL;?>/lib/captcha.php" alt="" class="captcha-append" /> <i class="icon-prepend icon hide"></i>
          <input type="text" name="captcha" placeholder="<?php echo Lang::$word->CAPTCHA;?>">
        </label>
      </div>
    </div>
    <div class="field">
      <label><strong><?php echo Lang::$word->PLG_CT_MSG;?></strong></label>
      <label class="textarea"> <i class="icon-append icon asterisk"></i>
        <textarea name="message" placeholder="<?php echo Lang::$word->PLG_CT_MSG;?>"></textarea>
      </label>
    </div>
    <div style="margin-bottom:25px" class="wojo fitted divider"></div>
    <div class="field">
		<button type="submit" name="dosubmit" class="wojo info button"><?php echo Lang::$word->PLG_CT_SEND;?></button>
		<input style="visibility:hidden" type="text" name="email" value="socios@cmicsonora.org">
      <!--<button data-url="/ajax/sendmail.php" type="button" name="dosubmit" class="wojo info button"><?php echo Lang::$word->PLG_CT_SEND;?></button>-->
    </div>
  </form>
</div>
<!-- End Contact Form/-->

<!-- Start Script -->
<script>
  $("form").on("submit", function(event){
    event.preventDefault();

    //Y se procede a hacerlo vía AJAX
    $.ajax({
        url: "/ajax/sendmail.php",
        type: $(this).prop("method"),
        data: $(this).serialize(),
    }).done(function(response){
      
      if (response != null)
      {
        var response = $.parseJSON(response);

        if (response.status == "success"){
          document.getElementById("msgholder").innerHTML = response.message;
          $("#msgholder").removeClass("caja_mensaje_error");
          $("#msgholder").addClass("caja_mensaje");
          $("#wojo_form")[0].reset();
          $("#msgholder").show();
        }
        else{
          document.getElementById("msgholder").innerHTML = response.message;
          $("#msgholder").removeClass("caja_mensaje");
          $("#msgholder").addClass("caja_mensaje_error");
          $("#msgholder").show();
        } 
      }
    });
  })
</script>
<!-- End Script -->