<?php if ($detect->isMobile()) { ?>
<div style="background-color: black;padding: 10vw; margin-top: 25px">

    <div class="container">
        <div class="ten columns">
            <h2 style="color: #fff;">Todas las categorías</h2>
        </div>

        <div class="six columns">
            <a href="<?php echo SITEURL; ?>/add-job.php" class="button">Publicar Oferta</a>
        </div>

    </div>
</div>

<!--
<div id="categories">

	<div class="categories-group">
		<div class="container">
			<?php $jobs->browseCategories(); ?>
		</div>
	</div>
-->
	<?php $popularCategories = $jobs->mostPopCategories(); ?>
<?php if($popularCategories): ?>
<div style="padding:30px;" class="container">
	<div class="sixteen columns">
		<ul>
      <?php foreach ($popularCategories as $category) {
          echo '<li style="font-size: 23px; padding:10px"><a href="browse-jobs.php?category=' . $category->cid . '"><i class="' . $icon . '"></i> ' . $category->name . '</a></li>';
      } ?>
		</ul>
	</div>
</div>
<?php endif; ?>

</div>

<?php } else { ?>

<div id="titlebar" class="photo-bg" style="background-image: url(images/all-categories-photo.jpg);">
	<div class="container">
		<div class="ten columns">
            <h2>Todas las categorías</h2>
		</div>

		<div class="six columns">
			<a href="<?php echo SITEURL; ?>/add-job.php" class="button">Publicar Oferta</a>
		</div>

	</div>
</div>

<!--
<div id="categories">

	<div class="categories-group">
		<div class="container">
			<?php $jobs->browseCategories(); ?>
		</div>
	</div>
-->
	<?php $popularCategories = $jobs->mostPopCategories(); ?>
<?php if($popularCategories): ?>
<div style="padding:30px;" class="container">
	<div class="sixteen columns">
		<ul id="popular-categories">
      <?php foreach ($popularCategories as $category) {
          $icon = ($category->icon == '') ? 'ln ln-icon-Align-Center' : $category->icon;
          echo '<li><a href="browse-jobs.php?category=' . $category->cid . '"><i class="' . $icon . '"></i> ' . $category->name . '</a></li>';
      } ?>
		</ul>
	</div>
</div>
<?php endif; ?>

</div>
<?php } ?>