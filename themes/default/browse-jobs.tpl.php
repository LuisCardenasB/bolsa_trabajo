<?php
if (!defined("_VALID_PHP"))
die('Direct access to this location is not allowed.');
?>
<?php include("header.tpl.php");?>
<!-- Titlebar
================================================== -->
<div style="margin-top: 25px;" id="titlebar">
	<div class="container">
		<div class="ten columns">
			<span>Encontramos las siguientes</span>
			<h2>Ofertas de trabajo</h2>
		</div>

		<div class="six columns">
			<a href="<?php echo SITEURL . '/add-job.php'; ?>" class="button">PUBLICAR OFERTA</a>
		</div>

	</div>
</div>

<!-- Content
================================================== -->
<div class="container">
	<!-- Recent Jobs -->
	<div class="eleven columns">
		<div class="padding-right">
			<form action="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" method="get" class="list-search">
				<button><i class="fa fa-search"></i></button>
				<input id="keyword" name="search" type="text" placeholder="Puesto, palabras clave" value="<?php echo ( isset($_GET['search']) && $_GET['search'] != '' ) ? $_GET['search'] : '';?>"/>
				<div class="clearfix"></div>
			</form>
			<ul class="job-list full">
				<?php if($alljobs): ?>
					<?php foreach ($alljobs as $job) { ?>
						<li><a href="job.php?id=<?php echo $job->id;?>">
							<img src="<?php echo UPLOADURL;?>avatars/<?php echo $job->avatar;?>" alt="">
							<div class="job-list-content">
								<h4><?php echo $job->title;?> <?php echo $jobs->jobType($job->type);?></h4>
								<div class="job-icons">
									<span><i class="fa fa-briefcase"></i> <?php echo $job->name; ?></span>
									<span><i class="fa fa-map-marker"></i> <?php echo $jobs->jobLocation($job->location); ?></span>
									<span><i class="fa fa-money"></i> <?php echo $job->salary;?></span>
								</div>
								<div><?php echo substr(strip_tags(cleanOut($job->description)),0,150); ?></div>
							</div>
							</a>
							<div class="clearfix"></div>
						</li>
					<?php } ?>
				<?php else: ?>
					<li>Lo sentimos, no se encontraron ofertas. Por favor, inténtelo de nuevo más tarde.
					</li>
				<?php endif; ?>
			</ul>
			<div class="clearfix"></div>
			<div class="pagination-container paginate">
				<p class="total-pages">
					<?php echo Lang::$word->TOTAL . ': ' . $pager->items_total;?> / <?php echo Lang::$word->CURPAGE . ': ' . $pager->current_page . ' ' . Lang::$word->OF . ' ' . $pager->num_pages;?>
				</p>
				<div class="pagination-new">
					<?php echo $pager->display_pages();?>
				</div>
			</div>
		</div>
	</div>
	<!-- Widgets -->
	<div class="five columns">
		<!-- Sort by -->
		<div class="widget">
			<h4>Ordenar por</h4>
			<!-- Select -->
			<select id="orderfilter" data-placeholder="Choose Order" class="chosen-select-no-single">
				<option value="recent" <?php echo (isset($_GET['short']) && $_GET['short'] == 'recent') ? 'selected="selected"' : ''; ?>>Más recientes</option>
				<option value="oldest" <?php echo (isset($_GET['short']) && $_GET['short'] == 'oldest') ? 'selected="selected"' : ''; ?>>Más antiguas</option>
				<option value="expiry" <?php echo (isset($_GET['short']) && $_GET['short'] == 'expiry') ? 'selected="selected"' : ''; ?>>Por finalizar</option>
			</select>
		</div>
		<!-- Job Type -->
		<div class="widget">
			<h4>Tipo de empleo</h4>
			<form action="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>">
				<ul class="checkboxes">
					<?php if($getType): ?>
						<li>
							<input id="check-1" type="checkbox" value="" checked>
							<label for="check-1">Cualquier tipo</label>
						</li>
						<?php foreach ($getType as $type) { ?>
						<li>
							<input id="<?php echo $type->id ?>" name="type[]" type="checkbox" value="<?php echo $type->id ?>">
							<label for="<?php echo $type->id ?>"><?php echo $type->name ?></label>
						</li>
					<?php } ?>
					<?php else: ?>
						<li>Lo sentimos, no se encontró ningun trabajo. Por favor, inténtelo de nuevo más tarde.</li>
					<?php endif; ?>
				</ul>
				<button style="margin-top:10px" class="button">Filtrar</button>
			</form>
		</div>
		<!-- Location -->
		<div class="widget">
			<h4>Ubicación</h4>
				<select id="state" name="state" onchange="getJobCityDropList()" data-placeholder="e.g. London" class="chosen-select-no-single">
					<?php $jobs->getJobLocationDropList($row->parent_id);?>
				</select>
			<form action="<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>" method="get">
				<div id="city" name="city"></div>
				<button style="margin-top:10px" class="button">Filtrar</button>
			</form>
		</div>
	</div>
	<!-- Widgets / End -->
</div>
<?php include("footer.tpl.php");?>
<script type="text/javascript">
  $(document).ready(function () {
      $('#orderfilter').change(function () {
			var order = $("#orderfilter option:selected").val();
			var keyword = $("#keyword").val();
			var url = '<?php echo SITEURL . '/browse-jobs.php'?>';
			if(order != '' && keyword != ''){
				var url = url + '?search=' + keyword + '&short=' + order;
			} else if (order == '' && keyword != '') {
				var url = url + '?search=' + keyword;
			} else if (order != '' && keyword == '') {
				var url = url + '?short=' + order;
			}
			window.location.href = url;
      })
  });
  
  
  function getJobCityDropList() {
	  var id = document.getElementById("state").value;
	  var dataString = 'getJobCityDropList=' + 1 + '&id=' + id;
	  
	  $.ajax({
		type:"POST",
		url:"ajax/search_location.php",
		data:dataString,
		success: function (html) {
			$('#city').empty();
			$("#city").append('<select id="cityLocation" name="city" data-placeholder="e.g. Hermosillo">')
			$("#city").css('display', 'block');
			$('#cityLocation').addClass('select');
			$('#cityLocation').addClass('estructura');
			$('#cityLocation').addClass('results');
			$("#cityLocation").html(html);
			$('#city').append('</select>');
		}
	  });
	  return false;
	}
</script>
