<?php
if (!defined("_VALID_PHP"))
die('Direct access to this location is not allowed.');
?>
<?php include("header.tpl.php");?>
<div id="titlebar" class="single">
	<div class="container">
		<div class="sixteen columns">
			<h2><?php echo $crumbs = include_once("crumbs.php");?></h2>
			<nav id="breadcrumbs">
				<ul>
					<li><?php echo Lang::$word->CRB_HERE;?>:</li>
					<li><a href="<?php echo SITEURL;?>/"><?php echo Lang::$word->CRB_HOME;?></a></li>
					<li>Ofertas por categoria</li>
				</ul>
			</nav>
		</div>
	</div>
</div>
<div id="categories">
<!--
	<div class="categories-group">
		<div class="container">
			<?php $jobs->browseCategories(); ?>
		</div>
	</div>

</div>
-->
<?php $popularCategories = $jobs->mostPopCategories(); ?>
<?php if($popularCategories): ?>
<div style="padding:30px;" class="container">
	<div class="sixteen columns">
		<ul id="popular-categories">
      <?php foreach ($popularCategories as $category) {
          $icon = ($category->icon == '') ? 'ln ln-icon-Align-Center' : $category->icon;
          echo '<li><a href="browse-jobs.php?category=' . $category->cid . '"><i class="' . $icon . '"></i> ' . $category->name . '</a></li>';
      } ?>
		</ul>
	</div>
</div>
<?php endif; ?>
<?php include("footer.tpl.php");?>
