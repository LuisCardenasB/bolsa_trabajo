<?php
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php include("header.tpl.php");?>
<div style="padding: 0px 50px;">
<div class="wojo-grid">
  <div class="vspace">
    <div class="wojo form">
      <h2 style="padding:50px; margin: 0 auto;"><?php echo Lang::$word->_UA_TITLE3;?> / <?php echo Lang::$word->_UA_SUB3;?></h2>
      <div style="width: 80%; margin: 0 auto; background-color:#C70933; border-radius:15px; padding:10px;">
      <h4 style="text-align:center;"><strong style="color:white;">Aquí puedes activar tu cuenta.<br> Ingrese su dirección de correo electrónico y el Token de activación recibido.</strong></h4>
      </div>
      <div style="padding:20px;">
      <div style="width:81%; border-radius:15px; margin: 0 auto;" id="msgholder"></div>
      </div>
      <form  id="wojo_form" name="wojo_form" method="post" style="width: 80%; margin: 0 auto;">
        <div class="field">
          <label><Strong><?php echo Lang::$word->EMAIL;?></Strong></label>
          <label class="input"> <i class="icon-prepend icon mail"></i> <i class="icon-append icon asterisk"></i>
            <input type="text" name="email" placeholder="example@mail.com">
          </label>
        </div>
        <div class="field">
          <label><strong><?php echo Lang::$word->_UA_TOKEN;?></strong></label>
          <label class="input"> <i class="icon-prepend icon filter"></i> <i class="icon-append icon asterisk"></i>
            <input type="text" name="token" placeholder="<?php echo Lang::$word->_UA_TOKEN;?>">
          </label>
        </div>
        <div class="wojo fitted divider">
        <div class="field">
          <div class="push-right"> <a href="login.php" class="wojo black labeled icon button"> <i class="left arrow icon"></i> <?php echo Lang::$word->BACKTOLOGIN;?></a> </div>
          <button type="submit" name="dosubmit" class="wojo info button"><?php echo Lang::$word->_UA_SUB3;?></button>
        </div>
        </div>
        <input name="accActivate" type="hidden" value="1">
      </form>
    </div>
  </div>
</div>
</div>
<?php include("footer.tpl.php");?>

<script>
  $("form").on("submit", function(event) {
    event.preventDefault();

    $.ajax({
      url: "/ajax/user.php",
      type: $(this).prop("method"),
      data: $(this).serialize(),
    }).done(function(response){
      var response = $.parseJSON(response);

      if (response.status == "success"){
          document.getElementById("msgholder").innerHTML = response.message;
          $("#msgholder").removeClass("perror");
          $("#msgholder").addClass("psuccess");
          $("#wojo_form")[0].reset();
          $("#msgholder").show();
        }
        else{
          document.getElementById("msgholder").innerHTML = response.message;
          $("#msgholder").removeClass("psuccess");
          $("#msgholder").addClass("perror");
          $("#msgholder").show();
        } 
    });
  });
</script>