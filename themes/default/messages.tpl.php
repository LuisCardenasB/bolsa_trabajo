<?php
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php include("header.tpl.php");?>
<div id="titlebar" class="single">
	<div class="container">
		<div class="sixteen columns">
			<h2><?php echo $crumbs = include_once("crumbs.php");?></h2>
			<nav id="breadcrumbs">
				<ul>
					<li><?php echo Lang::$word->CRB_HERE;?>:</li>
					<li><a href="<?php echo SITEURL;?>/"><?php echo Lang::$word->CRB_HOME;?></a></li>
					<li><?php echo $crumbs;?></li>
				</ul>
			</nav>
		</div>
	</div>
</div>
<?php function lang($value) {return $value; }?>
<?php switch(Filter::$action): case "view": ?>
<?php $single = $content->getMessageById();?>
<?php if($single->user1 == $user->uid or $single->user2 == $user->uid):?>
<?php $userp = $content->updateMessageStatus($single->user1);?>
<?php $msgdata = $content->renderMessages();?>
<div class="container">
<div><?php echo $Mmsg; ?></div>
<form class="form" method="post">
  <h3>Enviar mensajes instantáneos</h3>
  <ul id="reply-list">
    <?php foreach ($msgdata as $row):?>
    <li class="<?php echo ($user->uid == $row->userid) ? 'row-staff' : 'row-client'?>"><strong>De:</strong> <?php echo ($user->uid == $row->userid) ? '<span class="label2 label-you"> Usted</span>' : '<span class="label2 label-other">' . $row->username . '</span>';?> <?php echo dodate($row->created);?>
      <div class="message"><strong>Mensaje:</strong><div style="color:#5AB1EF;"> <?php echo cleanOut($row->body);?></div>
        <?php if($row->attachment):?>
        <div> <strong><?php echo lang('MAIL_REC_ATTACH');?>:</strong> <i class="icon-download-alt"></i> <a href="<?php echo UPLOADURL . 'tempfiles/' . $row->attachment;?>"><?php echo lang('FORM_DOWNLOAD');?></a> </div>
        <?php endif;?>
      </div>
    </li>
    <?php endforeach;?>
    <?php unset($row);?>
  </ul>

    <section>
        <div class="field-wrap wysiwyg-wrap">
            <textarea class="post" name="body" rows="5"></textarea>
        </div>
    </section>
    <button class="message-button" name="dosubmit" type="submit">Respuesta</button>
    <a href="<?php echo SITEURL; ?>/messages.php" class="message-button">Cancelar</a>
    <input name="id" type="hidden" value="<?php echo Filter::$id;?>" />
    <input name="uid2" type="hidden" value="<?php echo count($msgdata) + 1;?>" />
    <input name="userp" type="hidden" value="<?php echo $userp;?>" />
    <input name="msgsubject" type="hidden" value="<?php echo sanitize($single->msgsubject);?>" />
    <input name="recipient" type="hidden" value="<?php echo ($user->uid == $single->user1) ?  $single->user2 : $single->user1;?>" />
    <input name="update" type="hidden" value="1" />
    <input name="processMessage" type="hidden" value="1" />
</form>
<?php else:?>
    <p>Perdón, algo salió mal.</p>
<?php endif;?>
</div>
<?php break;?>
<?php case"add": ?>
<?php if ($user->userlevel > 2):?>
<div class="container">
    <?php $userlist = $user->getUserList(1);?>
    <form class="xform" id="admin_form" method="post">
        <h3>Enviar mensaje nuevo</h3>

        <div class="form">
            <h5>Asunto del mensaje</h5>
            <input type="text" name="msgsubject" placeholder="Message Subject">
        </div><br>

        <div class="form">
            <h5>Destinatario</h5>
            <select name="recipient" class="chosen-select-no-single">
                <option value="">--- Seleccionar destinatario ---</option>
                <?php if($userlist):?>
                <?php foreach ($userlist as $urow):?>
                <?php if($user->userlevel == 9):?>
                <option value="<?php echo $urow->id;?>"><?php echo $urow->name.' - '.$urow->email;?></option>
                <?php else:?>
                <option value="<?php echo $urow->id;?>"><?php echo $urow->name;?></option>
                <?php endif;?>
                <?php endforeach;?>
                <?php unset($urow);?>
                <?php endif;?>
            </select>
        </div><br>

        <div class="form">
            <h5>Contenido del mensaje</h5>
            <div class="field-wrap wysiwyg-wrap">
                <textarea class="post" name="body" rows="5"></textarea>
            </div>
        </div><br>

        <button class="button" name="dosubmit" type="submit">Enviar mensaje</button>
        <a href="<?php echo SITEURL; ?>/messages.php" class="button">Cancelar</a>
        <input type="hidden" name="processMessage" value="1">
    </form>
</div>
<?php endif; ?>
<?php break;?>
<?php default: ?>
<div class="container">
    <section class="widget">
    <?php $messagerow = $content->getMessages();?>
      <header>
        <?php if ($user->userlevel > 2):?>
            <!-- <div class="push-right"><a href="<?php echo SITEURL; ?>/messages.php?action=add" class="button">Send New Message</a></div> -->
        <?php endif; ?>
        <h1>Ver mensajes instantáneos</h1>
      </header>
      <div class="content2">
        <?php if(!$messagerow):?>
        <?php echo "Lo sentimos, no hay mensajes.";?>
        <?php else:?>
	        <?php if( isset($pmsg) && $pmsg != ''){ ?>
				<div class="message"><?php echo $pmsg; ?></div>
			<?php } ?>
        <form class="dform" method="post">
          <table class="jobboard basic table">
          <thead>
            <tr>
              <th class="header"></th>
              <th class="header">Nombre del remitente</th>
              <th class="header">Asunto del mensaje</th>
              <th class="header">#Respuestas</th>
              <th class="header">Mensaje enviado</th>
              <th class="header">Acciones</th>
            </tr>
          </thead>
          <?php foreach ($messagerow as $row):?>
          <tr>
            <td><span class="message-dropcap"><?php echo substr($row->name, 0, 1);?></span></td>
            <td><?php echo $row->name;?></td>
            <td><?php echo $row->msgsubject;?></td>
            <td><?php echo $row->replies - 1;?></td>
            <td><?php echo $row->created;?></td>
            <td>
                <a href="<?php echo SITEURL; ?>/messages.php?action=view&amp;id=<?php echo $row->uid1;?>" class="message-view-button"><i class="fa fa-eye"></i></a>
                <!--<a href="<?php echo SITEURL; ?>/messages.php?id=<?php echo $row->id ?>" id="<?php echo $row->id . ':' . $row->uid1;?>" class="message-remove-button" ><i class="fa fa-trash"></i></a>-->
                <button class="message-remove-button" name="dosubmit" type="submit" id="item_<?php echo $row->id . ':' . $row->uid1;?>"><i class="fa fa-trash"></i></button>
                <input name="id" id="id" type="hidden" value="<?php echo $row->id;?>" />
                <input name="uid" id="uid" type="hidden" value="<?php echo $row->uid1;?>" />
                <input type="hidden" name="deleteMessage" value="1">
            </td>
          </tr>
          <?php endforeach;?>
          <?php unset($row);?>
         </table>
        </form>
        <?php if($pager->display_pages()):?>
        <?php echo $pager->display_pages();?>
        <?php endif;?>
        <?php endif;?>
      </div>
    </section>
</div>
<?php break;?>
<?php endswitch;?>

<?php include("footer.tpl.php");?>
