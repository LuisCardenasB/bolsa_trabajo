<?php
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php include("header.tpl.php");?>
<div id="titlebar" class="single">
	<div class="container">
		<div class="sixteen columns">
			<h2><?php echo $crumbs = include_once("crumbs.php");?></h2>
			<nav id="breadcrumbs">
				<ul>
					<li><?php echo Lang::$word->CRB_HERE;?>:</li>
					<li><a href="<?php echo SITEURL;?>/"><?php echo Lang::$word->CRB_HOME;?></a></li>
					<li>Publicar Oferta</li>
				</ul>
			</nav>
		</div>
	</div>
</div>

<?php $limit = $jobs->jobPostCheck(); ?>
<?php if ($limit): ?>
<div class="container">
  <form id="wojo_form" name="wojo_form" method="post" enctype="multipart/form-data">
	<!-- Submit Page -->
	<div class="sixteen columns">
		<div class="submit-page">
			<?php if( isset($pmsg) && $pmsg != ''){ ?>
				<div class="message"><?php echo $pmsg; ?></div>
			<?php } ?>
			<div class="form">
				<h5>Titulo del empleo</h5>
				<input 
				name="title" 
				class="search-field" 
				type="text" 
				placeholder="Supervisor de obra" 
				value="<?php if(isset($_POST['title'])){echo $_POST['title'];}?>"
				/>
			</div>
			<div class="form">
				<h5>Ubicación</h5>
				<select id="state" name="state" onchange="getJobCityDropList()" data-placeholder="e.g. London" class="chosen-select-no-single">
					<?php $jobs->getJobLocationDropList($row->parent_id);?>
				</select>
				<div id="location" name="location"></div>
				<p class="note">Déjelo en blanco si la ubicación no es importante</p>
			</div>
			<div class="form">
				<h5>Tipo de contrato</h5>
				<select name="type" data-placeholder="Full-Time" class="chosen-select-no-single">
					<?php $jobs->getJobTypeDropList();?>
				</select>
			</div>
			<div class="form">
				<div class="select">
					<h5>Categoria</h5>
					<select name="categories[]" data-placeholder="Seleccionar categoria" class="chosen-select" multiple>
						<?php $jobs->getJobCatDropList(0, 0,"&#166;&nbsp;&nbsp;&nbsp;&nbsp;", $row->parent_id);?>
					</select>
				</div>
			</div>
			<div class="form">
				<h5>Etiquetas de habilidad <span>(opcional)</span></h5>
				<select name="skills[]" data-placeholder="Responsabilidad, Honestidad" class="chosen-select" multiple>
					<?php $jobs->getJobSkillDropList();?>
				</select>
			</div>
			<div class="form">
				<h5>Salario</h5>
				<input name="salary" class="search-field" type="text" placeholder="$8,000" value="<?php if(isset($_POST['salary'])){echo $_POST['salary'];}?>"/>
			</div>
			<div class="form">
				<h5>Descripción del trabajo</h5>
				<textarea name="description" class="WYSIWYG" cols="40" rows="3" id="description" spellcheck="true"></textarea>
			</div>
			<div class="form">
				<h5>Responsabilidad laboral</h5>
				<textarea name="responsibility" class="WYSIWYG" cols="40" rows="3" id="responsibility" spellcheck="true"></textarea>
			</div>
			<div class="form">
				<h5>Requisitos de experiencia</h5>
				<textarea name="experience" class="WYSIWYG" cols="40" rows="3" id="experience" spellcheck="true"></textarea>
			</div>
			<div class="form">
				<h5>Requerimientos Académicos</h5>
				<textarea name="education" class="WYSIWYG" cols="40" rows="3" id="education" spellcheck="true"></textarea>
			</div>
			<div class="form">
				<h5>Otros beneficios</h5>
				<textarea name="benefits" class="WYSIWYG" cols="40" rows="3" id="benefits" spellcheck="true"></textarea>
			</div>
			<div class="form">
				<h5>Información adicional</h5>
				<textarea name="additional_info" class="WYSIWYG" cols="40" rows="3" id="additional_info" spellcheck="true"></textarea>
			</div>
			<div class="form">
				<h5>Solicitud email / URL</h5>
				<input name="apply_url" type="text" placeholder="Ingrese una dirección de correo electrónico o URL del sitio web">
			</div>
			<div class="form">
				<h5>Fecha de publicación </h5>
				<input name="publish_date" id="publish_date" data-role="date" type="text" placeholder="DD-MM-AAAA" value="<?php if(isset($_POST['publish_date'])){echo $_POST['publish_date'];}?>">
				<p class="note">Publish from the date.</p>
			</div>
			<div class="form">
				<h5>Fecha de cierre </h5>
				<input name="expire_date" id="expire_date" data-role="date" type="text" placeholder="DD-MM-AAAA" value="<?php if(isset($_POST['expire_date'])){echo $_POST['expire_date'];}?>">
				<p class="note">Fecha límite para nuevos solicitantes.</p>
			</div>

			<input type="hidden" name="userid" value="<?php echo $user->uid; ?>">
			<input type="hidden" name="addJob" value="1">
			<div class="divider margin-top-0 padding-reset"></div>
			<input type="submit" value="Publicar Nuevo empleo">
		</div>
	</div>
  </form>
</div>
<?php else: ?>
  <div class="container">
  	<div class="sixteen columns">

  		<div class="info-banner">
  			<div class="info-content">
  				<?php  $userrow = $user->getUsers();?>
  				<h3>Hola <?php echo $user->fname;?> <?php echo $user->lname;?></h3>
  				<p>No tiene ningún paquete de suscripción disponible ahora. Por favor obtenga una suscripción.</p>
  			</div>
  			<a href="<?php echo SITEURL; ?>" class="button">Regresar al inicio</a>
  			<div class="clearfix"></div>
  		</div>

  	</div>
  </div>
<?php endif; ?>
<?php include("footer.tpl.php");?>

<script type="text/javascript">
function getJobCityDropList() {
	  var id = document.getElementById("state").value;
	  var dataString = 'getJobCityDropList=' + 1 + '&id=' + id;
	  
	  $.ajax({
		type:"POST",
		url:"ajax/search_location.php",
		data:dataString,
		success: function (html) {
			$("#location").empty();
			$("#location").append('<select id="cityLocation" name="cityLocation" data-placeholder="e.g. Hermosillo">')
			$("#location").css('display', 'block');
			$('#cityLocation').addClass('select');
			$('#cityLocation').addClass('estructura');
			$('#cityLocation').addClass('results');
			$("#cityLocation").html(html);
			$('#location').append('</select>');
		}
	  });
	  return false;
	}
</script>