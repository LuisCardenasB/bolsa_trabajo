<?php
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');

  /**
   * redirect_to()
   *
   * @param mixed $location
   * @return
   */
  function redirect_to($location)
  {
      if (!headers_sent()) {
          header('Location: ' . $location);
		  exit;
	  } else
          echo '<script type="text/javascript">';
          echo 'window.location.href="' . $location . '";';
          echo '</script>';
          echo '<noscript>';
          echo '<meta http-equiv="refresh" content="0;url=' . $location . '" />';
          echo '</noscript>';
  }

  function dodate($date) {
	$format = '%b %d, %Y';
	return strftime($format, strtotime($date));
  }

  /**
   * countEntries()
   *
   * @param mixed $table
   * @param string $where
   * @param string $what
   * @return
   */
  function countEntries($table, $where = '', $what = '')
  {
      if (!empty($where) && isset($what)) {
          $q = "SELECT COUNT(*) FROM " . $table . "  WHERE " . $where . " = '" . $what . "' LIMIT 1";
      } else
          $q = "SELECT COUNT(*) FROM " . $table . " LIMIT 1";

      $record = Registry::get("Database")->query($q);
      $total = Registry::get("Database")->fetchrow($record);
      return $total[0];
  }

  /**
   * getChecked()
   *
   * @param mixed $row
   * @param mixed $status
   * @return
   */
  function getChecked($row, $status)
  {
      if ($row == $status) {
          echo "checked=\"checked\"";
      }
  }

  /**
   * post()
   *
   * @param mixed $var
   * @return
   */
  function post($var)
  {
      if (isset($_POST[$var]))
          return $_POST[$var];
  }

  /**
   * get()
   *
   * @param mixed $var
   * @return
   */
  function get($var)
  {
      if (isset($_GET[$var]))
          return $_GET[$var];
  }

  /**
   * sanitize()
   *
   * @param mixed $string
   * @param bool $trim
   * @return
   */
  function sanitize($string, $trim = false,  $end_char = '&#8230;', $int = false, $str = false)
  {
      $string = filter_var($string, FILTER_SANITIZE_STRING);
      $string = trim($string);
      $string = stripslashes($string);
      $string = strip_tags($string);
      $string = str_replace(array('‘', '’', '“', '”'), array("'", "'", '"', '"'), $string);

	  if ($trim) {
        if (strlen($string) < $trim)
        {
            return $string;
        }

        $string = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $string));

        if (strlen($string) <= $trim)
        {
            return $string;
        }

        $out = "";
        foreach (explode(' ', trim($string)) as $val)
        {
            $out .= $val.' ';

            if (strlen($out) >= $trim)
            {
                $out = trim($out);
                return (strlen($out) == strlen($string)) ? $out : $out.$end_char;
            }
        }

          //$string = substr($string, 0, $trim);

	  }
      if ($int)
		  $string = preg_replace("/[^0-9\s]/", "", $string);
      if ($str)
		  $string = preg_replace("/[^a-zA-Z\s]/", "", $string);

      return $string;
  }

  /**
   * truncate()
   *
   * @param mixed $string
   * @param mixed $length
   * @param bool $ellipsis
   * @return
   */
  function truncate($string, $length, $ellipsis = true)
  {
      $wide = strlen(preg_replace('/[^A-Z0-9_@#%$&]/', '', $string));
      $length = round($length - $wide * 0.2);
      $clean_string = preg_replace('/&[^;]+;/', '-', $string);
      if (strlen($clean_string) <= $length)
          return $string;
      $difference = $length - strlen($clean_string);
      $result = substr($string, 0, $difference);
      if ($result != $string and $ellipsis) {
          $result = add_ellipsis($result);
      }
      return $result;
  }

  /**
   * add_ellipsis()
   *
   * @param mixed $string
   * @return
   */
  function add_ellipsis($string)
  {
      $string = substr($string, 0, strlen($string) - 3);
      return trim(preg_replace('/ .{1,3}$/', '', $string)) . '...';
  }

  /**
   * getValue()
   *
   * @param mixed $stwhatring
   * @param mixed $table
   * @param mixed $where
   * @return
   */
  function getValue($what, $table, $where)
  {
      $sql = "SELECT $what FROM $table WHERE $where";
      $row = Registry::get("Database")->first($sql);
      return ($row) ? $row->$what : '';
  }

  /**
   * getValueById()
   *
   * @param mixed $what
   * @param mixed $table
   * @param mixed $id
   * @return
   */
  function getValueById($what, $table, $id)
  {
      $sql = "SELECT $what FROM $table WHERE id = $id";
      $row = Registry::get("Database")->first($sql);
      return ($row) ? $row->$what : '';
  }

  /**
   * tooltip()
   *
   * @param mixed $tip
   * @return
   */
  function tooltip($tip)
  {
      return '<img src="'.ADMINURL.'/images/tooltip.png" alt="Tip" class="tooltip" title="' . $tip . '" />';
  }

  /**
   * required()
   *
   * @return
   */
  function required()
  {
      return '<img src="'.ADMINURL.'/images/required.png" alt="Required Field" class="tooltip" title="Required Field" />';
  }

  /**
   * cleanOut()
   *
   * @param mixed $text
   * @return
   */
  function cleanOut($text) {
	 $text =  strtr($text, array('\r\n' => "", '\r' => "", '\n' => ""));
	 $text = html_entity_decode($text, ENT_QUOTES, 'UTF-8');
	 //$text = str_replace('<br>', '<br />', $text);
	 return stripslashes($text);
  }

  /**
   * cleanSanitize()
   *
   * @param mixed $string
   * @param bool $trim
   * @return
   */
  function cleanSanitize($string, $trim = false,  $end_char = '&#8230;')
  {
	  $string = cleanOut($string);
      $string = filter_var($string, FILTER_SANITIZE_STRING);
      $string = trim($string);
      $string = stripslashes($string);
      $string = strip_tags($string);
      $string = str_replace(array('‘', '’', '“', '”'), array("'", "'", '"', '"'), $string);

	  if ($trim) {
        if (strlen($string) < $trim)
        {
            return $string;
        }

        $string = preg_replace("/\s+/", ' ', str_replace(array("\r\n", "\r", "\n"), ' ', $string));

        if (strlen($string) <= $trim)
        {
            return $string;
        }

        $out = "";
        foreach (explode(' ', trim($string)) as $val)
        {
            $out .= $val.' ';

            if (strlen($out) >= $trim)
            {
                $out = trim($out);
                return (strlen($out) == strlen($string)) ? $out : $out.$end_char;
            }
        }
	  }
      return $string;
  }


  /**
   * phpself()
   *
   * @return
   */
  function phpself()
  {
      return htmlspecialchars($_SERVER['PHP_SELF']);
  }

  /**
   * alphaBits()
   *
   * @param bool $all
   * @param array $vars
   * @return
   */
  function alphaBits($all = false, $vars)
  {
      if (!empty($_SERVER['QUERY_STRING'])) {
          $parts = explode("&amp;", $_SERVER['QUERY_STRING']);
          $vars = str_replace(" ", "", $vars);
          $c_vars = explode(",", $vars);
          $newParts = array();
          foreach ($parts as $val) {
              $val_parts = explode("=", $val);
              if (!in_array($val_parts[0], $c_vars)) {
                  array_push($newParts, $val);
              }
          }
          if (count($newParts) != 0) {
              $qs = "&amp;" . implode("&amp;", $newParts);
          } else {
              return false;
          }

		  $html = '';
          $charset = explode(",", Lang::$word->ALPHA);
          $html .= "<div class=\"wojo small pagination menu\">\n";
          foreach ($charset as $key) {
			  $active = ($key == get('letter')) ? ' active' : null;
              $html .= "<a class=\"item$active\" href=\"" . phpself() . "?letter=" . $key . $qs . "\">" . $key . "</a>\n";
          }
          $viewAll = ($all === false) ? phpself() : $all;
          $html .= "<a class=\"item\" href=\"" . $viewAll . "\">" . Lang::$word->VIEWALL . "</a>\n";
          $html .= "</div>\n";
		  unset($key);

		  return $html;
	  } else {
		  return false;
	  }
  }

  /**
   * isAdmin()
   *
   * @param mixed $userlevel
   * @return
   */
  function isAdmin($userlevel)
  {
	  switch ($userlevel) {
		  case 9:
		     $display = '<img src="'.SITEURL.'/images/superadmin.png" alt="" class="tooltip" title="Super Admin"/>';
			 break;

		  case 7:
		     $display = '<img src="'.SITEURL.'/images/level7.png" alt="" class="tooltip" title="User Level 7"/>';
			 break;

		  case 6:
		     $display = '<img src="'.SITEURL.'/images/level6.png" alt="" class="tooltip" title="User Level 6"/>';
			 break;

		  case 5:
		     $display = '<img src="'.SITEURL.'/images/level5.png" alt="" class="tooltip" title="User Level 5"/>';
			 break;

		  case 4:
		     $display = '<img src="'.SITEURL.'/images/level4.png" alt="" class="tooltip" title="User Level 4"/>';
			 break;

		  case 3:
		     $display = '<img src="'.SITEURL.'/images/level6.png" alt="" class="tooltip" title="User Level 3"/>';
			 break;

		  case 2:
		     $display = '<img src="'.SITEURL.'/images/level5.png" alt="" class="tooltip" title="User Level 2"/>';
			 break;

		  case 1:
		     $display = '<img src="'.SITEURL.'/images/user.png" alt="" class="tooltip" title="User"/>';
			 break;
	  }

      return $display;;
  }

  /**
   * userStatus()
   *
   * @param mixed $id
   * @return
   */
  function userStatus($status, $id)
  {
      switch ($status) {
          case "y":
              $display = '<span class="wojo positive label">' . Lang::$word->ACTIVE . '</span>';
              break;

          case "n":
              $display = '<a data-id="' . $id . '" class="activate wojo info label"><i class="icon adjust"></i> ' . Lang::$word->INACTIVE . '</a>';
              break;

          case "t":
              $display = '<span class="wojo warning label">' . Lang::$word->PENDING . '</span>';
              break;

          case "b":
              $display = '<span class="wojo negative label">' . Lang::$word->BANNED . '</span>';
              break;
      }

      return $display;
  }

  function jobFeatured($featured, $id)
  {
      switch ($featured) {
          case 'No Destacado':
              $display = '<a id="jobFeatured_' . $id . '" data-id="' . $id . '" class="toggleJobFeatured wojo notfeatured label"><i class="icon star"></i></a>';
              break;

          case 'Destacado':
               $display = '<a id="jobFeatured_' . $id . '" data-id="' . $id . '" class="toggleJobFeatured wojo featured label"><i class="icon star"></i> Destacado</a>';
              break;
      }

      return $display;
  }

  function resumeFeatured($featured, $id)
  {
      switch ($featured) {
          case 'notfeatured':
              $display = '<a id="resumeFeatured_' . $id . '" data-id="' . $id . '" class="toggleResumeFeatured wojo notfeatured label"><i class="icon star"></i> No destacado</a>';
              break;

          case 'featured':
               $display = '<a id="resumeFeatured_' . $id . '" data-id="' . $id . '" class="toggleResumeFeatured wojo featured label"><i class="icon star"></i> Destacado</a>';
              break;
      }

      return $display;
  }

  function jobStatus($status, $id)
  {
      switch ($status) {
          case "approved":
              $display = '<span class="wojo approved label"> Aprovado</span>';
              break;

          case "pending":
              $display = '<a id="jobStatus_' . $id . '" data-id="' . $id . '" class="activate wojo pending label"><i class="icon adjust"></i> Pendiente</a>';
              break;

          case "declined":
              $display = '<span class="wojo declined label"> Rechazado</span>';
              break;
      }

      return $display;
  }


  function jobType($status, $id)
  {
      switch ($status) {
          case "approved":
              $display = '<span class="wojo approved label"> Aprovado</span>';
              break;

          case "pending":
              $display = '<a data-id="' . $id . '" class="activate wojo pending label"><i class="icon adjust"></i> Pendiente</a>';
              break;

          case "declined":
              $display = '<span class="wojo declined label"> Rechazado</span>';
              break;
      }

      return $display;
  }

  /**
   * isActive()
   *
   * @param mixed $id
   * @return
   */
  function isActive($id)
  {
      if ($id == 1) {
          $display = '<i data-content="' . Lang::$word->ACTIVE . '" class="circular inverted icon check"></i>';
      } else {
          $display = '<i data-content="' . Lang::$word->PENDING . '" class="circular inverted icon time"></i>';
      }

      return $display;
  }

  /**
   * getTemplates()
   *
   * @param mixed $dir
   * @param mixed $site
   * @return
   */
  function getTemplates($dir, $site)
  {
      $getDir = dir($dir);
      while (false !== ($templDir = $getDir->read())) {
          if ($templDir != "." && $templDir != ".." && $templDir != "index.php") {
              $selected = ($site == $templDir) ? " selected=\"selected\"" : "";
              echo "<option value=\"{$templDir}\"{$selected}>{$templDir}</option>\n";
          }
      }
      $getDir->close();
  }

  /**
   * timesince()
   *
   * @param int $original
   * @return
   */
  function timesince($original)
  {
      // array of time period chunks
      $chunks = array(
          array(60 * 60 * 24 * 365, 'year'),
          array(60 * 60 * 24 * 30, 'month'),
          array(60 * 60 * 24 * 7, 'week'),
          array(60 * 60 * 24, 'day'),
          array(60 * 60, 'hour'),
          array(60, 'min'),
          array(1, 'sec'),
          );

      $today = time();
       /* Current unix time  */
      $since = $today - $original;

      // $j saves performing the count function each time around the loop
      for ($i = 0, $j = count($chunks); $i < $j; $i++) {
          $seconds = $chunks[$i][0];
          $name = $chunks[$i][1];

          // finding the biggest chunk (if the chunk fits, break)
          if (($count = floor($since / $seconds)) != 0) {
              break;
          }
      }

      $print = ($count == 1) ? '1 ' . $name : "$count {$name}s";

      if ($i + 1 < $j) {
          // now getting the second item
          $seconds2 = $chunks[$i + 1][0];
          $name2 = $chunks[$i + 1][1];

          // add second item if its greater than 0
          if (($count2 = floor(($since - ($seconds * $count)) / $seconds2)) != 0) {
              $print .= ($count2 == 1) ? ', 1 ' . $name2 : " $count2 {$name2}s";
          }
      }
      return $print . ' ' . Lang::$word->AGO;
  }

  /**
   * compareFloatNumbers()
   *
   * @param mixed $float1
   * @param mixed $float2
   * @param string $operator
   * @return
   */
  function compareFloatNumbers($float1, $float2, $operator='=')
  {
	  // Check numbers to 5 digits of precision
	  $epsilon = 0.00001;

	  $float1 = (float)$float1;
	  $float2 = (float)$float2;

	  switch ($operator)
	  {
		  // equal
		  case "=":
		  case "eq":
			  if (abs($float1 - $float2) < $epsilon) {
				  return true;
			  }
			  break;
		  // less than
		  case "<":
		  case "lt":
			  if (abs($float1 - $float2) < $epsilon) {
				  return false;
			  } else {
				  if ($float1 < $float2) {
					  return true;
				  }
			  }
			  break;
		  // less than or equal
		  case "<=":
		  case "lte":
			  if (compareFloatNumbers($float1, $float2, '<') || compareFloatNumbers($float1, $float2, '=')) {
				  return true;
			  }
			  break;
		  // greater than
		  case ">":
		  case "gt":
			  if (abs($float1 - $float2) < $epsilon) {
				  return false;
			  } else {
				  if ($float1 > $float2) {
					  return true;
				  }
			  }
			  break;
		  // greater than or equal
		  case ">=":
		  case "gte":
			  if (compareFloatNumbers($float1, $float2, '>') || compareFloatNumbers($float1, $float2, '=')) {
				  return true;
			  }
			  break;

		  case "<>":
		  case "!=":
		  case "ne":
			  if (abs($float1 - $float2) > $epsilon) {
				  return true;
			  }
			  break;
		  default:
			  die("Unknown operator '".$operator."' in compareFloatNumbers()");
	  }

	  return false;
  }

  /**
   * getSize()
   *
   * @param mixed $size
   * @param integer $precision
   * @param bool $long_name
   * @param bool $real_size
   * @return
   */
  function getSize($size, $precision = 2, $long_name = false, $real_size = true)
  {
	  $base = $real_size ? 1024 : 1000;
	  $pos = 0;
	  while ($size > $base) {
		  $size /= $base;
		  $pos++;
	  }
	  $prefix = _getSizePrefix($pos);
	  @$size_name = ($long_name) ? $prefix . "bytes" : $prefix[0] . "B";
	  return round($size, $precision) . ' ' . ucfirst($size_name);
  }

  /**
   * _getSizePrefix()
   *
   * @param mixed $pos
   * @return
   */
  function _getSizePrefix($pos)
  {
	  switch ($pos) {
		  case 00:
			  return "";
		  case 01:
			  return "kilo";
		  case 02:
			  return "mega";
		  case 03:
			  return "giga";
		  default:
			  return "?-";
	  }
  }

  /**
   * getFileType()
   *
   * @param mixed $filename
   * @return
   */
  function getFileType($filename)
  {
	  if (preg_match("/^.*\.(jpg|jpeg|png|gif|bmp)$/i", $filename) != 0) {
		  return 'image';

	  } elseif (preg_match("/^.*\.(txt|css|php|sql|js)$/i", $filename) != 0) {
		  return 'text';

	  } elseif (preg_match("/^.*\.(zip)$/i", $filename) != 0) {
		  return 'zip';
	  }
	  return 'generic';
  }
  /**
   * getMIMEtype()
   *
   * @param mixed $filename
   * @return
   */
  function getMIMEtype($filename)
  {
	  preg_match("|\.([a-z0-9]{2,4})$|i", $filename, $fileSuffix);

	  $fs = (isset($fileSuffix[1])) ? $fileSuffix[1] : null;

	  switch(strtolower($fs))
	  {
		  case "js" :
			  return "application/x-javascript";

		  case "json" :
			  return "application/json";

		  case "jpg" :
		  case "jpeg" :
		  case "jpe" :
			  return "image/jpg";

		  case "png" :
		  case "gif" :
		  case "bmp" :
		  case "tiff" :
			  return "image/".strtolower($fs);

		  case "css" :
			  return "text/css";

		  case "xml" :
			  return "application/xml";

		  case "doc" :
		  case "docx" :
			  return "application/msword";

		  case "xls" :
		  case "xlt" :
		  case "xlm" :
		  case "xld" :
		  case "xla" :
		  case "xlc" :
		  case "xlw" :
		  case "xll" :
			  return "application/vnd.ms-excel";

		  case "ppt" :
		  case "pps" :
			  return "application/vnd.ms-powerpoint";

		  case "rtf" :
			  return "application/rtf";

		  case "pdf" :
			  return "application/pdf";

		  case "html" :
		  case "htm" :
		  case "php" :
			  return "text/html";

		  case "txt" :
			  return "text/plain";

		  case "mpeg" :
		  case "mpg" :
		  case "mpe" :
			  return "video/mpeg";

		  case "mp3" :
			  return "audio/mpeg3";

		  case "wav" :
			  return "audio/wav";

		  case "aiff" :
		  case "aif" :
			  return "audio/aiff";

		  case "avi" :
			  return "video/msvideo";

		  case "wmv" :
			  return "video/x-ms-wmv";

		  case "mov" :
			  return "video/quicktime";

		  case "zip" :
			  return "application/zip";

		  case "tar" :
			  return "application/x-tar";

		  case "swf" :
			  return "application/x-shockwave-flash";

		  default :
		  if(function_exists("mime_content_type"))
		  {
			  $fileSuffix = mime_content_type($filename);
		  }

		  return "unknown/" . trim($fs, ".");
	  }
  }

  /**
   * randName()
   *
   * @return
   */
  function siteurl(){
	  if(isset($_SERVER['HTTPS'])){
		  $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
	  }
	  else{
		  $protocol = 'http';
	  }
	    $ds = '/';
		$url = preg_replace("#/+#", "/", $_SERVER['HTTP_HOST'] . $ds . Registry::get("Core")->site_dir);

	  return $protocol . "://" . $url;
  }

  /**
   * randName()
   *
   * @return
   */
  function randName() {
	  $code = '';
	  for($x = 0; $x<6; $x++) {
		  $code .= '-'.substr(strtoupper(sha1(rand(0,999999999999999))),2,6);
	  }
	  $code = substr($code,1);
	  return $code;
  }

  /**
   * downloadFile()
   *
   * @return
   */
  function downloadFile($fileLocation, $fileName, $update = false, $tid = 1, $maxSpeed = 1024)
  {
      if (connection_status() != 0)
          return (false);
      //$extension = strtolower(end(explode('.', $fileName)));
	  $extension = strtolower(substr($fileName, strrpos($fileName, '.') + 1));

      /* List of File Types */
      $fileTypes['swf'] = 'application/x-shockwave-flash';
      $fileTypes['pdf'] = 'application/pdf';
      $fileTypes['exe'] = 'application/octet-stream';
      $fileTypes['zip'] = 'application/zip';
      $fileTypes['doc'] = 'application/msword';
      $fileTypes['xls'] = 'application/vnd.ms-excel';
      $fileTypes['ppt'] = 'application/vnd.ms-powerpoint';
      $fileTypes['gif'] = 'image/gif';
      $fileTypes['png'] = 'image/png';
      $fileTypes['jpeg'] = 'image/jpg';
      $fileTypes['jpg'] = 'image/jpg';
      $fileTypes['rar'] = 'application/rar';

      $fileTypes['ra'] = 'audio/x-pn-realaudio';
      $fileTypes['ram'] = 'audio/x-pn-realaudio';
      $fileTypes['ogg'] = 'audio/x-pn-realaudio';

      $fileTypes['wav'] = 'video/x-msvideo';
      $fileTypes['wmv'] = 'video/x-msvideo';
      $fileTypes['avi'] = 'video/x-msvideo';
      $fileTypes['asf'] = 'video/x-msvideo';
      $fileTypes['divx'] = 'video/x-msvideo';

      $fileTypes['mp3'] = 'audio/mpeg';
      $fileTypes['mp4'] = 'audio/mpeg';
      $fileTypes['mpeg'] = 'video/mpeg';
      $fileTypes['mpg'] = 'video/mpeg';
      $fileTypes['mpe'] = 'video/mpeg';
      $fileTypes['mov'] = 'video/quicktime';
      $fileTypes['swf'] = 'video/quicktime';
      $fileTypes['3gp'] = 'video/quicktime';
      $fileTypes['m4a'] = 'video/quicktime';
      $fileTypes['aac'] = 'video/quicktime';
      $fileTypes['m3u'] = 'video/quicktime';

      $contentType = $fileTypes[$extension];


      header("Cache-Control: public");
      header("Content-Transfer-Encoding: binary\n");
      header('Content-Type: $contentType');

      $contentDisposition = 'attachment';

      if (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE")) {
          $fileName = preg_replace('/\./', '%2e', $fileName, substr_count($fileName, '.') - 1);
          header("Content-Disposition: $contentDisposition;filename=\"$fileName\"");
      } else {
          header("Content-Disposition: $contentDisposition;filename=\"$fileName\"");
      }

      header("Accept-Ranges: bytes");
      $range = 0;
      $size = filesize($fileLocation);

      if (isset($_SERVER['HTTP_RANGE'])) {
          list($a, $range) = explode("=", $_SERVER['HTTP_RANGE']);
          str_replace($range, "-", $range);
          $size2 = $size - 1;
          $new_length = $size - $range;
          header("HTTP/1.1 206 Partial Content");
          header("Content-Length: $new_length");
          header("Content-Range: bytes $range$size2/$size");
      } else {
          $size2 = $size - 1;
          header("Content-Range: bytes 0-$size2/$size");
          header("Content-Length: " . $size);
      }

      if ($size == 0) {
          die('Zero byte file! Aborting download');
      }

      $fp = fopen("$fileLocation", "rb");

      fseek($fp, $range);

      while (!feof($fp) and (connection_status() == 0)) {
          set_time_limit(0);
          print (fread($fp, 1024 * $maxSpeed));
          flush();
          @ob_flush();
          sleep(1);
      }
      fclose($fp);

	  if($update){
		  $data['downloads'] = "INC(1)";
		  Registry::get("Database")->update(Products::tTable, $data, "id='" . $tid . "'");
	  }
      exit;

      return ((connection_status() == 0) and !connection_aborted());
  }


  $params = array(
      'icons' => array(
        'fa fa-left'               => 'Left',
        'fa fa-right'              => 'Right',
        'fa fa-add-sign-box'       => 'Add Sign Box',
        'fa fa-add-sign'           => 'Add Sign',
        'fa fa-add'                => 'Add',
        'fa fa-adjust'             => 'Adjust',
        'fa fa-adn'=>'Adn',
        'fa fa-align-center'=>'Align Center',
        'fa fa-align-justify'=>'Align Justify',
        'fa fa-align-left'=>'Align Left',
        'fa fa-align-right'=>'Align Right',
        'fa fa-ambulance'=> 'Ambulance',
        'fa fa-anchor'=>'Anchor',
        'fa fa-android'=>'Android',
        'fa fa-angle-down'=>'Angle Down',
        'fa fa-angle-left'=>'Angle Left',
        'fa fa-angle-right'=>'Angle Right',
        'fa fa-angle-up'=>'Angle Up',
        'fa fa-apple'=>'Apple',
        'fa fa-archive'=>'Archive',
        'fa fa-arrow-down'=>'Arrow Down',
        'fa fa-arrow-left'=>'Arrow Left',
        'fa fa-arrow-right'=>'Arrow Right',
        'fa fa-arrow-sign-down'=>'Arrow Sign Down',
        'fa fa-arrow-sign-left'=>'Arrow Sign Left',
        'fa fa-arrow-sign-right'=>'Arrow Sign Right',
        'fa fa-arrow-sign-up'=>'Arrow Sign Up',
        'fa fa-arrow-up'=>'Arrow Up',
        'fa fa-line-chart'=>'Line Chart',
        'fa fa-building-o'=>'Building',
        'fa fa-graduation-cap'=>'Graduation Cap',
        'fa fa-medkit'=>'Medical Kit',
        'fa fa-bars'=>'Bars',



        'ln ln-icon-Add-Bag' => 'Add Bag',
        'ln ln-icon-Add-Basket' => 'Add Basket',
        'ln ln-icon-Add-Cart' => 'Add Cart',
        'ln ln-icon-Add-File' => 'Add File',
        'ln ln-icon-Address-Book' => 'Address Book',
        'ln ln-icon-Address-Book2' => 'Address Book2',
        'ln ln-icon-Administrator' => 'Administrator',
        'ln ln-icon-Aerobics-2' => 'Aerobics 2',
        'ln ln-icon-Aerobics-3' => 'Aerobics 3',
        'ln ln-icon-Aerobics' => 'Aerobics',
        'ln ln-icon-Affiliate' => 'Affiliate',
        'ln ln-icon-Aim' => 'Aim',
        'ln ln-icon-Air-Balloon' => 'Air Balloon',
        'ln ln-icon-Airbrush' => 'Airbrush',
        'ln ln-icon-Airship' => 'Airship',
        'ln ln-icon-Alarm-Clock' => 'Alarm Clock',
        'ln ln-icon-Alarm-Clock2' => 'Alarm Clock2',
        'ln ln-icon-Alarm' => 'Alarm',
        'ln ln-icon-Alien-2' => 'Alien 2',
        'ln ln-icon-Alien' => 'Alien',
        'ln ln-icon-Wordpress' => 'Wordpress',
        'ln ln-icon-Code-Window' => 'Code Window',
        'ln ln-icon-Coding' => 'Coding',
        'ln ln-icon-Android' => 'Android',
        'ln ln-icon-Cloud-Secure' => 'Cloud Secure',
        'ln ln-icon-Pencil-Ruler' => 'Pencil Ruler',
        'ln ln-icon-Plane' => 'Plane',
        'ln ln-icon-Email' => 'Email',
        'ln ln-icon-Target-Market' => 'Target Market',
        'ln ln-icon-Target' => 'Target',
        
        
        
        
	    'ln ln-icon-Align-Center'=>'Align-Center',
	    'ln ln-icon-Align-JustifyAll'=>'Align-JustifyAll',
	    'ln ln-icon-Align-JustifyCenter'=>'Align-JustifyCenter',
	    'ln ln-icon-Align-JustifyLeft'=>'Align-JustifyLeft',
	    'ln ln-icon-Align-JustifyRight'=>'Align-JustifyRight',
	    'ln ln-icon-Align-Left'=>'Align-Left',
	    'ln ln-icon-Align-Right'=>'Align-Right',
	    'ln ln-icon-Alpha'=>'Alpha',
	    'ln ln-icon-Ambulance'=>'Ambulance',
	    'ln ln-icon-AMX'=>'AMX',
	    'ln ln-icon-Anchor-2'=>'Anchor-2',
	    'ln ln-icon-Anchor'=>'Anchor',
	    'ln ln-icon-Android-Store'=>'Android-Store',
	    'ln ln-icon-Angel-Smiley'=>'Angel-Smiley',
	    'ln ln-icon-Angel'=>'Angel',
	    'ln ln-icon-Angry'=>'Angry',
	    'ln ln-icon-Apple-Bite'=>'Apple-Bite',
	    'ln ln-icon-Apple-Store'=>'Apple-Store',
	    'ln ln-icon-Apple'=>'Apple',
	    'ln ln-icon-Approved-Window'=>'Approved-Window',
	    'ln ln-icon-Aquarius-2'=>'Aquarius-2',
	    'ln ln-icon-Aquarius'=>'Aquarius',
	    'ln ln-icon-Archery-2'=>'Archery-2',
	    'ln ln-icon-Archery'=>'Archery',
	    'ln ln-icon-Argentina'=>'Argentina',
	    'ln ln-icon-Aries-2'=>'Aries-2',
	    'ln ln-icon-Aries'=>'Aries',
	    'ln ln-icon-Army-Key'=>'Army-Key',
	    'ln ln-icon-Arrow-Around'=>'Arrow-Around',
	    'ln ln-icon-Arrow-Back3'=>'Arrow-Back3',
	    'ln ln-icon-Arrow-Back'=>'Arrow-Back',
	    'ln ln-icon-Arrow-Back2'=>'Arrow-Back2',
	    'ln ln-icon-Arrow-Barrier'=>'Arrow-Barrier',
	    'ln ln-icon-Arrow-Circle'=>'Arrow-Circle',
	    'ln ln-icon-Arrow-Cross'=>'Arrow-Cross',
	    'ln ln-icon-Arrow-Down'=>'Arrow-Down',
	    'ln ln-icon-Arrow-Down2'=>'Arrow-Down2',
	    'ln ln-icon-Arrow-Down3'=>'Arrow-Down3',
	    'ln ln-icon-Arrow-DowninCircle'=>'Arrow-DowninCircle',
	    'ln ln-icon-Arrow-Fork'=>'Arrow-Fork',
	    'ln ln-icon-Arrow-Forward'=>'Arrow-Forward',
	    'ln ln-icon-Arrow-Forward2'=>'Arrow-Forward2',
	    'ln ln-icon-Arrow-From'=>'Arrow-From',
	    'ln ln-icon-Arrow-Inside'=>'Arrow-Inside',
	    'ln ln-icon-Arrow-Inside45'=>'Arrow-Inside45',
	    'ln ln-icon-Arrow-InsideGap'=>'Arrow-InsideGap',
	    'ln ln-icon-Arrow-InsideGap45'=>'Arrow-InsideGap45',
	    'ln ln-icon-Arrow-Into'=>'Arrow-Into',
	    'ln ln-icon-Arrow-Join'=>'Arrow-Join',
	    'ln ln-icon-Arrow-Junction'=>'Arrow-Junction',
	    'ln ln-icon-Arrow-Left'=>'Arrow-Left',
	    'ln ln-icon-Arrow-Left2'=>'Arrow-Left2',
	    'ln ln-icon-Arrow-LeftinCircle'=>'Arrow-LeftinCircle',
	    'ln ln-icon-Arrow-Loop'=>'Arrow-Loop',
	    'ln ln-icon-Arrow-Merge'=>'Arrow-Merge',
	    'ln ln-icon-Arrow-Mix'=>'Arrow-Mix',
	    'ln ln-icon-Arrow-Next'=>'Arrow-Next',
	    'ln ln-icon-Arrow-OutLeft'=>'Arrow-OutLeft',
	    'ln ln-icon-Arrow-OutRight'=>'Arrow-OutRight',
	    'ln ln-icon-Arrow-Outside'=>'Arrow-Outside',
	    'ln ln-icon-Arrow-Outside45'=>'Arrow-Outside45',
	    'ln ln-icon-Arrow-OutsideGap'=>'Arrow-OutsideGap',
	    'ln ln-icon-Arrow-OutsideGap45'=>'Arrow-OutsideGap45',
	    'ln ln-icon-Arrow-Over'=>'Arrow-Over',
	    'ln ln-icon-Arrow-Refresh'=>'Arrow-Refresh',
	    'ln ln-icon-Arrow-Refresh2'=>'Arrow-Refresh2',
	    'ln ln-icon-Arrow-Right'=>'Arrow-Right',
	    'ln ln-icon-Arrow-Right2'=>'Arrow-Right2',
	    'ln ln-icon-Arrow-RightinCircle'=>'Arrow-RightinCircle',
	    'ln ln-icon-Arrow-Shuffle'=>'Arrow-Shuffle',
	    'ln ln-icon-Arrow-Squiggly'=>'Arrow-Squiggly',
	    'ln ln-icon-Arrow-Through'=>'Arrow-Through',
	    'ln ln-icon-Arrow-To'=>'Arrow-To',
	    'ln ln-icon-Arrow-TurnLeft'=>'Arrow-TurnLeft',
	    'ln ln-icon-Arrow-TurnRight'=>'Arrow-TurnRight',
	    'ln ln-icon-Arrow-Up'=>'Arrow-Up',
	    'ln ln-icon-Arrow-Up2'=>'Arrow-Up2',
	    'ln ln-icon-Arrow-Up3'=>'Arrow-Up3',
	    'ln ln-icon-Arrow-UpinCircle'=>'Arrow-UpinCircle',
	    'ln ln-icon-Arrow-XLeft'=>'Arrow-XLeft',
	    'ln ln-icon-Arrow-XRight'=>'Arrow-XRight',
	    'ln ln-icon-Ask'=>'Ask',
	    'ln ln-icon-Assistant'=>'Assistant',
	    'ln ln-icon-Astronaut'=>'Astronaut',
	    'ln ln-icon-At-Sign'=>'At-Sign',
	    'ln ln-icon-ATM'=>'ATM',
	    'ln ln-icon-Atom'=>'Atom',
	    'ln ln-icon-Audio'=>'Audio',
	    'ln ln-icon-Auto-Flash'=>'Auto-Flash',
	    'ln ln-icon-Autumn'=>'Autumn',
	    'ln ln-icon-Baby-Clothes'=>'Baby-Clothes',
	    'ln ln-icon-Baby-Clothes2'=>'Baby-Clothes2',
	    'ln ln-icon-Baby-Cry'=>'Baby-Cry',
	    'ln ln-icon-Baby'=>'Baby',
	    'ln ln-icon-Back2'=>'Back2',
	    'ln ln-icon-Back-Media'=>'Back-Media',
	    'ln ln-icon-Back-Music'=>'Back-Music',
	    'ln ln-icon-Back'=>'Back',
	    'ln ln-icon-Background'=>'Background',
	    'ln ln-icon-Bacteria'=>'Bacteria',
	    'ln ln-icon-Bag-Coins'=>'Bag-Coins',
	    'ln ln-icon-Bag-Items'=>'Bag-Items',
	    'ln ln-icon-Bag-Quantity'=>'Bag-Quantity',
	    'ln ln-icon-Bag'=>'Bag',
	    'ln ln-icon-Bakelite'=>'Bakelite',
	    'ln ln-icon-Ballet-Shoes'=>'Ballet-Shoes',
	    'ln ln-icon-Balloon'=>'Balloon',
	    'ln ln-icon-Banana'=>'Banana',
	    'ln ln-icon-Band-Aid'=>'Band-Aid',
	    'ln ln-icon-Bank'=>'Bank',
	    'ln ln-icon-Bar-Chart'=>'Bar-Chart',
	    'ln ln-icon-Bar-Chart2'=>'Bar-Chart2',
	    'ln ln-icon-Bar-Chart3'=>'Bar-Chart3',
	    'ln ln-icon-Bar-Chart4'=>'Bar-Chart4',
	    'ln ln-icon-Bar-Chart5'=>'Bar-Chart5',
	    'ln ln-icon-Bar-Code'=>'Bar-Code',
	    'ln ln-icon-Barricade-2'=>'Barricade-2',
	    'ln ln-icon-Barricade'=>'Barricade',
	    'ln ln-icon-Baseball'=>'Baseball',
	    'ln ln-icon-Basket-Ball'=>'Basket-Ball',
	    'ln ln-icon-Basket-Coins'=>'Basket-Coins',
	    'ln ln-icon-Basket-Items'=>'Basket-Items',
	    'ln ln-icon-Basket-Quantity'=>'Basket-Quantity',
	    'ln ln-icon-Bat-2'=>'Bat-2',
	    'ln ln-icon-Bat'=>'Bat',
	    'ln ln-icon-Bathrobe'=>'Bathrobe',
	    'ln ln-icon-Batman-Mask'=>'Batman-Mask',
	    'ln ln-icon-Battery-0'=>'Battery-0',
	    'ln ln-icon-Battery-25'=>'Battery-25',
	    'ln ln-icon-Battery-50'=>'Battery-50',
	    'ln ln-icon-Battery-75'=>'Battery-75',
	    'ln ln-icon-Battery-100'=>'Battery-100',
	    'ln ln-icon-Battery-Charge'=>'Battery-Charge',
	    'ln ln-icon-Bear'=>'Bear',
	    'ln ln-icon-Beard-2'=>'Beard-2',
	    'ln ln-icon-Beard-3'=>'Beard-3',
	    'ln ln-icon-Beard'=>'Beard',
	    'ln ln-icon-Bebo'=>'Bebo',
	    'ln ln-icon-Bee'=>'Bee',
	    'ln ln-icon-Beer-Glass'=>'Beer-Glass',
	    'ln ln-icon-Beer'=>'Beer',
	    'ln ln-icon-Bell-2'=>'Bell-2',
	    'ln ln-icon-Bell'=>'Bell',
	    'ln ln-icon-Belt-2'=>'Belt-2',
	    'ln ln-icon-Belt-3'=>'Belt-3',
	    'ln ln-icon-Belt'=>'Belt',
	    'ln ln-icon-Berlin-Tower'=>'Berlin-Tower',
	    'ln ln-icon-Beta'=>'Beta',
	    'ln ln-icon-Betvibes'=>'Betvibes',
	    'ln ln-icon-Bicycle-2'=>'Bicycle-2',
	    'ln ln-icon-Bicycle-3'=>'Bicycle-3',
	    'ln ln-icon-Bicycle'=>'Bicycle',
	    'ln ln-icon-Big-Bang'=>'Big-Bang',
	    'ln ln-icon-Big-Data'=>'Big-Data',
	    'ln ln-icon-Bike-Helmet'=>'Bike-Helmet',
	    'ln ln-icon-Bikini'=>'Bikini',
	    'ln ln-icon-Bilk-Bottle2'=>'Bilk-Bottle2',
	    'ln ln-icon-Billing'=>'Billing',
	    'ln ln-icon-Bing'=>'Bing',
	    'ln ln-icon-Binocular'=>'Binocular',
	    'ln ln-icon-Bio-Hazard'=>'Bio-Hazard',
	    'ln ln-icon-Biotech'=>'Biotech',
	    'ln ln-icon-Bird-DeliveringLetter'=>'Bird-DeliveringLetter',
	    'ln ln-icon-Bird'=>'Bird',
	    'ln ln-icon-Birthday-Cake'=>'Birthday-Cake',
	    'ln ln-icon-Bisexual'=>'Bisexual',
	    'ln ln-icon-Bishop'=>'Bishop',
	    'ln ln-icon-Bitcoin'=>'Bitcoin',
	    'ln ln-icon-Black-Cat'=>'Black-Cat',
	    'ln ln-icon-Blackboard'=>'Blackboard',
	    'ln ln-icon-Blinklist'=>'Blinklist',
	    'ln ln-icon-Block-Cloud'=>'Block-Cloud',
	    'ln ln-icon-Block-Window'=>'Block-Window',
	    'ln ln-icon-Blogger'=>'Blogger',
	    'ln ln-icon-Blood'=>'Blood',
	    'ln ln-icon-Blouse'=>'Blouse',
	    'ln ln-icon-Blueprint'=>'Blueprint',
	    'ln ln-icon-Board'=>'Board',
	    'ln ln-icon-Bodybuilding'=>'Bodybuilding',
	    'ln ln-icon-Bold-Text'=>'Bold-Text',
	    'ln ln-icon-Bone'=>'Bone',
	    'ln ln-icon-Bones'=>'Bones',
	    'ln ln-icon-Book'=>'Book',
	    'ln ln-icon-Bookmark'=>'Bookmark',
	    'ln ln-icon-Books-2'=>'Books-2',
	    'ln ln-icon-Books'=>'Books',
	    'ln ln-icon-Boom'=>'Boom',
	    'ln ln-icon-Boot-2'=>'Boot-2',
	    'ln ln-icon-Boot'=>'Boot',
	    'ln ln-icon-Bottom-ToTop'=>'Bottom-ToTop',
	    'ln ln-icon-Bow-2'=>'Bow-2',
	    'ln ln-icon-Bow-3'=>'Bow-3',
	    'ln ln-icon-Bow-4'=>'Bow-4',
	    'ln ln-icon-Bow-5'=>'Bow-5',
	    'ln ln-icon-Bow-6'=>'Bow-6',
	    'ln ln-icon-Bow'=>'Bow',
	    'ln ln-icon-Bowling-2'=>'Bowling-2',
	    'ln ln-icon-Bowling'=>'Bowling',
	    'ln ln-icon-Box2'=>'Box2',
	    'ln ln-icon-Box-Close'=>'Box-Close',
	    'ln ln-icon-Box-Full'=>'Box-Full',
	    'ln ln-icon-Box-Open'=>'Box-Open',
	    'ln ln-icon-Box-withFolders'=>'Box-withFolders',
	    'ln ln-icon-Box'=>'Box',
	    'ln ln-icon-Boy'=>'Boy',
	    'ln ln-icon-Bra'=>'Bra',
	    'ln ln-icon-Brain-2'=>'Brain-2',
	    'ln ln-icon-Brain-3'=>'Brain-3',
	    'ln ln-icon-Brain'=>'Brain',
	    'ln ln-icon-Brazil'=>'Brazil',
	    'ln ln-icon-Bread-2'=>'Bread-2',
	    'ln ln-icon-Bread'=>'Bread',
	    'ln ln-icon-Bridge'=>'Bridge',
	    'ln ln-icon-Brightkite'=>'Brightkite',
	    'ln ln-icon-Broke-Link2'=>'Broke-Link2',
	    'ln ln-icon-Broken-Link'=>'Broken-Link',
	    'ln ln-icon-Broom'=>'Broom',
	    'ln ln-icon-Brush'=>'Brush',
	    'ln ln-icon-Bucket'=>'Bucket',
	    'ln ln-icon-Bug'=>'Bug',
	    'ln ln-icon-Building'=>'Building',
	    'ln ln-icon-Bulleted-List'=>'Bulleted-List',
	    'ln ln-icon-Bus-2'=>'Bus-2',
	    'ln ln-icon-Bus'=>'Bus',
	    'ln ln-icon-Business-Man'=>'Business-Man',
	    'ln ln-icon-Business-ManWoman'=>'Business-ManWoman',
	    'ln ln-icon-Business-Mens'=>'Business-Mens',
	    'ln ln-icon-Business-Woman'=>'Business-Woman',
	    'ln ln-icon-Butterfly'=>'Butterfly',
	    'ln ln-icon-Button'=>'Button',
	    'ln ln-icon-Cable-Car'=>'Cable-Car',
	    'ln ln-icon-Cake'=>'Cake',
	    'ln ln-icon-Calculator-2'=>'Calculator-2',
	    'ln ln-icon-Calculator-3'=>'Calculator-3',
	    'ln ln-icon-Calculator'=>'Calculator',
	    'ln ln-icon-Calendar-2'=>'Calendar-2',
	    'ln ln-icon-Calendar-3'=>'Calendar-3',
	    'ln ln-icon-Calendar-4'=>'Calendar-4',
	    'ln ln-icon-Calendar-Clock'=>'Calendar-Clock',
	    'ln ln-icon-Calendar'=>'Calendar',
	    'ln ln-icon-Camel'=>'Camel',
	    'ln ln-icon-Camera-2'=>'Camera-2',
	    'ln ln-icon-Camera-3'=>'Camera-3',
	    'ln ln-icon-Camera-4'=>'Camera-4',
	    'ln ln-icon-Camera-5'=>'Camera-5',
	    'ln ln-icon-Camera-Back'=>'Camera-Back',
	    'ln ln-icon-Camera'=>'Camera',
	    'ln ln-icon-Can-2'=>'Can-2',
	    'ln ln-icon-Can'=>'Can',
	    'ln ln-icon-Canada'=>'Canada',
	    'ln ln-icon-Cancer-2'=>'Cancer-2',
	    'ln ln-icon-Cancer-3'=>'Cancer-3',
	    'ln ln-icon-Cancer'=>'Cancer',
	    'ln ln-icon-Candle'=>'Candle',
	    'ln ln-icon-Candy-Cane'=>'Candy-Cane',
	    'ln ln-icon-Candy'=>'Candy',
	    'ln ln-icon-Cannon'=>'Cannon',
	    'ln ln-icon-Cap-2'=>'Cap-2',
	    'ln ln-icon-Cap-3'=>'Cap-3',
	    'ln ln-icon-Cap-Smiley'=>'Cap-Smiley',
	    'ln ln-icon-Cap'=>'Cap',
	    'ln ln-icon-Capricorn-2'=>'Capricorn-2',
	    'ln ln-icon-Capricorn'=>'Capricorn',
	    'ln ln-icon-Car-2'=>'Car-2',
	    'ln ln-icon-Car-3'=>'Car-3',
	    'ln ln-icon-Car-Coins'=>'Car-Coins',
	    'ln ln-icon-Car-Items'=>'Car-Items',
	    'ln ln-icon-Car-Wheel'=>'Car-Wheel',
	    'ln ln-icon-Car'=>'Car',
	    'ln ln-icon-Cardigan'=>'Cardigan',
	    'ln ln-icon-Cardiovascular'=>'Cardiovascular',
	    'ln ln-icon-Cart-Quantity'=>'Cart-Quantity',
	    'ln ln-icon-Casette-Tape'=>'Casette-Tape',
	    'ln ln-icon-Cash-Register'=>'Cash-Register',
	    'ln ln-icon-Cash-register2'=>'Cash-register2',
	    'ln ln-icon-Castle'=>'Castle',
	    'ln ln-icon-Cat'=>'Cat',
	    'ln ln-icon-Cathedral'=>'Cathedral',
	    'ln ln-icon-Cauldron'=>'Cauldron',
	    'ln ln-icon-CD-2'=>'CD-2',
	    'ln ln-icon-CD-Cover'=>'CD-Cover',
	    'ln ln-icon-CD'=>'CD',
	    'ln ln-icon-Cello'=>'Cello',
	    'ln ln-icon-Celsius'=>'Celsius',
	    'ln ln-icon-Chacked-Flag'=>'Chacked-Flag',
	    'ln ln-icon-Chair'=>'Chair',
	    'ln ln-icon-Charger'=>'Charger',
	    'ln ln-icon-Check-2'=>'Check-2',
	    'ln ln-icon-Check'=>'Check',
	    'ln ln-icon-Checked-User'=>'Checked-User',
	    'ln ln-icon-Checkmate'=>'Checkmate',
	    'ln ln-icon-Checkout-Bag'=>'Checkout-Bag',
	    'ln ln-icon-Checkout-Basket'=>'Checkout-Basket',
	    'ln ln-icon-Checkout'=>'Checkout',
	    'ln ln-icon-Cheese'=>'Cheese',
	    'ln ln-icon-Cheetah'=>'Cheetah',
	    'ln ln-icon-Chef-Hat'=>'Chef-Hat',
	    'ln ln-icon-Chef-Hat2'=>'Chef-Hat2',
	    'ln ln-icon-Chef'=>'Chef',
	    'ln ln-icon-Chemical-2'=>'Chemical-2',
	    'ln ln-icon-Chemical-3'=>'Chemical-3',
	    'ln ln-icon-Chemical-4'=>'Chemical-4',
	    'ln ln-icon-Chemical-5'=>'Chemical-5',
	    'ln ln-icon-Chemical'=>'Chemical',
	    'ln ln-icon-Chess-Board'=>'Chess-Board',
	    'ln ln-icon-Chess'=>'Chess',
	    'ln ln-icon-Chicken'=>'Chicken',
	    'ln ln-icon-Chile'=>'Chile',
	    'ln ln-icon-Chimney'=>'Chimney',
	    'ln ln-icon-China'=>'China',
	    'ln ln-icon-Chinese-Temple'=>'Chinese-Temple',
	    'ln ln-icon-Chip'=>'Chip',
	    'ln ln-icon-Chopsticks-2'=>'Chopsticks-2',
	    'ln ln-icon-Chopsticks'=>'Chopsticks',
	    'ln ln-icon-Christmas-Ball'=>'Christmas-Ball',
	    'ln ln-icon-Christmas-Bell'=>'Christmas-Bell',
	    'ln ln-icon-Christmas-Candle'=>'Christmas-Candle',
	    'ln ln-icon-Christmas-Hat'=>'Christmas-Hat',
	    'ln ln-icon-Christmas-Sleigh'=>'Christmas-Sleigh',
	    'ln ln-icon-Christmas-Snowman'=>'Christmas-Snowman',
	    'ln ln-icon-Christmas-Sock'=>'Christmas-Sock',
	    'ln ln-icon-Christmas-Tree'=>'Christmas-Tree',
	    'ln ln-icon-Christmas'=>'Christmas',
	    'ln ln-icon-Chrome'=>'Chrome',
	    'ln ln-icon-Chrysler-Building'=>'Chrysler-Building',
	    'ln ln-icon-Cinema'=>'Cinema',
	    'ln ln-icon-Circular-Point'=>'Circular-Point',
	    'ln ln-icon-City-Hall'=>'City-Hall',
	    'ln ln-icon-Clamp'=>'Clamp',
	    'ln ln-icon-Clapperboard-Close'=>'Clapperboard-Close',
	    'ln ln-icon-Clapperboard-Open'=>'Clapperboard-Open',
	    'ln ln-icon-Claps'=>'Claps',
	    'ln ln-icon-Clef'=>'Clef',
	    'ln ln-icon-Clinic'=>'Clinic',
	    'ln ln-icon-Clock-2'=>'Clock-2',
	    'ln ln-icon-Clock-3'=>'Clock-3',
	    'ln ln-icon-Clock-4'=>'Clock-4',
	    'ln ln-icon-Clock-Back'=>'Clock-Back',
	    'ln ln-icon-Clock-Forward'=>'Clock-Forward',
	    'ln ln-icon-Clock'=>'Clock',
	    'ln ln-icon-Close-Window'=>'Close-Window',
	    'ln ln-icon-Close'=>'Close',
	    'ln ln-icon-Clothing-Store'=>'Clothing-Store',
	    'ln ln-icon-Cloud--'=>'Cloud--',
	    'ln ln-icon-Cloud-'=>'Cloud-',
	    'ln ln-icon-Cloud-Camera'=>'Cloud-Camera',
	    'ln ln-icon-Cloud-Computer'=>'Cloud-Computer',
	    'ln ln-icon-Cloud-Email'=>'Cloud-Email',
	    'ln ln-icon-Cloud-Hail'=>'Cloud-Hail',
	    'ln ln-icon-Cloud-Laptop'=>'Cloud-Laptop',
	    'ln ln-icon-Cloud-Lock'=>'Cloud-Lock',
	    'ln ln-icon-Cloud-Moon'=>'Cloud-Moon',
	    'ln ln-icon-Cloud-Music'=>'Cloud-Music',
	    'ln ln-icon-Cloud-Picture'=>'Cloud-Picture',
	    'ln ln-icon-Cloud-Rain'=>'Cloud-Rain',
	    'ln ln-icon-Cloud-Remove'=>'Cloud-Remove',
	    'ln ln-icon-Cloud-Settings'=>'Cloud-Settings',
	    'ln ln-icon-Cloud-Smartphone'=>'Cloud-Smartphone',
	    'ln ln-icon-Cloud-Snow'=>'Cloud-Snow',
	    'ln ln-icon-Cloud-Sun'=>'Cloud-Sun',
	    'ln ln-icon-Cloud-Tablet'=>'Cloud-Tablet',
	    'ln ln-icon-Cloud-Video'=>'Cloud-Video',
	    'ln ln-icon-Cloud-Weather'=>'Cloud-Weather',
	    'ln ln-icon-Cloud'=>'Cloud',
	    'ln ln-icon-Clouds-Weather'=>'Clouds-Weather',
	    'ln ln-icon-Clouds'=>'Clouds',
	    'ln ln-icon-Clown'=>'Clown',
	    'ln ln-icon-CMYK'=>'CMYK',
	    'ln ln-icon-Coat'=>'Coat',
	    'ln ln-icon-Cocktail'=>'Cocktail',
	    'ln ln-icon-Coconut'=>'Coconut',
	    'ln ln-icon-Coffee-2'=>'Coffee-2',
	    'ln ln-icon-Coffee-Bean'=>'Coffee-Bean',
	    'ln ln-icon-Coffee-Machine'=>'Coffee-Machine',
	    'ln ln-icon-Coffee-toGo'=>'Coffee-toGo',
	    'ln ln-icon-Coffee'=>'Coffee',
	    'ln ln-icon-Coffin'=>'Coffin',
	    'ln ln-icon-Coin'=>'Coin',
	    'ln ln-icon-Coins-2'=>'Coins-2',
	    'ln ln-icon-Coins-3'=>'Coins-3',
	    'ln ln-icon-Coins'=>'Coins',
	    'ln ln-icon-Colombia'=>'Colombia',
	    'ln ln-icon-Colosseum'=>'Colosseum',
	    'ln ln-icon-Column-2'=>'Column-2',
	    'ln ln-icon-Column-3'=>'Column-3',
	    'ln ln-icon-Column'=>'Column',
	    'ln ln-icon-Comb-2'=>'Comb-2',
	    'ln ln-icon-Comb'=>'Comb',
	    'ln ln-icon-Communication-Tower'=>'Communication-Tower',
	    'ln ln-icon-Communication-Tower2'=>'Communication-Tower2',
	    'ln ln-icon-Compass-2'=>'Compass-2',
	    'ln ln-icon-Compass-3'=>'Compass-3',
	    'ln ln-icon-Compass-4'=>'Compass-4',
	    'ln ln-icon-Compass-Rose'=>'Compass-Rose',
	    'ln ln-icon-Compass'=>'Compass',
	    'ln ln-icon-Computer-2'=>'Computer-2',
	    'ln ln-icon-Computer-3'=>'Computer-3',
	    'ln ln-icon-Computer-Secure'=>'Computer-Secure',
	    'ln ln-icon-Computer'=>'Computer',
	    'ln ln-icon-Conference'=>'Conference',
	    'ln ln-icon-Confused'=>'Confused',
	    'ln ln-icon-Conservation'=>'Conservation',
	    'ln ln-icon-Consulting'=>'Consulting',
	    'ln ln-icon-Contrast'=>'Contrast',
	    'ln ln-icon-Control-2'=>'Control-2',
	    'ln ln-icon-Control'=>'Control',
	    'ln ln-icon-Cookie-Man'=>'Cookie-Man',
	    'ln ln-icon-Cookies'=>'Cookies',
	    'ln ln-icon-Cool-Guy'=>'Cool-Guy',
	    'ln ln-icon-Cool'=>'Cool',
	    'ln ln-icon-Copyright'=>'Copyright',
	    'ln ln-icon-Costume'=>'Costume',
	    'ln ln-icon-Couple-Sign'=>'Couple-Sign',
	    'ln ln-icon-Cow'=>'Cow',
	    'ln ln-icon-CPU'=>'CPU',
	    'ln ln-icon-Crane'=>'Crane',
	    'ln ln-icon-Cranium'=>'Cranium',
	    'ln ln-icon-Credit-Card'=>'Credit-Card',
	    'ln ln-icon-Credit-Card2'=>'Credit-Card2',
	    'ln ln-icon-Credit-Card3'=>'Credit-Card3',
	    'ln ln-icon-Cricket'=>'Cricket',
	    'ln ln-icon-Criminal'=>'Criminal',
	    'ln ln-icon-Croissant'=>'Croissant',
	    'ln ln-icon-Crop-2'=>'Crop-2',
	    'ln ln-icon-Crop-3'=>'Crop-3',
	    'ln ln-icon-Crown-2'=>'Crown-2',
	    'ln ln-icon-Crown'=>'Crown',
	    'ln ln-icon-Crying'=>'Crying',
	    'ln ln-icon-Cube-Molecule'=>'Cube-Molecule',
	    'ln ln-icon-Cube-Molecule2'=>'Cube-Molecule2',
	    'ln ln-icon-Cupcake'=>'Cupcake',
	    'ln ln-icon-Cursor-Click'=>'Cursor-Click',
	    'ln ln-icon-Cursor-Click2'=>'Cursor-Click2',
	    'ln ln-icon-Cursor-Move'=>'Cursor-Move',
	    'ln ln-icon-Cursor-Move2'=>'Cursor-Move2',
	    'ln ln-icon-Cursor-Select'=>'Cursor-Select',
	    'ln ln-icon-Cursor'=>'Cursor',
	    'ln ln-icon-D-Eyeglasses'=>'D-Eyeglasses',
	    'ln ln-icon-D-Eyeglasses2'=>'D-Eyeglasses2',
	    'ln ln-icon-Dam'=>'Dam',
	    'ln ln-icon-Danemark'=>'Danemark',
	    'ln ln-icon-Danger-2'=>'Danger-2',
	    'ln ln-icon-Danger'=>'Danger',
	    'ln ln-icon-Dashboard'=>'Dashboard',
	    'ln ln-icon-Data-Backup'=>'Data-Backup',
	    'ln ln-icon-Data-Block'=>'Data-Block',
	    'ln ln-icon-Data-Center'=>'Data-Center',
	    'ln ln-icon-Data-Clock'=>'Data-Clock',
	    'ln ln-icon-Data-Cloud'=>'Data-Cloud',
	    'ln ln-icon-Data-Compress'=>'Data-Compress',
	    'ln ln-icon-Data-Copy'=>'Data-Copy',
	    'ln ln-icon-Data-Download'=>'Data-Download',
	    'ln ln-icon-Data-Financial'=>'Data-Financial',
	    'ln ln-icon-Data-Key'=>'Data-Key',
	    'ln ln-icon-Data-Lock'=>'Data-Lock',
	    'ln ln-icon-Data-Network'=>'Data-Network',
	    'ln ln-icon-Data-Password'=>'Data-Password',
	    'ln ln-icon-Data-Power'=>'Data-Power',
	    'ln ln-icon-Data-Refresh'=>'Data-Refresh',
	    'ln ln-icon-Data-Save'=>'Data-Save',
	    'ln ln-icon-Data-Search'=>'Data-Search',
	    'ln ln-icon-Data-Security'=>'Data-Security',
	    'ln ln-icon-Data-Settings'=>'Data-Settings',
	    'ln ln-icon-Data-Sharing'=>'Data-Sharing',
	    'ln ln-icon-Data-Shield'=>'Data-Shield',
	    'ln ln-icon-Data-Signal'=>'Data-Signal',
	    'ln ln-icon-Data-Storage'=>'Data-Storage',
	    'ln ln-icon-Data-Stream'=>'Data-Stream',
	    'ln ln-icon-Data-Transfer'=>'Data-Transfer',
	    'ln ln-icon-Data-Unlock'=>'Data-Unlock',
	    'ln ln-icon-Data-Upload'=>'Data-Upload',
	    'ln ln-icon-Data-Yes'=>'Data-Yes',
	    'ln ln-icon-Data'=>'Data',
	    'ln ln-icon-David-Star'=>'David-Star',
	    'ln ln-icon-Daylight'=>'Daylight',
	    'ln ln-icon-Death'=>'Death',
	    'ln ln-icon-Debian'=>'Debian',
	    'ln ln-icon-Dec'=>'Dec',
	    'ln ln-icon-Decrase-Inedit'=>'Decrase-Inedit',
	    'ln ln-icon-Deer-2'=>'Deer-2',
	    'ln ln-icon-Deer'=>'Deer',
	    'ln ln-icon-Delete-File'=>'Delete-File',
	    'ln ln-icon-Delete-Window'=>'Delete-Window',
	    'ln ln-icon-Delicious'=>'Delicious',
	    'ln ln-icon-Depression'=>'Depression',
	    'ln ln-icon-Deviantart'=>'Deviantart',
	    'ln ln-icon-Device-SyncwithCloud'=>'Device-SyncwithCloud',
	    'ln ln-icon-Diamond'=>'Diamond',
	    'ln ln-icon-Dice-2'=>'Dice-2',
	    'ln ln-icon-Dice'=>'Dice',
	    'ln ln-icon-Digg'=>'Digg',
	    'ln ln-icon-Digital-Drawing'=>'Digital-Drawing',
	    'ln ln-icon-Diigo'=>'Diigo',
	    'ln ln-icon-Dinosaur'=>'Dinosaur',
	    'ln ln-icon-Diploma-2'=>'Diploma-2',
	    'ln ln-icon-Diploma'=>'Diploma',
	    'ln ln-icon-Direction-East'=>'Direction-East',
	    'ln ln-icon-Direction-North'=>'Direction-North',
	    'ln ln-icon-Direction-South'=>'Direction-South',
	    'ln ln-icon-Direction-West'=>'Direction-West',
	    'ln ln-icon-Director'=>'Director',
	    'ln ln-icon-Disk'=>'Disk',
	    'ln ln-icon-Dj'=>'Dj',
	    'ln ln-icon-DNA-2'=>'DNA-2',
	    'ln ln-icon-DNA-Helix'=>'DNA-Helix',
	    'ln ln-icon-DNA'=>'DNA',
	    'ln ln-icon-Doctor'=>'Doctor',
	    'ln ln-icon-Dog'=>'Dog',
	    'ln ln-icon-Dollar-Sign'=>'Dollar-Sign',
	    'ln ln-icon-Dollar-Sign2'=>'Dollar-Sign2',
	    'ln ln-icon-Dollar'=>'Dollar',
	    'ln ln-icon-Dolphin'=>'Dolphin',
	    'ln ln-icon-Domino'=>'Domino',
	    'ln ln-icon-Door-Hanger'=>'Door-Hanger',
	    'ln ln-icon-Door'=>'Door',
	    'ln ln-icon-Doplr'=>'Doplr',
	    'ln ln-icon-Double-Circle'=>'Double-Circle',
	    'ln ln-icon-Double-Tap'=>'Double-Tap',
	    'ln ln-icon-Doughnut'=>'Doughnut',
	    'ln ln-icon-Dove'=>'Dove',
	    'ln ln-icon-Down-2'=>'Down-2',
	    'ln ln-icon-Down-3'=>'Down-3',
	    'ln ln-icon-Down-4'=>'Down-4',
	    'ln ln-icon-Down'=>'Down',
	    'ln ln-icon-Download-2'=>'Download-2',
	    'ln ln-icon-Download-fromCloud'=>'Download-fromCloud',
	    'ln ln-icon-Download-Window'=>'Download-Window',
	    'ln ln-icon-Download'=>'Download',
	    'ln ln-icon-Downward'=>'Downward',
	    'ln ln-icon-Drag-Down'=>'Drag-Down',
	    'ln ln-icon-Drag-Left'=>'Drag-Left',
	    'ln ln-icon-Drag-Right'=>'Drag-Right',
	    'ln ln-icon-Drag-Up'=>'Drag-Up',
	    'ln ln-icon-Drag'=>'Drag',
	    'ln ln-icon-Dress'=>'Dress',
	    'ln ln-icon-Drill-2'=>'Drill-2',
	    'ln ln-icon-Drill'=>'Drill',
	    'ln ln-icon-Drop'=>'Drop',
	    'ln ln-icon-Dropbox'=>'Dropbox',
	    'ln ln-icon-Drum'=>'Drum',
	    'ln ln-icon-Dry'=>'Dry',
	    'ln ln-icon-Duck'=>'Duck',
	    'ln ln-icon-Dumbbell'=>'Dumbbell',
	    'ln ln-icon-Duplicate-Layer'=>'Duplicate-Layer',
	    'ln ln-icon-Duplicate-Window'=>'Duplicate-Window',
	    'ln ln-icon-DVD'=>'DVD',
	    'ln ln-icon-Eagle'=>'Eagle',
	    'ln ln-icon-Ear'=>'Ear',
	    'ln ln-icon-Earphones-2'=>'Earphones-2',
	    'ln ln-icon-Earphones'=>'Earphones',
	    'ln ln-icon-Eci-Icon'=>'Eci-Icon',
	    'ln ln-icon-Edit-Map'=>'Edit-Map',
	    'ln ln-icon-Edit'=>'Edit',
	    'ln ln-icon-Eggs'=>'Eggs',
	    'ln ln-icon-Egypt'=>'Egypt',
	    'ln ln-icon-Eifel-Tower'=>'Eifel-Tower',
	    'ln ln-icon-eject-2'=>'eject-2',
	    'ln ln-icon-Eject'=>'Eject',
	    'ln ln-icon-El-Castillo'=>'El-Castillo',
	    'ln ln-icon-Elbow'=>'Elbow',
	    'ln ln-icon-Electric-Guitar'=>'Electric-Guitar',
	    'ln ln-icon-Electricity'=>'Electricity',
	    'ln ln-icon-Elephant'=>'Elephant',
	    'ln ln-icon-Embassy'=>'Embassy',
	    'ln ln-icon-Empire-StateBuilding'=>'Empire-StateBuilding',
	    'ln ln-icon-Empty-Box'=>'Empty-Box',
	    'ln ln-icon-End2'=>'End2',
	    'ln ln-icon-End-2'=>'End-2',
	    'ln ln-icon-End'=>'End',
	    'ln ln-icon-Endways'=>'Endways',
	    'ln ln-icon-Engineering'=>'Engineering',
	    'ln ln-icon-Envelope-2'=>'Envelope-2',
	    'ln ln-icon-Envelope'=>'Envelope',
	    'ln ln-icon-Environmental-2'=>'Environmental-2',
	    'ln ln-icon-Environmental-3'=>'Environmental-3',
	    'ln ln-icon-Environmental'=>'Environmental',
	    'ln ln-icon-Equalizer'=>'Equalizer',
	    'ln ln-icon-Eraser-2'=>'Eraser-2',
	    'ln ln-icon-Eraser-3'=>'Eraser-3',
	    'ln ln-icon-Eraser'=>'Eraser',
	    'ln ln-icon-Error-404Window'=>'Error-404Window',
	    'ln ln-icon-Euro-Sign'=>'Euro-Sign',
	    'ln ln-icon-Euro-Sign2'=>'Euro-Sign2',
	    'ln ln-icon-Euro'=>'Euro',
	    'ln ln-icon-Evernote'=>'Evernote',
	    'ln ln-icon-Evil'=>'Evil',
	    'ln ln-icon-Explode'=>'Explode',
	    'ln ln-icon-Eye-2'=>'Eye-2',
	    'ln ln-icon-Eye-Blind'=>'Eye-Blind',
	    'ln ln-icon-Eye-Invisible'=>'Eye-Invisible',
	    'ln ln-icon-Eye-Scan'=>'Eye-Scan',
	    'ln ln-icon-Eye-Visible'=>'Eye-Visible',
	    'ln ln-icon-Eye'=>'Eye',
	    'ln ln-icon-Eyebrow-2'=>'Eyebrow-2',
	    'ln ln-icon-Eyebrow-3'=>'Eyebrow-3',
	    'ln ln-icon-Eyebrow'=>'Eyebrow',
	    'ln ln-icon-Eyeglasses-Smiley'=>'Eyeglasses-Smiley',
	    'ln ln-icon-Eyeglasses-Smiley2'=>'Eyeglasses-Smiley2',
	    'ln ln-icon-Face-Style'=>'Face-Style',
	    'ln ln-icon-Face-Style2'=>'Face-Style2',
	    'ln ln-icon-Face-Style3'=>'Face-Style3',
	    'ln ln-icon-Face-Style4'=>'Face-Style4',
	    'ln ln-icon-Face-Style5'=>'Face-Style5',
	    'ln ln-icon-Face-Style6'=>'Face-Style6',
	    'ln ln-icon-Facebook-2'=>'Facebook-2',
	    'ln ln-icon-Facebook'=>'Facebook',
	    'ln ln-icon-Factory-2'=>'Factory-2',
	    'ln ln-icon-Factory'=>'Factory',
	    'ln ln-icon-Fahrenheit'=>'Fahrenheit',
	    'ln ln-icon-Family-Sign'=>'Family-Sign',
	    'ln ln-icon-Fan'=>'Fan',
	    'ln ln-icon-Farmer'=>'Farmer',
	    'ln ln-icon-Fashion'=>'Fashion',
	    'ln ln-icon-Favorite-Window'=>'Favorite-Window',
	    'ln ln-icon-Fax'=>'Fax',
	    'ln ln-icon-Feather'=>'Feather',
	    'ln ln-icon-Feedburner'=>'Feedburner',
	    'ln ln-icon-Female-2'=>'Female-2',
	    'ln ln-icon-Female-Sign'=>'Female-Sign',
	    'ln ln-icon-Female'=>'Female',
	    'ln ln-icon-File-Block'=>'File-Block',
	    'ln ln-icon-File-Bookmark'=>'File-Bookmark',
	    'ln ln-icon-File-Chart'=>'File-Chart',
	    'ln ln-icon-File-Clipboard'=>'File-Clipboard',
	    'ln ln-icon-File-ClipboardFileText'=>'File-ClipboardFileText',
	    'ln ln-icon-File-ClipboardTextImage'=>'File-ClipboardTextImage',
	    'ln ln-icon-File-Cloud'=>'File-Cloud',
	    'ln ln-icon-File-Copy'=>'File-Copy',
	    'ln ln-icon-File-Copy2'=>'File-Copy2',
	    'ln ln-icon-File-CSV'=>'File-CSV',
	    'ln ln-icon-File-Download'=>'File-Download',
	    'ln ln-icon-File-Edit'=>'File-Edit',
	    'ln ln-icon-File-Excel'=>'File-Excel',
	    'ln ln-icon-File-Favorite'=>'File-Favorite',
	    'ln ln-icon-File-Fire'=>'File-Fire',
	    'ln ln-icon-File-Graph'=>'File-Graph',
	    'ln ln-icon-File-Hide'=>'File-Hide',
	    'ln ln-icon-File-Horizontal'=>'File-Horizontal',
	    'ln ln-icon-File-HorizontalText'=>'File-HorizontalText',
	    'ln ln-icon-File-HTML'=>'File-HTML',
	    'ln ln-icon-File-JPG'=>'File-JPG',
	    'ln ln-icon-File-Link'=>'File-Link',
	    'ln ln-icon-File-Loading'=>'File-Loading',
	    'ln ln-icon-File-Lock'=>'File-Lock',
	    'ln ln-icon-File-Love'=>'File-Love',
	    'ln ln-icon-File-Music'=>'File-Music',
	    'ln ln-icon-File-Network'=>'File-Network',
	    'ln ln-icon-File-Pictures'=>'File-Pictures',
	    'ln ln-icon-File-Pie'=>'File-Pie',
	    'ln ln-icon-File-Presentation'=>'File-Presentation',
	    'ln ln-icon-File-Refresh'=>'File-Refresh',
	    'ln ln-icon-File-Search'=>'File-Search',
	    'ln ln-icon-File-Settings'=>'File-Settings',
	    'ln ln-icon-File-Share'=>'File-Share',
	    'ln ln-icon-File-TextImage'=>'File-TextImage',
	    'ln ln-icon-File-Trash'=>'File-Trash',
	    'ln ln-icon-File-TXT'=>'File-TXT',
	    'ln ln-icon-File-Upload'=>'File-Upload',
	    'ln ln-icon-File-Video'=>'File-Video',
	    'ln ln-icon-File-Word'=>'File-Word',
	    'ln ln-icon-File-Zip'=>'File-Zip',
	    'ln ln-icon-File'=>'File',
	    'ln ln-icon-Files'=>'Files',
	    'ln ln-icon-Film-Board'=>'Film-Board',
	    'ln ln-icon-Film-Cartridge'=>'Film-Cartridge',
	    'ln ln-icon-Film-Strip'=>'Film-Strip',
	    'ln ln-icon-Film-Video'=>'Film-Video',
	    'ln ln-icon-Film'=>'Film',
	    'ln ln-icon-Filter-2'=>'Filter-2',
	    'ln ln-icon-Filter'=>'Filter',
	    'ln ln-icon-Financial'=>'Financial',
	    'ln ln-icon-Find-User'=>'Find-User',
	    'ln ln-icon-Finger-DragFourSides'=>'Finger-DragFourSides',
	    'ln ln-icon-Finger-DragTwoSides'=>'Finger-DragTwoSides',
	    'ln ln-icon-Finger-Print'=>'Finger-Print',
	    'ln ln-icon-Finger'=>'Finger',
	    'ln ln-icon-Fingerprint-2'=>'Fingerprint-2',
	    'ln ln-icon-Fingerprint'=>'Fingerprint',
	    'ln ln-icon-Fire-Flame'=>'Fire-Flame',
	    'ln ln-icon-Fire-Flame2'=>'Fire-Flame2',
	    'ln ln-icon-Fire-Hydrant'=>'Fire-Hydrant',
	    'ln ln-icon-Fire-Staion'=>'Fire-Staion',
	    'ln ln-icon-Firefox'=>'Firefox',
	    'ln ln-icon-Firewall'=>'Firewall',
	    'ln ln-icon-First-Aid'=>'First-Aid',
	    'ln ln-icon-First'=>'First',
	    'ln ln-icon-Fish-Food'=>'Fish-Food',
	    'ln ln-icon-Fish'=>'Fish',
	    'ln ln-icon-Fit-To'=>'Fit-To',
	    'ln ln-icon-Fit-To2'=>'Fit-To2',
	    'ln ln-icon-Five-Fingers'=>'Five-Fingers',
	    'ln ln-icon-Five-FingersDrag'=>'Five-FingersDrag',
	    'ln ln-icon-Five-FingersDrag2'=>'Five-FingersDrag2',
	    'ln ln-icon-Five-FingersTouch'=>'Five-FingersTouch',
	    'ln ln-icon-Flag-2'=>'Flag-2',
	    'ln ln-icon-Flag-3'=>'Flag-3',
	    'ln ln-icon-Flag-4'=>'Flag-4',
	    'ln ln-icon-Flag-5'=>'Flag-5',
	    'ln ln-icon-Flag-6'=>'Flag-6',
	    'ln ln-icon-Flag'=>'Flag',
	    'ln ln-icon-Flamingo'=>'Flamingo',
	    'ln ln-icon-Flash-2'=>'Flash-2',
	    'ln ln-icon-Flash-Video'=>'Flash-Video',
	    'ln ln-icon-Flash'=>'Flash',
	    'ln ln-icon-Flashlight'=>'Flashlight',
	    'ln ln-icon-Flask-2'=>'Flask-2',
	    'ln ln-icon-Flask'=>'Flask',
	    'ln ln-icon-Flick'=>'Flick',
	    'ln ln-icon-Flickr'=>'Flickr',
	    'ln ln-icon-Flowerpot'=>'Flowerpot',
	    'ln ln-icon-Fluorescent'=>'Fluorescent',
	    'ln ln-icon-Fog-Day'=>'Fog-Day',
	    'ln ln-icon-Fog-Night'=>'Fog-Night',
	    'ln ln-icon-Folder-Add'=>'Folder-Add',
	    'ln ln-icon-Folder-Archive'=>'Folder-Archive',
	    'ln ln-icon-Folder-Binder'=>'Folder-Binder',
	    'ln ln-icon-Folder-Binder2'=>'Folder-Binder2',
	    'ln ln-icon-Folder-Block'=>'Folder-Block',
	    'ln ln-icon-Folder-Bookmark'=>'Folder-Bookmark',
	    'ln ln-icon-Folder-Close'=>'Folder-Close',
	    'ln ln-icon-Folder-Cloud'=>'Folder-Cloud',
	    'ln ln-icon-Folder-Delete'=>'Folder-Delete',
	    'ln ln-icon-Folder-Download'=>'Folder-Download',
	    'ln ln-icon-Folder-Edit'=>'Folder-Edit',
	    'ln ln-icon-Folder-Favorite'=>'Folder-Favorite',
	    'ln ln-icon-Folder-Fire'=>'Folder-Fire',
	    'ln ln-icon-Folder-Hide'=>'Folder-Hide',
	    'ln ln-icon-Folder-Link'=>'Folder-Link',
	    'ln ln-icon-Folder-Loading'=>'Folder-Loading',
	    'ln ln-icon-Folder-Lock'=>'Folder-Lock',
	    'ln ln-icon-Folder-Love'=>'Folder-Love',
	    'ln ln-icon-Folder-Music'=>'Folder-Music',
	    'ln ln-icon-Folder-Network'=>'Folder-Network',
	    'ln ln-icon-Folder-Open'=>'Folder-Open',
	    'ln ln-icon-Folder-Open2'=>'Folder-Open2',
	    'ln ln-icon-Folder-Organizing'=>'Folder-Organizing',
	    'ln ln-icon-Folder-Pictures'=>'Folder-Pictures',
	    'ln ln-icon-Folder-Refresh'=>'Folder-Refresh',
	    'ln ln-icon-Folder-Remove-'=>'Folder-Remove-',
	    'ln ln-icon-Folder-Search'=>'Folder-Search',
	    'ln ln-icon-Folder-Settings'=>'Folder-Settings',
	    'ln ln-icon-Folder-Share'=>'Folder-Share',
	    'ln ln-icon-Folder-Trash'=>'Folder-Trash',
	    'ln ln-icon-Folder-Upload'=>'Folder-Upload',
	    'ln ln-icon-Folder-Video'=>'Folder-Video',
	    'ln ln-icon-Folder-WithDocument'=>'Folder-WithDocument',
	    'ln ln-icon-Folder-Zip'=>'Folder-Zip',
	    'ln ln-icon-Folder'=>'Folder',
	    'ln ln-icon-Folders'=>'Folders',
	    'ln ln-icon-Font-Color'=>'Font-Color',
	    'ln ln-icon-Font-Name'=>'Font-Name',
	    'ln ln-icon-Font-Size'=>'Font-Size',
	    'ln ln-icon-Font-Style'=>'Font-Style',
	    'ln ln-icon-Font-StyleSubscript'=>'Font-StyleSubscript',
	    'ln ln-icon-Font-StyleSuperscript'=>'Font-StyleSuperscript',
	    'ln ln-icon-Font-Window'=>'Font-Window',
	    'ln ln-icon-Foot-2'=>'Foot-2',
	    'ln ln-icon-Foot'=>'Foot',
	    'ln ln-icon-Football-2'=>'Football-2',
	    'ln ln-icon-Football'=>'Football',
	    'ln ln-icon-Footprint-2'=>'Footprint-2',
	    'ln ln-icon-Footprint-3'=>'Footprint-3',
	    'ln ln-icon-Footprint'=>'Footprint',
	    'ln ln-icon-Forest'=>'Forest',
	    'ln ln-icon-Fork'=>'Fork',
	    'ln ln-icon-Formspring'=>'Formspring',
	    'ln ln-icon-Formula'=>'Formula',
	    'ln ln-icon-Forsquare'=>'Forsquare',
	    'ln ln-icon-Forward'=>'Forward',
	    'ln ln-icon-Fountain-Pen'=>'Fountain-Pen',
	    'ln ln-icon-Four-Fingers'=>'Four-Fingers',
	    'ln ln-icon-Four-FingersDrag'=>'Four-FingersDrag',
	    'ln ln-icon-Four-FingersDrag2'=>'Four-FingersDrag2',
	    'ln ln-icon-Four-FingersTouch'=>'Four-FingersTouch',
	    'ln ln-icon-Fox'=>'Fox',
	    'ln ln-icon-Frankenstein'=>'Frankenstein',
	    'ln ln-icon-French-Fries'=>'French-Fries',
	    'ln ln-icon-Friendfeed'=>'Friendfeed',
	    'ln ln-icon-Friendster'=>'Friendster',
	    'ln ln-icon-Frog'=>'Frog',
	    'ln ln-icon-Fruits'=>'Fruits',
	    'ln ln-icon-Fuel'=>'Fuel',
	    'ln ln-icon-Full-Bag'=>'Full-Bag',
	    'ln ln-icon-Full-Basket'=>'Full-Basket',
	    'ln ln-icon-Full-Cart'=>'Full-Cart',
	    'ln ln-icon-Full-Moon'=>'Full-Moon',
	    'ln ln-icon-Full-Screen'=>'Full-Screen',
	    'ln ln-icon-Full-Screen2'=>'Full-Screen2',
	    'ln ln-icon-Full-View'=>'Full-View',
	    'ln ln-icon-Full-View2'=>'Full-View2',
	    'ln ln-icon-Full-ViewWindow'=>'Full-ViewWindow',
	    'ln ln-icon-Function'=>'Function',
	    'ln ln-icon-Funky'=>'Funky',
	    'ln ln-icon-Funny-Bicycle'=>'Funny-Bicycle',
	    'ln ln-icon-Furl'=>'Furl',
	    'ln ln-icon-Gamepad-2'=>'Gamepad-2',
	    'ln ln-icon-Gamepad'=>'Gamepad',
	    'ln ln-icon-Gas-Pump'=>'Gas-Pump',
	    'ln ln-icon-Gaugage-2'=>'Gaugage-2',
	    'ln ln-icon-Gaugage'=>'Gaugage',
	    'ln ln-icon-Gay'=>'Gay',
	    'ln ln-icon-Gear-2'=>'Gear-2',
	    'ln ln-icon-Gear'=>'Gear',
	    'ln ln-icon-Gears-2'=>'Gears-2',
	    'ln ln-icon-Gears'=>'Gears',
	    'ln ln-icon-Geek-2'=>'Geek-2',
	    'ln ln-icon-Geek'=>'Geek',
	    'ln ln-icon-Gemini-2'=>'Gemini-2',
	    'ln ln-icon-Gemini'=>'Gemini',
	    'ln ln-icon-Genius'=>'Genius',
	    'ln ln-icon-Gentleman'=>'Gentleman',
	    'ln ln-icon-Geo--'=>'Geo--',
	    'ln ln-icon-Geo-'=>'Geo-',
	    'ln ln-icon-Geo-Close'=>'Geo-Close',
	    'ln ln-icon-Geo-Love'=>'Geo-Love',
	    'ln ln-icon-Geo-Number'=>'Geo-Number',
	    'ln ln-icon-Geo-Star'=>'Geo-Star',
	    'ln ln-icon-Geo'=>'Geo',
	    'ln ln-icon-Geo2--'=>'Geo2--',
	    'ln ln-icon-Geo2-'=>'Geo2-',
	    'ln ln-icon-Geo2-Close'=>'Geo2-Close',
	    'ln ln-icon-Geo2-Love'=>'Geo2-Love',
	    'ln ln-icon-Geo2-Number'=>'Geo2-Number',
	    'ln ln-icon-Geo2-Star'=>'Geo2-Star',
	    'ln ln-icon-Geo2'=>'Geo2',
	    'ln ln-icon-Geo3--'=>'Geo3--',
	    'ln ln-icon-Geo3-'=>'Geo3-',
	    'ln ln-icon-Geo3-Close'=>'Geo3-Close',
	    'ln ln-icon-Geo3-Love'=>'Geo3-Love',
	    'ln ln-icon-Geo3-Number'=>'Geo3-Number',
	    'ln ln-icon-Geo3-Star'=>'Geo3-Star',
	    'ln ln-icon-Geo3'=>'Geo3',
	    'ln ln-icon-Gey'=>'Gey',
	    'ln ln-icon-Gift-Box'=>'Gift-Box',
	    'ln ln-icon-Giraffe'=>'Giraffe',
	    'ln ln-icon-Girl'=>'Girl',
	    'ln ln-icon-Glass-Water'=>'Glass-Water',
	    'ln ln-icon-Glasses-2'=>'Glasses-2',
	    'ln ln-icon-Glasses-3'=>'Glasses-3',
	    'ln ln-icon-Glasses'=>'Glasses',
	    'ln ln-icon-Global-Position'=>'Global-Position',
	    'ln ln-icon-Globe-2'=>'Globe-2',
	    'ln ln-icon-Globe'=>'Globe',
	    'ln ln-icon-Gloves'=>'Gloves',
	    'ln ln-icon-Go-Bottom'=>'Go-Bottom',
	    'ln ln-icon-Go-Top'=>'Go-Top',
	    'ln ln-icon-Goggles'=>'Goggles',
	    'ln ln-icon-Golf-2'=>'Golf-2',
	    'ln ln-icon-Golf'=>'Golf',
	    'ln ln-icon-Google-Buzz'=>'Google-Buzz',
	    'ln ln-icon-Google-Drive'=>'Google-Drive',
	    'ln ln-icon-Google-Play'=>'Google-Play',
	    'ln ln-icon-Google-Plus'=>'Google-Plus',
	    'ln ln-icon-Google'=>'Google',
	    'ln ln-icon-Gopro'=>'Gopro',
	    'ln ln-icon-Gorilla'=>'Gorilla',
	    'ln ln-icon-Gowalla'=>'Gowalla',
	    'ln ln-icon-Grave'=>'Grave',
	    'ln ln-icon-Graveyard'=>'Graveyard',
	    'ln ln-icon-Greece'=>'Greece',
	    'ln ln-icon-Green-Energy'=>'Green-Energy',
	    'ln ln-icon-Green-House'=>'Green-House',
	    'ln ln-icon-Guitar'=>'Guitar',
	    'ln ln-icon-Gun-2'=>'Gun-2',
	    'ln ln-icon-Gun-3'=>'Gun-3',
	    'ln ln-icon-Gun'=>'Gun',
	    'ln ln-icon-Gymnastics'=>'Gymnastics',
	    'ln ln-icon-Hair-2'=>'Hair-2',
	    'ln ln-icon-Hair-3'=>'Hair-3',
	    'ln ln-icon-Hair-4'=>'Hair-4',
	    'ln ln-icon-Hair'=>'Hair',
	    'ln ln-icon-Half-Moon'=>'Half-Moon',
	    'ln ln-icon-Halloween-HalfMoon'=>'Halloween-HalfMoon',
	    'ln ln-icon-Halloween-Moon'=>'Halloween-Moon',
	    'ln ln-icon-Hamburger'=>'Hamburger',
	    'ln ln-icon-Hammer'=>'Hammer',
	    'ln ln-icon-Hand-Touch'=>'Hand-Touch',
	    'ln ln-icon-Hand-Touch2'=>'Hand-Touch2',
	    'ln ln-icon-Hand-TouchSmartphone'=>'Hand-TouchSmartphone',
	    'ln ln-icon-Hand'=>'Hand',
	    'ln ln-icon-Hands'=>'Hands',
	    'ln ln-icon-Handshake'=>'Handshake',
	    'ln ln-icon-Hanger'=>'Hanger',
	    'ln ln-icon-Happy'=>'Happy',
	    'ln ln-icon-Hat-2'=>'Hat-2',
	    'ln ln-icon-Hat'=>'Hat',
	    'ln ln-icon-Haunted-House'=>'Haunted-House',
	    'ln ln-icon-HD-Video'=>'HD-Video',
	    'ln ln-icon-HD'=>'HD',
	    'ln ln-icon-HDD'=>'HDD',
	    'ln ln-icon-Headphone'=>'Headphone',
	    'ln ln-icon-Headphones'=>'Headphones',
	    'ln ln-icon-Headset'=>'Headset',
	    'ln ln-icon-Heart-2'=>'Heart-2',
	    'ln ln-icon-Heart'=>'Heart',
	    'ln ln-icon-Heels-2'=>'Heels-2',
	    'ln ln-icon-Heels'=>'Heels',
	    'ln ln-icon-Height-Window'=>'Height-Window',
	    'ln ln-icon-Helicopter-2'=>'Helicopter-2',
	    'ln ln-icon-Helicopter'=>'Helicopter',
	    'ln ln-icon-Helix-2'=>'Helix-2',
	    'ln ln-icon-Hello'=>'Hello',
	    'ln ln-icon-Helmet-2'=>'Helmet-2',
	    'ln ln-icon-Helmet-3'=>'Helmet-3',
	    'ln ln-icon-Helmet'=>'Helmet',
	    'ln ln-icon-Hipo'=>'Hipo',
	    'ln ln-icon-Hipster-Glasses'=>'Hipster-Glasses',
	    'ln ln-icon-Hipster-Glasses2'=>'Hipster-Glasses2',
	    'ln ln-icon-Hipster-Glasses3'=>'Hipster-Glasses3',
	    'ln ln-icon-Hipster-Headphones'=>'Hipster-Headphones',
	    'ln ln-icon-Hipster-Men'=>'Hipster-Men',
	    'ln ln-icon-Hipster-Men2'=>'Hipster-Men2',
	    'ln ln-icon-Hipster-Men3'=>'Hipster-Men3',
	    'ln ln-icon-Hipster-Sunglasses'=>'Hipster-Sunglasses',
	    'ln ln-icon-Hipster-Sunglasses2'=>'Hipster-Sunglasses2',
	    'ln ln-icon-Hipster-Sunglasses3'=>'Hipster-Sunglasses3',
	    'ln ln-icon-Hokey'=>'Hokey',
	    'ln ln-icon-Holly'=>'Holly',
	    'ln ln-icon-Home-2'=>'Home-2',
	    'ln ln-icon-Home-3'=>'Home-3',
	    'ln ln-icon-Home-4'=>'Home-4',
	    'ln ln-icon-Home-5'=>'Home-5',
	    'ln ln-icon-Home-Window'=>'Home-Window',
	    'ln ln-icon-Home'=>'Home',
	    'ln ln-icon-Homosexual'=>'Homosexual',
	    'ln ln-icon-Honey'=>'Honey',
	    'ln ln-icon-Hong-Kong'=>'Hong-Kong',
	    'ln ln-icon-Hoodie'=>'Hoodie',
	    'ln ln-icon-Horror'=>'Horror',
	    'ln ln-icon-Horse'=>'Horse',
	    'ln ln-icon-Hospital-2'=>'Hospital-2',
	    'ln ln-icon-Hospital'=>'Hospital',
	    'ln ln-icon-Host'=>'Host',
	    'ln ln-icon-Hot-Dog'=>'Hot-Dog',
	    'ln ln-icon-Hotel'=>'Hotel',
	    'ln ln-icon-Hour'=>'Hour',
	    'ln ln-icon-Hub'=>'Hub',
	    'ln ln-icon-Humor'=>'Humor',
	    'ln ln-icon-Hurt'=>'Hurt',
	    'ln ln-icon-Ice-Cream'=>'Ice-Cream',
	    'ln ln-icon-ICQ'=>'ICQ',
	    'ln ln-icon-ID-2'=>'ID-2',
	    'ln ln-icon-ID-3'=>'ID-3',
	    'ln ln-icon-ID-Card'=>'ID-Card',
	    'ln ln-icon-Idea-2'=>'Idea-2',
	    'ln ln-icon-Idea-3'=>'Idea-3',
	    'ln ln-icon-Idea-4'=>'Idea-4',
	    'ln ln-icon-Idea-5'=>'Idea-5',
	    'ln ln-icon-Idea'=>'Idea',
	    'ln ln-icon-Identification-Badge'=>'Identification-Badge',
	    'ln ln-icon-ImDB'=>'ImDB',
	    'ln ln-icon-Inbox-Empty'=>'Inbox-Empty',
	    'ln ln-icon-Inbox-Forward'=>'Inbox-Forward',
	    'ln ln-icon-Inbox-Full'=>'Inbox-Full',
	    'ln ln-icon-Inbox-Into'=>'Inbox-Into',
	    'ln ln-icon-Inbox-Out'=>'Inbox-Out',
	    'ln ln-icon-Inbox-Reply'=>'Inbox-Reply',
	    'ln ln-icon-Inbox'=>'Inbox',
	    'ln ln-icon-Increase-Inedit'=>'Increase-Inedit',
	    'ln ln-icon-Indent-FirstLine'=>'Indent-FirstLine',
	    'ln ln-icon-Indent-LeftMargin'=>'Indent-LeftMargin',
	    'ln ln-icon-Indent-RightMargin'=>'Indent-RightMargin',
	    'ln ln-icon-India'=>'India',
	    'ln ln-icon-Info-Window'=>'Info-Window',
	    'ln ln-icon-Information'=>'Information',
	    'ln ln-icon-Inifity'=>'Inifity',
	    'ln ln-icon-Instagram'=>'Instagram',
	    'ln ln-icon-Internet-2'=>'Internet-2',
	    'ln ln-icon-Internet-Explorer'=>'Internet-Explorer',
	    'ln ln-icon-Internet-Smiley'=>'Internet-Smiley',
	    'ln ln-icon-Internet'=>'Internet',
	    'ln ln-icon-iOS-Apple'=>'iOS-Apple',
	    'ln ln-icon-Israel'=>'Israel',
	    'ln ln-icon-Italic-Text'=>'Italic-Text',
	    'ln ln-icon-Jacket-2'=>'Jacket-2',
	    'ln ln-icon-Jacket'=>'Jacket',
	    'ln ln-icon-Jamaica'=>'Jamaica',
	    'ln ln-icon-Japan'=>'Japan',
	    'ln ln-icon-Japanese-Gate'=>'Japanese-Gate',
	    'ln ln-icon-Jeans'=>'Jeans',
	    'ln ln-icon-Jeep-2'=>'Jeep-2',
	    'ln ln-icon-Jeep'=>'Jeep',
	    'ln ln-icon-Jet'=>'Jet',
	    'ln ln-icon-Joystick'=>'Joystick',
	    'ln ln-icon-Juice'=>'Juice',
	    'ln ln-icon-Jump-Rope'=>'Jump-Rope',
	    'ln ln-icon-Kangoroo'=>'Kangoroo',
	    'ln ln-icon-Kenya'=>'Kenya',
	    'ln ln-icon-Key-2'=>'Key-2',
	    'ln ln-icon-Key-3'=>'Key-3',
	    'ln ln-icon-Key-Lock'=>'Key-Lock',
	    'ln ln-icon-Key'=>'Key',
	    'ln ln-icon-Keyboard'=>'Keyboard',
	    'ln ln-icon-Keyboard3'=>'Keyboard3',
	    'ln ln-icon-Keypad'=>'Keypad',
	    'ln ln-icon-King-2'=>'King-2',
	    'ln ln-icon-King'=>'King',
	    'ln ln-icon-Kiss'=>'Kiss',
	    'ln ln-icon-Knee'=>'Knee',
	    'ln ln-icon-Knife-2'=>'Knife-2',
	    'ln ln-icon-Knife'=>'Knife',
	    'ln ln-icon-Knight'=>'Knight',
	    'ln ln-icon-Koala'=>'Koala',
	    'ln ln-icon-Korea'=>'Korea',
	    'ln ln-icon-Lamp'=>'Lamp',
	    'ln ln-icon-Landscape-2'=>'Landscape-2',
	    'ln ln-icon-Landscape'=>'Landscape',
	    'ln ln-icon-Lantern'=>'Lantern',
	    'ln ln-icon-Laptop-2'=>'Laptop-2',
	    'ln ln-icon-Laptop-3'=>'Laptop-3',
	    'ln ln-icon-Laptop-Phone'=>'Laptop-Phone',
	    'ln ln-icon-Laptop-Secure'=>'Laptop-Secure',
	    'ln ln-icon-Laptop-Tablet'=>'Laptop-Tablet',
	    'ln ln-icon-Laptop'=>'Laptop',
	    'ln ln-icon-Laser'=>'Laser',
	    'ln ln-icon-Last-FM'=>'Last-FM',
	    'ln ln-icon-Last'=>'Last',
	    'ln ln-icon-Laughing'=>'Laughing',
	    'ln ln-icon-Layer-1635'=>'Layer-1635',
	    'ln ln-icon-Layer-1646'=>'Layer-1646',
	    'ln ln-icon-Layer-Backward'=>'Layer-Backward',
	    'ln ln-icon-Layer-Forward'=>'Layer-Forward',
	    'ln ln-icon-Leafs-2'=>'Leafs-2',
	    'ln ln-icon-Leafs'=>'Leafs',
	    'ln ln-icon-Leaning-Tower'=>'Leaning-Tower',
	    'ln ln-icon-Left--Right'=>'Left--Right',
	    'ln ln-icon-Left--Right3'=>'Left--Right3',
	    'ln ln-icon-Left-2'=>'Left-2',
	    'ln ln-icon-Left-3'=>'Left-3',
	    'ln ln-icon-Left-4'=>'Left-4',
	    'ln ln-icon-Left-ToRight'=>'Left-ToRight',
	    'ln ln-icon-Left'=>'Left',
	    'ln ln-icon-Leg-2'=>'Leg-2',
	    'ln ln-icon-Leg'=>'Leg',
	    'ln ln-icon-Lego'=>'Lego',
	    'ln ln-icon-Lemon'=>'Lemon',
	    'ln ln-icon-Len-2'=>'Len-2',
	    'ln ln-icon-Len-3'=>'Len-3',
	    'ln ln-icon-Len'=>'Len',
	    'ln ln-icon-Leo-2'=>'Leo-2',
	    'ln ln-icon-Leo'=>'Leo',
	    'ln ln-icon-Leopard'=>'Leopard',
	    'ln ln-icon-Lesbian'=>'Lesbian',
	    'ln ln-icon-Lesbians'=>'Lesbians',
	    'ln ln-icon-Letter-Close'=>'Letter-Close',
	    'ln ln-icon-Letter-Open'=>'Letter-Open',
	    'ln ln-icon-Letter-Sent'=>'Letter-Sent',
	    'ln ln-icon-Libra-2'=>'Libra-2',
	    'ln ln-icon-Libra'=>'Libra',
	    'ln ln-icon-Library-2'=>'Library-2',
	    'ln ln-icon-Library'=>'Library',
	    'ln ln-icon-Life-Jacket'=>'Life-Jacket',
	    'ln ln-icon-Life-Safer'=>'Life-Safer',
	    'ln ln-icon-Light-Bulb'=>'Light-Bulb',
	    'ln ln-icon-Light-Bulb2'=>'Light-Bulb2',
	    'ln ln-icon-Light-BulbLeaf'=>'Light-BulbLeaf',
	    'ln ln-icon-Lighthouse'=>'Lighthouse',
	    'ln ln-icon-Like-2'=>'Like-2',
	    'ln ln-icon-Like'=>'Like',
	    'ln ln-icon-Line-Chart'=>'Line-Chart',
	    'ln ln-icon-Line-Chart2'=>'Line-Chart2',
	    'ln ln-icon-Line-Chart3'=>'Line-Chart3',
	    'ln ln-icon-Line-Chart4'=>'Line-Chart4',
	    'ln ln-icon-Line-Spacing'=>'Line-Spacing',
	    'ln ln-icon-Line-SpacingText'=>'Line-SpacingText',
	    'ln ln-icon-Link-2'=>'Link-2',
	    'ln ln-icon-Link'=>'Link',
	    'ln ln-icon-Linkedin-2'=>'Linkedin-2',
	    'ln ln-icon-Linkedin'=>'Linkedin',
	    'ln ln-icon-Linux'=>'Linux',
	    'ln ln-icon-Lion'=>'Lion',
	    'ln ln-icon-Livejournal'=>'Livejournal',
	    'ln ln-icon-Loading-2'=>'Loading-2',
	    'ln ln-icon-Loading-3'=>'Loading-3',
	    'ln ln-icon-Loading-Window'=>'Loading-Window',
	    'ln ln-icon-Loading'=>'Loading',
	    'ln ln-icon-Location-2'=>'Location-2',
	    'ln ln-icon-Location'=>'Location',
	    'ln ln-icon-Lock-2'=>'Lock-2',
	    'ln ln-icon-Lock-3'=>'Lock-3',
	    'ln ln-icon-Lock-User'=>'Lock-User',
	    'ln ln-icon-Lock-Window'=>'Lock-Window',
	    'ln ln-icon-Lock'=>'Lock',
	    'ln ln-icon-Lollipop-2'=>'Lollipop-2',
	    'ln ln-icon-Lollipop-3'=>'Lollipop-3',
	    'ln ln-icon-Lollipop'=>'Lollipop',
	    'ln ln-icon-Loop'=>'Loop',
	    'ln ln-icon-Loud'=>'Loud',
	    'ln ln-icon-Loudspeaker'=>'Loudspeaker',
	    'ln ln-icon-Love-2'=>'Love-2',
	    'ln ln-icon-Love-User'=>'Love-User',
	    'ln ln-icon-Love-Window'=>'Love-Window',
	    'ln ln-icon-Love'=>'Love',
	    'ln ln-icon-Lowercase-Text'=>'Lowercase-Text',
	    'ln ln-icon-Luggafe-Front'=>'Luggafe-Front',
	    'ln ln-icon-Luggage-2'=>'Luggage-2',
	    'ln ln-icon-Macro'=>'Macro',
	    'ln ln-icon-Magic-Wand'=>'Magic-Wand',
	    'ln ln-icon-Magnet'=>'Magnet',
	    'ln ln-icon-Magnifi-Glass-'=>'Magnifi-Glass-',
	    'ln ln-icon-Magnifi-Glass'=>'Magnifi-Glass',
	    'ln ln-icon-Magnifi-Glass2'=>'Magnifi-Glass2',
	    'ln ln-icon-Mail-2'=>'Mail-2',
	    'ln ln-icon-Mail-3'=>'Mail-3',
	    'ln ln-icon-Mail-Add'=>'Mail-Add',
	    'ln ln-icon-Mail-Attachement'=>'Mail-Attachement',
	    'ln ln-icon-Mail-Block'=>'Mail-Block',
	    'ln ln-icon-Mail-Delete'=>'Mail-Delete',
	    'ln ln-icon-Mail-Favorite'=>'Mail-Favorite',
	    'ln ln-icon-Mail-Forward'=>'Mail-Forward',
	    'ln ln-icon-Mail-Gallery'=>'Mail-Gallery',
	    'ln ln-icon-Mail-Inbox'=>'Mail-Inbox',
	    'ln ln-icon-Mail-Link'=>'Mail-Link',
	    'ln ln-icon-Mail-Lock'=>'Mail-Lock',
	    'ln ln-icon-Mail-Love'=>'Mail-Love',
	    'ln ln-icon-Mail-Money'=>'Mail-Money',
	    'ln ln-icon-Mail-Open'=>'Mail-Open',
	    'ln ln-icon-Mail-Outbox'=>'Mail-Outbox',
	    'ln ln-icon-Mail-Password'=>'Mail-Password',
	    'ln ln-icon-Mail-Photo'=>'Mail-Photo',
	    'ln ln-icon-Mail-Read'=>'Mail-Read',
	    'ln ln-icon-Mail-Removex'=>'Mail-Removex',
	    'ln ln-icon-Mail-Reply'=>'Mail-Reply',
	    'ln ln-icon-Mail-ReplyAll'=>'Mail-ReplyAll',
	    'ln ln-icon-Mail-Search'=>'Mail-Search',
	    'ln ln-icon-Mail-Send'=>'Mail-Send',
	    'ln ln-icon-Mail-Settings'=>'Mail-Settings',
	    'ln ln-icon-Mail-Unread'=>'Mail-Unread',
	    'ln ln-icon-Mail-Video'=>'Mail-Video',
	    'ln ln-icon-Mail-withAtSign'=>'Mail-withAtSign',
	    'ln ln-icon-Mail-WithCursors'=>'Mail-WithCursors',
	    'ln ln-icon-Mail'=>'Mail',
	    'ln ln-icon-Mailbox-Empty'=>'Mailbox-Empty',
	    'ln ln-icon-Mailbox-Full'=>'Mailbox-Full',
	    'ln ln-icon-Male-2'=>'Male-2',
	    'ln ln-icon-Male-Sign'=>'Male-Sign',
	    'ln ln-icon-Male'=>'Male',
	    'ln ln-icon-MaleFemale'=>'MaleFemale',
	    'ln ln-icon-Man-Sign'=>'Man-Sign',
	    'ln ln-icon-Management'=>'Management',
	    'ln ln-icon-Mans-Underwear'=>'Mans-Underwear',
	    'ln ln-icon-Mans-Underwear2'=>'Mans-Underwear2',
	    'ln ln-icon-Map-Marker'=>'Map-Marker',
	    'ln ln-icon-Map-Marker2'=>'Map-Marker2',
	    'ln ln-icon-Map-Marker3'=>'Map-Marker3',
	    'ln ln-icon-Map'=>'Map',
	    'ln ln-icon-Map2'=>'Map2',
	    'ln ln-icon-Marker-2'=>'Marker-2',
	    'ln ln-icon-Marker-3'=>'Marker-3',
	    'ln ln-icon-Marker'=>'Marker',
	    'ln ln-icon-Martini-Glass'=>'Martini-Glass',
	    'ln ln-icon-Mask'=>'Mask',
	    'ln ln-icon-Master-Card'=>'Master-Card',
	    'ln ln-icon-Maximize-Window'=>'Maximize-Window',
	    'ln ln-icon-Maximize'=>'Maximize',
	    'ln ln-icon-Medal-2'=>'Medal-2',
	    'ln ln-icon-Medal-3'=>'Medal-3',
	    'ln ln-icon-Medal'=>'Medal',
	    'ln ln-icon-Medical-Sign'=>'Medical-Sign',
	    'ln ln-icon-Medicine-2'=>'Medicine-2',
	    'ln ln-icon-Medicine-3'=>'Medicine-3',
	    'ln ln-icon-Medicine'=>'Medicine',
	    'ln ln-icon-Megaphone'=>'Megaphone',
	    'ln ln-icon-Memory-Card'=>'Memory-Card',
	    'ln ln-icon-Memory-Card2'=>'Memory-Card2',
	    'ln ln-icon-Memory-Card3'=>'Memory-Card3',
	    'ln ln-icon-Men'=>'Men',
	    'ln ln-icon-Menorah'=>'Menorah',
	    'ln ln-icon-Mens'=>'Mens',
	    'ln ln-icon-Metacafe'=>'Metacafe',
	    'ln ln-icon-Mexico'=>'Mexico',
	    'ln ln-icon-Mic'=>'Mic',
	    'ln ln-icon-Microphone-2'=>'Microphone-2',
	    'ln ln-icon-Microphone-3'=>'Microphone-3',
	    'ln ln-icon-Microphone-4'=>'Microphone-4',
	    'ln ln-icon-Microphone-5'=>'Microphone-5',
	    'ln ln-icon-Microphone-6'=>'Microphone-6',
	    'ln ln-icon-Microphone-7'=>'Microphone-7',
	    'ln ln-icon-Microphone'=>'Microphone',
	    'ln ln-icon-Microscope'=>'Microscope',
	    'ln ln-icon-Milk-Bottle'=>'Milk-Bottle',
	    'ln ln-icon-Mine'=>'Mine',
	    'ln ln-icon-Minimize-Maximize-Close-Window'=>'Minimize-Maximize-Close-Window',
	    'ln ln-icon-Minimize-Window'=>'Minimize-Window',
	    'ln ln-icon-Minimize'=>'Minimize',
	    'ln ln-icon-Mirror'=>'Mirror',
	    'ln ln-icon-Mixer'=>'Mixer',
	    'ln ln-icon-Mixx'=>'Mixx',
	    'ln ln-icon-Money-2'=>'Money-2',
	    'ln ln-icon-Money-Bag'=>'Money-Bag',
	    'ln ln-icon-Money-Smiley'=>'Money-Smiley',
	    'ln ln-icon-Money'=>'Money',
	    'ln ln-icon-Monitor-2'=>'Monitor-2',
	    'ln ln-icon-Monitor-3'=>'Monitor-3',
	    'ln ln-icon-Monitor-4'=>'Monitor-4',
	    'ln ln-icon-Monitor-5'=>'Monitor-5',
	    'ln ln-icon-Monitor-Analytics'=>'Monitor-Analytics',
	    'ln ln-icon-Monitor-Laptop'=>'Monitor-Laptop',
	    'ln ln-icon-Monitor-phone'=>'Monitor-phone',
	    'ln ln-icon-Monitor-Tablet'=>'Monitor-Tablet',
	    'ln ln-icon-Monitor-Vertical'=>'Monitor-Vertical',
	    'ln ln-icon-Monitor'=>'Monitor',
	    'ln ln-icon-Monitoring'=>'Monitoring',
	    'ln ln-icon-Monkey'=>'Monkey',
	    'ln ln-icon-Monster'=>'Monster',
	    'ln ln-icon-Morocco'=>'Morocco',
	    'ln ln-icon-Motorcycle'=>'Motorcycle',
	    'ln ln-icon-Mouse-2'=>'Mouse-2',
	    'ln ln-icon-Mouse-3'=>'Mouse-3',
	    'ln ln-icon-Mouse-4'=>'Mouse-4',
	    'ln ln-icon-Mouse-Pointer'=>'Mouse-Pointer',
	    'ln ln-icon-Mouse'=>'Mouse',
	    'ln ln-icon-Moustache-Smiley'=>'Moustache-Smiley',
	    'ln ln-icon-Movie-Ticket'=>'Movie-Ticket',
	    'ln ln-icon-Movie'=>'Movie',
	    'ln ln-icon-Mp3-File'=>'Mp3-File',
	    'ln ln-icon-Museum'=>'Museum',
	    'ln ln-icon-Mushroom'=>'Mushroom',
	    'ln ln-icon-Music-Note'=>'Music-Note',
	    'ln ln-icon-Music-Note2'=>'Music-Note2',
	    'ln ln-icon-Music-Note3'=>'Music-Note3',
	    'ln ln-icon-Music-Note4'=>'Music-Note4',
	    'ln ln-icon-Music-Player'=>'Music-Player',
	    'ln ln-icon-Mustache-2'=>'Mustache-2',
	    'ln ln-icon-Mustache-3'=>'Mustache-3',
	    'ln ln-icon-Mustache-4'=>'Mustache-4',
	    'ln ln-icon-Mustache-5'=>'Mustache-5',
	    'ln ln-icon-Mustache-6'=>'Mustache-6',
	    'ln ln-icon-Mustache-7'=>'Mustache-7',
	    'ln ln-icon-Mustache-8'=>'Mustache-8',
	    'ln ln-icon-Mustache'=>'Mustache',
	    'ln ln-icon-Mute'=>'Mute',
	    'ln ln-icon-Myspace'=>'Myspace',
	    'ln ln-icon-Navigat-Start'=>'Navigat-Start',
	    'ln ln-icon-Navigate-End'=>'Navigate-End',
	    'ln ln-icon-Navigation-LeftWindow'=>'Navigation-LeftWindow',
	    'ln ln-icon-Navigation-RightWindow'=>'Navigation-RightWindow',
	    'ln ln-icon-Nepal'=>'Nepal',
	    'ln ln-icon-Netscape'=>'Netscape',
	    'ln ln-icon-Network-Window'=>'Network-Window',
	    'ln ln-icon-Network'=>'Network',
	    'ln ln-icon-Neutron'=>'Neutron',
	    'ln ln-icon-New-Mail'=>'New-Mail',
	    'ln ln-icon-New-Tab'=>'New-Tab',
	    'ln ln-icon-Newspaper-2'=>'Newspaper-2',
	    'ln ln-icon-Newspaper'=>'Newspaper',
	    'ln ln-icon-Newsvine'=>'Newsvine',
	    'ln ln-icon-Next2'=>'Next2',
	    'ln ln-icon-Next-3'=>'Next-3',
	    'ln ln-icon-Next-Music'=>'Next-Music',
	    'ln ln-icon-Next'=>'Next',
	    'ln ln-icon-No-Battery'=>'No-Battery',
	    'ln ln-icon-No-Drop'=>'No-Drop',
	    'ln ln-icon-No-Flash'=>'No-Flash',
	    'ln ln-icon-No-Smoking'=>'No-Smoking',
	    'ln ln-icon-Noose'=>'Noose',
	    'ln ln-icon-Normal-Text'=>'Normal-Text',
	    'ln ln-icon-Note'=>'Note',
	    'ln ln-icon-Notepad-2'=>'Notepad-2',
	    'ln ln-icon-Notepad'=>'Notepad',
	    'ln ln-icon-Nuclear'=>'Nuclear',
	    'ln ln-icon-Numbering-List'=>'Numbering-List',
	    'ln ln-icon-Nurse'=>'Nurse',
	    'ln ln-icon-Office-Lamp'=>'Office-Lamp',
	    'ln ln-icon-Office'=>'Office',
	    'ln ln-icon-Oil'=>'Oil',
	    'ln ln-icon-Old-Camera'=>'Old-Camera',
	    'ln ln-icon-Old-Cassette'=>'Old-Cassette',
	    'ln ln-icon-Old-Clock'=>'Old-Clock',
	    'ln ln-icon-Old-Radio'=>'Old-Radio',
	    'ln ln-icon-Old-Sticky'=>'Old-Sticky',
	    'ln ln-icon-Old-Sticky2'=>'Old-Sticky2',
	    'ln ln-icon-Old-Telephone'=>'Old-Telephone',
	    'ln ln-icon-Old-TV'=>'Old-TV',
	    'ln ln-icon-On-Air'=>'On-Air',
	    'ln ln-icon-On-Off-2'=>'On-Off-2',
	    'ln ln-icon-On-Off-3'=>'On-Off-3',
	    'ln ln-icon-On-off'=>'On-off',
	    'ln ln-icon-One-Finger'=>'One-Finger',
	    'ln ln-icon-One-FingerTouch'=>'One-FingerTouch',
	    'ln ln-icon-One-Window'=>'One-Window',
	    'ln ln-icon-Open-Banana'=>'Open-Banana',
	    'ln ln-icon-Open-Book'=>'Open-Book',
	    'ln ln-icon-Opera-House'=>'Opera-House',
	    'ln ln-icon-Opera'=>'Opera',
	    'ln ln-icon-Optimization'=>'Optimization',
	    'ln ln-icon-Orientation-2'=>'Orientation-2',
	    'ln ln-icon-Orientation-3'=>'Orientation-3',
	    'ln ln-icon-Orientation'=>'Orientation',
	    'ln ln-icon-Orkut'=>'Orkut',
	    'ln ln-icon-Ornament'=>'Ornament',
	    'ln ln-icon-Over-Time'=>'Over-Time',
	    'ln ln-icon-Over-Time2'=>'Over-Time2',
	    'ln ln-icon-Owl'=>'Owl',
	    'ln ln-icon-Pac-Man'=>'Pac-Man',
	    'ln ln-icon-Paint-Brush'=>'Paint-Brush',
	    'ln ln-icon-Paint-Bucket'=>'Paint-Bucket',
	    'ln ln-icon-Paintbrush'=>'Paintbrush',
	    'ln ln-icon-Palette'=>'Palette',
	    'ln ln-icon-Palm-Tree'=>'Palm-Tree',
	    'ln ln-icon-Panda'=>'Panda',
	    'ln ln-icon-Panorama'=>'Panorama',
	    'ln ln-icon-Pantheon'=>'Pantheon',
	    'ln ln-icon-Pantone'=>'Pantone',
	    'ln ln-icon-Pants'=>'Pants',
	    'ln ln-icon-Paper-Plane'=>'Paper-Plane',
	    'ln ln-icon-Paper'=>'Paper',
	    'ln ln-icon-Parasailing'=>'Parasailing',
	    'ln ln-icon-Parrot'=>'Parrot',
	    'ln ln-icon-Password-2shopping'=>'Password-2shopping',
	    'ln ln-icon-Password-Field'=>'Password-Field',
	    'ln ln-icon-Password-shopping'=>'Password-shopping',
	    'ln ln-icon-Password'=>'Password',
	    'ln ln-icon-pause-2'=>'pause-2',
	    'ln ln-icon-Pause'=>'Pause',
	    'ln ln-icon-Paw'=>'Paw',
	    'ln ln-icon-Pawn'=>'Pawn',
	    'ln ln-icon-Paypal'=>'Paypal',
	    'ln ln-icon-Pen-2'=>'Pen-2',
	    'ln ln-icon-Pen-3'=>'Pen-3',
	    'ln ln-icon-Pen-4'=>'Pen-4',
	    'ln ln-icon-Pen-5'=>'Pen-5',
	    'ln ln-icon-Pen-6'=>'Pen-6',
	    'ln ln-icon-Pen'=>'Pen',
	    'ln ln-icon-Pencil'=>'Pencil',
	    'ln ln-icon-Penguin'=>'Penguin',
	    'ln ln-icon-Pentagon'=>'Pentagon',
	    'ln ln-icon-People-onCloud'=>'People-onCloud',
	    'ln ln-icon-Pepper-withFire'=>'Pepper-withFire',
	    'ln ln-icon-Pepper'=>'Pepper',
	    'ln ln-icon-Petrol'=>'Petrol',
	    'ln ln-icon-Petronas-Tower'=>'Petronas-Tower',
	    'ln ln-icon-Philipines'=>'Philipines',
	    'ln ln-icon-Phone-2'=>'Phone-2',
	    'ln ln-icon-Phone-3'=>'Phone-3',
	    'ln ln-icon-Phone-3G'=>'Phone-3G',
	    'ln ln-icon-Phone-4G'=>'Phone-4G',
	    'ln ln-icon-Phone-Simcard'=>'Phone-Simcard',
	    'ln ln-icon-Phone-SMS'=>'Phone-SMS',
	    'ln ln-icon-Phone-Wifi'=>'Phone-Wifi',
	    'ln ln-icon-Phone'=>'Phone',
	    'ln ln-icon-Photo-2'=>'Photo-2',
	    'ln ln-icon-Photo-3'=>'Photo-3',
	    'ln ln-icon-Photo-Album'=>'Photo-Album',
	    'ln ln-icon-Photo-Album2'=>'Photo-Album2',
	    'ln ln-icon-Photo-Album3'=>'Photo-Album3',
	    'ln ln-icon-Photo'=>'Photo',
	    'ln ln-icon-Photos'=>'Photos',
	    'ln ln-icon-Physics'=>'Physics',
	    'ln ln-icon-Pi'=>'Pi',
	    'ln ln-icon-Piano'=>'Piano',
	    'ln ln-icon-Picasa'=>'Picasa',
	    'ln ln-icon-Pie-Chart'=>'Pie-Chart',
	    'ln ln-icon-Pie-Chart2'=>'Pie-Chart2',
	    'ln ln-icon-Pie-Chart3'=>'Pie-Chart3',
	    'ln ln-icon-Pilates-2'=>'Pilates-2',
	    'ln ln-icon-Pilates-3'=>'Pilates-3',
	    'ln ln-icon-Pilates'=>'Pilates',
	    'ln ln-icon-Pilot'=>'Pilot',
	    'ln ln-icon-Pinch'=>'Pinch',
	    'ln ln-icon-Ping-Pong'=>'Ping-Pong',
	    'ln ln-icon-Pinterest'=>'Pinterest',
	    'ln ln-icon-Pipe'=>'Pipe',
	    'ln ln-icon-Pipette'=>'Pipette',
	    'ln ln-icon-Piramids'=>'Piramids',
	    'ln ln-icon-Pisces-2'=>'Pisces-2',
	    'ln ln-icon-Pisces'=>'Pisces',
	    'ln ln-icon-Pizza-Slice'=>'Pizza-Slice',
	    'ln ln-icon-Pizza'=>'Pizza',
	    'ln ln-icon-Plane-2'=>'Plane-2',
	    'ln ln-icon-Plant'=>'Plant',
	    'ln ln-icon-Plasmid'=>'Plasmid',
	    'ln ln-icon-Plaster'=>'Plaster',
	    'ln ln-icon-Plastic-CupPhone'=>'Plastic-CupPhone',
	    'ln ln-icon-Plastic-CupPhone2'=>'Plastic-CupPhone2',
	    'ln ln-icon-Plate'=>'Plate',
	    'ln ln-icon-Plates'=>'Plates',
	    'ln ln-icon-Plaxo'=>'Plaxo',
	    'ln ln-icon-Play-Music'=>'Play-Music',
	    'ln ln-icon-Plug-In'=>'Plug-In',
	    'ln ln-icon-Plug-In2'=>'Plug-In2',
	    'ln ln-icon-Plurk'=>'Plurk',
	    'ln ln-icon-Pointer'=>'Pointer',
	    'ln ln-icon-Poland'=>'Poland',
	    'ln ln-icon-Police-Man'=>'Police-Man',
	    'ln ln-icon-Police-Station'=>'Police-Station',
	    'ln ln-icon-Police-Woman'=>'Police-Woman',
	    'ln ln-icon-Police'=>'Police',
	    'ln ln-icon-Polo-Shirt'=>'Polo-Shirt',
	    'ln ln-icon-Portrait'=>'Portrait',
	    'ln ln-icon-Portugal'=>'Portugal',
	    'ln ln-icon-Post-Mail'=>'Post-Mail',
	    'ln ln-icon-Post-Mail2'=>'Post-Mail2',
	    'ln ln-icon-Post-Office'=>'Post-Office',
	    'ln ln-icon-Post-Sign'=>'Post-Sign',
	    'ln ln-icon-Post-Sign2ways'=>'Post-Sign2ways',
	    'ln ln-icon-Posterous'=>'Posterous',
	    'ln ln-icon-Pound-Sign'=>'Pound-Sign',
	    'ln ln-icon-Pound-Sign2'=>'Pound-Sign2',
	    'ln ln-icon-Pound'=>'Pound',
	    'ln ln-icon-Power-2'=>'Power-2',
	    'ln ln-icon-Power-3'=>'Power-3',
	    'ln ln-icon-Power-Cable'=>'Power-Cable',
	    'ln ln-icon-Power-Station'=>'Power-Station',
	    'ln ln-icon-Power'=>'Power',
	    'ln ln-icon-Prater'=>'Prater',
	    'ln ln-icon-Present'=>'Present',
	    'ln ln-icon-Presents'=>'Presents',
	    'ln ln-icon-Press'=>'Press',
	    'ln ln-icon-Preview'=>'Preview',
	    'ln ln-icon-Previous'=>'Previous',
	    'ln ln-icon-Pricing'=>'Pricing',
	    'ln ln-icon-Printer'=>'Printer',
	    'ln ln-icon-Professor'=>'Professor',
	    'ln ln-icon-Profile'=>'Profile',
	    'ln ln-icon-Project'=>'Project',
	    'ln ln-icon-Projector-2'=>'Projector-2',
	    'ln ln-icon-Projector'=>'Projector',
	    'ln ln-icon-Pulse'=>'Pulse',
	    'ln ln-icon-Pumpkin'=>'Pumpkin',
	    'ln ln-icon-Punk'=>'Punk',
	    'ln ln-icon-Punker'=>'Punker',
	    'ln ln-icon-Puzzle'=>'Puzzle',
	    'ln ln-icon-QIK'=>'QIK',
	    'ln ln-icon-QR-Code'=>'QR-Code',
	    'ln ln-icon-Queen-2'=>'Queen-2',
	    'ln ln-icon-Queen'=>'Queen',
	    'ln ln-icon-Quill-2'=>'Quill-2',
	    'ln ln-icon-Quill-3'=>'Quill-3',
	    'ln ln-icon-Quill'=>'Quill',
	    'ln ln-icon-Quotes-2'=>'Quotes-2',
	    'ln ln-icon-Quotes'=>'Quotes',
	    'ln ln-icon-Radio'=>'Radio',
	    'ln ln-icon-Radioactive'=>'Radioactive',
	    'ln ln-icon-Rafting'=>'Rafting',
	    'ln ln-icon-Rain-Drop'=>'Rain-Drop',
	    'ln ln-icon-Rainbow-2'=>'Rainbow-2',
	    'ln ln-icon-Rainbow'=>'Rainbow',
	    'ln ln-icon-Ram'=>'Ram',
	    'ln ln-icon-Razzor-Blade'=>'Razzor-Blade',
	    'ln ln-icon-Receipt-2'=>'Receipt-2',
	    'ln ln-icon-Receipt-3'=>'Receipt-3',
	    'ln ln-icon-Receipt-4'=>'Receipt-4',
	    'ln ln-icon-Receipt'=>'Receipt',
	    'ln ln-icon-Record2'=>'Record2',
	    'ln ln-icon-Record-3'=>'Record-3',
	    'ln ln-icon-Record-Music'=>'Record-Music',
	    'ln ln-icon-Record'=>'Record',
	    'ln ln-icon-Recycling-2'=>'Recycling-2',
	    'ln ln-icon-Recycling'=>'Recycling',
	    'ln ln-icon-Reddit'=>'Reddit',
	    'ln ln-icon-Redhat'=>'Redhat',
	    'ln ln-icon-Redirect'=>'Redirect',
	    'ln ln-icon-Redo'=>'Redo',
	    'ln ln-icon-Reel'=>'Reel',
	    'ln ln-icon-Refinery'=>'Refinery',
	    'ln ln-icon-Refresh-Window'=>'Refresh-Window',
	    'ln ln-icon-Refresh'=>'Refresh',
	    'ln ln-icon-Reload-2'=>'Reload-2',
	    'ln ln-icon-Reload-3'=>'Reload-3',
	    'ln ln-icon-Reload'=>'Reload',
	    'ln ln-icon-Remote-Controll'=>'Remote-Controll',
	    'ln ln-icon-Remote-Controll2'=>'Remote-Controll2',
	    'ln ln-icon-Remove-Bag'=>'Remove-Bag',
	    'ln ln-icon-Remove-Basket'=>'Remove-Basket',
	    'ln ln-icon-Remove-Cart'=>'Remove-Cart',
	    'ln ln-icon-Remove-File'=>'Remove-File',
	    'ln ln-icon-Remove-User'=>'Remove-User',
	    'ln ln-icon-Remove-Window'=>'Remove-Window',
	    'ln ln-icon-Remove'=>'Remove',
	    'ln ln-icon-Rename'=>'Rename',
	    'ln ln-icon-Repair'=>'Repair',
	    'ln ln-icon-Repeat-2'=>'Repeat-2',
	    'ln ln-icon-Repeat-3'=>'Repeat-3',
	    'ln ln-icon-Repeat-4'=>'Repeat-4',
	    'ln ln-icon-Repeat-5'=>'Repeat-5',
	    'ln ln-icon-Repeat-6'=>'Repeat-6',
	    'ln ln-icon-Repeat-7'=>'Repeat-7',
	    'ln ln-icon-Repeat'=>'Repeat',
	    'ln ln-icon-Reset'=>'Reset',
	    'ln ln-icon-Resize'=>'Resize',
	    'ln ln-icon-Restore-Window'=>'Restore-Window',
	    'ln ln-icon-Retouching'=>'Retouching',
	    'ln ln-icon-Retro-Camera'=>'Retro-Camera',
	    'ln ln-icon-Retro'=>'Retro',
	    'ln ln-icon-Retweet'=>'Retweet',
	    'ln ln-icon-Reverbnation'=>'Reverbnation',
	    'ln ln-icon-Rewind'=>'Rewind',
	    'ln ln-icon-RGB'=>'RGB',
	    'ln ln-icon-Ribbon-2'=>'Ribbon-2',
	    'ln ln-icon-Ribbon-3'=>'Ribbon-3',
	    'ln ln-icon-Ribbon'=>'Ribbon',
	    'ln ln-icon-Right-2'=>'Right-2',
	    'ln ln-icon-Right-3'=>'Right-3',
	    'ln ln-icon-Right-4'=>'Right-4',
	    'ln ln-icon-Right-ToLeft'=>'Right-ToLeft',
	    'ln ln-icon-Right'=>'Right',
	    'ln ln-icon-Road-2'=>'Road-2',
	    'ln ln-icon-Road-3'=>'Road-3',
	    'ln ln-icon-Road'=>'Road',
	    'ln ln-icon-Robot-2'=>'Robot-2',
	    'ln ln-icon-Robot'=>'Robot',
	    'ln ln-icon-Rock-andRoll'=>'Rock-andRoll',
	    'ln ln-icon-Rocket'=>'Rocket',
	    'ln ln-icon-Roller'=>'Roller',
	    'ln ln-icon-Roof'=>'Roof',
	    'ln ln-icon-Rook'=>'Rook',
	    'ln ln-icon-Rotate-Gesture'=>'Rotate-Gesture',
	    'ln ln-icon-Rotate-Gesture2'=>'Rotate-Gesture2',
	    'ln ln-icon-Rotate-Gesture3'=>'Rotate-Gesture3',
	    'ln ln-icon-Rotation-390'=>'Rotation-390',
	    'ln ln-icon-Rotation'=>'Rotation',
	    'ln ln-icon-Router-2'=>'Router-2',
	    'ln ln-icon-Router'=>'Router',
	    'ln ln-icon-RSS'=>'RSS',
	    'ln ln-icon-Ruler-2'=>'Ruler-2',
	    'ln ln-icon-Ruler'=>'Ruler',
	    'ln ln-icon-Running-Shoes'=>'Running-Shoes',
	    'ln ln-icon-Running'=>'Running',
	    'ln ln-icon-Safari'=>'Safari',
	    'ln ln-icon-Safe-Box'=>'Safe-Box',
	    'ln ln-icon-Safe-Box2'=>'Safe-Box2',
	    'ln ln-icon-Safety-PinClose'=>'Safety-PinClose',
	    'ln ln-icon-Safety-PinOpen'=>'Safety-PinOpen',
	    'ln ln-icon-Sagittarus-2'=>'Sagittarus-2',
	    'ln ln-icon-Sagittarus'=>'Sagittarus',
	    'ln ln-icon-Sailing-Ship'=>'Sailing-Ship',
	    'ln ln-icon-Sand-watch'=>'Sand-watch',
	    'ln ln-icon-Sand-watch2'=>'Sand-watch2',
	    'ln ln-icon-Santa-Claus'=>'Santa-Claus',
	    'ln ln-icon-Santa-Claus2'=>'Santa-Claus2',
	    'ln ln-icon-Santa-onSled'=>'Santa-onSled',
	    'ln ln-icon-Satelite-2'=>'Satelite-2',
	    'ln ln-icon-Satelite'=>'Satelite',
	    'ln ln-icon-Save-Window'=>'Save-Window',
	    'ln ln-icon-Save'=>'Save',
	    'ln ln-icon-Saw'=>'Saw',
	    'ln ln-icon-Saxophone'=>'Saxophone',
	    'ln ln-icon-Scale'=>'Scale',
	    'ln ln-icon-Scarf'=>'Scarf',
	    'ln ln-icon-Scissor'=>'Scissor',
	    'ln ln-icon-Scooter-Front'=>'Scooter-Front',
	    'ln ln-icon-Scooter'=>'Scooter',
	    'ln ln-icon-Scorpio-2'=>'Scorpio-2',
	    'ln ln-icon-Scorpio'=>'Scorpio',
	    'ln ln-icon-Scotland'=>'Scotland',
	    'ln ln-icon-Screwdriver'=>'Screwdriver',
	    'ln ln-icon-Scroll-Fast'=>'Scroll-Fast',
	    'ln ln-icon-Scroll'=>'Scroll',
	    'ln ln-icon-Scroller-2'=>'Scroller-2',
	    'ln ln-icon-Scroller'=>'Scroller',
	    'ln ln-icon-Sea-Dog'=>'Sea-Dog',
	    'ln ln-icon-Search-onCloud'=>'Search-onCloud',
	    'ln ln-icon-Search-People'=>'Search-People',
	    'ln ln-icon-secound'=>'secound',
	    'ln ln-icon-secound2'=>'secound2',
	    'ln ln-icon-Security-Block'=>'Security-Block',
	    'ln ln-icon-Security-Bug'=>'Security-Bug',
	    'ln ln-icon-Security-Camera'=>'Security-Camera',
	    'ln ln-icon-Security-Check'=>'Security-Check',
	    'ln ln-icon-Security-Settings'=>'Security-Settings',
	    'ln ln-icon-Security-Smiley'=>'Security-Smiley',
	    'ln ln-icon-Securiy-Remove'=>'Securiy-Remove',
	    'ln ln-icon-Seed'=>'Seed',
	    'ln ln-icon-Selfie'=>'Selfie',
	    'ln ln-icon-Serbia'=>'Serbia',
	    'ln ln-icon-Server-2'=>'Server-2',
	    'ln ln-icon-Server'=>'Server',
	    'ln ln-icon-Servers'=>'Servers',
	    'ln ln-icon-Settings-Window'=>'Settings-Window',
	    'ln ln-icon-Sewing-Machine'=>'Sewing-Machine',
	    'ln ln-icon-Sexual'=>'Sexual',
	    'ln ln-icon-Share-onCloud'=>'Share-onCloud',
	    'ln ln-icon-Share-Window'=>'Share-Window',
	    'ln ln-icon-Share'=>'Share',
	    'ln ln-icon-Sharethis'=>'Sharethis',
	    'ln ln-icon-Shark'=>'Shark',
	    'ln ln-icon-Sheep'=>'Sheep',
	    'ln ln-icon-Sheriff-Badge'=>'Sheriff-Badge',
	    'ln ln-icon-Shield'=>'Shield',
	    'ln ln-icon-Ship-2'=>'Ship-2',
	    'ln ln-icon-Ship'=>'Ship',
	    'ln ln-icon-Shirt'=>'Shirt',
	    'ln ln-icon-Shoes-2'=>'Shoes-2',
	    'ln ln-icon-Shoes-3'=>'Shoes-3',
	    'ln ln-icon-Shoes'=>'Shoes',
	    'ln ln-icon-Shop-2'=>'Shop-2',
	    'ln ln-icon-Shop-3'=>'Shop-3',
	    'ln ln-icon-Shop-4'=>'Shop-4',
	    'ln ln-icon-Shop'=>'Shop',
	    'ln ln-icon-Shopping-Bag'=>'Shopping-Bag',
	    'ln ln-icon-Shopping-Basket'=>'Shopping-Basket',
	    'ln ln-icon-Shopping-Cart'=>'Shopping-Cart',
	    'ln ln-icon-Short-Pants'=>'Short-Pants',
	    'ln ln-icon-Shoutwire'=>'Shoutwire',
	    'ln ln-icon-Shovel'=>'Shovel',
	    'ln ln-icon-Shuffle-2'=>'Shuffle-2',
	    'ln ln-icon-Shuffle-3'=>'Shuffle-3',
	    'ln ln-icon-Shuffle-4'=>'Shuffle-4',
	    'ln ln-icon-Shuffle'=>'Shuffle',
	    'ln ln-icon-Shutter'=>'Shutter',
	    'ln ln-icon-Sidebar-Window'=>'Sidebar-Window',
	    'ln ln-icon-Signal'=>'Signal',
	    'ln ln-icon-Singapore'=>'Singapore',
	    'ln ln-icon-Skate-Shoes'=>'Skate-Shoes',
	    'ln ln-icon-Skateboard-2'=>'Skateboard-2',
	    'ln ln-icon-Skateboard'=>'Skateboard',
	    'ln ln-icon-Skeleton'=>'Skeleton',
	    'ln ln-icon-Ski'=>'Ski',
	    'ln ln-icon-Skirt'=>'Skirt',
	    'ln ln-icon-Skrill'=>'Skrill',
	    'ln ln-icon-Skull'=>'Skull',
	    'ln ln-icon-Skydiving'=>'Skydiving',
	    'ln ln-icon-Skype'=>'Skype',
	    'ln ln-icon-Sled-withGifts'=>'Sled-withGifts',
	    'ln ln-icon-Sled'=>'Sled',
	    'ln ln-icon-Sleeping'=>'Sleeping',
	    'ln ln-icon-Sleet'=>'Sleet',
	    'ln ln-icon-Slippers'=>'Slippers',
	    'ln ln-icon-Smart'=>'Smart',
	    'ln ln-icon-Smartphone-2'=>'Smartphone-2',
	    'ln ln-icon-Smartphone-3'=>'Smartphone-3',
	    'ln ln-icon-Smartphone-4'=>'Smartphone-4',
	    'ln ln-icon-Smartphone-Secure'=>'Smartphone-Secure',
	    'ln ln-icon-Smartphone'=>'Smartphone',
	    'ln ln-icon-Smile'=>'Smile',
	    'ln ln-icon-Smoking-Area'=>'Smoking-Area',
	    'ln ln-icon-Smoking-Pipe'=>'Smoking-Pipe',
	    'ln ln-icon-Snake'=>'Snake',
	    'ln ln-icon-Snorkel'=>'Snorkel',
	    'ln ln-icon-Snow-2'=>'Snow-2',
	    'ln ln-icon-Snow-Dome'=>'Snow-Dome',
	    'ln ln-icon-Snow-Storm'=>'Snow-Storm',
	    'ln ln-icon-Snow'=>'Snow',
	    'ln ln-icon-Snowflake-2'=>'Snowflake-2',
	    'ln ln-icon-Snowflake-3'=>'Snowflake-3',
	    'ln ln-icon-Snowflake-4'=>'Snowflake-4',
	    'ln ln-icon-Snowflake'=>'Snowflake',
	    'ln ln-icon-Snowman'=>'Snowman',
	    'ln ln-icon-Soccer-Ball'=>'Soccer-Ball',
	    'ln ln-icon-Soccer-Shoes'=>'Soccer-Shoes',
	    'ln ln-icon-Socks'=>'Socks',
	    'ln ln-icon-Solar'=>'Solar',
	    'ln ln-icon-Sound-Wave'=>'Sound-Wave',
	    'ln ln-icon-Sound'=>'Sound',
	    'ln ln-icon-Soundcloud'=>'Soundcloud',
	    'ln ln-icon-Soup'=>'Soup',
	    'ln ln-icon-South-Africa'=>'South-Africa',
	    'ln ln-icon-Space-Needle'=>'Space-Needle',
	    'ln ln-icon-Spain'=>'Spain',
	    'ln ln-icon-Spam-Mail'=>'Spam-Mail',
	    'ln ln-icon-Speach-Bubble'=>'Speach-Bubble',
	    'ln ln-icon-Speach-Bubble2'=>'Speach-Bubble2',
	    'ln ln-icon-Speach-Bubble3'=>'Speach-Bubble3',
	    'ln ln-icon-Speach-Bubble4'=>'Speach-Bubble4',
	    'ln ln-icon-Speach-Bubble5'=>'Speach-Bubble5',
	    'ln ln-icon-Speach-Bubble6'=>'Speach-Bubble6',
	    'ln ln-icon-Speach-Bubble7'=>'Speach-Bubble7',
	    'ln ln-icon-Speach-Bubble8'=>'Speach-Bubble8',
	    'ln ln-icon-Speach-Bubble9'=>'Speach-Bubble9',
	    'ln ln-icon-Speach-Bubble10'=>'Speach-Bubble10',
	    'ln ln-icon-Speach-Bubble11'=>'Speach-Bubble11',
	    'ln ln-icon-Speach-Bubble12'=>'Speach-Bubble12',
	    'ln ln-icon-Speach-Bubble13'=>'Speach-Bubble13',
	    'ln ln-icon-Speach-BubbleAsking'=>'Speach-BubbleAsking',
	    'ln ln-icon-Speach-BubbleComic'=>'Speach-BubbleComic',
	    'ln ln-icon-Speach-BubbleComic2'=>'Speach-BubbleComic2',
	    'ln ln-icon-Speach-BubbleComic3'=>'Speach-BubbleComic3',
	    'ln ln-icon-Speach-BubbleComic4'=>'Speach-BubbleComic4',
	    'ln ln-icon-Speach-BubbleDialog'=>'Speach-BubbleDialog',
	    'ln ln-icon-Speach-Bubbles'=>'Speach-Bubbles',
	    'ln ln-icon-Speak-2'=>'Speak-2',
	    'ln ln-icon-Speak'=>'Speak',
	    'ln ln-icon-Speaker-2'=>'Speaker-2',
	    'ln ln-icon-Speaker'=>'Speaker',
	    'ln ln-icon-Spell-Check'=>'Spell-Check',
	    'ln ln-icon-Spell-CheckABC'=>'Spell-CheckABC',
	    'ln ln-icon-Spermium'=>'Spermium',
	    'ln ln-icon-Spider'=>'Spider',
	    'ln ln-icon-Spiderweb'=>'Spiderweb',
	    'ln ln-icon-Split-FourSquareWindow'=>'Split-FourSquareWindow',
	    'ln ln-icon-Split-Horizontal'=>'Split-Horizontal',
	    'ln ln-icon-Split-Horizontal2Window'=>'Split-Horizontal2Window',
	    'ln ln-icon-Split-Vertical'=>'Split-Vertical',
	    'ln ln-icon-Split-Vertical2'=>'Split-Vertical2',
	    'ln ln-icon-Split-Window'=>'Split-Window',
	    'ln ln-icon-Spoder'=>'Spoder',
	    'ln ln-icon-Spoon'=>'Spoon',
	    'ln ln-icon-Sport-Mode'=>'Sport-Mode',
	    'ln ln-icon-Sports-Clothings1'=>'Sports-Clothings1',
	    'ln ln-icon-Sports-Clothings2'=>'Sports-Clothings2',
	    'ln ln-icon-Sports-Shirt'=>'Sports-Shirt',
	    'ln ln-icon-Spot'=>'Spot',
	    'ln ln-icon-Spray'=>'Spray',
	    'ln ln-icon-Spread'=>'Spread',
	    'ln ln-icon-Spring'=>'Spring',
	    'ln ln-icon-Spurl'=>'Spurl',
	    'ln ln-icon-Spy'=>'Spy',
	    'ln ln-icon-Squirrel'=>'Squirrel',
	    'ln ln-icon-SSL'=>'SSL',
	    'ln ln-icon-St-BasilsCathedral'=>'St-BasilsCathedral',
	    'ln ln-icon-St-PaulsCathedral'=>'St-PaulsCathedral',
	    'ln ln-icon-Stamp-2'=>'Stamp-2',
	    'ln ln-icon-Stamp'=>'Stamp',
	    'ln ln-icon-Stapler'=>'Stapler',
	    'ln ln-icon-Star-Track'=>'Star-Track',
	    'ln ln-icon-Star'=>'Star',
	    'ln ln-icon-Starfish'=>'Starfish',
	    'ln ln-icon-Start2'=>'Start2',
	    'ln ln-icon-Start-3'=>'Start-3',
	    'ln ln-icon-Start-ways'=>'Start-ways',
	    'ln ln-icon-Start'=>'Start',
	    'ln ln-icon-Statistic'=>'Statistic',
	    'ln ln-icon-Stethoscope'=>'Stethoscope',
	    'ln ln-icon-stop--2'=>'stop--2',
	    'ln ln-icon-Stop-Music'=>'Stop-Music',
	    'ln ln-icon-Stop'=>'Stop',
	    'ln ln-icon-Stopwatch-2'=>'Stopwatch-2',
	    'ln ln-icon-Stopwatch'=>'Stopwatch',
	    'ln ln-icon-Storm'=>'Storm',
	    'ln ln-icon-Street-View'=>'Street-View',
	    'ln ln-icon-Street-View2'=>'Street-View2',
	    'ln ln-icon-Strikethrough-Text'=>'Strikethrough-Text',
	    'ln ln-icon-Stroller'=>'Stroller',
	    'ln ln-icon-Structure'=>'Structure',
	    'ln ln-icon-Student-Female'=>'Student-Female',
	    'ln ln-icon-Student-Hat'=>'Student-Hat',
	    'ln ln-icon-Student-Hat2'=>'Student-Hat2',
	    'ln ln-icon-Student-Male'=>'Student-Male',
	    'ln ln-icon-Student-MaleFemale'=>'Student-MaleFemale',
	    'ln ln-icon-Students'=>'Students',
	    'ln ln-icon-Studio-Flash'=>'Studio-Flash',
	    'ln ln-icon-Studio-Lightbox'=>'Studio-Lightbox',
	    'ln ln-icon-Stumbleupon'=>'Stumbleupon',
	    'ln ln-icon-Suit'=>'Suit',
	    'ln ln-icon-Suitcase'=>'Suitcase',
	    'ln ln-icon-Sum-2'=>'Sum-2',
	    'ln ln-icon-Sum'=>'Sum',
	    'ln ln-icon-Summer'=>'Summer',
	    'ln ln-icon-Sun-CloudyRain'=>'Sun-CloudyRain',
	    'ln ln-icon-Sun'=>'Sun',
	    'ln ln-icon-Sunglasses-2'=>'Sunglasses-2',
	    'ln ln-icon-Sunglasses-3'=>'Sunglasses-3',
	    'ln ln-icon-Sunglasses-Smiley'=>'Sunglasses-Smiley',
	    'ln ln-icon-Sunglasses-Smiley2'=>'Sunglasses-Smiley2',
	    'ln ln-icon-Sunglasses-W'=>'Sunglasses-W',
	    'ln ln-icon-Sunglasses-W2'=>'Sunglasses-W2',
	    'ln ln-icon-Sunglasses-W3'=>'Sunglasses-W3',
	    'ln ln-icon-Sunglasses'=>'Sunglasses',
	    'ln ln-icon-Sunrise'=>'Sunrise',
	    'ln ln-icon-Sunset'=>'Sunset',
	    'ln ln-icon-Superman'=>'Superman',
	    'ln ln-icon-Support'=>'Support',
	    'ln ln-icon-Surprise'=>'Surprise',
	    'ln ln-icon-Sushi'=>'Sushi',
	    'ln ln-icon-Sweden'=>'Sweden',
	    'ln ln-icon-Swimming-Short'=>'Swimming-Short',
	    'ln ln-icon-Swimming'=>'Swimming',
	    'ln ln-icon-Swimmwear'=>'Swimmwear',
	    'ln ln-icon-Switch'=>'Switch',
	    'ln ln-icon-Switzerland'=>'Switzerland',
	    'ln ln-icon-Sync-Cloud'=>'Sync-Cloud',
	    'ln ln-icon-Sync'=>'Sync',
	    'ln ln-icon-Synchronize-2'=>'Synchronize-2',
	    'ln ln-icon-Synchronize'=>'Synchronize',
	    'ln ln-icon-T-Shirt'=>'T-Shirt',
	    'ln ln-icon-Tablet-2'=>'Tablet-2',
	    'ln ln-icon-Tablet-3'=>'Tablet-3',
	    'ln ln-icon-Tablet-Orientation'=>'Tablet-Orientation',
	    'ln ln-icon-Tablet-Phone'=>'Tablet-Phone',
	    'ln ln-icon-Tablet-Secure'=>'Tablet-Secure',
	    'ln ln-icon-Tablet-Vertical'=>'Tablet-Vertical',
	    'ln ln-icon-Tablet'=>'Tablet',
	    'ln ln-icon-Tactic'=>'Tactic',
	    'ln ln-icon-Tag-2'=>'Tag-2',
	    'ln ln-icon-Tag-3'=>'Tag-3',
	    'ln ln-icon-Tag-4'=>'Tag-4',
	    'ln ln-icon-Tag-5'=>'Tag-5',
	    'ln ln-icon-Tag'=>'Tag',
	    'ln ln-icon-Taj-Mahal'=>'Taj-Mahal',
	    'ln ln-icon-Talk-Man'=>'Talk-Man',
	    'ln ln-icon-Tap'=>'Tap',
	    'ln ln-icon-Taurus-2'=>'Taurus-2',
	    'ln ln-icon-Taurus'=>'Taurus',
	    'ln ln-icon-Taxi-2'=>'Taxi-2',
	    'ln ln-icon-Taxi-Sign'=>'Taxi-Sign',
	    'ln ln-icon-Taxi'=>'Taxi',
	    'ln ln-icon-Teacher'=>'Teacher',
	    'ln ln-icon-Teapot'=>'Teapot',
	    'ln ln-icon-Technorati'=>'Technorati',
	    'ln ln-icon-Teddy-Bear'=>'Teddy-Bear',
	    'ln ln-icon-Tee-Mug'=>'Tee-Mug',
	    'ln ln-icon-Telephone-2'=>'Telephone-2',
	    'ln ln-icon-Telephone'=>'Telephone',
	    'ln ln-icon-Telescope'=>'Telescope',
	    'ln ln-icon-Temperature-2'=>'Temperature-2',
	    'ln ln-icon-Temperature-3'=>'Temperature-3',
	    'ln ln-icon-Temperature'=>'Temperature',
	    'ln ln-icon-Temple'=>'Temple',
	    'ln ln-icon-Tennis-Ball'=>'Tennis-Ball',
	    'ln ln-icon-Tennis'=>'Tennis',
	    'ln ln-icon-Tent'=>'Tent',
	    'ln ln-icon-Test-Tube'=>'Test-Tube',
	    'ln ln-icon-Test-Tube2'=>'Test-Tube2',
	    'ln ln-icon-Testimonal'=>'Testimonal',
	    'ln ln-icon-Text-Box'=>'Text-Box',
	    'ln ln-icon-Text-Effect'=>'Text-Effect',
	    'ln ln-icon-Text-HighlightColor'=>'Text-HighlightColor',
	    'ln ln-icon-Text-Paragraph'=>'Text-Paragraph',
	    'ln ln-icon-Thailand'=>'Thailand',
	    'ln ln-icon-The-WhiteHouse'=>'The-WhiteHouse',
	    'ln ln-icon-This-SideUp'=>'This-SideUp',
	    'ln ln-icon-Thread'=>'Thread',
	    'ln ln-icon-Three-ArrowFork'=>'Three-ArrowFork',
	    'ln ln-icon-Three-Fingers'=>'Three-Fingers',
	    'ln ln-icon-Three-FingersDrag'=>'Three-FingersDrag',
	    'ln ln-icon-Three-FingersDrag2'=>'Three-FingersDrag2',
	    'ln ln-icon-Three-FingersTouch'=>'Three-FingersTouch',
	    'ln ln-icon-Thumb'=>'Thumb',
	    'ln ln-icon-Thumbs-DownSmiley'=>'Thumbs-DownSmiley',
	    'ln ln-icon-Thumbs-UpSmiley'=>'Thumbs-UpSmiley',
	    'ln ln-icon-Thunder'=>'Thunder',
	    'ln ln-icon-Thunderstorm'=>'Thunderstorm',
	    'ln ln-icon-Ticket'=>'Ticket',
	    'ln ln-icon-Tie-2'=>'Tie-2',
	    'ln ln-icon-Tie-3'=>'Tie-3',
	    'ln ln-icon-Tie-4'=>'Tie-4',
	    'ln ln-icon-Tie'=>'Tie',
	    'ln ln-icon-Tiger'=>'Tiger',
	    'ln ln-icon-Time-Backup'=>'Time-Backup',
	    'ln ln-icon-Time-Bomb'=>'Time-Bomb',
	    'ln ln-icon-Time-Clock'=>'Time-Clock',
	    'ln ln-icon-Time-Fire'=>'Time-Fire',
	    'ln ln-icon-Time-Machine'=>'Time-Machine',
	    'ln ln-icon-Time-Window'=>'Time-Window',
	    'ln ln-icon-Timer-2'=>'Timer-2',
	    'ln ln-icon-Timer'=>'Timer',
	    'ln ln-icon-To-Bottom'=>'To-Bottom',
	    'ln ln-icon-To-Bottom2'=>'To-Bottom2',
	    'ln ln-icon-To-Left'=>'To-Left',
	    'ln ln-icon-To-Right'=>'To-Right',
	    'ln ln-icon-To-Top'=>'To-Top',
	    'ln ln-icon-To-Top2'=>'To-Top2',
	    'ln ln-icon-Token-'=>'Token-',
	    'ln ln-icon-Tomato'=>'Tomato',
	    'ln ln-icon-Tongue'=>'Tongue',
	    'ln ln-icon-Tooth-2'=>'Tooth-2',
	    'ln ln-icon-Tooth'=>'Tooth',
	    'ln ln-icon-Top-ToBottom'=>'Top-ToBottom',
	    'ln ln-icon-Touch-Window'=>'Touch-Window',
	    'ln ln-icon-Tourch'=>'Tourch',
	    'ln ln-icon-Tower-2'=>'Tower-2',
	    'ln ln-icon-Tower-Bridge'=>'Tower-Bridge',
	    'ln ln-icon-Tower'=>'Tower',
	    'ln ln-icon-Trace'=>'Trace',
	    'ln ln-icon-Tractor'=>'Tractor',
	    'ln ln-icon-traffic-Light'=>'traffic-Light',
	    'ln ln-icon-Traffic-Light2'=>'Traffic-Light2',
	    'ln ln-icon-Train-2'=>'Train-2',
	    'ln ln-icon-Train'=>'Train',
	    'ln ln-icon-Tram'=>'Tram',
	    'ln ln-icon-Transform-2'=>'Transform-2',
	    'ln ln-icon-Transform-3'=>'Transform-3',
	    'ln ln-icon-Transform-4'=>'Transform-4',
	    'ln ln-icon-Transform'=>'Transform',
	    'ln ln-icon-Trash-withMen'=>'Trash-withMen',
	    'ln ln-icon-Tree-2'=>'Tree-2',
	    'ln ln-icon-Tree-3'=>'Tree-3',
	    'ln ln-icon-Tree-4'=>'Tree-4',
	    'ln ln-icon-Tree-5'=>'Tree-5',
	    'ln ln-icon-Tree'=>'Tree',
	    'ln ln-icon-Trekking'=>'Trekking',
	    'ln ln-icon-Triangle-ArrowDown'=>'Triangle-ArrowDown',
	    'ln ln-icon-Triangle-ArrowLeft'=>'Triangle-ArrowLeft',
	    'ln ln-icon-Triangle-ArrowRight'=>'Triangle-ArrowRight',
	    'ln ln-icon-Triangle-ArrowUp'=>'Triangle-ArrowUp',
	    'ln ln-icon-Tripod-2'=>'Tripod-2',
	    'ln ln-icon-Tripod-andVideo'=>'Tripod-andVideo',
	    'ln ln-icon-Tripod-withCamera'=>'Tripod-withCamera',
	    'ln ln-icon-Tripod-withGopro'=>'Tripod-withGopro',
	    'ln ln-icon-Trophy-2'=>'Trophy-2',
	    'ln ln-icon-Trophy'=>'Trophy',
	    'ln ln-icon-Truck'=>'Truck',
	    'ln ln-icon-Trumpet'=>'Trumpet',
	    'ln ln-icon-Tumblr'=>'Tumblr',
	    'ln ln-icon-Turkey'=>'Turkey',
	    'ln ln-icon-Turn-Down'=>'Turn-Down',
	    'ln ln-icon-Turn-Down2'=>'Turn-Down2',
	    'ln ln-icon-Turn-DownFromLeft'=>'Turn-DownFromLeft',
	    'ln ln-icon-Turn-DownFromRight'=>'Turn-DownFromRight',
	    'ln ln-icon-Turn-Left'=>'Turn-Left',
	    'ln ln-icon-Turn-Left3'=>'Turn-Left3',
	    'ln ln-icon-Turn-Right'=>'Turn-Right',
	    'ln ln-icon-Turn-Right3'=>'Turn-Right3',
	    'ln ln-icon-Turn-Up'=>'Turn-Up',
	    'ln ln-icon-Turn-Up2'=>'Turn-Up2',
	    'ln ln-icon-Turtle'=>'Turtle',
	    'ln ln-icon-Tuxedo'=>'Tuxedo',
	    'ln ln-icon-TV'=>'TV',
	    'ln ln-icon-Twister'=>'Twister',
	    'ln ln-icon-Twitter-2'=>'Twitter-2',
	    'ln ln-icon-Twitter'=>'Twitter',
	    'ln ln-icon-Two-Fingers'=>'Two-Fingers',
	    'ln ln-icon-Two-FingersDrag'=>'Two-FingersDrag',
	    'ln ln-icon-Two-FingersDrag2'=>'Two-FingersDrag2',
	    'ln ln-icon-Two-FingersScroll'=>'Two-FingersScroll',
	    'ln ln-icon-Two-FingersTouch'=>'Two-FingersTouch',
	    'ln ln-icon-Two-Windows'=>'Two-Windows',
	    'ln ln-icon-Type-Pass'=>'Type-Pass',
	    'ln ln-icon-Ukraine'=>'Ukraine',
	    'ln ln-icon-Umbrela'=>'Umbrela',
	    'ln ln-icon-Umbrella-2'=>'Umbrella-2',
	    'ln ln-icon-Umbrella-3'=>'Umbrella-3',
	    'ln ln-icon-Under-LineText'=>'Under-LineText',
	    'ln ln-icon-Undo'=>'Undo',
	    'ln ln-icon-United-Kingdom'=>'United-Kingdom',
	    'ln ln-icon-United-States'=>'United-States',
	    'ln ln-icon-University-2'=>'University-2',
	    'ln ln-icon-University'=>'University',
	    'ln ln-icon-Unlike-2'=>'Unlike-2',
	    'ln ln-icon-Unlike'=>'Unlike',
	    'ln ln-icon-Unlock-2'=>'Unlock-2',
	    'ln ln-icon-Unlock-3'=>'Unlock-3',
	    'ln ln-icon-Unlock'=>'Unlock',
	    'ln ln-icon-Up--Down'=>'Up--Down',
	    'ln ln-icon-Up--Down3'=>'Up--Down3',
	    'ln ln-icon-Up-2'=>'Up-2',
	    'ln ln-icon-Up-3'=>'Up-3',
	    'ln ln-icon-Up-4'=>'Up-4',
	    'ln ln-icon-Up'=>'Up',
	    'ln ln-icon-Upgrade'=>'Upgrade',
	    'ln ln-icon-Upload-2'=>'Upload-2',
	    'ln ln-icon-Upload-toCloud'=>'Upload-toCloud',
	    'ln ln-icon-Upload-Window'=>'Upload-Window',
	    'ln ln-icon-Upload'=>'Upload',
	    'ln ln-icon-Uppercase-Text'=>'Uppercase-Text',
	    'ln ln-icon-Upward'=>'Upward',
	    'ln ln-icon-URL-Window'=>'URL-Window',
	    'ln ln-icon-Usb-2'=>'Usb-2',
	    'ln ln-icon-Usb-Cable'=>'Usb-Cable',
	    'ln ln-icon-Usb'=>'Usb',
	    'ln ln-icon-User'=>'User',
	    'ln ln-icon-Ustream'=>'Ustream',
	    'ln ln-icon-Vase'=>'Vase',
	    'ln ln-icon-Vector-2'=>'Vector-2',
	    'ln ln-icon-Vector-3'=>'Vector-3',
	    'ln ln-icon-Vector-4'=>'Vector-4',
	    'ln ln-icon-Vector-5'=>'Vector-5',
	    'ln ln-icon-Vector'=>'Vector',
	    'ln ln-icon-Venn-Diagram'=>'Venn-Diagram',
	    'ln ln-icon-Vest-2'=>'Vest-2',
	    'ln ln-icon-Vest'=>'Vest',
	    'ln ln-icon-Viddler'=>'Viddler',
	    'ln ln-icon-Video-2'=>'Video-2',
	    'ln ln-icon-Video-3'=>'Video-3',
	    'ln ln-icon-Video-4'=>'Video-4',
	    'ln ln-icon-Video-5'=>'Video-5',
	    'ln ln-icon-Video-6'=>'Video-6',
	    'ln ln-icon-Video-GameController'=>'Video-GameController',
	    'ln ln-icon-Video-Len'=>'Video-Len',
	    'ln ln-icon-Video-Len2'=>'Video-Len2',
	    'ln ln-icon-Video-Photographer'=>'Video-Photographer',
	    'ln ln-icon-Video-Tripod'=>'Video-Tripod',
	    'ln ln-icon-Video'=>'Video',
	    'ln ln-icon-Vietnam'=>'Vietnam',
	    'ln ln-icon-View-Height'=>'View-Height',
	    'ln ln-icon-View-Width'=>'View-Width',
	    'ln ln-icon-Vimeo'=>'Vimeo',
	    'ln ln-icon-Virgo-2'=>'Virgo-2',
	    'ln ln-icon-Virgo'=>'Virgo',
	    'ln ln-icon-Virus-2'=>'Virus-2',
	    'ln ln-icon-Virus-3'=>'Virus-3',
	    'ln ln-icon-Virus'=>'Virus',
	    'ln ln-icon-Visa'=>'Visa',
	    'ln ln-icon-Voice'=>'Voice',
	    'ln ln-icon-Voicemail'=>'Voicemail',
	    'ln ln-icon-Volleyball'=>'Volleyball',
	    'ln ln-icon-Volume-Down'=>'Volume-Down',
	    'ln ln-icon-Volume-Up'=>'Volume-Up',
	    'ln ln-icon-VPN'=>'VPN',
	    'ln ln-icon-Wacom-Tablet'=>'Wacom-Tablet',
	    'ln ln-icon-Waiter'=>'Waiter',
	    'ln ln-icon-Walkie-Talkie'=>'Walkie-Talkie',
	    'ln ln-icon-Wallet-2'=>'Wallet-2',
	    'ln ln-icon-Wallet-3'=>'Wallet-3',
	    'ln ln-icon-Wallet'=>'Wallet',
	    'ln ln-icon-Warehouse'=>'Warehouse',
	    'ln ln-icon-Warning-Window'=>'Warning-Window',
	    'ln ln-icon-Watch-2'=>'Watch-2',
	    'ln ln-icon-Watch-3'=>'Watch-3',
	    'ln ln-icon-Watch'=>'Watch',
	    'ln ln-icon-Wave-2'=>'Wave-2',
	    'ln ln-icon-Wave'=>'Wave',
	    'ln ln-icon-Webcam'=>'Webcam',
	    'ln ln-icon-weight-Lift'=>'weight-Lift',
	    'ln ln-icon-Wheelbarrow'=>'Wheelbarrow',
	    'ln ln-icon-Wheelchair'=>'Wheelchair',
	    'ln ln-icon-Width-Window'=>'Width-Window',
	    'ln ln-icon-Wifi-2'=>'Wifi-2',
	    'ln ln-icon-Wifi-Keyboard'=>'Wifi-Keyboard',
	    'ln ln-icon-Wifi'=>'Wifi',
	    'ln ln-icon-Wind-Turbine'=>'Wind-Turbine',
	    'ln ln-icon-Windmill'=>'Windmill',
	    'ln ln-icon-Window-2'=>'Window-2',
	    'ln ln-icon-Window'=>'Window',
	    'ln ln-icon-Windows-2'=>'Windows-2',
	    'ln ln-icon-Windows-Microsoft'=>'Windows-Microsoft',
	    'ln ln-icon-Windows'=>'Windows',
	    'ln ln-icon-Windsock'=>'Windsock',
	    'ln ln-icon-Windy'=>'Windy',
	    'ln ln-icon-Wine-Bottle'=>'Wine-Bottle',
	    'ln ln-icon-Wine-Glass'=>'Wine-Glass',
	    'ln ln-icon-Wink'=>'Wink',
	    'ln ln-icon-Winter-2'=>'Winter-2',
	    'ln ln-icon-Winter'=>'Winter',
	    'ln ln-icon-Wireless'=>'Wireless',
	    'ln ln-icon-Witch-Hat'=>'Witch-Hat',
	    'ln ln-icon-Witch'=>'Witch',
	    'ln ln-icon-Wizard'=>'Wizard',
	    'ln ln-icon-Wolf'=>'Wolf',
	    'ln ln-icon-Woman-Sign'=>'Woman-Sign',
	    'ln ln-icon-WomanMan'=>'WomanMan',
	    'ln ln-icon-Womans-Underwear'=>'Womans-Underwear',
	    'ln ln-icon-Womans-Underwear2'=>'Womans-Underwear2',
	    'ln ln-icon-Women'=>'Women',
	    'ln ln-icon-Wonder-Woman'=>'Wonder-Woman',
	    'ln ln-icon-Worker-Clothes'=>'Worker-Clothes',
	    'ln ln-icon-Worker'=>'Worker',
	    'ln ln-icon-Wrap-Text'=>'Wrap-Text',
	    'ln ln-icon-Wreath'=>'Wreath',
	    'ln ln-icon-Wrench'=>'Wrench',
	    'ln ln-icon-X-Box'=>'X-Box',
	    'ln ln-icon-X-ray'=>'X-ray',
	    'ln ln-icon-Xanga'=>'Xanga',
	    'ln ln-icon-Xing'=>'Xing',
	    'ln ln-icon-Yacht'=>'Yacht',
	    'ln ln-icon-Yahoo-Buzz'=>'Yahoo-Buzz',
	    'ln ln-icon-Yahoo'=>'Yahoo',
	    'ln ln-icon-Yelp'=>'Yelp',
	    'ln ln-icon-Yes'=>'Yes',
	    'ln ln-icon-Ying-Yang'=>'Ying-Yang',
	    'ln ln-icon-Youtube'=>'Youtube',
	    'ln ln-icon-Z-A'=>'Z-A',
	    'ln ln-icon-Zebra'=>'Zebra',
	    'ln ln-icon-Zombie'=>'Zombie',
	    'ln ln-icon-Zoom-Gesture'=>'Zoom-Gesture',
	    'ln ln-icon-Zootool'=>'Zootool',


      ),
  );



?>
