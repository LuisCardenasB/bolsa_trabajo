-- --------------------------------------------------------------------------------
-- 
-- @version: cmicsono_jobboard.sql Oct 3, 2019 12:42 gewa
-- @package Freelance Manager
-- @author wojoscripts.com.
-- @copyright 2011
-- 
-- --------------------------------------------------------------------------------
-- Host: localhost
-- Database: cmicsono_jobboard
-- Time: Oct 3, 2019-12:42
-- MySQL version: 5.6.41-84.1
-- PHP version: 5.6.30
-- --------------------------------------------------------------------------------

#
# Database: `cmicsono_jobboard`
#


-- --------------------------------------------------
# -- Table structure for table `bookmarks`
-- --------------------------------------------------
DROP TABLE IF EXISTS `bookmarks`;
CREATE TABLE `bookmarks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` enum('job','resume') NOT NULL,
  `user_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------
# Dumping data for table `bookmarks`
-- --------------------------------------------------



-- --------------------------------------------------
# -- Table structure for table `cart`
-- --------------------------------------------------
DROP TABLE IF EXISTS `cart`;
CREATE TABLE `cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL,
  `price` float(7,2) DEFAULT NULL,
  `uid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `cart`
-- --------------------------------------------------



-- --------------------------------------------------
# -- Table structure for table `companies`
-- --------------------------------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `uid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `about` text NOT NULL,
  `business` text NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `gplus` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------
# Dumping data for table `companies`
-- --------------------------------------------------

INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('2', 'mi empresa', '&lt;p&gt;&lt;span id=&quot;sceditor-end-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;span id=&quot;sceditor-start-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;mi empresa&lt;/p&gt;', '&lt;p&gt;&lt;span id=&quot;sceditor-end-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;span id=&quot;sceditor-start-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;mi empresa&lt;/p&gt;', 'COM_EF5C14-1FDD71-DC167D-F4D3E0-3B7488-16D4C8.png', 'CAÑADA 2213, LAS MISIONES', '6441257744', 'EXAMPLE@GMAIL.COM', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('4', 'PROYECTOS Y CONSTRUCCIONES VIRGO S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_1DD6DE-C2FAA6-8F010B-AA44D6-318073-9A43C9.png', 'BLVD. SOLIDARIDAD 1002 1, LA MOSCA', '6621100280', 'administracion@pcvirgo.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('5', 'CONSTRUCTORA MENDEZ D S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_9C94A8-2A1009-B556F6-E18C59-A77645-0E5A61.jpg', 'YAÑEZ 34, SAN BENITO', '6622562054', 'mmdessens@yahoo.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('6', '\tCONSEIN EDIFICACIONES S.A. DE C.V.\t ', '', '', '', '\tMELCHOR OCAMPO 1, MELCHOR OCAMPO\t ', '\t6343421330\t ', '\tconsein@conseinedificaciones.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('7', 'DESARROLLO CANORAS S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_4ABA98-5A9021-F73DA8-4C4EF9-675A22-9F3C44.jpg', 'PASEO DE LOS ALAMOS S/N, VILLA DEL SOL', '6622181000', 'vvalencia@canoras.com.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('8', 'GLUYAS CONSTRUCCIONES S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_D39EE3-20B164-75211F-8954F4-004A49-12DD80.png', 'TARASCA 16 SUR NUEVA ESTACION', '6622104721', 'info@gluyasconstrucciones.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('9', 'GRUPO CONSTRUCCIONES PLANIFICADAS S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_30D307-4F3B11-ABBFC7-29073A-33B836-0D1F70.png', 'PERIFERICO PONIENTE 770 EMILIANO ZAPATA', '6621080500', 'ventas@construplan.com.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('10', '\tLA AZTECA CONSTRUCCIONES Y URBANIZACIONES S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6622161087\t ', '\tazteca860203@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('11', '\tLA GRANDE INGENIERIA EN INFRAESTRUCTURA S.A. DE C.V.\t ', '', '', '', '\tAVENIDA DOCTOR PALIZA PADILLA 92 CENTENARIO\t ', '\t6622126787\t ', '\tgrupo8a@prodigy.net.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('12', '\tLUNA CONSTRUCCIONES DE OBREGON S.A. DE C.V.\t ', '', '', '', '\tTAMAULIPAS 551 NTE. ZONA NORTE\t ', '\t6444135326\t ', '\tacabrera@luconsa.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('13', 'OPOSON CONSTRUCCIONES Y SERVICIOS S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_9E3D69-FB06FE-3FC469-48A0ED-338493-335B80.png', 'AVE. TEHUANTEPEC 111 CENTENARIO', '6622131724', 'ROSORIO@OPOSON.COM.MX', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('14', 'PALOFIERRO CONSTRUCCIONES S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_E5FD72-44C322-C18153-E0614F-E41FF9-11AC1A.png', 'CAMPECHE 462 PIMENTEL', '6622600232', 'gamezosio@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('15', '\tPROMOTORA MAJERUS S. DE R.L.\t ', '', '', '', '\tHERMENEGILDO RANGEL LUGO 167 5 DE MAYO\t ', '\t6623127736\t ', '\tale.majerus@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('16', 'CODISOSA (CONSTRUDISEÑOS DE SONORA)', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_0A0BAD-A8988C-2F19A0-10BA83-C92B86-EF6F02.png', 'ANGEL GARCIA ABURTO 221 JESUS GARCIA', '2108054', 'CONSTRUDISENOSDESONORA@PRODIGY.NET.MX', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('17', 'CW METAL', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_8508D8-EEE57D-8D3A04-F555D6-5AD669-734DDC.png', 'corregidores 50 villa satelite', '0000000000', 'ACAMOU@CWMETAL.COM.MX', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('18', 'INGENIEROS CIVILES, S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_A917B9-2010C4-D45554-F8F3DB-90AE35-1D0E28.jpg', 'CARRETERA FEDERAL MEXICO 015 NTE. 3101 1, ZONA URBANA', '6444131061', 'aharispuru@icsa.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('19', 'INMOBILIARIA Y CONSTRUCTORA HARBOR', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_6B8E02-AB8AFC-ED384C-79C372-D62552-D2BB79.jpg', 'CARRETERA FEDERAL MEXICO 015 NTE. 3101, ZONA URBANA', '644131061', 'jharispuru@icsa.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('20', '\tLARSON EDIFICACIÒN METALICA\t ', '', '', '', '\tDEL ORO 130, PARQUE INDUSTRIAL\t ', '\t6622520404\t ', '\tcfd@larivdesarrollos.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('21', '\tPUENTE 391 S.A. DE C.V.\t ', '', '', '', '\tAVENIDA 7 20 6, JESUS GARCIA\t ', '\t6622187798\t ', '\tcontacto@puente391.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('22', 'SPAZICORP S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_F3AE94-DAA2F2-DAB0FB-E6F075-78C7CA-4F289E.jpg', 'QUINTA DE LA FLOR 13 D, LAS QUINTAS', '6622187798', 'patyl@spazicorp.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('23', 'CONCRETO Y ESPACIO URBANO S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_C70BA2-45E0F1-5198EA-46FFF0-A59920-5385AA.png', 'AVENIDA DE LOS YAQUIS 1187, NUEVA ESPAÑA', '6622103031 ', 'mitzi.ibarra@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('24', 'DESARROLLADORA Y URBANIZADORA INMEX S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_66C5B1-A8B977-C7B643-3ECBBE-BFA1D2-E2B661.png', 'DOMICILIO CONOCIDO', '6622855332', 'OBRAS@INMEX.MX', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('25', 'CONSTRUPIMA S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_A9DF44-F1AE25-E57FBC-26873C-19F1D8-3843FB.png', 'DOMICILIO CONOCIDO', '6622606342', 'karina@construpima.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('26', 'EDIFICACIONES BOZA S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_4CFEEB-DC5144-D22CF7-BF839A-C9391E-7ADA74.jpg', 'GARCIA MORALES 601 A JUAREZ', '6424226803', 'edificacionesboza@hotmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('27', '\tJOSE DARIO CABRAL VALDEZ\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6444186376\t ', '\tcvdario@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('28', '\tLC PROYECTOS Y CONSTRUCCIONES S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6424210767\t ', '\tlcproyectos@yahoo.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('29', 'NENNCA SOLUCIONES DE INGENIERIA S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_D4E10C-52F786-C1047C-9D714A-F5D5F7-D7FC61.png', 'DOMICILIO CONOCIDO', '6623109573', 'contacto@nennca.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('30', 'VIAS Y URBANIZACIONES MVP SA DE CV', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_7B24FD-D10667-770463-EBA23A-B0EE59-7764CF.jpg', 'Blvd. Luis Donaldo Colosio 294-F', '0000000000', 'contacto@viasmvp.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('31', '\tCONSTRUCCIONES GARALE S.A. DE C.V.\t ', '', '', '', '\tGUADALUPE VICTORIA 15 26, SAN BENITO\t ', '\t6622158246\t ', '\tinfo@cgarale.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('32', '\tFRANCISCO JAVIER CORDOVA LOPEZ\t ', '', '', '', '\tCUERNAVACA 51 SAN BENITO\t ', '\t6622205277\t ', '\tacclaboratorio@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('33', 'GIBHER CONSTRUCTORES S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_74AFA6-CD490C-09B0CC-1F4CAF-3953A3-285265.png', 'GUELATAO 3606 FRACC BUGAMBILIAS', '6424228028', 'lauracmicnavojoa@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('34', 'MINAR GRUPO CONSTRUCTOR S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_58AA6E-5AC3E9-10AA42-E39CAD-796114-E54B30.png', 'CUAUHTEMOC 123 D ALTOS CONSTITUCION', '6424213777', 'minargc@prodigy.net.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('35', 'DESARROLLOS Y SERVICIOS OLBAP S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_5C8949-E73660-F9D340-A299C9-B39444-B1D874.png', 'PASEO DE LA COLINA 66 VALLE VERDE', '6623105840', 'dso.construcciones@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('36', 'GRUPO TOTAL INBAS', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_56531F-A8ECCE-469FA6-670938-6C6D1A-2D47A4.jpg', 'AVENIDA SIETE 225, APOLO', '6622564741', 'grupototalinbas@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('37', '\tINECA CONSTRUCCIONES\t ', '', '', '', '\tQUINTA LAS CANORAS 59 las quintas\t ', '\t0000000000\t ', '\tinecaconstrucciones@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('38', 'MANSER PROYECTOS, SA DE CV', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_4DD60E-2D21E4-0CA383-3643AC-D743AA-58715E.png', 'FRANCISCO MONTEVERDE 38, SAN BENITO', '6622166509', 'ienriquez@manser.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('39', 'Maquinaria y Agregados Gala', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_D76952-FA3D19-43B6BD-F29887-5D6FB7-9F2AD3.jpg', 'REPUBLICA DE JAMAICA 12, EL SAHUARO', '6621400693', 'contacto@maqgala.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('40', 'MARSA DISEÑO Y CONSTRUCCIÓN', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_3B45EF-4E262E-FCE00F-C6373E-B4470D-4CA96D.png', 'CALLE DALIAS 9, PALO VERDE', '2153423', 'ALEXIBARRA@PRODIGY.NET.MX', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('41', 'ADOBE DESARROLLOS S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_C21D09-9929B0-5125AC-F99738-BC883D-B9BF36.jpg', 'EVERARDO MONROY 118 10, SAN BENITO', '6621530210', 'brauliofontane@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('42', 'ELECTRICA DEL COBRE S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_7821B4-77A243-D5BD5C-FBEB2D-E2A06F-0CE901.jpg', 'TLAXCALA 77, SAN BENITO', '6621078476', 'INGELECTRICADELCOBRE@HOTMAIL.COM', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('43', '\tR&R CONCRETOS SA DE CV\t ', '', '', '', '\tAVENIDA NIÑOS HEROES 115 A CENTRO\t ', '\t2140285\t ', '\tCONSTRUCCIONESR.R@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('44', 'CONSTRUCTORA E INMOBILIARIA VELIS S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_2EFFEC-B79D40-E3B9E1-8FDBC6-2576DC-6B9AAB.png', 'DOMICILIO CONOCIDO', '6444144813', 'contacto@constructoravelis.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('45', '\tCONCRETOS Y AGREGADOS TAJIMAROA S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6444110281\t ', '\tfacturacion@edinosa.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('46', '\tDAPCI S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6444120035\t ', '\tdapci.construccion@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('47', '\tEDIFICACIONES DOUR S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6622168075\t ', '\tbenitodosamantes@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('48', '\tGRUPO MESIS S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6424212691\t ', '\tgrupomesis@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('49', '\tGYS CONSTRUCTORES S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6622605431\t ', '\tgys_constructores@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('50', '\tHECTOR ISLAS CASTRO\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6341089383\t ', '\tanaelcamacho@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('51', '\tI WET CONCEPT DE MEXICO S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t7773161057\t ', '\tjlpopocasotelo@yahoo.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('52', 'IVAN MLADOSICH ESTRADA', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_5ADE0F-4B01CB-0D12E7-A92E10-B58A24-56E160.jpg', 'DOMICILIO CONOCIDO', '6424224246', 'imeconstrucciones@hotmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('53', '\tJAVIER ENRIQUE FELIX GARCIA ALONSO\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6441794603\t ', '\tconstrucciones.felgar@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('54', '\tMAOMI DESARROLLOS S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6624675920\t ', '\tPROCONS.08@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('55', 'OESTEC DE MEXICO S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_722E45-CE554A-5732D2-28848F-38559D-F5D785.jpg', 'DOMICILIO CONOCIDO', '6622165565', 'oestec@oestec.com.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('56', 'PIXO ACABADOS Y CONCRETOS S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_AA7675-EF2E7B-69AFCB-20848D-1ABA94-4F60BB.png', 'DOMICILIO CONOCIDO', '6622070889', 'HMARTINEZ@PIXO.MX', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('57', '\tPROYECTOS Y CONSTRUCCIONES MAGUS S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6424212691\t ', '\tpycmagus@yahoo.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('58', 'SERVICIOS ELECTRICOS Y CONTROL DE HERMOSILLO S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_5B7086-D3DBAB-7A6088-1D52A2-B2825D-C0118A.png', 'Camelia 619, Libertad', '6623126379', 'maricela.ramirez@servechermosillo.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('59', '\tALFA MURANO S.C.\t ', '', '', '', '\tBLVD. NAVARRETE 97 25\t ', '\t6622135683\t ', '\trplatt@conde.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('60', 'CASNOR CONSTRUCCIONES S.C. de P de R.L. de C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_1E387D-F3A44F-000370-930F6C-7160E0-AD3849.png', 'PINO SUAREZ 104, SAN BENITO', '6621418141', 'info@casnor.com.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('61', '\tCONSTRUCTORA ANAMARK S.A. DE C.V.\t ', '', '', '', '\tAVE. MICHOACAN0 221, SAN BENITO\t ', '\t6622126633\t ', '\tcoanamark@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('62', '\tCONSULTORIA Y CONSTRUCCION DEL NOROESTE S.A. DE C.V.\t ', '', '', '', '\tDUBLIN 17, RAQUET CLUB II\t ', '\t6621418734\t ', '\tcoconosa@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('63', '\tDESARROLLOS DOBLE A S.A. DE C.V.\t ', '', '', '', '\tJUAN JOSE AGUIRRE 295, BALDERRAMA\t ', '\t6622172001\t ', '\talejandropra@me.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('64', '\tDURA CONSTRUCCIONES S.A. DE C.V.\t ', '', '', '', '\tTEPIC 16, PALO VERDE\t ', '\t6622416793\t ', '\tyolanda@grupoimc.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('65', 'ELECTROINSUMOS GMBH S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_D3622C-CD1E0C-2B4339-0C4ECD-A7C890-E49D1B.png', 'TLAXCALA 334, SAN BENITO', '6622162007', 'administracion@electroinsumos.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('66', '\tENRIQUE SAMOANO VALENZUELA\t ', '', '', '', '\tTAMAULIPAS 712, DEPORTIVA\t ', '\t6424223188\t ', '\tbig_samoano@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('67', '\tCERTUS GERENCIA DE PROYECTOS S.A. DE C.V.\t ', '', '', '', '\tMONTERREY 131, CENTRO\t ', '\t6622133894\t ', '\tluiscano@certusgerencia.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('68', 'ESCOBO S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_707D2A-BE144F-ED102C-67AD31-53CA7F-46B290.jpg', 'AVENIDA NUEVE 150 PRADOS DEL SOL', '6623132626', 'ADMIN@ESCOBO.COM.MX', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('69', '\tFILIBERTO LUGO RODRIGUEZ\t ', '', '', '', '\tCALLEJON AGUA ZARCA 5 OLIVARES\t ', '\t6622162150\t ', '\tverificacion2000@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('70', 'FORTE INGENIERIA EN OBRAS Y PROYECTOS S. DE R.L. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_13992D-E01FB6-7C422E-F04E8F-2FEB88-5CB03C.jpg', 'OLGA CAMOU 17 SAN JUAN', '6629486966', 'FORTEIOP@HOTMAIL.COM', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('71', '\tCONSTRUCTORA RODO S.A. DE C.V.\t ', '', '', '', '\tLEOCADIO SALCEDO 136, JESUS GARCIA\t ', '\t6623011086\t ', '\tcontratistarodo@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('72', '\tEPS Y ASOCIADOS OBRA CIVIL Y TERRACERIAS S.A.S. DE C.V.\t ', '', '', '', '\tCALLE JULIA TRUJILLO 1019, HOGAR Y PATRIMONIO\t ', '\t6444181037\t ', '\tcontacto@princeconstrucciones.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('73', '\tGUSTAVO MACHADO BELTRAN\t ', '', '', '', '\tDONATO GUERRA 1252 SOCHILOA\t ', '\t6444160286\t ', '\tgmb-64@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('74', '\tFERNANDO ENRIQUE AVILA GAMBOA\t ', '', '', '', '\tAV. MANUEL OJEDA RIVERA, VILLA DE SERIS\t ', '\t6622518332\t ', '\tfernando.avila@industrialavila.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('75', '\tHUGO CESAR NAVARRO BELTRÁN\t ', '', '', '', '\tAVENIDA ENRIQUE GARCÍA SÁNCHEZ 100 C CENTRO\t ', '\t6623026667\t ', '\thugolfnavarro@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('76', '\tGERENCIA DE PROYECTOS IEQ S.A. DE C.V.\t ', '', '', '', '\tQUINTA ALEGRE 15, LAS QUINTAS\t ', '\t6625039755\t ', '\tinfogerpro@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('77', '\tIBLOP CONSTRUCCIONES S.A. DE C.V.\t ', '', '', '', '\tCIRCUITO VENECIA 96 VILLA DORADA\t ', '\t6424228028\t ', '\tiblopconstrucciones@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('78', '\tINFRANOR S.A. DE C.V.\t ', '', '', '', '\tDOCTOR PESQUEIRA 70 A, CENTENARIO\t ', '\t0000000000\t ', '\tinvino2014@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('79', 'INGENIERIA Y CONSTRUCCIONES SABODEPA S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_34AE0F-2863EE-CCB5D6-3AB395-B7CB4C-28A80B.jpg', 'ANGEL ARREOLA 119 VALLE DORADO', '6622192218', 'contabilidad_icssa@hotmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('80', '\tINGENIERIA PAAY S.A. DE C.V.\t ', '', '', '', '\tHIDALGO 619 6 ALTOS, CENTRO\t ', '\t6441699688\t ', '\tingenieria_paay@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('81', '\tJESUS ANDRES PACHECO GONZALEZ\t ', '', '', '', '\tAVE TLAXCALA 334 LOCAL 1 OLIVARES\t ', '\t6622605228\t ', '\tandres_pacheco@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('82', '\tINMOBILIARIA SITTEN AYALA S.A. DE C.V.\t ', '', '', '', '\tAV. SEGURO SOCIAL 30, MODELO\t ', '\t6622540088\t ', '\tsitten_trax@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('83', 'MAQUIEQUIPOS JC S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_62D066-8025A3-6143CD-F79DFB-1FF1F1-EE84C7.jpg', 'PRIMERA DE BUGAMBILIAS 85, BUGAMBILIAS', '6622600957', 'jorlo2001@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('84', 'LESPI PROYECTOS E INGENIERIA S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_BF980F-0210AE-D1CF2C-3ED253-2D78D9-8AF6E9.jpg', 'CALLE ALDAMA 106 SAN BENITO', '6622855866', 'lespiproyectos@yahoo.com.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('85', '\tPROTEKO DESARROLLOS E INFRAESTRUCTURA S.A. DE C.V.\t ', '', '', '', '\tCALIFORNIA 326 - 5 NORTE, CENTRO\t ', '\t6444154197\t ', '\tproteko@prodigy.net.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('86', 'OCEANUS SUPERVISION Y PROYECTOS S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_8C3345-285E45-3A6CA3-6FF8F5-54940A-57D4B2.png', 'AVENIDA 1 61 SAN VICENTE', '6222219302', 'oceanussyp@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('87', '\tSCJ CONSTRUCTORA S.A. DE C.V.\t ', '', '', '', '\tDE LAS FERIAS 11723 1, PUEBLO BONITO\t ', '\t6424222489\t ', '\tscjconstruye@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('88', 'SECOIP S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_71E133-D6AE71-768D03-FD3B10-82E96D-18C862.jpg', 'VILLA SIENA 2, VILLAS DEL MEDITERRANEO', '6622000429', 'felipefimbresr@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('89', '\tCONSTRUCCIONES Y DISEÑOS OPOSURA S.A. DE C.V.\t ', '', '', '', '\tAVE. TENOCHTITLAN 180 VALLE DEL MARQUEZ\t ', '\t6622542712\t ', '\tOPOSURADO@YAHOO.COM.MX\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('90', '\tPETROTRANSMODAL S.C. DE R.L. DE C.V.\t ', '', '', '', '\tAVE PLAN DE AGUA PRIETA 254 LEY 57\t ', '\t6622605228\t ', '\tadministracion@petrotransmodal.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('91', 'CALLIANDRA CONSTRUCTORA', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_151F3E-66A40D-A4613C-9DE8F5-0755D9-D6DACF.jpg', 'LEON GUZMAN 21, CONSTITUCION', '6622149504', 'calliandraconstructora@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('92', '\tCLAMI INGENIEROS SA DE CV\t ', '', '', '', '\tARIZONA 333 A, BALDERRAMA\t ', '\t6622148036\t ', '\ttocaing@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('93', '\tCONCRETOS Y AGREGADOS DE CAJEME\t ', '', '', '', '\tCARRETERA FEDERAL MEXICO 015 NTE. 3101 2, ZONA URBANA\t ', '\t6444 20 22 90\t ', '\tinformes@icsa.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('94', '\tUNIVERSO ROJO S.A. DE C.V.\t ', '', '', '', '\tCHIAPAS 807 JUAREZ\t ', '\t6424273202\t ', '\ting_bojorquez9@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('95', '\tVERTICA PROYECTOS ARQUITECTONICOS Y SUPERVISION S.C.\t ', '', '', '', '\tBLVD. LAZARO CARDENAS 10 K AMPLIACION LADRILLERAS\t ', '\t6622620561\t ', '\tadministracion@verticasc.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('96', '\tCONSTRUCCIONES RENAV S.A. DE C.V.\t ', '', '', '', '\tBABILOMO 6 12, VALLEVERDE\t ', '\t0000000000\t ', '\tconstruccionesrenav@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('97', '\tACSA CONSTRUCTORES\t ', '', '', '', '\tJUAN JOSE AGUIRRE 19 BALDERRAMA\t ', '\t6622154309\t ', '\tacsaconstructores@outlook.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('98', '\tCONSTRUCCIONES Y URBANIZACIONES RG\t ', '', '', '', '\tTERRENATE 20, RESIDENCIAL DE ANZA\t ', '\t6628486194\t ', '\tcyurgsa@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('99', 'ALFA LEAN CONSTRUCTIONS SA DE CV', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_181A4A-870E99-475D76-533696-AB14A6-755DBF.jpg', 'PRIVADA MORELIA 56 CENTRO', '6621076134', 'GSAMANIEGO@ALFALEANCONSTRUCTION.COM', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('100', '\tEdificaciones caminos y puentes del sur\t ', '', '', '', '\tPARROQUIA 13, VILLA SATELITE\t ', '\t17441887557\t ', '\tecapusa.2016@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('101', 'ARQCO ARQUITECTOS S.C.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_37D954-F7C0EB-E84A4B-7C6503-89C7B0-1A8BAB.png', 'BLVD. MORELOS 389 LOCAL 20 BACHOCO', '2110009', 'penelope@arqcoenlinea.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('102', '\tBell Hill SA de CV\t ', '', '', '', '\tGUADALUPE VICTORIA 121 SAN BENITO\t ', '\t6622366556\t ', '\thector.cuellar@panelrex.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('103', '\tGRUPO MERCLA\t ', '', '', '', '\tARIZONA 333 ALTOS, BALDERRAMA\t ', '\t6622148036\t ', '\tgrupomercla@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('104', '\tGrupo Valenzuela Porchas\t ', '', '', '', '\tPASEO DE LAS FUENTES 33, VALLE VERDE\t ', '\t6622160535\t ', '\tadministracion@penaporchas.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('105', 'ECLIPSE DEL NOROESTE', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_630E16-AA7048-87B5AE-7B6EB2-BBE3FE-30DAE3.jpg', 'ALFREDO EGUIARTE 268 10 jesus garcia', '6622060704', 'hugollano.2393@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('106', '\tESTRUKTURAL BIENES RAICES S.A DE C.V\t ', '', '', '', '\tBLVD HIDALGO 70 centenario\t ', '\t2121242\t ', '\tmarthagd@derex.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('107', '\tGSR GLOBAL STRATEGIC RESOURCES\t ', '', '', '', '\tBOULEVARD GARCIA MORALES KM 6.5 el llano\t ', '\t2607775\t ', '\td.rubio@ryrce.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('108', '\tJONATAN EULALIO RIVERA MACOTELA\t ', '', '', '', '\tINDEPENDENCIA 336, 2 DE ABRIL\t ', '\t0000000000\t ', '\tjr2group@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('109', '\tJESUS ROBERTO MALDONADO GASTELUM\t ', '', '', '', '\tmiguel hidalgo 354 palmar del sol\t ', '\t6622606342\t ', '\tjr_maldonadog@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('110', 'MAQUINARIA Y TRANSPORTE MVP', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_A256C4-9BA23A-8E2247-311A21-3AF757-5AAD47.jpg', 'LUIS DONALDO COLOSIO 294 F, PRADOS DE CENTENARIO', '6622120936', 'info@maquinaria.biz', 'http://www.maquinaria.biz/', 'https://www.facebook.com/makinaryamvp/', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('111', '\tJOPADA EDIFICACIONES\t ', '', '', '', '\tARDILLAS 115 casa linda\t ', '\t6622414172\t ', '\tjopada.edificaciones@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('112', '\tJUAN ISLAS BELTRAN\t ', '', '', '', '\tCALLES 17 Y 18 AV. 42 S/N NUEVO PROGRESO\t ', '\t6333334699\t ', '\tISLAS_BELTRANJUAN@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('113', '\tproyectos y supervisón de hermosillo\t ', '', '', '', '\tREAL DEL ARCO 26, VILLA SATELITE\t ', '\t6622564219\t ', '\tgamezosio@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('114', 'RUBIO Y RUBIO CONSTRUCCIÓN ESPECIALIZADAS', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_F64255-2FEE3B-270CE8-6708A2-163F80-3D8479.jpg', 'BLVD. GARCIA MORALES KM. 6.5 S/N EL LLANO', '2607775', 'd.rubio@ryrce.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('115', '\tSASAEM CONSTRUCCION Y SERVICIOS MULTIPLES S.A. DE C.V.\t ', '', '', '', '\tVALLE DE ALGODON 1545, VILLA FONTANA\t ', '\t6441796329\t ', '\tsasaem.construccion@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('116', '\tEQUIPLAN S.A. DE C.V.\t ', '', '', '', '\tPASEO VALLE GRANDE 134 INT.3, VALLE GRANDE\t ', '\t6622600957\t ', '\tcarlos.lopez@equiplan.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('117', '\tSIT COMUNICACIONES, S.A. DE C.V. S.A. DE C.V.\t ', '', '', '', '\tAV. DEL PASEO 65, PASEO DEL SOL\t ', '\t6623119216\t ', '\tsithermosillo@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('118', '\tTOCA INGENIEROS S.C.\t ', '', '', '', '\tCALLEJON BENJAMIN HILL 60, SAN BENITO\t ', '\t6622148036\t ', '\tmeridamillan@prodigy.net.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('119', '\tCAMANEY CONSTRUCCION Y DESARROLLO S.A. DE C.V.\t ', '', '', '', '\tGUILLERMO ARREOLA 243, OLIVARES\t ', '\t6622185521\t ', '\tJOSE.AGUIRRE210457@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('120', '\tINMOBILIARIA CERRO COLORADO DE ESQUEDA S.A. DE C.V.\t ', '', '', '', '\tCALLE DE LA REFORMA 237, CENTENARIO\t ', '\t6622825618\t ', '\tINMCCDE@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('121', '\tCETYA CONSTRUCCIONES S.A. DE C.V.\t ', '', '', '', '\tAVENIDA MARTIN DE VALENCIA 647, PROGRESISTA\t ', '\t6622168778\t ', '\tCETYACONSTRUCCIONES@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('122', 'CLIMA 3 DEL PACIFICO S.A DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_D3AEC1-55A487-705CAD-D8BE2F-3D88B7-FF3478.jpg', 'AVE. ALFREDO EGUIARTE 90, JESUS GARCIA', '6623692884', 'RNS@CLIMA3.MX', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('123', '\tIMT CONSTRUCCIONES S. DE R.L. DE C.V.\t ', '', '', '', '\tPAPALOTE 47, HACIENDAS\t ', '\t6621572738\t ', '\tIMT_CONSTRUCCION@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('124', '\tISAFRA CONSTRUCCIONES S.A. DE C.V.\t ', '', '', '', '\tAGUA FRIA 22, SANTA FE\t ', '\t6621036415\t ', '\tmaxfregoso@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('125', '\tEDIFICADORA Y COMERCIALIZADORA JUAN PABLO II S.A. DE C.V.\t ', '', '', '', '\tJOSE CARMELO 141, SAN BENITO\t ', '\t6623003210\t ', '\tMACONTR02@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('126', '\tLUIS MIRANDA ROMERO\t ', '', '', '', '\tTEPIC 44, PALO VERDE\t ', '\t6622279771\t ', '\tINDICO.HMO@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('127', '\tMOCUZARI CONSTRUCTORA S.A DE C.V.\t ', '', '', '', '\tIXTLAXIHUATL 75, Y GRIEGA\t ', '\t6623011251\t ', '\tGERMANING1223@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('128', '\tCONSTRUCTORA MUSARO S.A. DE C.V.\t ', '', '', '', '\tCALLEJON JOSE MARIA MENDOZA 961, EL SAHUARO\t ', '\t6622139640\t ', '\tCONSTRUCTORA_MUSARO@YAHOO.COM.MX\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('129', '\tNIKO CONSTRUCCIONES S.A. DE C.V.\t ', '', '', '', '\tSAN FELIPE 123, SAN ANGEL\t ', '\t6622566283\t ', '\talberto_sanchez_encinas@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('130', '\tGRUPO NORIGA CONSTRUCCIONES S.A. DE C.V.\t ', '', '', '', '\tJUAN ESCUTIA 29, LET ECHEVERRIA\t ', '\t6441216930\t ', '\tDIRECCION.NORIGA@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('131', '\tPANELREX S.A. DE C.V.\t ', '', '', '', '\tGUADALUPE VICTORIA 121, SAN BENITO\t ', '\t6622366556\t ', '\thector.cuellar@panelrex.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('132', '\tTONAHUAC GLOBAL TRADING S.A. DE C.V.\t ', '', '', '', '\tJOHN F KENNEDY 61 D2, SAN JUAN\t ', '\t6622121554\t ', '\tTONAHUACGT@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('133', '\tVARANTECH DE MEXICO S.A. DE C.V.\t ', '', '', '', '\tGUADALUPE VICTORIA 10 INT.3, SAN BENITO\t ', '\t3011559\t ', '\tEDGARDO_ANGUIS@ESPACIODU.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('134', 'ALFARAENA S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_E3E2E2-04EBB3-DDDBA5-5E2633-80D618-E9DA10.jpg', 'PODER LEGISLATIVO 140, MISION DEL REAL', '2123093', 'alfaraenaconstrucciones@hotmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('135', '\tARQUINFRA S.A. DE C.V.\t ', '', '', '', '\tCIRCUITO LOMA ALTA 89, PRIVADA LAS LOMAS\t ', '\t6622515371\t ', '\tarquinfra@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('136', '\tACVT INGENIERIA S.A. DE C.V.\t ', '', '', '', '\tFRACC SIERRA BLANCA 78, FRACC SIERRA BLANCA\t ', '\t6622112145\t ', '\tfranciscavazqueztrillas@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('137', '\tJAIME WENCESLAO PARRA MOROYOQUI\t ', '', '', '', '\tBVLD. LOS ALAMOS 402, SIERRA VISTA\t ', '\t63163131704\t ', '\tjaimeparramo@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('138', '\tJUAN MENDOZA CASTRO\t ', '', '', '', '\tQUINTANA ROO 727 NTE. noroeste\t ', '\t6441695172\t ', '\tJUANMENDOZA1@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('139', '\tCRIBADOS Y OBRAS S.A. DE C.V.\t ', '', '', '', '\tCOAHUILA 816 SUR, CENTRO\t ', '\t6444175018\t ', '\tSALMARVI@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('140', '\tMARAGUA CONSTRUCCIONES SA DE CV\t ', '', '', '', '\tsosa chavez 409 deportiva\t ', '\t6424224546\t ', '\tMARAGUACONSTRUCCIONES@YAHOO.COM.MX\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('141', '\tEDIFICACIONES Y PROYECTOS MOCELIK S.A. DE C.V.\t ', '', '', '', '\tARIZONA 333 BALDERRAMA\t ', '\t6622127843\t ', '\tEPMOCELICK@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('142', '\tDESARROLLOS TECNOLOGICOS DEL NOROESTE S.A. DE C.V.\t ', '', '', '', '\tVILLA MARIA 10, VILLA SOL\t ', '\t6622130072\t ', '\trecepcion@dtnmex.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('143', 'PITIC SINERGIA CONSTRUCTIVA S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_48AA67-73FF41-5603BB-FCD6AD-23F86A-B9804B.jpg', 'GUADALUPE VICTORIA 143 C SAN BENITO', '6621541686', 'CRLIMON@HOTMAIL.COM', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('144', '\tFRANCISCO JAVIER TOLEDO RUIZ\t ', '', '', '', '\tMADERO 184, CENTRO\t ', '\t6444180294\t ', '\tjavier_toledoruiz@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('145', '\tRAFAEL QUINTERO LOPEZ\t ', '', '', '', '\tAVENIDA LIBERTAD Y CALLE 13 1210 RESIDENCIAS\t ', '\t6538497103\t ', '\tacclaboratorio@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('146', '\tGRARUB CONSTRUCCIONES\t ', '', '', '', '\tPASEO SANTA FE 57, SANTA FE\t ', '\t6622671229\t ', '\tmarielit@msn.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('147', '\tINMOBILIARIA TIERRAS DEL DESIERTO S.A DE C.V S.A. DE C.V.\t ', '', '', '', '\tPUERTO VALLARTA 4, CAFE COMBATE\t ', '\t6621138575\t ', '\tspencervlza@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('148', '\tRODRIGO APONTE ESPINOSA\t ', '', '', '', '\tPASEO DEL PRADO 39 VALLE GRANDE\t ', '\t6623160224\t ', '\tprocodesa.construcciones@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('149', '\tINMOBILIARIA SOCE S.A. DE C.V.\t ', '', '', '', '\tDR. PALIZA 78, CENTENARIO\t ', '\t6622132712\t ', '\tINM.SOCE@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('150', '\tSCULL GROUP DE MÉXICO\t ', '', '', '', '\tLUIS ORCI 472 CENTRO\t ', '\t2157792\t ', '\tSCULLGROUP@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('151', '\tJESUS ALFONSO VALENZUELA FIGUEROA\t ', '', '', '', '\tMISIÓN DE OQUITOA 19, FRACCIONAMIENTO AMANECER DE KINO\t ', '\t63210255733\t ', '\tJAVALENZUELA@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('152', '\tTERPLAN SA E C\t ', '', '', '', '\tVERACRUZ 467 LOCAL 3 ZONA NORTE\t ', '\t6444154699\t ', '\tTERPLAN_OBREGON@YAHOO.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('153', '\tACARREOS Y AGREGADOS AR S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6421169182\t ', '\tarsagregados@yahoo.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('154', 'CONSTRUCCIONES Y EDIFICACIONES SAYG S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_861F8D-6F023F-BF4713-57C652-7941AE-948438.jpg', 'DOMICILIO CONOCIDO', '6424223188', 'const.sayg@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('155', 'BARREDA PROYECTO Y CONSTRUCCIONES S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_176B43-E003E6-1A8550-73164E-7A25C6-350B29.jpg', 'DOMICILIO CONOCIDO', '2100415', 'administracion@bapco.com.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('156', '\tCONSTRUCCIONES E INSTALACIONES DEL PITIC S.A. DE C.V.\t ', '', '', '', '\tGUADALUPE VICTORIA 143 C SAN BENITO\t ', '\t6622189730\t ', '\tcpitic@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('157', 'CONSTRUCTORA GARACO S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_DFE7EA-B9466B-BBA01A-FAC113-AB1029-888B5E.jpg', 'DOMICILIO CONOCIDO', '6622133863', 'constructoragaraco84@yahoo.com.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('158', '\tCOGRE CONSTRUCCIONES S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6621680941\t ', '\trussell_57mx@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('159', '\tCONSTRUCCIONES Y ASESORIAS XIRO S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6622168578\t ', '\tcoaxiro@yahoo.com.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('160', 'COSTOS Y CALIDAD DE OBREGON S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_6916C5-695A42-99E33C-9D4646-4C9093-95E279.png', 'DOMICILIO CONOCIDO', '6441392439', 'cosco.obregon@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('161', '\tDEINNIC S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6622605228\t ', '\tadministracion@electroinsumos.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('162', '\tDAGOBERTO RODRIGUEZ KIRKBRIDE\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6444159339\t ', '\tadmon.drk@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('163', '\tDISTRIBUIDORA ELECTRICA GARZA S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t0000000000\t ', '\tgarza012013@outlook.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('164', '\tDISTRIBUCIONES Y DESARROLLOS GALAL S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6624364284\t ', '\tGALINDO_FIERRO@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('165', 'GYG CONSTRUCTORES S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_D47428-E7BF32-48D21B-A917E0-7E7475-C5D159.png', 'DOMICILIO CONOCIDO', '6621200234', 'REZ@GYGCONSTRUCTORES.COM.MX', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('166', '\tELECTRO INGENIERIA DEL PACIFICO S.A. DE C.V.\t ', '', '', '', '\tAVENIDA ALFREDO EGUIARTE COL. JESUS GARCÍA\t ', '\t6622080100\t ', '\tRNS@CLIMA3.MX\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('167', 'GUJA DISTRIBUIDORA S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_D660F6-7BEBB2-6A2A9C-8301B0-A1AD22-059A5A.jpg', 'DOMICILIO CONOCIDO', '0000000000', 'gujaconstructores@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('168', '\tINMOBILIARIA CARLOS ALBERTO S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t2130045\t ', '\tcmiramarsadecv@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('169', '\tINVMEX S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t0000000000\t ', '\tmiguelruelas@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('170', '\tISRAEL TOLANO CHACON\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t0000000000\t ', '\tmiguelruelas@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('171', '\tJOSE SEPCHEM ARMENTA NAVA\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6623536293\t ', '\tjosearmenta70@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('172', '\tJOSE MARIA QUINTANA ALVAREZ\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6424835201\t ', '\tINGQUINTANA@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('173', 'LACOSI S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_D2B90D-626F86-EE0D20-99DAF5-DD7A20-A4087A.png', 'DOMICILIO CONOCIDO', '6622102941', 'contacto@lacosi.com.mx', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('174', '\tLAURA MARIA CAMARGO ROJO\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6424829249\t ', '\tLCAMARGOROJO@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('175', '\tLECTA CONSTRUCCIONES S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6622760188\t ', '\tjimm.ledezma@lecta.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('176', '\tMANUEL ROGELIO GONZALEZ FERRA MARTINEZ\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6622048577\t ', '\tROGER_G14@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('177', '\tMONTAJES Y TENDIDOS DE POTENCIA S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6622605167\t ', '\tmtp1@outlook.es\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('178', 'OSMAN INDUSTRIAL S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_78FC27-DE67C2-EAC47B-4C619D-4B49F7-80D6ED.jpg', 'DOMICILIO CONOCIDO', '6621916227', 'osmanindustrial@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('179', '\tPAVIMENTOS Y DESARROLLOS KAT S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t66212760040\t ', '\telgudo@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('180', '\tEDIFICACIONES Y PROYECTOS SAN CARLOS S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t622 222 0803\t ', '\teypsancarlos@yahoo.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('181', 'PUEBLA ARQUITECTOS CONSTRUCCION S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_36E031-DE7F08-5F62B5-4E5F36-525AEE-221715.jpg', 'DOMICILIO CONOCIDO', '2137846', 'pueblaarquitectos@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('182', '\tREME SISTEMAS CONSTRUCTIVOS S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6474263478\t ', '\tRMSISTEMASCONSTRUCTIVOS@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('183', 'ROOX ESTUDIO S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_98C79F-5C4E51-362C3D-C6672A-331FD5-0E1D25.jpg', 'DOMICILIO CONOCIDO', '6622595500', 'rocio-robles84@hotmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('184', '\tSCALACK S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t0000000000\t ', '\tcortinas.cmh@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('185', '\tSISTEMAS Y PROCESOS DE INGENIERIA Y CONSTRUCCION S.A.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6444163936\t ', '\tconradosanchezd@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('186', '\tSOMECON S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6621203674\t ', '\tsomedesarrollos@gmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('187', 'TEMARQ S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_D1D4A8-7F94CA-CFA9B3-605369-089BB8-76668C.png', 'DOMICILIO CONOCIDO', '6441694202', 'temarqsa@gmail.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('188', 'VALPO DESARROLLOS S.A. DE C.V.', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'COM_FAD3EC-C502DB-62F4D3-165B1B-604630-59C7F8.jpg', 'DOMICILIO CONOCIDO', '6222161768', 'administracion@grupovalpo.com', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('189', '\tXXI CONSTRUCCIONES S.A. DE C.V.\t ', '', '', '', '\tDOMICILIO CONOCIDO\t ', '\t6424222777\t ', '\trmartinez@xxiconstrucciones.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('190', '\tRAUL MANUEL TERAN SALCIDO\t ', '', '', '', '\tOTHON ALMADA 230, BALDERRAMA\t ', '\t2157537\t ', '\telectrosupplier@prodigy.net.mx\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('191', '\tPROYECTOS Y EDIFICACIONES RANDA S.A. DE C.V.\t ', '', '', '', '\tAGUASCALIENTES 158 1, SAN BENITO\t ', '\t6622114053\t ', '\ting_javitre@hotmail.com\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('192', '\tJose de Jesus Rubio Morales\t ', '', '', '', '\tNAVOJOA 1144, CAMINO REAL\t ', '\t6623118016\t ', '\tRUBIO_SIEMS@HOTMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('193', '\tFRANCISCO JAVIER CORONADO CASTRO\t ', '', '', '', '\tCAÑADA 2213, LAS MISIONES\t ', '\t6441257743\t ', '\tFCOJCORONADOCASTRO@GMAIL.COM\t ', '', '', '', '', '', '2019-09-18 03:00:00');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('206', 'Empresa de prueba', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '', '', 'mail@prueba.com', '', '', '', '', '', '2019-09-21 14:30:22');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('207', 'KARINA', '&lt;p&gt;KARUNA&lt;span id=&quot;sceditor-end-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;span id=&quot;sceditor-start-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;/p&gt;', '&lt;p&gt;KARINA&lt;span id=&quot;sceditor-end-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;span id=&quot;sceditor-start-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;/p&gt;', '', '123 Burke Street, Toronto ON, CANADA', '', 'k.wilson.a@gmail.com', '', '', '', '', '', '2019-09-23 13:56:17');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('208', 'CONOCIDO', '&lt;p&gt;&lt;span id=&quot;sceditor-end-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;span id=&quot;sceditor-start-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;Es una compañia grande&lt;/p&gt;', '&lt;p&gt;&lt;span id=&quot;sceditor-end-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;span id=&quot;sceditor-start-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;Es una compañia grande&lt;br&gt;&lt;/p&gt;', '', 'CONOCIDO', '', 'lprueba@prueba.com', '', '', '', '', '', '2019-09-23 14:43:18');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('211', 'empleador Company', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '123 Burke Street, Toronto ON, CANADA', '', 'luisfernandobarrazamartinez@gmail.com', '', '', '', '', '', '2019-09-24 17:32:22');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('219', 'empleador', '&lt;p&gt;&lt;span id=&quot;sceditor-end-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;span id=&quot;sceditor-start-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;br&gt;&lt;/p&gt;', '&lt;p&gt;&lt;br&gt;&lt;/p&gt;', '', '123 Burke Street, Toronto ON, CANADA', '', 'k.wilson.a@gmail.com', '', '', '', '', '', '2019-09-27 13:05:04');
INSERT INTO `companies` (`uid`, `name`, `about`, `business`, `avatar`, `address`, `phone`, `email`, `website`, `facebook`, `twitter`, `linkedin`, `gplus`, `created`) VALUES ('225', 'Mi empresa', '&lt;p&gt;&lt;span id=&quot;sceditor-end-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;span id=&quot;sceditor-start-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;prueba&lt;/p&gt;', '&lt;p&gt;&lt;span id=&quot;sceditor-end-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;&lt;span id=&quot;sceditor-start-marker&quot; class=&quot;sceditor-selection sceditor-ignore&quot; style=&quot;line-height: 0; display: none;&quot;&gt; &lt;/span&gt;prueba&lt;br&gt;&lt;/p&gt;', 'COM_B725D8-2FCFF5-17B2AD-A88542-AED0A3-8B4F74.png', 'prueba', '6621010101', 'prueba@example.com', '', '', '', '', '', '2019-10-01 15:16:36');


-- --------------------------------------------------
# -- Table structure for table `countries`
-- --------------------------------------------------
DROP TABLE IF EXISTS `countries`;
CREATE TABLE `countries` (
  `id` smallint(6) DEFAULT NULL,
  `abbr` varchar(6) DEFAULT NULL,
  `name` varchar(210) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `home` tinyint(1) DEFAULT NULL,
  `vat` decimal(7,0) DEFAULT NULL,
  `sorting` smallint(6) DEFAULT NULL,
  KEY `idx` (`abbr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `countries`
-- --------------------------------------------------

INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('1', 'AF', 'Afghanistan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('2', 'AL', 'Albania', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('3', 'DZ', 'Algeria', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('4', 'AS', 'American Samoa', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('5', 'AD', 'Andorra', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('6', 'AO', 'Angola', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('7', 'AI', 'Anguilla', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('8', 'AQ', 'Antarctica', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('9', 'AG', 'Antigua and Barbuda', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('10', 'AR', 'Argentina', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('11', 'AM', 'Armenia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('12', 'AW', 'Aruba', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('13', 'AU', 'Australia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('14', 'AT', 'Austria', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('15', 'AZ', 'Azerbaijan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('16', 'BS', 'Bahamas', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('17', 'BH', 'Bahrain', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('18', 'BD', 'Bangladesh', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('19', 'BB', 'Barbados', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('20', 'BY', 'Belarus', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('21', 'BE', 'Belgium', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('22', 'BZ', 'Belize', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('23', 'BJ', 'Benin', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('24', 'BM', 'Bermuda', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('25', 'BT', 'Bhutan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('26', 'BO', 'Bolivia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('27', 'BA', 'Bosnia and Herzegowina', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('28', 'BW', 'Botswana', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('29', 'BV', 'Bouvet Island', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('30', 'BR', 'Brazil', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('31', 'IO', 'British Indian Ocean Territory', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('32', 'VG', 'British Virgin Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('33', 'BN', 'Brunei Darussalam', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('34', 'BG', 'Bulgaria', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('35', 'BF', 'Burkina Faso', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('36', 'BI', 'Burundi', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('37', 'KH', 'Cambodia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('38', 'CM', 'Cameroon', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('39', 'CA', 'Canada', '1', '', '13', '1000');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('40', 'CV', 'Cape Verde', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('41', 'KY', 'Cayman Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('42', 'CF', 'Central African Republic', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('43', 'TD', 'Chad', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('44', 'CL', 'Chile', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('45', 'CN', 'China', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('46', 'CX', 'Christmas Island', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('47', 'CC', 'Cocos (Keeling) Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('48', 'CO', 'Colombia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('49', 'KM', 'Comoros', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('50', 'CG', 'Congo', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('51', 'CK', 'Cook Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('52', 'CR', 'Costa Rica', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('53', 'CI', 'Cote D&#39;ivoire', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('54', 'HR', 'Croatia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('55', 'CU', 'Cuba', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('56', 'CY', 'Cyprus', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('57', 'CZ', 'Czech Republic', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('58', 'DK', 'Denmark', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('59', 'DJ', 'Djibouti', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('60', 'DM', 'Dominica', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('61', 'DO', 'Dominican Republic', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('62', 'TP', 'East Timor', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('63', 'EC', 'Ecuador', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('64', 'EG', 'Egypt', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('65', 'SV', 'El Salvador', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('66', 'GQ', 'Equatorial Guinea', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('67', 'ER', 'Eritrea', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('68', 'EE', 'Estonia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('69', 'ET', 'Ethiopia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('70', 'FK', 'Falkland Islands (Malvinas)', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('71', 'FO', 'Faroe Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('72', 'FJ', 'Fiji', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('73', 'FI', 'Finland', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('74', 'FR', 'France', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('75', 'GF', 'French Guiana', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('76', 'PF', 'French Polynesia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('77', 'TF', 'French Southern Territories', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('78', 'GA', 'Gabon', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('79', 'GM', 'Gambia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('80', 'GE', 'Georgia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('81', 'DE', 'Germany', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('82', 'GH', 'Ghana', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('83', 'GI', 'Gibraltar', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('84', 'GR', 'Greece', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('85', 'GL', 'Greenland', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('86', 'GD', 'Grenada', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('87', 'GP', 'Guadeloupe', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('88', 'GU', 'Guam', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('89', 'GT', 'Guatemala', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('90', 'GN', 'Guinea', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('91', 'GW', 'Guinea-Bissau', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('92', 'GY', 'Guyana', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('93', 'HT', 'Haiti', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('94', 'HM', 'Heard and McDonald Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('95', 'HN', 'Honduras', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('96', 'HK', 'Hong Kong', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('97', 'HU', 'Hungary', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('98', 'IS', 'Iceland', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('99', 'IN', 'India', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('100', 'ID', 'Indonesia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('101', 'IQ', 'Iraq', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('102', 'IE', 'Ireland', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('103', 'IR', 'Islamic Republic of Iran', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('104', 'IL', 'Israel', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('105', 'IT', 'Italy', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('106', 'JM', 'Jamaica', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('107', 'JP', 'Japan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('108', 'JO', 'Jordan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('109', 'KZ', 'Kazakhstan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('110', 'KE', 'Kenya', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('111', 'KI', 'Kiribati', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('112', 'KP', 'Korea, Dem. Peoples Rep of', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('113', 'KR', 'Korea, Republic of', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('114', 'KW', 'Kuwait', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('115', 'KG', 'Kyrgyzstan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('116', 'LA', 'Laos', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('117', 'LV', 'Latvia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('118', 'LB', 'Lebanon', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('119', 'LS', 'Lesotho', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('120', 'LR', 'Liberia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('121', 'LY', 'Libyan Arab Jamahiriya', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('122', 'LI', 'Liechtenstein', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('123', 'LT', 'Lithuania', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('124', 'LU', 'Luxembourg', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('125', 'MO', 'Macau', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('126', 'MK', 'Macedonia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('127', 'MG', 'Madagascar', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('128', 'MW', 'Malawi', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('129', 'MY', 'Malaysia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('130', 'MV', 'Maldives', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('131', 'ML', 'Mali', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('132', 'MT', 'Malta', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('133', 'MH', 'Marshall Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('134', 'MQ', 'Martinique', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('135', 'MR', 'Mauritania', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('136', 'MU', 'Mauritius', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('137', 'YT', 'Mayotte', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('138', 'MX', 'Mexico', '1', '1', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('139', 'FM', 'Micronesia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('140', 'MD', 'Moldova, Republic of', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('141', 'MC', 'Monaco', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('142', 'MN', 'Mongolia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('143', 'MS', 'Montserrat', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('144', 'MA', 'Morocco', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('145', 'MZ', 'Mozambique', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('146', 'MM', 'Myanmar', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('147', 'NA', 'Namibia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('148', 'NR', 'Nauru', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('149', 'NP', 'Nepal', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('150', 'NL', 'Netherlands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('151', 'AN', 'Netherlands Antilles', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('152', 'NC', 'New Caledonia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('153', 'NZ', 'New Zealand', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('154', 'NI', 'Nicaragua', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('155', 'NE', 'Niger', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('156', 'NG', 'Nigeria', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('157', 'NU', 'Niue', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('158', 'NF', 'Norfolk Island', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('159', 'MP', 'Northern Mariana Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('160', 'NO', 'Norway', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('161', 'OM', 'Oman', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('162', 'PK', 'Pakistan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('163', 'PW', 'Palau', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('164', 'PA', 'Panama', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('165', 'PG', 'Papua New Guinea', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('166', 'PY', 'Paraguay', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('167', 'PE', 'Peru', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('168', 'PH', 'Philippines', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('169', 'PN', 'Pitcairn', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('170', 'PL', 'Poland', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('171', 'PT', 'Portugal', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('172', 'PR', 'Puerto Rico', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('173', 'QA', 'Qatar', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('174', 'RE', 'Reunion', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('175', 'RO', 'Romania', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('176', 'RU', 'Russian Federation', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('177', 'RW', 'Rwanda', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('178', 'LC', 'Saint Lucia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('179', 'WS', 'Samoa', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('180', 'SM', 'San Marino', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('181', 'ST', 'Sao Tome and Principe', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('182', 'SA', 'Saudi Arabia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('183', 'SN', 'Senegal', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('184', 'RS', 'Serbia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('185', 'SC', 'Seychelles', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('186', 'SL', 'Sierra Leone', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('187', 'SG', 'Singapore', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('188', 'SK', 'Slovakia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('189', 'SI', 'Slovenia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('190', 'SB', 'Solomon Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('191', 'SO', 'Somalia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('192', 'ZA', 'South Africa', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('193', 'ES', 'Spain', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('194', 'LK', 'Sri Lanka', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('195', 'SH', 'St. Helena', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('196', 'KN', 'St. Kitts and Nevis', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('197', 'PM', 'St. Pierre and Miquelon', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('198', 'VC', 'St. Vincent and the Grenadines', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('199', 'SD', 'Sudan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('200', 'SR', 'Suriname', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('201', 'SJ', 'Svalbard and Jan Mayen Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('202', 'SZ', 'Swaziland', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('203', 'SE', 'Sweden', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('204', 'CH', 'Switzerland', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('205', 'SY', 'Syrian Arab Republic', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('206', 'TW', 'Taiwan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('207', 'TJ', 'Tajikistan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('208', 'TZ', 'Tanzania, United Republic of', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('209', 'TH', 'Thailand', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('210', 'TG', 'Togo', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('211', 'TK', 'Tokelau', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('212', 'TO', 'Tonga', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('213', 'TT', 'Trinidad and Tobago', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('214', 'TN', 'Tunisia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('215', 'TR', 'Turkey', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('216', 'TM', 'Turkmenistan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('217', 'TC', 'Turks and Caicos Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('218', 'TV', 'Tuvalu', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('219', 'UG', 'Uganda', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('220', 'UA', 'Ukraine', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('221', 'AE', 'United Arab Emirates', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('222', 'GB', 'United Kingdom (GB)', '1', '', '23', '999');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('224', 'US', 'United States', '1', '', '8', '998');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('225', 'VI', 'United States Virgin Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('226', 'UY', 'Uruguay', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('227', 'UZ', 'Uzbekistan', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('228', 'VU', 'Vanuatu', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('229', 'VA', 'Vatican City State', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('230', 'VE', 'Venezuela', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('231', 'VN', 'Vietnam', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('232', 'WF', 'Wallis And Futuna Islands', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('233', 'EH', 'Western Sahara', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('234', 'YE', 'Yemen', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('235', 'ZR', 'Zaire', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('236', 'ZM', 'Zambia', '1', '', '0', '0');
INSERT INTO `countries` (`id`, `abbr`, `name`, `active`, `home`, `vat`, `sorting`) VALUES ('237', 'ZW', 'Zimbabwe', '1', '', '0', '0');


-- --------------------------------------------------
# -- Table structure for table `email_templates`
-- --------------------------------------------------
DROP TABLE IF EXISTS `email_templates`;
CREATE TABLE `email_templates` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `help` text,
  `body` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `email_templates`
-- --------------------------------------------------

INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('1', 'Correo electrónico de registro', 'Bienvenido a bolsa de trabajo CMIC. Por favor complete su registro!', 'Esta plantilla se utiliza para enviar correos electrónicos de verificación de registro, cuando en Configuración-&gt; Verificación de registro se establece en &quot;SÍ&quot;.', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nBienvenido(a) [NAME]\n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nHola, &lt;br&gt;\n&lt;br&gt;\nAhora eres miembro de [SITE_NAME]. \n&lt;br&gt;\nAquí están sus detalles de inicio de sesión. Guárdelos en un lugar seguro:\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;5&quot; cellspacing=&quot;2&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot; width=&quot;130&quot;&gt;\n&lt;b&gt;Usuario:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[USERNAME]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;&lt;b&gt;Contraseña:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[PASSWORD]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;El administrador de este sitio ha solicitado que todas las cuentas nuevas sean activadas por los usuarios que las crearon, por lo que su cuenta está actualmente inactiva.&lt;br&gt;Para activar su cuenta, visite el siguiente enlace e ingrese lo siguiente:&lt;br&gt;&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;table border=&quot;0&quot; cellpadding=&quot;4&quot; cellspacing=&quot;2&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot; width=&quot;130&quot;&gt;\n&lt;b&gt;Token:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[TOKEN]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;Email:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[EMAIL]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;a href=&quot;[LINK]&quot;&gt;&lt;b&gt;Haga clic aquí para activar su cuenta. &lt;/b&gt;&lt;/a&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;&lt;i&gt;Gracias&lt;br&gt;\n[SITE_NAME]&amp;nbsp;&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('2', 'Olvidé mi contraseña de correo electrónico', 'Restablecimiento de contraseña', 'Esta plantilla se usa para recuperar la contraseña de usuario perdida', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color:#ccc; font-size:16px;padding:5px;border-bottom:2px solid #fff;&quot;&gt;\nNueva contraseña restablecida desde  [SITE_NAME]!\n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nHola, &lt;b&gt;[USERNAME]&lt;/b&gt;&lt;br&gt;\n&lt;br&gt;Parece que usted o alguien solicitó una nueva contraseña para usted.&lt;br&gt;Hemos generado una nueva contraseña, según lo solicitado:&lt;br&gt;Tu nueva contraseña: \n&lt;b&gt;[PASSWORD]&lt;/b&gt;&lt;br&gt;\n&lt;br&gt;\nPara usar la nueva contraseña, debe activarla. Para hacerlo, haga clic en el enlace que se proporciona a continuación e inicie sesión con su nueva contraseña.\n&lt;br&gt;\n&lt;a href=&quot;[LINK]&quot;&gt;[LINK]&lt;/a&gt;&lt;br&gt;\n&lt;br&gt;\nPuede cambiar su contraseña después de iniciar sesión.\n&lt;hr&gt;\nContraseña solicitada de IP: [IP]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;\n&lt;i&gt;Gracias&lt;br&gt;\n[SITE_NAME]&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('3', 'Correo de bienvenida del administrador', 'Has sido registrado', 'Esta plantilla se utiliza para enviar correos electrónicos de bienvenida, cuando el administrador agrega un usuario', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nBienvenido(a) [NAME]\n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nHola, &lt;br&gt;\n&lt;br&gt;\nAhora eres miembro de&amp;nbsp; [SITE_NAME]. \n&lt;br&gt;\nAquí están sus detalles de inicio de sesión. Guárdelos en un lugar seguro:\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;5&quot; cellspacing=&quot;2&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot; width=&quot;130&quot;&gt;\n&lt;b&gt;Usuario:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[USERNAME]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;&lt;b&gt;Contraseña:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[PASSWORD]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;&lt;i&gt;Gracias&lt;br&gt;\n[SITE_NAME]&amp;nbsp;&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('4', 'Boletín predeterminado', 'Hoja informativa', 'Esta es una plantilla de boletín predeterminada', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nHola [NAME]!\n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;Está recibiendo este correo electrónico como parte de su suscripción al boletín.&lt;br&gt;Aquí va el contenido de su boletín ...&lt;br&gt;&lt;/td&gt;&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;background-color:#f1f1f1;border-top-width:2px; border-top-color:#fff; border-top-style:solid&quot;&gt;\n&lt;i&gt;Gracias&lt;br&gt;\n[SITE_NAME]&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;\n&lt;i&gt;Para dejar de recibir boletines futuros, inicie sesión en su cuenta y desactive la casilla de suscripción al boletín.&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('5', 'Transacción completada', 'Pago completado', 'Esta plantilla se usa para notificar al administrador sobre una transacción de pago exitosa', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nHola administrador&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left&quot;&gt;\nHas recibido una nueva transacción de pago:\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;5&quot; cellspacing=&quot;2&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot; width=&quot;130&quot;&gt;\n&lt;b&gt;Usuario:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[USERNAME]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;&lt;b&gt;Estado:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[STATUS]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;Cantidad:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[TOTAL]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;Procesador:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[PP]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;IP:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[IP]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;\n&lt;i&gt;&lt;b&gt;Puede ver esta transacción desde su panel de administración. &lt;/b&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('6', 'Transacción sospechosa', 'Transacción sospechosa', 'Esta plantilla se utiliza para notificar al administrador sobre transacciones de pago fallidas / sospechosas', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;div&gt;Hola administrador&lt;/div&gt;&lt;div&gt;La siguiente transacción se ha deshabilitado debido a actividad sospechosa:&lt;/div&gt;&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;5&quot; cellspacing=&quot;2&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot; width=&quot;130&quot;&gt;\n&lt;b&gt;Usuario:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[USERNAME]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;&lt;b&gt;Estado:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[STATUS]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;&lt;b&gt;Cantidad:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[TOTAL]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;Procesador:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[PP]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;IP:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[IP]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;\n&lt;i&gt;Verifique que esta transacción sea correcta. Parece que alguien intentó obtener productos de su sitio de manera fraudulenta.&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('7', 'Correo electrónico de bienvenida', 'Bienvenida', 'Esta plantilla se usa para dar la bienvenida al usuario recién registrado cuando en Configuración-&gt; Verificación de registro y Configuración-&gt; Registro automático están configurados en &quot;SÍ&quot;', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nBienvenido(a) [NAME]\n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nHola, &lt;br&gt;\n&lt;br&gt;\nAhora eres miembro de [SITE_NAME]. \n&lt;br&gt;\nAquí están sus detalles de inicio de sesión. Guárdelos en un lugar seguro:\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;5&quot; cellspacing=&quot;2&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot; width=&quot;130&quot;&gt;\n&lt;b&gt;Usuario:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[USERNAME]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;&lt;b&gt;Contraseña:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[PASSWORD]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;&lt;i&gt;Gracias&lt;br&gt;\n[SITE_NAME]&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('8', 'Usuario de transacción completada', 'Transacción completada', 'Esta plantilla se utiliza para notificar al usuario sobre la compra exitosa.', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nHola [USERNAME]\n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nHas comprado lo siguiente:\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n[ITEMS]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nAhora puede descargar sus artículos desde su panel de control.\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;&lt;i&gt;Gracias&lt;br&gt;\n[SITE_NAME]&amp;nbsp;&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('9', 'Notificar a la transacción del usuario', 'Su producto está listo para descargar.', 'Esta plantilla se utiliza para notificar al usuario cuando se ha procesado la transacción manual.', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nHola [USERNAME]\n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;Su producto está listo para descargar:\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;5&quot; cellspacing=&quot;2&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot; width=&quot;130&quot;&gt;\n&lt;b&gt;Producto:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[ITEMNAME]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;Precio:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[PRICE]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;Cantidad:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[QTY]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nAhora puede descargar sus artículos desde su panel de control.\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;&lt;i&gt;Gracias&lt;br&gt;\n[SITENAME]&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('10', 'Formulario de contacto', 'Consulta de contacto', 'Esta plantilla se utiliza para enviar el formulario de solicitud de contacto predeterminado.', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n\t&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n\t&lt;tbody&gt;\n\t&lt;tr&gt;\n\t\t&lt;th style=&quot;background-color:#ccc; font-size:16px;padding:5px;border-bottom:2px solid #fff;&quot;&gt;\n Hola, Administrador\n\t\t&lt;/th&gt;\n\t&lt;/tr&gt;\n\t&lt;tr&gt;\n\t\t&lt;td style=&quot;text-align: left&quot;&gt;\n Has recibido un nuevo mensaje desde el formulario de contacto en&amp;nbsp;[SITE_NAME].&lt;/td&gt;\n\t&lt;/tr&gt;\n\t&lt;tr&gt;\n\t\t&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n\t\t\t&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;5&quot; cellspacing=&quot;2&quot;&gt;\n\t\t\t&lt;tbody&gt;\n\t\t\t&lt;tr&gt;\n\t\t\t\t&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid;&quot; width=&quot;150&quot;&gt;\n\t\t\t\t\t&lt;b&gt;De:&lt;br&gt;\n\t\t\t\t\t&lt;/b&gt;\n\t\t\t\t&lt;/td&gt;\n\t\t\t\t&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid;&quot;&gt;\n\t\t\t\t\t[NAME]&amp;nbsp;&lt;br&gt;\n\t\t\t\t&lt;/td&gt;\n\t\t\t&lt;/tr&gt;\n&lt;tr&gt;\n\t\t\t\t&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid;&quot; width=&quot;150&quot;&gt;&lt;b&gt;Correo electrónico:&lt;/b&gt;&lt;br&gt;\n\t\t\t\t\t\n\t\t\t\t&lt;/td&gt;\n\t\t\t\t&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid;&quot;&gt;\n\t\t\t\t\t[EMAIL]&amp;nbsp;&lt;br&gt;\n\t\t\t\t&lt;/td&gt;\n\t\t\t&lt;/tr&gt;\n\t\t\t&lt;tr&gt;\n\t\t\t\t&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid;&quot; width=&quot;150&quot;&gt;\n\t\t\t\t\t&lt;b&gt;Asunto:&lt;/b&gt;\n\t\t\t\t&lt;/td&gt;\n\t\t\t\t&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid;&quot;&gt;\n [MAILSUBJECT]\n\t\t\t\t&lt;/td&gt;\n\t\t\t&lt;/tr&gt;\n\t\t\t&lt;tr&gt;\n\t\t\t\t&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid;&quot; width=&quot;150&quot;&gt;&lt;b&gt;Mensaje:&lt;/b&gt;&lt;br&gt;&lt;/td&gt;\n\t\t\t\t&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid;&quot;&gt;[MESSAGE]&lt;br&gt;&lt;/td&gt;\n\t\t\t&lt;/tr&gt;\n\t\t\t&lt;tr&gt;\n\t\t\t\t&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid;&quot; width=&quot;150&quot;&gt;&lt;b&gt;&lt;b&gt;IP:&lt;/b&gt;&lt;/b&gt;\n\t\t\t\t&lt;/td&gt;\n\t\t\t\t&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid;&quot;&gt;[IP]\n\t\t\t\t&lt;/td&gt;\n\t\t\t&lt;/tr&gt;\n\t\t\t&lt;/tbody&gt;\n\t\t\t&lt;/table&gt;\n\t\t&lt;/td&gt;\n\t&lt;/tr&gt;\n\t&lt;/tbody&gt;\n\t&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('12', 'Email único', 'Correo electrónico de usuario único', 'Esta plantilla se usa para enviar un correo electrónico a un solo usuario', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nHola [NAME]\n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nTu mensaje va aquí ...\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;\n&lt;i&gt;Gracias&lt;br&gt;\n[SITE_NAME]&amp;nbsp;&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('13', 'Notificar al administrador', 'Registro de nuevo usuario', 'Esta plantilla se utiliza para notificar al administrador del nuevo registro cuando en Configuración-&gt; Notificación de registro se establece en &quot;SÍ&quot;.', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nhola administrador \n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nTiene un nuevo registro de usuario:\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;5&quot; cellspacing=&quot;2&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot; width=&quot;130&quot;&gt;&lt;b&gt;&amp;nbsp;Usuario:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[SENDER]&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;Nombre:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n&amp;nbsp;[NAME]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;IP:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[IP]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;\nPuede iniciar sesión en su panel de administración para ver los detalles.\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('14', 'Registro pendiente', 'Verificación de registro pendiente', 'Esta plantilla se utiliza para enviar un correo electrónico de verificación de registro, cuando en Configuración-&gt; Registro automático se establece en &quot;NO&quot;.', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n  &lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nBienvenido(a) [NAME]\n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nHola, &lt;br&gt;\n&lt;br&gt;\nAhora eres miembro de [SITE_NAME]. \n&lt;br&gt;\nAquí están sus detalles de inicio de sesión. Guárdelos en un lugar seguro:\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;5&quot; cellspacing=&quot;2&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot; width=&quot;130&quot;&gt;\n&lt;b&gt;Username:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[USERNAME]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;Password:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[PASSWORD]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nEl administrador de este sitio ha solicitado que todas las cuentas nuevas sean activadas por los usuarios que las crearon, por lo que su cuenta está actualmente pendiente del proceso de verificación.\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;&lt;i&gt;Gracias&lt;br&gt;\n[SITE_NAME]&amp;nbsp;&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('11', 'Nuevo comentario', 'Nuevo comentario agregado', 'Esta plantilla se utiliza para notificar al administrador cuando se ha agregado un nuevo comentario.', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color: rgb(204, 204, 204); font-size:16px;padding:5px;border-bottom-width:2px; border-bottom-color:#fff; border-bottom-style:solid&quot;&gt;\nhola administrador \n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nTienes una nueva publicación de comentario:\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;&quot; border=&quot;0&quot; width=&quot;100%&quot; cellpadding=&quot;5&quot; cellspacing=&quot;2&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot; width=&quot;130&quot;&gt;&lt;b&gt;DE:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[SENDER] - [NAME]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;www:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[WWW]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;Product Url:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n&lt;a href=&quot;[PAGEURL]&quot;&gt;[PAGEURL]&lt;/a&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;right&quot;&gt;\n&lt;b&gt;IP:&lt;/b&gt;\n&lt;/td&gt;\n&lt;td style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot;&gt;\n[IP]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td colspan=&quot;2&quot; style=&quot;border-bottom-width:1px; border-bottom-color:#bbb; border-bottom-style:dashed&quot; align=&quot;left&quot;&gt;\n[MESSAGE]\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left; background-color:#fff;border-top-width:2px; border-top-color:#ccc; border-top-style:solid&quot; valign=&quot;top&quot;&gt;\nPuede iniciar sesión en su panel de administración para ver detalles\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');
INSERT INTO `email_templates` (`id`, `name`, `subject`, `help`, `body`) VALUES ('15', 'Activación de cuenta', 'Su cuenta ha sido activada', 'Esta plantilla se usa para notificar al usuario cuando se completa la activación manual de la cuenta', '&lt;div style=&quot;color:#000;margin-top:20px;margin-left:auto;margin-right:auto;max-width:800px;background-color:#F4F4F4&quot;&gt;\n&lt;table style=&quot;font-family: Helvetica Neue,Helvetica,Arial, sans-serif; font-size:13px;background: #F4F4F4; width: 100%; border: 4px solid #bbbbbb;&quot; cellpadding=&quot;10&quot; cellspacing=&quot;5&quot;&gt;\n&lt;tbody&gt;\n&lt;tr&gt;\n&lt;th style=&quot;background-color:#ccc; font-size:16px;padding:5px;border-bottom:2px solid #fff;&quot;&gt;\nHola, [NAME]! &lt;br&gt;\n&lt;/th&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot; valign=&quot;top&quot;&gt;\nHola,&lt;br&gt;\n&lt;br&gt;\n  Ahora eres miembro de [SITE_NAME].\n&lt;br&gt;\n&lt;br&gt;\n  Su cuenta ahora está completamente activada, y puede iniciar sesión en \n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;\n&lt;hr&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;tr&gt;\n&lt;td style=&quot;text-align: left;&quot;&gt;\n&lt;i&gt;Gracias&lt;br&gt;\n  [SITE_NAME]&lt;br&gt;\n&lt;a href=&quot;[URL]&quot;&gt;[URL]&lt;/a&gt;&lt;/i&gt;\n&lt;/td&gt;\n&lt;/tr&gt;\n&lt;/tbody&gt;\n&lt;/table&gt;\n&lt;/div&gt;');


-- --------------------------------------------------
# -- Table structure for table `extras`
-- --------------------------------------------------
DROP TABLE IF EXISTS `extras`;
CREATE TABLE `extras` (
  `user_id` varchar(50) NOT NULL DEFAULT '0',
  `tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `totaltax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `coupon` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `originalprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `totalprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `extras`
-- --------------------------------------------------



-- --------------------------------------------------
# -- Table structure for table `faq`
-- --------------------------------------------------
DROP TABLE IF EXISTS `faq`;
CREATE TABLE `faq` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `question` varchar(150) DEFAULT NULL,
  `answer` text,
  `position` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `faq`
-- --------------------------------------------------

INSERT INTO `faq` (`id`, `question`, `answer`, `position`) VALUES ('1', '¿En cuánto tiempo obtengo una respuesta?', '&lt;br&gt;\n&lt;p&gt;\n\tSi cumples con el perfil requerido, el empleador responsable se comunicara contigo lo mas pronto posible.\n\t&lt;br&gt;\n\tTambién deberás tener presente el tiempo de finalización de la vacante y el número de aspirantes que aplicaron a ella.\n&lt;/p&gt;\n&lt;strong&gt;Nota: Te recordamos llenar tu curriculum para que tus solicitudes a empleos sean correctamente visualizadas por los empleadores.&lt;/strong&gt;', '3');
INSERT INTO `faq` (`id`, `question`, `answer`, `position`) VALUES ('2', '¿Cómo puedo postularme a una vacante?', '&lt;br&gt;\n&lt;p&gt;Para poder aplicar a una vacante necesitas estar registrado.&lt;br&gt;\n\tUna vez que estés registrado e inicies sesión, tendrás disponible un botón en cada oferta de empleo. Donde solo deberás dar clic para aplicar como aspirante.\n&lt;/p&gt;\n&lt;strong&gt;Nota: Deberás completar tu curriculum para que  el empleador pueda visualizar tu solicitud.&lt;/strong&gt;', '2');
INSERT INTO `faq` (`id`, `question`, `answer`, `position`) VALUES ('3', '¿Dónde puedo consultar si he sido aceptado?', '&lt;br&gt;\n&lt;p&gt;\n\tEn caso de ser elegido para una vacante, el empleador se pondrá en contacto contigo por el medio que mas crea conveniente.&lt;br&gt;\n\t Te recomendamos ingresar un número de teléfono y correo electrónico valido.\n&lt;/p&gt;\n&lt;p&gt;\n\t&lt;strong&gt;Recuerda&lt;/strong&gt; revisar tu bandeja de mensajes instantáneos. el cual se encuentra en&amp;nbsp;tu menú de aspirante.\n&lt;/p&gt;', '4');
INSERT INTO `faq` (`id`, `question`, `answer`, `position`) VALUES ('4', '¿Dónde puedo ver las ofertas de trabajo?', '&lt;br&gt;\n&lt;p&gt;\n\tLas ofertas de trabajo están disponibles en la sección de &lt;strong&gt;&quot;OFERTAS DE EMPLEO&quot;&lt;/strong&gt;, o podrás buscarlas por categoría en nuestra sección de &lt;strong&gt;&quot;CATEGORÍAS&quot;&lt;/strong&gt;.\n&lt;/p&gt;', '1');
INSERT INTO `faq` (`id`, `question`, `answer`, `position`) VALUES ('8', '¿Como puedo publicar una oferta de empleo?', '&lt;p&gt;\n&lt;/p&gt;\n&lt;p&gt;\n\tEste es un servicio exclusivo para &lt;strong&gt;socios&lt;/strong&gt; de la&amp;nbsp;Cámara Mexicana de la Industria de la Construcción &amp;nbsp;(&lt;a target=&quot;_blank&quot; href=&quot;http://www.cmic.org/&quot;&gt;CMIC&lt;/a&gt;).\n&lt;/p&gt;\n&lt;p&gt;\n\t¿Te gustaría ser &lt;strong&gt;socio&lt;/strong&gt; de &lt;a target=&quot;_blank&quot; href=&quot;http://www.cmic.org/&quot; &gt;CMIC&lt;/a&gt; y obtener acceso a los grandes beneficios que ofrecemos? ¿Que esperas?&lt;br&gt;\n\tEntra a este enlace y envíanos tus datos&amp;nbsp;&lt;a target=&quot;_blank&quot; href=&quot;https://www.cmic.org.mx/cmic/afiliacion/&quot;&gt;Afiliate aquí!&lt;/a&gt;\n&lt;/p&gt;', '5');


-- --------------------------------------------------
# -- Table structure for table `gateways`
-- --------------------------------------------------
DROP TABLE IF EXISTS `gateways`;
CREATE TABLE `gateways` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `displayname` varchar(200) NOT NULL,
  `dir` varchar(200) NOT NULL,
  `demo` tinyint(1) NOT NULL DEFAULT '1',
  `extra_txt` varchar(200) NOT NULL,
  `extra_txt2` varchar(200) NOT NULL,
  `extra_txt3` varchar(200) DEFAULT NULL,
  `extra` varchar(200) NOT NULL,
  `extra2` varchar(200) NOT NULL,
  `extra3` varchar(200) DEFAULT NULL,
  `info` text,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `gateways`
-- --------------------------------------------------

INSERT INTO `gateways` (`id`, `name`, `displayname`, `dir`, `demo`, `extra_txt`, `extra_txt2`, `extra_txt3`, `extra`, `extra2`, `extra3`, `info`, `active`) VALUES ('1', 'paypal', 'PayPal', 'paypal', '0', 'Paypal Email Address', 'Currency Code', 'Not in Use', 'facilitator@gmail.com', 'CAD', '', '&lt;p&gt;&lt;a href=&quot;http://www.paypal.com/&quot; title=&quot;PayPal&quot; rel=&quot;nofollow&quot; target=&quot;_blank&quot;&gt;Click here to setup an account with Paypal&lt;/a&gt; &lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Gateway Name&lt;/strong&gt; - Enter the name of the payment provider here.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Active&lt;/strong&gt; - Select Yes to make this payment provider active. &lt;br/&gt;\r\nIf this box is not checked, the payment provider will not show up in the payment provider list during checkout.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Set Live Mode&lt;/strong&gt; - If you would like to test the payment provider settings, please select No. &lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Paypal email address&lt;/strong&gt; - Enter your PayPal Business email address here. &lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Currency Code&lt;/strong&gt; - Enter your currency code here. Valid choices are: &lt;/p&gt;\r\n&lt;ul&gt;\r\n&lt;li&gt; USD (U.S. Dollar)&lt;/li&gt;\r\n&lt;li&gt; EUR (Euro) &lt;/li&gt;\r\n&lt;li&gt; GBP (Pound Sterling) &lt;/li&gt;\r\n&lt;li&gt; CAD (Canadian Dollar) &lt;/li&gt;\r\n&lt;li&gt; JPY (Yen). &lt;/li&gt;\r\n&lt;li&gt; If omitted, all monetary fields will use default system setting Currency Code. &lt;/li&gt;\r\n&lt;/ul&gt;\r\n&lt;p&gt;&lt;strong&gt;Not in Use&lt;/strong&gt; - This field it&#039;s not in use. Leave it empty. &lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;IPN Url&lt;/strong&gt; - If using recurring payment method, you need to set up and activate the IPN Url in your account: &lt;/p&gt;', '1');
INSERT INTO `gateways` (`id`, `name`, `displayname`, `dir`, `demo`, `extra_txt`, `extra_txt2`, `extra_txt3`, `extra`, `extra2`, `extra3`, `info`, `active`) VALUES ('2', 'skrill', 'Skrill', 'skrill', '0', 'Skrill Email Address', 'Currency Code', 'Secret Passphrase', 'gewa@rogers.com', 'EUR', 'mypassphrase', '&lt;p&gt;&lt;a href=&quot;http://www.moneybookers.com/&quot; title=&quot;http://www.moneybookers.net/&quot; rel=&quot;nofollow&quot;&gt;Click here to setup an account with MoneyBookers&lt;/a&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Gateway Name&lt;/strong&gt; - Enter the name of the payment provider here.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Active&lt;/strong&gt; - Select Yes to make this payment provider active. &lt;br/&gt;\r\nIf this box is not checked, the payment provider will not show up in the payment provider list during checkout.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Set Live Mode&lt;/strong&gt; - MoneyBookers does not have demo mode. You need to open testing acounts. One seller and one buyer. &lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;MoneyBookers email address&lt;/strong&gt; - Enter your MoneyBookers email address here. &lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Secret Passphrase&lt;/strong&gt; - This field must be set at Moneybookers.com.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;IPN Url&lt;/strong&gt; - If using recurring payment method, you need to set up and activate the IPN Url in your account: &lt;/p&gt;', '1');
INSERT INTO `gateways` (`id`, `name`, `displayname`, `dir`, `demo`, `extra_txt`, `extra_txt2`, `extra_txt3`, `extra`, `extra2`, `extra3`, `info`, `active`) VALUES ('3', 'payza', 'Payza', 'payza', '0', 'Payza Email Address', 'Currency Code', 'IPN Security Code', 'seller_1_alex.kuzmanovic@gmail.com', 'USD', 'd9vL9oYVOGpmVM2i', '&lt;p&gt;&lt;a href=&quot;http://www.payza.com/&quot; title=&quot;http://www.payza.com/&quot; rel=&quot;nofollow&quot;&gt;Click here to setup an account with Payza&lt;/a&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Gateway Name&lt;/strong&gt; - Enter the name of the payment provider here.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Active&lt;/strong&gt; - Select Yes to make this payment provider active. &lt;br/&gt;\r\n  If this box is not checked, the payment provider will not show up in the payment provider list during checkout.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Set Live Mode&lt;/strong&gt; - If you would like to test the payment provider settings, please select No. &lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;AlertPay email address&lt;/strong&gt; - Enter your Payza email address here. &lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;IPN Security Code&lt;/strong&gt; - This code needs to be generated in your Payza control panel.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;IPN Url&lt;/strong&gt; - This has to be set in the Payza control panel. You will also need to check the &quot;IPN Status&quot; to enabled.&lt;/p&gt;', '1');
INSERT INTO `gateways` (`id`, `name`, `displayname`, `dir`, `demo`, `extra_txt`, `extra_txt2`, `extra_txt3`, `extra`, `extra2`, `extra3`, `info`, `active`) VALUES ('4', 'stripe', 'Stripe', 'stripe', '1', 'Secret Key', 'Currency Code', 'Publishable Key', 'sk_test_6sDE6weBXgEuHbrjZKyG5MlQ', 'CAD', 'pk_test_vRosykAcmL59P2r7H9hziwrg', '&lt;p&gt;&lt;a href=&quot;https://stripe.com/ca&quot; title=&quot;https://stripe.com/ca&quot; rel=&quot;nofollow&quot;&gt;Click here to setup an account with Stripe&lt;/a&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Gateway Name&lt;/strong&gt; - Enter the name of the payment provider here.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Active&lt;/strong&gt; - Select Yes to make this payment provider active. &lt;br&gt;\r\n  If this box is not checked, the payment provider will not show up in the payment provider list during checkout.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Set Live Mode&lt;/strong&gt; - To test Stripe, use your test keys instead of live ones.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;API Keys&lt;/strong&gt; - To obtain your API Keys:&lt;/p&gt;\r\n&lt;ul&gt;\r\n  &lt;li&gt; 1. Log into the Dashboard at &lt;a href=&quot;https://stripe.com/ca&quot; target=&quot;_blank&quot;&gt;https://stripe.com/ca&lt;/a&gt;&lt;/li&gt;\r\n  &lt;li&gt; 2. Select Account Settings under Your Account in the main menu on the left &lt;/li&gt;\r\n  &lt;li&gt; 3. Click API Keys&lt;/li&gt;\r\n  &lt;li&gt; 4. Your keys will be displayed &lt;/li&gt;\r\n\r\n&lt;p&gt;You should use test keys first to verify, that everything is working smoothly, before going live.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;IPN Url&lt;/strong&gt; - This option it&#039;s not being used.&lt;/p&gt;\r\n&lt;/ul&gt;', '1');
INSERT INTO `gateways` (`id`, `name`, `displayname`, `dir`, `demo`, `extra_txt`, `extra_txt2`, `extra_txt3`, `extra`, `extra2`, `extra3`, `info`, `active`) VALUES ('5', 'anet', 'Authorize Net', 'anet', '1', 'API Login Id', 'MD5 Hash Key', 'Transaction Key', '9KDgMm2mw46V', '', '5wek3X3DX5e39YAQ', '&lt;p&gt;&lt;a href=&quot;http://www.authorize.net/&quot; title=&quot;http://www.authorize.net//&quot; rel=&quot;nofollow&quot;&gt;Click here to setup an account with Authorize.Net&lt;/a&gt;&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Gateway Name&lt;/strong&gt; - Enter the name of the payment provider here.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Active&lt;/strong&gt; - Select Yes to make this payment provider active. &lt;br/&gt;\r\n  If this box is not checked, the payment provider will not show up in the payment provider list during checkout.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Set Live Mode&lt;/strong&gt; - If you would like to test the payment provider settings, please select No. &lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;Login ID&lt;/strong&gt; - To obtain your API Login ID:&lt;/p&gt;\r\n&lt;ol type=&quot;1&quot;&gt;\r\n  &lt;li&gt; Log into the Merchant Interface at &lt;a href=&quot;https://secure.authorize.net&quot; target=&quot;_blank&quot;&gt;https://secure.authorize.net&lt;/a&gt;&lt;/li&gt;\r\n  &lt;li&gt; Select Settings under Account in the main menu on the left &lt;/li&gt;\r\n  &lt;li&gt; Click API Login ID and Transaction Key in the Security Settings section &lt;/li&gt;\r\n  &lt;li&gt; If you have not already obtained an API Login ID and Transaction Key for your account,&lt;br/&gt;\r\nyou will need to enter the secret answer to the secret question you configured at account activation. &lt;/li&gt;\r\n  &lt;li&gt; Click Submit. &lt;/li&gt;\r\n&lt;/ol&gt;\r\n&lt;p&gt;&lt;strong&gt;MD5 Hash&lt;/strong&gt; - To obtain your MD5 Hash:&lt;/p&gt;\r\n&lt;ol type=&quot;1&quot;&gt;\r\n  &lt;li&gt; Log into the Merchant Interface at &lt;a href=&quot;https://secure.authorize.net&quot; target=&quot;_blank&quot;&gt;https://secure.authorize.net&lt;/a&gt;&lt;/li&gt;\r\n  &lt;li&gt; Select Settings under Account in the main menu on the left &lt;/li&gt;\r\n  &lt;li&gt; Click MD5 Hash in the Security Settings section &lt;/li&gt;\r\n  &lt;li&gt;Enter a secret word, phrase, or value and remember this.&lt;/li&gt;\r\n  &lt;li&gt; Click Submit. &lt;/li&gt;\r\n&lt;/ol&gt;\r\n&lt;strong&gt;Transaction Key&lt;/strong&gt; - To obtain a Transaction Key:\r\n&lt;ol type=&quot;1&quot;&gt;\r\n  &lt;li&gt; Log on to the Merchant Interface at &lt;a href=&quot;https://secure.authorize.net&quot; target=&quot;_blank&quot;&gt;https://secure.authorize.net&lt;/a&gt;&lt;/li&gt;\r\n  &lt;li&gt; Select Settings under Account in the main menu on the left &lt;/li&gt;\r\n  &lt;li&gt; Click API Login ID and Transaction Key in the Security Settings section &lt;/li&gt;\r\n  &lt;li&gt; Enter the secret answer to the secret question you configured when you activated your user account &lt;/li&gt;\r\n  &lt;li&gt; Click Submit &lt;/li&gt;\r\n&lt;/ol&gt;\r\n&lt;p&gt;The Transaction Key for your account is displayed on a confirmation page.&lt;/p&gt;\r\n&lt;p&gt;&lt;strong&gt;IPN Url&lt;/strong&gt; - This option it\\&#039;s not being used.&lt;/p&gt;', '1');
INSERT INTO `gateways` (`id`, `name`, `displayname`, `dir`, `demo`, `extra_txt`, `extra_txt2`, `extra_txt3`, `extra`, `extra2`, `extra3`, `info`, `active`) VALUES ('6', 'invalido', 'Invalido', 'invalido', '1', '', '', '', '', '', '', '', '1');


-- --------------------------------------------------
# -- Table structure for table `invoices`
-- --------------------------------------------------
DROP TABLE IF EXISTS `invoices`;
CREATE TABLE `invoices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invid` int(11) NOT NULL DEFAULT '0',
  `items` varchar(250) DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `totaltax` decimal(10,2) NOT NULL DEFAULT '0.00',
  `coupon` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `originalprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `totalprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `currency` varchar(6) DEFAULT NULL,
  `created` datetime DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `invoices`
-- --------------------------------------------------

INSERT INTO `invoices` (`id`, `invid`, `items`, `user_id`, `tax`, `totaltax`, `coupon`, `total`, `originalprice`, `totalprice`, `currency`, `created`) VALUES ('1', '201503011', 'a:2:{s:4:"2.99";s:20:"Company Subscription";s:5:"10.99";s:23:"Individual Subscription";}', '18', '0.13', '2.92', '2.50', '22.47', '24.97', '25.39', 'CAD', '2018-01-30 22:54:48');


-- --------------------------------------------------
# -- Table structure for table `job_applications`
-- --------------------------------------------------
DROP TABLE IF EXISTS `job_applications`;
CREATE TABLE `job_applications` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `jobid` int(11) NOT NULL,
  `message` text NOT NULL,
  `note` text NOT NULL,
  `resume` varchar(255) NOT NULL,
  `expected` varchar(20) NOT NULL,
  `rating` enum('no','one','two','three','four','five') NOT NULL,
  `status` enum('new','interviewed','offer extended','hired','archived') NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `job_applications`
-- --------------------------------------------------



-- --------------------------------------------------
# -- Table structure for table `job_categories`
-- --------------------------------------------------
DROP TABLE IF EXISTS `job_categories`;
CREATE TABLE `job_categories` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `parent_id` int(6) NOT NULL DEFAULT '0',
  `slug` varchar(200) NOT NULL,
  `position` tinyint(2) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `job_categories`
-- --------------------------------------------------

INSERT INTO `job_categories` (`id`, `name`, `icon`, `parent_id`, `slug`, `position`, `active`) VALUES ('91', 'Proyectos', 'ln ln-icon-Bridge', '0', 'proyectos', '6', '1');
INSERT INTO `job_categories` (`id`, `name`, `icon`, `parent_id`, `slug`, `position`, `active`) VALUES ('90', 'Presupuestos', 'ln ln-icon-Billing', '0', 'presupuestos', '5', '1');
INSERT INTO `job_categories` (`id`, `name`, `icon`, `parent_id`, `slug`, `position`, `active`) VALUES ('87', 'Mantenimiento', 'ln ln-icon-Settings-Window', '0', 'mantenimiento', '3', '1');
INSERT INTO `job_categories` (`id`, `name`, `icon`, `parent_id`, `slug`, `position`, `active`) VALUES ('88', 'Seguridad, higiene y ecología', 'ln ln-icon-Securiy-Remove', '0', 'seguridad-higiene-y-ecologia', '7', '1');
INSERT INTO `job_categories` (`id`, `name`, `icon`, `parent_id`, `slug`, `position`, `active`) VALUES ('89', 'Construcción', 'fa fa-building-o', '0', 'construccion', '2', '1');
INSERT INTO `job_categories` (`id`, `name`, `icon`, `parent_id`, `slug`, `position`, `active`) VALUES ('85', 'Administrativos', 'ln ln-icon-Administrator', '0', 'administrativos', '1', '1');
INSERT INTO `job_categories` (`id`, `name`, `icon`, `parent_id`, `slug`, `position`, `active`) VALUES ('86', 'Operadores', 'ln ln-icon-Helmet-2', '0', 'operadores', '4', '1');
INSERT INTO `job_categories` (`id`, `name`, `icon`, `parent_id`, `slug`, `position`, `active`) VALUES ('84', 'Supervisores y coordinadores de obra', 'ln ln-icon-Worker', '0', 'supervisores-y-coordinadores-de-obra', '8', '1');


-- --------------------------------------------------
# -- Table structure for table `job_locations`
-- --------------------------------------------------
DROP TABLE IF EXISTS `job_locations`;
CREATE TABLE `job_locations` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `parent_id` int(6) NOT NULL DEFAULT '0',
  `slug` varchar(200) NOT NULL,
  `position` tinyint(2) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1901 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `job_locations`
-- --------------------------------------------------

INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1517', 'SANTA CATARINA AYOMETLA', '1415', 'SANTA CATARINA AYOMETLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1511', 'SAN JOSÉ TEACALCO', '1415', 'SAN JOSÉ TEACALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1512', 'SAN JUAN HUACTZINCO', '1415', 'SAN JUAN HUACTZINCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1513', 'SAN LORENZO AXOCOMANITLA', '1415', 'SAN LORENZO AXOCOMANITLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1514', 'SAN LUCAS TECOPILCO', '1415', 'SAN LUCAS TECOPILCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1515', 'SANTA ANA NOPALUCAN', '1415', 'SANTA ANA NOPALUCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1516', 'SANTA APOLONIA TEACALCO', '1415', 'SANTA APOLONIA TEACALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1509', 'SAN FRANCISCO TETLANOHCAN', '1415', 'SAN FRANCISCO TETLANOHCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1510', 'SAN JERÓNIMO ZACUALPAN', '1415', 'SAN JERÓNIMO ZACUALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1508', 'SAN DAMIÁN TEXÓLOC', '1415', 'SAN DAMIÁN TEXÓLOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1507', 'LA MAGDALENA TLALTELULCO', '1415', 'LA MAGDALENA TLALTELULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1506', 'LÁZARO CÁRDENAS', '1415', 'LÁZARO CÁRDENAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1505', 'EMILIANO ZAPATA', '1415', 'EMILIANO ZAPATA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1504', 'BENITO JUÁREZ', '1415', 'BENITO JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1503', 'ZACATELCO', '1415', 'ZACATELCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1502', 'YAUHQUEMEHCAN', '1415', 'YAUHQUEMEHCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1501', 'XICOHTZINCO', '1415', 'XICOHTZINCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1500', 'PAPALOTLA DE XICOHTÉNCATL', '1415', 'PAPALOTLA DE XICOHTÉNCATL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1499', 'XALTOCAN', '1415', 'XALTOCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1498', 'XALOZTOC', '1415', 'XALOZTOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1497', 'TZOMPANTEPEC', '1415', 'TZOMPANTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1496', 'ZILTLALTÉPEC DE TRINIDAD SÁNCHEZ SANTOS', '1415', 'ZILTLALTÉPEC DE TRINIDAD SÁNCHEZ SANTOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1495', 'TOTOLAC', '1415', 'TOTOLAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1494', 'TOCATLÁN', '1415', 'TOCATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1493', 'TLAXCO', '1415', 'TLAXCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1492', 'TLAXCALA', '1415', 'TLAXCALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1491', 'TETLATLAHUCA', '1415', 'TETLATLAHUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1490', 'TETLA DE LA SOLIDARIDAD', '1415', 'TETLA DE LA SOLIDARIDAD', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1489', 'TERRENATE', '1415', 'TERRENATE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1488', 'TEPEYANCO', '1415', 'TEPEYANCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1487', 'TEOLOCHOLCO', '1415', 'TEOLOCHOLCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1486', 'TENANCINGO', '1415', 'TENANCINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1485', 'SANTA CRUZ TLAXCALA', '1415', 'SANTA CRUZ TLAXCALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1484', 'SAN PABLO DEL MONTE', '1415', 'SAN PABLO DEL MONTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1483', 'PANOTLA', '1415', 'PANOTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1482', 'NATÍVITAS', '1415', 'NATÍVITAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1481', 'ACUAMANALA DE MIGUEL HIDALGO', '1415', 'ACUAMANALA DE MIGUEL HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1480', 'NANACAMILPA DE MARIANO ARISTA', '1415', 'NANACAMILPA DE MARIANO ARISTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1479', 'SANCTÓRUM DE LÁZARO CÁRDENAS', '1415', 'SANCTÓRUM DE LÁZARO CÁRDENAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1478', 'TEPETITLA DE LARDIZÁBAL', '1415', 'TEPETITLA DE LARDIZÁBAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1477', 'CONTLA DE JUAN CUAMATZI', '1415', 'CONTLA DE JUAN CUAMATZI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1476', 'MAZATECOCHCO DE JOSÉ MARÍA MORELOS', '1415', 'MAZATECOCHCO DE JOSÉ MARÍA MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1475', 'IXTENCO', '1415', 'IXTENCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1474', 'IXTACUIXTLA DE MARIANO MATAMOROS', '1415', 'IXTACUIXTLA DE MARIANO MATAMOROS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1473', 'HUEYOTLIPAN', '1415', 'HUEYOTLIPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1472', 'HUAMANTLA', '1415', 'HUAMANTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1471', 'ESPAÑITA', '1415', 'ESPAÑITA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1470', 'MUÑOZ DE DOMINGO ARENAS', '1415', 'MUÑOZ DE DOMINGO ARENAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1469', 'CHIAUTEMPAN', '1415', 'CHIAUTEMPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1468', 'CUAXOMULCO', '1415', 'CUAXOMULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1467', 'CUAPIAXTLA', '1415', 'CUAPIAXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1466', 'EL CARMEN TEQUEXQUITLA', '1415', 'EL CARMEN TEQUEXQUITLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1465', 'CALPULALPAN', '1415', 'CALPULALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1464', 'APIZACO', '1415', 'APIZACO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1463', 'ATLTZAYANCA', '1415', 'ATLTZAYANCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1462', 'ATLANGATEPEC', '1415', 'ATLANGATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1461', 'APETATITLÁN DE ANTONIO CARVAJAL', '1415', 'APETATITLÁN DE ANTONIO CARVAJAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1460', 'AMAXAC DE GUERRERO', '1415', 'AMAXAC DE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1459', 'TLAXCALA', '1415', 'TLAXCALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1458', 'XICOTÉNCATL', '1415', 'XICOTÉNCATL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1457', 'VILLAGRÁN', '1415', 'VILLAGRÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1456', 'VICTORIA', '1415', 'VICTORIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1455', 'VALLE HERMOSO', '1415', 'VALLE HERMOSO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1454', 'TULA', '1415', 'TULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1453', 'TAMPICO', '1415', 'TAMPICO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1452', 'SOTO LA MARINA', '1415', 'SOTO LA MARINA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1451', 'SAN NICOLÁS', '1415', 'SAN NICOLÁS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1450', 'SAN FERNANDO', '1415', 'SAN FERNANDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1449', 'SAN CARLOS', '1415', 'SAN CARLOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1448', 'RÍO BRAVO', '1415', 'RÍO BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1447', 'REYNOSA', '1415', 'REYNOSA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1446', 'PALMILLAS', '1415', 'PALMILLAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1445', 'PADILLA', '1415', 'PADILLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1444', 'OCAMPO', '1415', 'OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1443', 'NUEVO MORELOS', '1415', 'NUEVO MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1442', 'NUEVO LAREDO', '1415', 'NUEVO LAREDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1441', 'MIQUIHUANA', '1415', 'MIQUIHUANA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1440', 'MIGUEL ALEMÁN', '1415', 'MIGUEL ALEMÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1439', 'MIER', '1415', 'MIER', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1438', 'MÉNDEZ', '1415', 'MÉNDEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1437', 'MATAMOROS', '1415', 'MATAMOROS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1436', 'EL MANTE', '1415', 'EL MANTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1435', 'MAINERO', '1415', 'MAINERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1434', 'LLERA', '1415', 'LLERA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1433', 'JIMÉNEZ', '1415', 'JIMÉNEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1432', 'JAUMAVE', '1415', 'JAUMAVE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1431', 'HIDALGO', '1415', 'HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1430', 'GUSTAVO DÍAZ ORDAZ', '1415', 'GUSTAVO DÍAZ ORDAZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1429', 'GUERRERO', '1415', 'GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1428', 'GÜÉMEZ', '1415', 'GÜÉMEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1427', 'GONZÁLEZ', '1415', 'GONZÁLEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1426', 'GÓMEZ FARÍAS', '1415', 'GÓMEZ FARÍAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1425', 'CRUILLAS', '1415', 'CRUILLAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1424', 'CIUDAD MADERO', '1415', 'CIUDAD MADERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1423', 'CASAS', '1415', 'CASAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1422', 'CAMARGO', '1415', 'CAMARGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1421', 'BUSTAMANTE', '1415', 'BUSTAMANTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1420', 'BURGOS', '1415', 'BURGOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1419', 'ANTIGUO MORELOS', '1415', 'ANTIGUO MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1418', 'ALTAMIRA', '1415', 'ALTAMIRA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1417', 'ALDAMA', '1415', 'ALDAMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1416', 'ABASOLO', '1415', 'ABASOLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1415', 'TAMAULIPAS', '0', 'TAMAULIPAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1414', 'TENOSIQUE', '1397', 'TENOSIQUE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1413', 'TEAPA', '1397', 'TEAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1412', 'TACOTALPA', '1397', 'TACOTALPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1411', 'PARAÍSO', '1397', 'PARAÍSO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1410', 'NACAJUCA', '1397', 'NACAJUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1409', 'MACUSPANA', '1397', 'MACUSPANA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1408', 'JONUTA', '1397', 'JONUTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1407', 'JALPA DE MÉNDEZ', '1397', 'JALPA DE MÉNDEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1406', 'JALAPA', '1397', 'JALAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1405', 'HUIMANGUILLO', '1397', 'HUIMANGUILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1404', 'EMILIANO ZAPATA', '1397', 'EMILIANO ZAPATA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1403', 'CUNDUACÁN', '1397', 'CUNDUACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1402', 'COMALCALCO', '1397', 'COMALCALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1401', 'CENTRO', '1397', 'CENTRO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1400', 'CENTLA', '1397', 'CENTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1399', 'CÁRDENAS', '1397', 'CÁRDENAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1398', 'BALANCÁN', '1397', 'BALANCÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1397', 'TABASCO', '0', 'TABASCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1396', 'SAN IGNACIO RÍO MUERTO', '1324', 'SAN IGNACIO RÍO MUERTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1395', 'BENITO JUÁREZ', '1324', 'BENITO JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1394', 'GENERAL PLUTARCO ELÍAS CALLES', '1324', 'GENERAL PLUTARCO ELÍAS CALLES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1393', 'YÉCORA', '1324', 'YÉCORA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1392', 'VILLA PESQUEIRA', '1324', 'VILLA PESQUEIRA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1391', 'VILLA HIDALGO', '1324', 'VILLA HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1390', 'URES', '1324', 'URES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1389', 'TUBUTAMA', '1324', 'TUBUTAMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1388', 'TRINCHERAS', '1324', 'TRINCHERAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1387', 'TEPACHE', '1324', 'TEPACHE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1386', 'SUAQUI GRANDE', '1324', 'SUAQUI GRANDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1385', 'SOYOPA', '1324', 'SOYOPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1384', 'SÁRIC', '1324', 'SÁRIC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1383', 'SANTA CRUZ', '1324', 'SANTA CRUZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1382', 'SANTA ANA', '1324', 'SANTA ANA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1381', 'SAN PEDRO DE LA CUEVA', '1324', 'SAN PEDRO DE LA CUEVA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1380', 'SAN MIGUEL DE HORCASITAS', '1324', 'SAN MIGUEL DE HORCASITAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1379', 'SAN LUIS RÍO COLORADO', '1324', 'SAN LUIS RÍO COLORADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1378', 'SAN JAVIER', '1324', 'SAN JAVIER', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1377', 'SAN FELIPE DE JESÚS', '1324', 'SAN FELIPE DE JESÚS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1376', 'SAHUARIPA', '1324', 'SAHUARIPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1375', 'ROSARIO', '1324', 'ROSARIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1374', 'RAYÓN', '1324', 'RAYÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1373', 'QUIRIEGO', '1324', 'QUIRIEGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1372', 'PUERTO PEÑASCO', '1324', 'PUERTO PEÑASCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1371', 'PITIQUITO', '1324', 'PITIQUITO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1370', 'OQUITOA', '1324', 'OQUITOA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1369', 'OPODEPE', '1324', 'OPODEPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1368', 'ONAVAS', '1324', 'ONAVAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1367', 'NOGALES', '1324', 'NOGALES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1366', 'NAVOJOA', '1324', 'NAVOJOA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1365', 'NACOZARI DE GARCÍA', '1324', 'NACOZARI DE GARCÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1364', 'NÁCORI CHICO', '1324', 'NÁCORI CHICO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1363', 'NACO', '1324', 'NACO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1362', 'MOCTEZUMA', '1324', 'MOCTEZUMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1361', 'MAZATÁN', '1324', 'MAZATÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1360', 'MAGDALENA', '1324', 'MAGDALENA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1359', 'IMURIS', '1324', 'IMURIS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1358', 'HUÉPAC', '1324', 'HUÉPAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1356', 'HUÁSABAS', '1324', 'HUÁSABAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1357', 'HUATABAMPO', '1324', 'HUATABAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1355', 'HUACHINERA', '1324', 'HUACHINERA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1354', 'HERMOSILLO', '1324', 'HERMOSILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1353', 'GUAYMAS', '1324', 'GUAYMAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1352', 'GRANADOS', '1324', 'GRANADOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1351', 'FRONTERAS', '1324', 'FRONTERAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1350', 'ETCHOJOA', '1324', 'ETCHOJOA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1349', 'EMPALME', '1324', 'EMPALME', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1348', 'DIVISADEROS', '1324', 'DIVISADEROS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1347', 'CUMPAS', '1324', 'CUMPAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1346', 'CUCURPE', '1324', 'CUCURPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1345', 'LA COLORADA', '1324', 'LA COLORADA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1344', 'CARBÓ', '1324', 'CARBÓ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1343', 'CANANEA', '1324', 'CANANEA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1342', 'CAJEME', '1324', 'CAJEME', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1341', 'CABORCA', '1324', 'CABORCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1340', 'BENJAMÍN HILL', '1324', 'BENJAMÍN HILL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1339', 'BAVISPE', '1324', 'BAVISPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1338', 'BAVIÁCORA', '1324', 'BAVIÁCORA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1337', 'BANÁMICHI', '1324', 'BANÁMICHI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1335', 'BACOACHI', '1324', 'BACOACHI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1336', 'BÁCUM', '1324', 'BÁCUM', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1334', 'BACERAC', '1324', 'BACERAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1333', 'BACANORA', '1324', 'BACANORA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1332', 'BACADÉHUACHI', '1324', 'BACADÉHUACHI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1331', 'ATIL', '1324', 'ATIL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1330', 'ARIZPE', '1324', 'ARIZPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1329', 'ARIVECHI', '1324', 'ARIVECHI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1328', 'ALTAR', '1324', 'ALTAR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1327', 'ALAMOS', '1324', 'ALAMOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1326', 'AGUA PRIETA', '1324', 'AGUA PRIETA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1325', 'ACONCHI', '1324', 'ACONCHI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1324', 'SONORA', '0', 'SONORA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1323', 'NAVOLATO', '1305', 'NAVOLATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1322', 'SINALOA', '1305', 'SINALOA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1321', 'SAN IGNACIO', '1305', 'SAN IGNACIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1320', 'SALVADOR ALVARADO', '1305', 'SALVADOR ALVARADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1319', 'ROSARIO', '1305', 'ROSARIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1318', 'MOCORITO', '1305', 'MOCORITO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1317', 'MAZATLÁN', '1305', 'MAZATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1316', 'GUASAVE', '1305', 'GUASAVE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1315', 'EL FUERTE', '1305', 'EL FUERTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1314', 'ESCUINAPA', '1305', 'ESCUINAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1313', 'ELOTA', '1305', 'ELOTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1312', 'CHOIX', '1305', 'CHOIX', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1311', 'CULIACÁN', '1305', 'CULIACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1309', 'CONCORDIA', '1305', 'CONCORDIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1310', 'COSALÁ', '1305', 'COSALÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1308', 'BADIRAGUATO', '1305', 'BADIRAGUATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1307', 'ANGOSTURA', '1305', 'ANGOSTURA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1306', 'AHOME', '1305', 'AHOME', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1305', 'SINALOA', '0', 'SINALOA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1304', 'EL NARANJO', '1234', 'EL NARANJO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1303', 'MATLAPA', '1234', 'MATLAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1302', 'VILLA DE ARISTA', '1234', 'VILLA DE ARISTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1301', 'ZARAGOZA', '1234', 'ZARAGOZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1300', 'XILITLA', '1234', 'XILITLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1299', 'AXTLA DE TERRAZAS', '1234', 'AXTLA DE TERRAZAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1298', 'VILLA JUÁREZ', '1234', 'VILLA JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1297', 'VILLA HIDALGO', '1234', 'VILLA HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1296', 'VILLA DE REYES', '1234', 'VILLA DE REYES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1295', 'VILLA DE RAMOS', '1234', 'VILLA DE RAMOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1294', 'VILLA DE LA PAZ', '1234', 'VILLA DE LA PAZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1293', 'VILLA DE GUADALUPE', '1234', 'VILLA DE GUADALUPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1292', 'VILLA DE ARRIAGA', '1234', 'VILLA DE ARRIAGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1291', 'VENADO', '1234', 'VENADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1290', 'VANEGAS', '1234', 'VANEGAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1289', 'TIERRA NUEVA', '1234', 'TIERRA NUEVA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1288', 'TANQUIÁN DE ESCOBEDO', '1234', 'TANQUIÁN DE ESCOBEDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1287', 'TANLAJÁS', '1234', 'TANLAJÁS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1286', 'TAMUÍN', '1234', 'TAMUÍN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1285', 'TAMPAMOLÓN CORONA', '1234', 'TAMPAMOLÓN CORONA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1284', 'TAMPACÁN', '1234', 'TAMPACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1283', 'TAMAZUNCHALE', '1234', 'TAMAZUNCHALE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1282', 'TAMASOPO', '1234', 'TAMASOPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1281', 'SOLEDAD DE GRACIANO SÁNCHEZ', '1234', 'SOLEDAD DE GRACIANO SÁNCHEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1280', 'SAN VICENTE TANCUAYALAB', '1234', 'SAN VICENTE TANCUAYALAB', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1279', 'SANTO DOMINGO', '1234', 'SANTO DOMINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1278', 'SANTA MARÍA DEL RÍO', '1234', 'SANTA MARÍA DEL RÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1277', 'SANTA CATARINA', '1234', 'SANTA CATARINA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1276', 'SAN NICOLÁS TOLENTINO', '1234', 'SAN NICOLÁS TOLENTINO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1275', 'SAN MARTÍN CHALCHICUAUTLA', '1234', 'SAN MARTÍN CHALCHICUAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1274', 'SAN LUIS POTOSÍ', '1234', 'SAN LUIS POTOSÍ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1273', 'SAN CIRO DE ACOSTA', '1234', 'SAN CIRO DE ACOSTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1271', 'SALINAS', '1234', 'SALINAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1272', 'SAN ANTONIO', '1234', 'SAN ANTONIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1270', 'RIOVERDE', '1234', 'RIOVERDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1269', 'RAYÓN', '1234', 'RAYÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1268', 'MOCTEZUMA', '1234', 'MOCTEZUMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1267', 'MEXQUITIC DE CARMONA', '1234', 'MEXQUITIC DE CARMONA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1266', 'MATEHUALA', '1234', 'MATEHUALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1265', 'LAGUNILLAS', '1234', 'LAGUNILLAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1264', 'HUEHUETLÁN', '1234', 'HUEHUETLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1263', 'GUADALCÁZAR', '1234', 'GUADALCÁZAR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1262', 'EBANO', '1234', 'EBANO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1261', 'CHARCAS', '1234', 'CHARCAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1260', 'COXCATLÁN', '1234', 'COXCATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1259', 'CIUDAD VALLES', '1234', 'CIUDAD VALLES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1258', 'TANCANHUITZ', '1234', 'TANCANHUITZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1257', 'CIUDAD FERNÁNDEZ', '1234', 'CIUDAD FERNÁNDEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1256', 'CIUDAD DEL MAÍZ', '1234', 'CIUDAD DEL MAÍZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1255', 'CERRO DE SAN PEDRO', '1234', 'CERRO DE SAN PEDRO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1254', 'CERRITOS', '1234', 'CERRITOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1253', 'CEDRAL', '1234', 'CEDRAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1252', 'CATORCE', '1234', 'CATORCE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1251', 'CÁRDENAS', '1234', 'CÁRDENAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1250', 'ARMADILLO DE LOS INFANTE', '1234', 'ARMADILLO DE LOS INFANTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1249', 'AQUISMÓN', '1234', 'AQUISMÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1248', 'ALAQUINES', '1234', 'ALAQUINES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1247', 'AHUALULCO', '1234', 'AHUALULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1246', 'SAN LUIS POTOSÍ', '1234', 'SAN LUIS POTOSÍ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1245', 'PUERTO MORELOS', '1234', 'PUERTO MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1244', 'BACALAR', '1234', 'BACALAR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1243', 'TULUM', '1234', 'TULUM', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1242', 'SOLIDARIDAD', '1234', 'SOLIDARIDAD', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1241', 'LÁZARO CÁRDENAS', '1234', 'LÁZARO CÁRDENAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1240', 'JOSÉ MARÍA MORELOS', '1234', 'JOSÉ MARÍA MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1239', 'BENITO JUÁREZ', '1234', 'BENITO JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1238', 'OTHÓN P. BLANCO', '1234', 'OTHÓN P. BLANCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1237', 'ISLA MUJERES', '1234', 'ISLA MUJERES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1236', 'FELIPE CARRILLO PUERTO', '1234', 'FELIPE CARRILLO PUERTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1235', 'COZUMEL', '1234', 'COZUMEL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1234', 'QUINTANA ROO', '0', 'QUINTANA ROO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1233', 'TOLIMÁN', '1028', 'TOLIMÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1232', 'TEQUISQUIAPAN', '1028', 'TEQUISQUIAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1231', 'SAN JUAN DEL RÍO', '1028', 'SAN JUAN DEL RÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1230', 'SAN JOAQUÍN', '1028', 'SAN JOAQUÍN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1229', 'QUERÉTARO', '1028', 'QUERÉTARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1228', 'PEÑA MILLER', '1028', 'PEÑA MILLER', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1227', 'PEDRO ESCOBEDO', '1028', 'PEDRO ESCOBEDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1226', 'EL MARQUÉS', '1028', 'EL MARQUÉS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1225', 'LANDA DE MATAMOROS', '1028', 'LANDA DE MATAMOROS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1224', 'JALPAN DE SERRA', '1028', 'JALPAN DE SERRA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1223', 'HUIMILPAN', '1028', 'HUIMILPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1222', 'EZEQUIEL MONTES', '1028', 'EZEQUIEL MONTES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1221', 'CORREGIDORA', '1028', 'CORREGIDORA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1220', 'COLÓN', '1028', 'COLÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1219', 'CADEREYTA DE MONTES', '1028', 'CADEREYTA DE MONTES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1218', 'ARROYO SECO', '1028', 'ARROYO SECO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1217', 'PINAL DE AMOLES', '1028', 'PINAL DE AMOLES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1216', 'AMEALCO DE BONFIL', '1028', 'AMEALCO DE BONFIL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1215', 'QUERÉTARO', '1028', 'QUERÉTARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1214', 'ZOQUITLÁN', '1028', 'ZOQUITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1213', 'ZOQUIAPAN', '1028', 'ZOQUIAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1212', 'ZONGOZOTLA', '1028', 'ZONGOZOTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1211', 'ZINACATEPEC', '1028', 'ZINACATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1210', 'ZIHUATEUTLA', '1028', 'ZIHUATEUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1208', 'ZARAGOZA', '1028', 'ZARAGOZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1209', 'ZAUTLA', '1028', 'ZAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1207', 'ZAPOTITLÁN DE MÉNDEZ', '1028', 'ZAPOTITLÁN DE MÉNDEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1206', 'ZAPOTITLÁN', '1028', 'ZAPOTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1205', 'ZACATLÁN', '1028', 'ZACATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1204', 'ZACAPOAXTLA', '1028', 'ZACAPOAXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1203', 'ZACAPALA', '1028', 'ZACAPALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1202', 'YEHUALTEPEC', '1028', 'YEHUALTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1201', 'YAONÁHUAC', '1028', 'YAONÁHUAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1200', 'XOCHITLÁN TODOS SANTOS', '1028', 'XOCHITLÁN TODOS SANTOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1199', 'XOCHITLÁN DE VICENTE SUÁREZ', '1028', 'XOCHITLÁN DE VICENTE SUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1198', 'XOCHILTEPEC', '1028', 'XOCHILTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1197', 'XOCHIAPULCO', '1028', 'XOCHIAPULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1196', 'XIUTETELCO', '1028', 'XIUTETELCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1195', 'XICOTLÁN', '1028', 'XICOTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1194', 'XICOTEPEC', '1028', 'XICOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1193', 'XAYACATLÁN DE BRAVO', '1028', 'XAYACATLÁN DE BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1192', 'VICENTE GUERRERO', '1028', 'VICENTE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1191', 'VENUSTIANO CARRANZA', '1028', 'VENUSTIANO CARRANZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1190', 'TZICATLACOYAN', '1028', 'TZICATLACOYAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1189', 'TUZAMAPAN DE GALEANA', '1028', 'TUZAMAPAN DE GALEANA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1188', 'TULCINGO', '1028', 'TULCINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1187', 'TOTOLTEPEC DE GUERRERO', '1028', 'TOTOLTEPEC DE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1186', 'TOCHTEPEC', '1028', 'TOCHTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1185', 'TOCHIMILCO', '1028', 'TOCHIMILCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1184', 'TLAXCO', '1028', 'TLAXCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1183', 'TLATLAUQUITEPEC', '1028', 'TLATLAUQUITEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1182', 'TLAPANALÁ', '1028', 'TLAPANALÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1181', 'TLAPACOYA', '1028', 'TLAPACOYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1180', 'TLAOLA', '1028', 'TLAOLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1179', 'TLANEPANTLA', '1028', 'TLANEPANTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1178', 'TLALTENANGO', '1028', 'TLALTENANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1177', 'TLAHUAPAN', '1028', 'TLAHUAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1176', 'TLACHICHUCA', '1028', 'TLACHICHUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1175', 'TLACUILOTEPEC', '1028', 'TLACUILOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1174', 'TLACOTEPEC DE BENITO JUÁREZ', '1028', 'TLACOTEPEC DE BENITO JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1173', 'TILAPA', '1028', 'TILAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1172', 'TIANGUISMANALCO', '1028', 'TIANGUISMANALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1171', 'TEZIUTLÁN', '1028', 'TEZIUTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1170', 'TETELES DE AVILA CASTILLO', '1028', 'TETELES DE AVILA CASTILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1169', 'TETELA DE OCAMPO', '1028', 'TETELA DE OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1168', 'TEPEYAHUALCO DE CUAUHTÉMOC', '1028', 'TEPEYAHUALCO DE CUAUHTÉMOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1167', 'TEPEYAHUALCO', '1028', 'TEPEYAHUALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1166', 'TEPEXI DE RODRÍGUEZ', '1028', 'TEPEXI DE RODRÍGUEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1165', 'TEPEXCO', '1028', 'TEPEXCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1163', 'TEPEOJUMA', '1028', 'TEPEOJUMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1164', 'TEPETZINTLA', '1028', 'TEPETZINTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1162', 'TEPEMAXALCO', '1028', 'TEPEMAXALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1161', 'TEPEACA', '1028', 'TEPEACA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1160', 'TEPATLAXCO DE HIDALGO', '1028', 'TEPATLAXCO DE HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1159', 'TEPANGO DE RODRÍGUEZ', '1028', 'TEPANGO DE RODRÍGUEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1158', 'TEPANCO DE LÓPEZ', '1028', 'TEPANCO DE LÓPEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1157', 'TEOTLALCO', '1028', 'TEOTLALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1156', 'TEOPANTLÁN', '1028', 'TEOPANTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1155', 'TENAMPULCO', '1028', 'TENAMPULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1154', 'TEHUITZINGO', '1028', 'TEHUITZINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1153', 'TEHUACÁN', '1028', 'TEHUACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1152', 'TECOMATLÁN', '1028', 'TECOMATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1151', 'TECAMACHALCO', '1028', 'TECAMACHALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1149', 'SOLTEPEC', '1028', 'SOLTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1150', 'TECALI DE HERRERA', '1028', 'TECALI DE HERRERA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1148', 'SANTO TOMÁS HUEYOTLIPAN', '1028', 'SANTO TOMÁS HUEYOTLIPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1147', 'SAN ANDRÉS CHOLULA', '1028', 'SAN ANDRÉS CHOLULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1146', 'LOS REYES DE JUÁREZ', '1028', 'LOS REYES DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1145', 'RAFAEL LARA GRAJALES', '1028', 'RAFAEL LARA GRAJALES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1144', 'QUIMIXTLÁN', '1028', 'QUIMIXTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1143', 'QUECHOLAC', '1028', 'QUECHOLAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1142', 'PUEBLA', '1028', 'PUEBLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1141', 'PIAXTLA', '1028', 'PIAXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1140', 'PETLALCINGO', '1028', 'PETLALCINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1139', 'PANTEPEC', '1028', 'PANTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1138', 'PALMAR DE BRAVO', '1028', 'PALMAR DE BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1137', 'PAHUATLÁN', '1028', 'PAHUATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1136', 'ORIENTAL', '1028', 'ORIENTAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1135', 'OLINTLA', '1028', 'OLINTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1134', 'OCOYUCAN', '1028', 'OCOYUCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1133', 'OCOTEPEC', '1028', 'OCOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1132', 'NOPALUCAN', '1028', 'NOPALUCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1131', 'NICOLÁS BRAVO', '1028', 'NICOLÁS BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1130', 'NEALTICAN', '1028', 'NEALTICAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1129', 'NAUZONTLA', '1028', 'NAUZONTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1128', 'NAUPAN', '1028', 'NAUPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1126', 'MOLCAXAC', '1028', 'MOLCAXAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1127', 'CAÑADA MORELOS', '1028', 'CAÑADA MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1125', 'MIXTLA', '1028', 'MIXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1124', 'MAZAPILTEPEC DE JUÁREZ', '1028', 'MAZAPILTEPEC DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1123', 'LA MAGDALENA TLATLAUQUITEPEC', '1028', 'LA MAGDALENA TLATLAUQUITEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1122', 'LIBRES', '1028', 'LIBRES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1121', 'LAFRAGUA', '1028', 'LAFRAGUA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1120', 'JUAN N. MÉNDEZ', '1028', 'JUAN N. MÉNDEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1119', 'JUAN GALINDO', '1028', 'JUAN GALINDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1118', 'JUAN C. BONILLA', '1028', 'JUAN C. BONILLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1116', 'JONOTLA', '1028', 'JONOTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1117', 'JOPALA', '1028', 'JOPALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1115', 'JOLALPAN', '1028', 'JOLALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1114', 'JALPAN', '1028', 'JALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1113', 'IZÚCAR DE MATAMOROS', '1028', 'IZÚCAR DE MATAMOROS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1112', 'IXTEPEC', '1028', 'IXTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1111', 'IXTACAMAXTITLÁN', '1028', 'IXTACAMAXTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1110', 'IXCAQUIXTLA', '1028', 'IXCAQUIXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1109', 'IXCAMILPA DE GUERRERO', '1028', 'IXCAMILPA DE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1108', 'ATLEQUIZAYAN', '1028', 'ATLEQUIZAYAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1107', 'HUITZILTEPEC', '1028', 'HUITZILTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1106', 'HUITZILAN DE SERDÁN', '1028', 'HUITZILAN DE SERDÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1105', 'HUEYTLALPAN', '1028', 'HUEYTLALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1104', 'HUEYTAMALCO', '1028', 'HUEYTAMALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1103', 'HUEYAPAN', '1028', 'HUEYAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1102', 'HUEJOTZINGO', '1028', 'HUEJOTZINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1101', 'HUEHUETLÁN EL CHICO', '1028', 'HUEHUETLÁN EL CHICO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1100', 'HUEHUETLA', '1028', 'HUEHUETLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1099', 'HUAUCHINANGO', '1028', 'HUAUCHINANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1098', 'HUATLATLAUCA', '1028', 'HUATLATLAUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1097', 'HUAQUECHULA', '1028', 'HUAQUECHULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1096', 'HERMENEGILDO GALEANA', '1028', 'HERMENEGILDO GALEANA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1095', 'GUADALUPE VICTORIA', '1028', 'GUADALUPE VICTORIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1094', 'GUADALUPE', '1028', 'GUADALUPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1093', 'GENERAL FELIPE ÁNGELES', '1028', 'GENERAL FELIPE ÁNGELES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1092', 'FRANCISCO Z. MENA', '1028', 'FRANCISCO Z. MENA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1091', 'ESPERANZA', '1028', 'ESPERANZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1090', 'EPATLÁN', '1028', 'EPATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1089', 'ELOXOCHITLÁN', '1028', 'ELOXOCHITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1088', 'DOMINGO ARENAS', '1028', 'DOMINGO ARENAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1087', 'CHINANTLA', '1028', 'CHINANTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1085', 'HONEY', '1028', 'HONEY', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1086', 'CHILCHOTLA', '1028', 'CHILCHOTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1084', 'CHILA DE LA SAL', '1028', 'CHILA DE LA SAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1083', 'CHILA', '1028', 'CHILA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1082', 'CHIGNAUTLA', '1028', 'CHIGNAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1081', 'CHIGNAHUAPAN', '1028', 'CHIGNAHUAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1080', 'CHIGMECATITLÁN', '1028', 'CHIGMECATITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1079', 'CHIETLA', '1028', 'CHIETLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1078', 'CHICHIQUILA', '1028', 'CHICHIQUILA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1077', 'CHICONCUAUTLA', '1028', 'CHICONCUAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1076', 'CHIAUTZINGO', '1028', 'CHIAUTZINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1075', 'CHIAUTLA', '1028', 'CHIAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1074', 'CHAPULCO', '1028', 'CHAPULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1073', 'CHALCHICOMULA DE SESMA', '1028', 'CHALCHICOMULA DE SESMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1072', 'CUYOACO', '1028', 'CUYOACO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1071', 'CUETZALAN DEL PROGRESO', '1028', 'CUETZALAN DEL PROGRESO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1070', 'CUAYUCA DE ANDRADE', '1028', 'CUAYUCA DE ANDRADE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1069', 'CUAUTLANCINGO', '1028', 'CUAUTLANCINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1068', 'CUAUTINCHÁN', '1028', 'CUAUTINCHÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1067', 'CUAUTEMPAN', '1028', 'CUAUTEMPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1066', 'CUAPIAXTLA DE MADERO', '1028', 'CUAPIAXTLA DE MADERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1065', 'COYOTEPEC', '1028', 'COYOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1064', 'COYOMEAPAN', '1028', 'COYOMEAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1063', 'COXCATLÁN', '1028', 'COXCATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1062', 'CORONANGO', '1028', 'CORONANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1061', 'COHUECAN', '1028', 'COHUECAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1060', 'COHETZALA', '1028', 'COHETZALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1059', 'COATZINGO', '1028', 'COATZINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1058', 'COATEPEC', '1028', 'COATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1057', 'CAXHUACAN', '1028', 'CAXHUACAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1056', 'CAMOCUAUTLA', '1028', 'CAMOCUAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1055', 'CALTEPEC', '1028', 'CALTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1054', 'CALPAN', '1028', 'CALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1053', 'AYOTOXCO DE GUERRERO', '1028', 'AYOTOXCO DE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1052', 'AXUTLA', '1028', 'AXUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1051', 'ATZITZINTLA', '1028', 'ATZITZINTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1050', 'ATZITZIHUACÁN', '1028', 'ATZITZIHUACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1049', 'ATZALA', '1028', 'ATZALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1048', 'ATOYATEMPAN', '1028', 'ATOYATEMPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1047', 'ATLIXCO', '1028', 'ATLIXCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1046', 'ATEXCAL', '1028', 'ATEXCAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1045', 'ATEMPAN', '1028', 'ATEMPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1044', 'AQUIXTLA', '1028', 'AQUIXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1043', 'AMOZOC', '1028', 'AMOZOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1042', 'AMIXTLÁN', '1028', 'AMIXTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1041', 'ALTEPEXI', '1028', 'ALTEPEXI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1040', 'ALJOJUCA', '1028', 'ALJOJUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1039', 'ALBINO ZERTUCHE', '1028', 'ALBINO ZERTUCHE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1038', 'AJALPAN', '1028', 'AJALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1037', 'AHUEHUETITLA', '1028', 'AHUEHUETITLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1036', 'AHUAZOTEPEC', '1028', 'AHUAZOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1035', 'AHUATLÁN', '1028', 'AHUATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1034', 'AHUACATLÁN', '1028', 'AHUACATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1033', 'ACTEOPAN', '1028', 'ACTEOPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1032', 'ACATZINGO', '1028', 'ACATZINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1031', 'ACATLÁN', '1028', 'ACATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1030', 'ACATENO', '1028', 'ACATENO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1029', 'ACAJETE', '1028', 'ACAJETE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1028', 'PUEBLA', '0', 'PUEBLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1027', 'SAN AGUSTÍN AMATENGO', '947', 'SAN AGUSTÍN AMATENGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1026', 'SALINA CRUZ', '947', 'SALINA CRUZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1025', 'ROJAS DE CUAUHTÉMOC', '947', 'ROJAS DE CUAUHTÉMOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1024', 'REYES ETLA', '947', 'REYES ETLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1023', 'LA REFORMA', '947', 'LA REFORMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1022', 'REFORMA DE PINEDA', '947', 'REFORMA DE PINEDA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1021', 'SANTA CATARINA QUIOQUITANI', '947', 'SANTA CATARINA QUIOQUITANI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1020', 'PUTLAVILLA DE GUERRERO', '947', 'PUTLA VILLA DE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1019', 'SAN JOSÉ DEL PROGRESO', '947', 'SAN JOSÉ DEL PROGRESO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1018', 'PLUMA HIDALGO', '947', 'PLUMA HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1017', 'PINOTEPA DE DON LUIS', '947', 'PINOTEPA DE DON LUIS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1015', 'OCOTLÁN DE MORELOS', '947', 'OCOTLÁN DE MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1014', 'OAXACA DE JUÁREZ', '947', 'OAXACA DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1013', 'SANTIAGO NILTEPEC', '947', 'SANTIAGO NILTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1012', 'IXPANTEPEC NIEVES', '947', 'IXPANTEPEC NIEVES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1011', 'NEJAPA DE MADERO', '947', 'NEJAPA DE MADERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1010', 'NAZARENO ETLA', '947', 'NAZARENO ETLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1007', 'MIXISTLÁN DE LA REFORMA', '947', 'MIXISTLÁN DE LA REFORMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1006', 'MIAHUATLÁN DE PORFIRIO DÍAZ', '947', 'MIAHUATLÁN DE PORFIRIO DÍAZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1005', 'MAZATLÁN VILLA DE FLORES', '947', 'MAZATLÁN VILLA DE FLORES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1004', 'MATÍAS ROMERO AVENDAÑO', '947', 'MATÍAS ROMERO AVENDAÑO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1003', 'MÁRTIRES DE TACUBAYA', '947', 'MÁRTIRES DE TACUBAYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1002', 'MARISCALA DE JUÁREZ', '947', 'MARISCALA DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1001', 'MAGDALENA ZAHUATLÁN', '947', 'MAGDALENA ZAHUATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1000', 'MAGDALENA TLACOTEPEC', '947', 'MAGDALENA TLACOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('999', 'MAGDALENA TEQUISISTLÁN', '947', 'MAGDALENA TEQUISISTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('998', 'MAGDALENA TEITIPAC', '947', 'MAGDALENA TEITIPAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('997', 'MAGDALENA PEÑASCO', '947', 'MAGDALENA PEÑASCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('996', 'MAGDALENA OCOTLÁN', '947', 'MAGDALENA OCOTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('995', 'MAGDALENA MIXTEPEC', '947', 'MAGDALENA MIXTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('994', 'SANTA MAGDALENA JICOTLÁN', '947', 'SANTA MAGDALENA JICOTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('993', 'MAGDALENA JALTEPEC', '947', 'MAGDALENA JALTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('992', 'MAGDALENA APASCO', '947', 'MAGDALENA APASCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('991', 'LOMA BONITA', '947', 'LOMA BONITA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('990', 'HEROICA CIUDAD DE JUCHITÁN DE ZARAGOZA', '947', 'HEROICA CIUDAD DE JUCHITÁN DE ZARAGOZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('989', 'IXTLÁN DE JUÁREZ', '947', 'IXTLÁN DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('988', 'HUAUTLA DE JIMÉNEZ', '947', 'HUAUTLA DE JIMÉNEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('987', 'HUAUTEPEC', '947', 'HUAUTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('986', 'HEROICA CIUDAD DE HUAJUAPAN DE LEÓN', '947', 'HEROICA CIUDAD DE HUAJUAPAN DE LEÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('985', 'VILLA HIDALGO', '947', 'VILLA HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('984', 'MESONES HIDALGO', '947', 'MESONES HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('983', 'GUEVEA DE HUMBOLDT', '947', 'GUEVEA DE HUMBOLDT', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('982', 'GUELATAO DE JUÁREZ', '947', 'GUELATAO DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('981', 'GUADALUPE DE RAMÍREZ', '947', 'GUADALUPE DE RAMÍREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('980', 'GUADALUPE ETLA', '947', 'GUADALUPE ETLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('979', 'FRESNILLO DE TRUJANO', '947', 'FRESNILLO DE TRUJANO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('978', 'TAMAZULÁPAM DEL ESPÍRITU SANTO', '947', 'TAMAZULÁPAM DEL ESPÍRITU SANTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('977', 'EL ESPINAL', '947', 'EL ESPINAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('976', 'ELOXOCHITLÁN DE FLORES MAGÓN', '947', 'ELOXOCHITLÁN DE FLORES MAGÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('975', 'HEROICA CIUDAD DE EJUTLA DE CRESPO', '947', 'HEROICA CIUDAD DE EJUTLA DE CRESPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('974', 'CHIQUIHUITLÁN DE BENITO JUÁREZ', '947', 'CHIQUIHUITLÁN DE BENITO JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('973', 'CHALCATONGO DE HIDALGO', '947', 'CHALCATONGO DE HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('972', 'CHAHUITES', '947', 'CHAHUITES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('971', 'CUYAMECALCO VILLA DE ZARAGOZA', '947', 'CUYAMECALCO VILLA DE ZARAGOZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('970', 'CUILÁPAM DE GUERRERO', '947', 'CUILÁPAM DE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('969', 'COSOLTEPEC', '947', 'COSOLTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('968', 'COSOLAPA', '947', 'COSOLAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('967', 'CONSTANCIA DEL ROSARIO', '947', 'CONSTANCIA DEL ROSARIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('966', 'CONCEPCIÓN PÁPALO', '947', 'CONCEPCIÓN PÁPALO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('965', 'CONCEPCIÓN BUENAVISTA', '947', 'CONCEPCIÓN BUENAVISTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('964', 'LA COMPAÑÍA', '947', 'LA COMPAÑÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('963', 'COICOYÁN DE LAS FLORES', '947', 'COICOYÁN DE LAS FLORES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('962', 'COATECAS ALTAS', '947', 'COATECAS ALTAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('961', 'CIUDAD IXTEPEC', '947', 'CIUDAD IXTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('960', 'CIÉNEGA DE ZIMATLÁN', '947', 'CIÉNEGA DE ZIMATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('959', 'CANDELARIA LOXICHA', '947', 'CANDELARIA LOXICHA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('958', 'CALIHUALÁ', '947', 'CALIHUALÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('957', 'EL BARRIO DE LA SOLEDAD', '947', 'EL BARRIO DE LA SOLEDAD', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('956', 'AYOTZINTEPEC', '947', 'AYOTZINTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('955', 'ASUNCIÓN TLACOLULITA', '947', 'ASUNCIÓNTLACOLULITA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('954', 'ASUNCIÓN OCOTLÁN', '947', 'ASUNCIÓN OCOTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('953', 'ASUNCIÓN NOCHIXTLÁN', '947', 'ASUNCIÓN NOCHIXTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('952', 'ASUNCIÓN IXTALTEPEC', '947', 'ASUNCIÓN IXTALTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('951', 'ASUNCIÓN CUYOTEPEJI', '947', 'ASUNCIÓN CUYOTEPEJI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('950', 'ASUNCIÓN CACALOTEPEC', '947', 'ASUNCIÓN CACALOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('949', 'ACATLÁN DE PÉREZ FIGUEROA', '947', 'ACATLÁN DE PÉREZ FIGUEROA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('948', 'ABEJONES', '947', 'ABEJONES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('947', 'OAXACA', '0', 'OAXACA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('946', 'VILLALDAMA', '895', 'VILLALDAMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('945', 'VALLECILLO', '895', 'VALLECILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('944', 'SANTIAGO', '895', 'SANTIAGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('943', 'SANTA CATARINA', '895', 'SANTA CATARINA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('942', 'HIDALGO', '895', 'HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('941', 'SAN NICOLÁS DE LOS GARZA', '895', 'SAN NICOLÁS DE LOS GARZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('940', 'SALINAS VICTORIA', '895', 'SALINAS VICTORIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('939', 'SABINAS HIDALGO', '895', 'SABINAS HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('938', 'RAYONES', '895', 'RAYONES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('937', 'LOS RAMONES', '895', 'LOS RAMONES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('936', 'PESQUERÍA', '895', 'PESQUERÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('935', 'PARÁS', '895', 'PARÁS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('934', 'MONTERREY', '895', 'MONTERREY', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('933', 'MONTEMORELOS', '895', 'MONTEMORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('932', 'MINA', '895', 'MINA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('931', 'MIER Y NORIEGA', '895', 'MIER Y NORIEGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('930', 'MELCHOR OCAMPO', '895', 'MELCHOR OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('929', 'MARÍN', '895', 'MARÍN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('928', 'LINARES', '895', 'LINARES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('926', 'JUÁREZ', '895', 'JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('927', 'LAMPAZOS DE NARANJO', '895', 'LAMPAZOS DE NARANJO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('925', 'ITURBIDE', '895', 'ITURBIDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('924', 'HUALAHUISES', '895', 'HUALAHUISES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('923', 'HIGUERAS', '895', 'HIGUERAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('922', 'LOS HERRERAS', '895', 'LOS HERRERAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('921', 'GUADALUPE', '895', 'GUADALUPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('920', 'GENERAL ZUAZUA', '895', 'GENERAL ZUAZUA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('919', 'GENERAL ZARAGOZA', '895', 'GENERAL ZARAGOZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('918', 'GENERAL TREVIÑO', '895', 'GENERAL TREVIÑO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('917', 'GENERAL TERÁN', '895', 'GENERAL TERÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('916', 'GENERAL ESCOBEDO', '895', 'GENERAL ESCOBEDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('915', 'GENERAL BRAVO', '895', 'GENERAL BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('914', 'SAN PEDRO GARZA GARCÍA', '895', 'SAN PEDRO GARZA GARCÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('913', 'GARCÍA', '895', 'GARCÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('912', 'GALEANA', '895', 'GALEANA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('911', 'DOCTOR GONZÁLEZ', '895', 'DOCTOR GONZÁLEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('910', 'DOCTOR COSS', '895', 'DOCTOR COSS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('909', 'DOCTOR ARROYO', '895', 'DOCTOR ARROYO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('908', 'CHINA', '895', 'CHINA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('907', 'CIÉNEGA DE FLORES', '895', 'CIÉNEGA DE FLORES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('906', 'CERRALVO', '895', 'CERRALVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('905', 'EL CARMEN', '895', 'EL CARMEN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('904', 'CADEREYTA JIMÉNEZ', '895', 'CADEREYTA JIMÉNEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('903', 'BUSTAMANTE', '895', 'BUSTAMANTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('901', 'APODACA', '895', 'APODACA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('902', 'ARAMBERRI', '895', 'ARAMBERRI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('900', 'ANÁHUAC', '895', 'ANÁHUAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('899', 'ALLENDE', '895', 'ALLENDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('898', 'LOS ALDAMAS', '895', 'LOS ALDAMAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('897', 'AGUALEGUAS', '895', 'AGUALEGUAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('896', 'ABASOLO', '895', 'ABASOLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('895', 'NUEVO LEÓN', '0', 'NUEVO LEÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('894', 'BAHÍA DE BANDERAS', '840', 'BAHÍA DE BANDERAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('893', 'LA YESCA', '840', 'LA YESCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('892', 'TUXPAN', '840', 'TUXPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('891', 'TEPIC', '840', 'TEPIC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('890', 'TECUALA', '840', 'TECUALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('889', 'SANTIAGO IXCUINTLA', '840', 'SANTIAGO IXCUINTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('888', 'SANTA MARÍA DEL ORO', '840', 'SANTA MARÍA DEL ORO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('887', 'SAN PEDRO LAGUNILLAS', '840', 'SAN PEDRO LAGUNILLAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('886', 'SAN BLAS', '840', 'SAN BLAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('885', 'RUÍZ', '840', 'RUÍZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('884', 'ROSAMORADA', '840', 'ROSAMORADA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('883', 'DEL NAYAR', '840', 'DEL NAYAR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('882', 'XALISCO', '840', 'XALISCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('881', 'JALA', '840', 'JALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('880', 'IXTLÁN DEL RÍO', '840', 'IXTLÁN DEL RÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('879', 'HUAJICORI', '840', 'HUAJICORI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('878', 'COMPOSTELA', '840', 'COMPOSTELA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('877', 'AMATLÁN DE CAÑAS', '840', 'AMATLÁN DE CAÑAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('876', 'AHUACATLÁN', '840', 'AHUACATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('875', 'ACAPONETA', '840', 'ACAPONETA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('874', 'NAYARIT', '840', 'NAYARIT', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('873', 'TEMOAC', '840', 'TEMOAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('872', 'ZACUALPAN DE AMILPAS', '840', 'ZACUALPAN DE AMILPAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('871', 'ZACATEPEC', '840', 'ZACATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('870', 'YECAPIXTLA', '840', 'YECAPIXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('869', 'YAUTEPEC', '840', 'YAUTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('868', 'XOCHITEPEC', '840', 'XOCHITEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('867', 'TOTOLAPAN', '840', 'TOTOLAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('866', 'TLAYACAPAN', '840', 'TLAYACAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('865', 'TLAQUILTENANGO', '840', 'TLAQUILTENANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('864', 'TLALTIZAPÁN DE ZAPATA', '840', 'TLALTIZAPÁN DE ZAPATA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('863', 'TLALNEPANTLA', '840', 'TLALNEPANTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('862', 'TETELA DEL VOLCÁN', '840', 'TETELA DEL VOLCÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('861', 'TETECALA', '840', 'TETECALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('860', 'TEPOZTLÁN', '840', 'TEPOZTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('859', 'TEPALCINGO', '840', 'TEPALCINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('858', 'TEMIXCO', '840', 'TEMIXCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('857', 'PUENTE DE IXTLA', '840', 'PUENTE DE IXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('856', 'OCUITUCO', '840', 'OCUITUCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('855', 'MIACATLÁN', '840', 'MIACATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('854', 'MAZATEPEC', '840', 'MAZATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('853', 'JONACATEPEC DE LEANDRO VALLE', '840', 'JONACATEPEC DE LEANDRO VALLE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('852', 'JOJUTLA', '840', 'JOJUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('851', 'JIUTEPEC', '840', 'JIUTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('850', 'JANTETELCO', '840', 'JANTETELCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('849', 'HUITZILAC', '840', 'HUITZILAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('848', 'EMILIANO ZAPATA', '840', 'EMILIANO ZAPATA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('847', 'CUERNAVACA', '840', 'CUERNAVACA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('846', 'CUAUTLA', '840', 'CUAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('845', 'COATLÁN DEL RÍO', '840', 'COATLÁN DEL RÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('844', 'AYALA', '840', 'AYALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('843', 'AXOCHIAPAN', '840', 'AXOCHIAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('842', 'ATLATLAHUCAN', '840', 'ATLATLAHUCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('841', 'AMACUZAC', '840', 'AMACUZAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('840', 'MORELOS', '0', 'MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('839', 'JOSÉ SIXTO VERDUZCO', '726', 'JOSÉ SIXTO VERDUZCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('838', 'ZITÁCUARO', '726', 'ZITÁCUARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('837', 'ZIRACUARETIRO', '726', 'ZIRACUARETIRO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('836', 'ZINAPÉCUARO', '726', 'ZINAPÉCUARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('835', 'ZINÁPARO', '726', 'ZINÁPARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('834', 'ZAMORA', '726', 'ZAMORA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('833', 'ZACAPU', '726', 'ZACAPU', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('832', 'YURÉCUARO', '726', 'YURÉCUARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('831', 'VISTA HERMOSA', '726', 'VISTA HERMOSA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('830', 'VILLAMAR', '726', 'VILLAMAR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('829', 'VENUSTIANO CARRANZA', '726', 'VENUSTIANO CARRANZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('828', 'URUAPAN', '726', 'URUAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('827', 'TZITZIO', '726', 'TZITZIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('826', 'TZINTZUNTZAN', '726', 'TZINTZUNTZAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('825', 'TUZANTLA', '726', 'TUZANTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('824', 'TUXPAN', '726', 'TUXPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('823', 'TURICATO', '726', 'TURICATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('822', 'TUMBISCATÍO', '726', 'TUMBISCATÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('821', 'TOCUMBO', '726', 'TOCUMBO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('820', 'TLAZAZALCA', '726', 'TLAZAZALCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('819', 'TLALPUJAHUA', '726', 'TLALPUJAHUA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('818', 'TIQUICHEO DE NICOLÁS ROMERO', '726', 'TIQUICHEO DE NICOLÁS ROMERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('817', 'TINGÜINDÍN', '726', 'TINGÜINDÍN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('816', 'TINGAMBATO', '726', 'TINGAMBATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('815', 'TEPALCATEPEC', '726', 'TEPALCATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('814', 'TARÍMBARO', '726', 'TARÍMBARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('813', 'TARETAN', '726', 'TARETAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('812', 'TANHUATO', '726', 'TANHUATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('811', 'TANGANCÍCUARO', '726', 'TANGANCÍCUARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('810', 'TANGAMANDAPIO', '726', 'TANGAMANDAPIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('809', 'TANCÍTARO', '726', 'TANCÍTARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('808', 'TACÁMBARO', '726', 'TACÁMBARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('807', 'SUSUPUATO', '726', 'SUSUPUATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('806', 'SENGUIO', '726', 'SENGUIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('805', 'SALVADOR ESCALANTE', '726', 'SALVADOR ESCALANTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('804', 'SANTA ANA MAYA', '726', 'SANTA ANA MAYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('802', 'SAHUAYO', '726', 'SAHUAYO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('803', 'SAN LUCAS', '726', 'SAN LUCAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('801', 'LOS REYES', '726', 'LOS REYES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('799', 'QUIROGA', '726', 'QUIROGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('800', 'COJUMATLÁN DE RÉGULES', '726', 'COJUMATLÁN DE RÉGULES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('798', 'QUERÉNDARO', '726', 'QUERÉNDARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('796', 'PURÉPERO', '726', 'PURÉPERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('797', 'PURUÁNDIRO', '726', 'PURUÁNDIRO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('795', 'LA PIEDAD', '726', 'LA PIEDAD', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('793', 'PENJAMILLO', '726', 'PENJAMILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('794', 'PERIBÁN', '726', 'PERIBÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('792', 'PÁTZCUARO', '726', 'PÁTZCUARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('791', 'PARACHO', '726', 'PARACHO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('790', 'PARÁCUARO', '726', 'PARÁCUARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('789', 'PANINDÍCUARO', '726', 'PANINDÍCUARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('788', 'PAJACUARÁN', '726', 'PAJACUARÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('787', 'OCAMPO', '726', 'OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('786', 'NUMARÁN', '726', 'NUMARÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('785', 'NUEVO URECHO', '726', 'NUEVO URECHO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('784', 'NUEVO PARANGARICUTIRO', '726', 'NUEVO PARANGARICUTIRO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('783', 'NOCUPÉTARO', '726', 'NOCUPÉTARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('782', 'NAHUATZEN', '726', 'NAHUATZEN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('780', 'MORELOS', '726', 'MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('781', 'MÚGICA', '726', 'MÚGICA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('779', 'MORELIA', '726', 'MORELIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('778', 'LÁZARO CÁRDENAS', '726', 'LÁZARO CÁRDENAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('777', 'MARCOS CASTELLANOS', '726', 'MARCOS CASTELLANOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('776', 'MARAVATÍO', '726', 'MARAVATÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('775', 'MADERO', '726', 'MADERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('774', 'LAGUNILLAS', '726', 'LAGUNILLAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('773', 'JUNGAPEO', '726', 'JUNGAPEO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('772', 'JUÁREZ', '726', 'JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('771', 'JIQUILPAN', '726', 'JIQUILPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('770', 'JIMÉNEZ', '726', 'JIMÉNEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('769', 'JACONA', '726', 'JACONA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('768', 'IXTLÁN', '726', 'IXTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('767', 'IRIMBO', '726', 'IRIMBO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('766', 'INDAPARAPEO', '726', 'INDAPARAPEO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('765', 'HUIRAMBA', '726', 'HUIRAMBA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('763', 'HUANIQUEO', '726', 'HUANIQUEO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('764', 'HUETAMO', '726', 'HUETAMO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('762', 'HUANDACAREO', '726', 'HUANDACAREO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('761', 'LA HUACANA', '726', 'LA HUACANA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('760', 'HIDALGO', '726', 'HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('759', 'GABRIEL ZAMORA', '726', 'GABRIEL ZAMORA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('758', 'ERONGARÍCUARO', '726', 'ERONGARÍCUARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('757', 'EPITACIO HUERTA', '726', 'EPITACIO HUERTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('756', 'ECUANDUREO', '726', 'ECUANDUREO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('755', 'CHURUMUCO', '726', 'CHURUMUCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('754', 'CHURINTZIO', '726', 'CHURINTZIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('753', 'CHUCÁNDIRO', '726', 'CHUCÁNDIRO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('752', 'CHINICUILA', '726', 'CHINICUILA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('751', 'CHILCHOTA', '726', 'CHILCHOTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('750', 'CHERÁN', '726', 'CHERÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('749', 'CHAVINDA', '726', 'CHAVINDA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('748', 'CHARO', '726', 'CHARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('747', 'CHARAPAN', '726', 'CHARAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('746', 'CUITZEO', '726', 'CUITZEO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('745', 'COTIJA', '726', 'COTIJA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('744', 'COPÁNDARO', '726', 'COPÁNDARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('743', 'CONTEPEC', '726', 'CONTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('742', 'COENEO', '726', 'COENEO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('741', 'COALCOMÁN DE VÁZQUEZ PALLARES', '726', 'COALCOMÁN DE VÁZQUEZ PALLARES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('740', 'COAHUAYANA', '726', 'COAHUAYANA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('739', 'CARÁCUARO', '726', 'CARÁCUARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('738', 'BUENAVISTA', '726', 'BUENAVISTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('737', 'BRISEÑAS', '726', 'BRISEÑAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('736', 'ARTEAGA', '726', 'ARTEAGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('735', 'ARIO', '726', 'ARIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('734', 'AQUILA', '726', 'AQUILA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('733', 'APORO', '726', 'APORO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('732', 'APATZINGÁN', '726', 'APATZINGÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('731', 'ANGANGUEO', '726', 'ANGANGUEO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('730', 'ANGAMACUTIRO', '726', 'ANGAMACUTIRO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('729', 'ÁLVARO OBREGÓN', '726', 'ÁLVARO OBREGÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('728', 'AGUILILLA', '726', 'AGUILILLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('727', 'ACUITZIO', '726', 'ACUITZIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('726', 'MICHOACÁN', '0', 'MICHOACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('725', 'TONANITLA', '600', 'TONANITLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('724', 'SAN JOSÉ DEL RINCÓN', '600', 'SAN JOSÉ DEL RINCÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('723', 'LUVIANOS', '600', 'LUVIANOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('722', 'VALLE DE CHALCO SOLIDARIDAD', '600', 'VALLE DE CHALCO SOLIDARIDAD', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('721', 'CUAUTITLÁN IZCALLI', '600', 'CUAUTITLÁN IZCALLI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('720', 'ZUMPANGO', '600', 'ZUMPANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('719', 'ZUMPAHUACÁN', '600', 'ZUMPAHUACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('718', 'ZINACANTEPEC', '600', 'ZINACANTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('717', 'ZACUALPAN', '600', 'ZACUALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('716', 'ZACAZONAPAN', '600', 'ZACAZONAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('715', 'XONACATLÁN', '600', 'XONACATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('714', 'VILLA VICTORIA', '600', 'VILLA VICTORIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('713', 'VILLA GUERRERO', '600', 'VILLA GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('712', 'VILLA DEL CARBÓN', '600', 'VILLA DEL CARBÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('711', 'VILLA DE ALLENDE', '600', 'VILLA DE ALLENDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('710', 'VALLE DE BRAVO', '600', 'VALLE DE BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('709', 'TULTITLÁN', '600', 'TULTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('708', 'TULTEPEC', '600', 'TULTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('707', 'TONATICO', '600', 'TONATICO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('706', 'TOLUCA', '600', 'TOLUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('705', 'TLATLAYA', '600', 'TLATLAYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('704', 'TLALNEPANTLA DE BAZ', '600', 'TLALNEPANTLA DE BAZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('703', 'TLALMANALCO', '600', 'TLALMANALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('702', 'TIMILPAN', '600', 'TIMILPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('701', 'TIANGUISTENCO', '600', 'TIANGUISTENCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('700', 'TEZOYUCA', '600', 'TEZOYUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('699', 'TEXCOCO', '600', 'TEXCOCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('698', 'TEXCALYACAC', '600', 'TEXCALYACAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('697', 'TEXCALTITLÁN', '600', 'TEXCALTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('696', 'TEQUIXQUIAC', '600', 'TEQUIXQUIAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('695', 'TEPOTZOTLÁN', '600', 'TEPOTZOTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('694', 'TEPETLIXPA', '600', 'TEPETLIXPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('693', 'TEPETLAOXTOC', '600', 'TEPETLAOXTOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('692', 'TEOTIHUACÁN', '600', 'TEOTIHUACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('691', 'TEOLOYUCAN', '600', 'TEOLOYUCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('690', 'TENANGO DEL VALLE', '600', 'TENANGO DEL VALLE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('689', 'TENANGO DEL AIRE', '600', 'TENANGO DEL AIRE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('688', 'TENANCINGO', '600', 'TENANCINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('687', 'TEMOAYA', '600', 'TEMOAYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('686', 'TEMASCALTEPEC', '600', 'TEMASCALTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('685', 'TEMASCALCINGO', '600', 'TEMASCALCINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('684', 'TEMASCALAPA', '600', 'TEMASCALAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('683', 'TEMAMATLA', '600', 'TEMAMATLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('682', 'TEJUPILCO', '600', 'TEJUPILCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('681', 'TECÁMAC', '600', 'TECÁMAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('680', 'SULTEPEC', '600', 'SULTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('679', 'SOYANIQUILPAN DE JUÁREZ', '600', 'SOYANIQUILPAN DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('678', 'SANTO TOMÁS', '600', 'SANTO TOMÁS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('677', 'SAN SIMÓN DE GUERRERO', '600', 'SAN SIMÓN DE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('676', 'SAN MATEO ATENCO', '600', 'SAN MATEO ATENCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('675', 'SAN MARTÍN DE LAS PIRÁMIDES', '600', 'SAN MARTÍN DE LAS PIRÁMIDES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('674', 'SAN FELIPE DEL PROGRESO', '600', 'SAN FELIPE DEL PROGRESO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('673', 'SAN ANTONIO LA ISLA', '600', 'SAN ANTONIO LA ISLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('672', 'RAYÓN', '600', 'RAYÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('671', 'POLOTITLÁN', '600', 'POLOTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('670', 'LA PAZ', '600', 'LA PAZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('669', 'PAPALOTLA', '600', 'PAPALOTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('668', 'OZUMBA', '600', 'OZUMBA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('667', 'OTZOLOTEPEC', '600', 'OTZOLOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('665', 'OTUMBA', '600', 'OTUMBA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('666', 'OTZOLOAPAN', '600', 'OTZOLOAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('664', 'EL ORO', '600', 'EL ORO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('663', 'OCUILAN', '600', 'OCUILAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('662', 'OCOYOACAC', '600', 'OCOYOACAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('661', 'NOPALTEPEC', '600', 'NOPALTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('660', 'NICOLÁS ROMERO', '600', 'NICOLÁS ROMERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('659', 'NEXTLALPAN', '600', 'NEXTLALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('658', 'NEZAHUALCÓYOTL', '600', 'NEZAHUALCÓYOTL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('657', 'NAUCALPAN DE JUÁREZ', '600', 'NAUCALPAN DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('656', 'MORELOS', '600', 'MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('655', 'MEXICALTZINGO', '600', 'MEXICALTZINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('654', 'METEPEC', '600', 'METEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('653', 'MELCHOR OCAMPO', '600', 'MELCHOR OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('652', 'MALINALCO', '600', 'MALINALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('651', 'LERMA', '600', 'LERMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('650', 'JUCHITEPEC', '600', 'JUCHITEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('649', 'JOQUICINGO', '600', 'JOQUICINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('648', 'JOCOTITLÁN', '600', 'JOCOTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('647', 'JIQUIPILCO', '600', 'JIQUIPILCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('646', 'JILOTZINGO', '600', 'JILOTZINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('645', 'JILOTEPEC', '600', 'JILOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('644', 'JALTENCO', '600', 'JALTENCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('643', 'XALATLACO', '600', 'XALATLACO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('642', 'IXTLAHUACA', '600', 'IXTLAHUACA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('641', 'IXTAPAN DEL ORO', '600', 'IXTAPAN DEL ORO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('640', 'IXTAPAN DE LA SAL', '600', 'IXTAPAN DE LA SAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('639', 'IXTAPALUCA', '600', 'IXTAPALUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('638', 'ISIDRO FABELA', '600', 'ISIDRO FABELA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('637', 'HUIXQUILUCAN', '600', 'HUIXQUILUCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('636', 'HUEYPOXTLA', '600', 'HUEYPOXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('635', 'HUEHUETOCA', '600', 'HUEHUETOCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('634', 'ECATZINGO', '600', 'ECATZINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('633', 'ECATEPEC DE MORELOS', '600', 'ECATEPEC DE MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('632', 'DONATO GUERRA', '600', 'DONATO GUERRA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('631', 'CHIMALHUACÁN', '600', 'CHIMALHUACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('630', 'CHICONCUAC', '600', 'CHICONCUAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('629', 'CHICOLOAPAN', '600', 'CHICOLOAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('628', 'CHIAUTLA', '600', 'CHIAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('627', 'CHAPULTEPEC', '600', 'CHAPULTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('626', 'CHAPA DE MOTA', '600', 'CHAPA DE MOTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('625', 'CHALCO', '600', 'CHALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('624', 'CUAUTITLÁN', '600', 'CUAUTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('623', 'COYOTEPEC', '600', 'COYOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('622', 'COCOTITLÁN', '600', 'COCOTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('621', 'COATEPEC HARINAS', '600', 'COATEPEC HARINAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('620', 'COACALCO DE BERRIOZÁBAL', '600', 'COACALCO DE BERRIOZÁBAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('619', 'CAPULHUAC', '600', 'CAPULHUAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('618', 'CALIMAYA', '600', 'CALIMAYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('617', 'AYAPANGO', '600', 'AYAPANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('616', 'AXAPUSCO', '600', 'AXAPUSCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('615', 'ATLAUTLA', '600', 'ATLAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('614', 'ATLACOMULCO', '600', 'ATLACOMULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('613', 'ATIZAPÁN DE ZARAGOZA', '600', 'ATIZAPÁN DE ZARAGOZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('612', 'ATIZAPÁN', '600', 'ATIZAPÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('611', 'ATENCO', '600', 'ATENCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('610', 'APAXCO', '600', 'APAXCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('609', 'AMECAMECA', '600', 'AMECAMECA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('608', 'AMATEPEC', '600', 'AMATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('607', 'AMANALCO', '600', 'AMANALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('606', 'ALMOLOYA DEL RÍO', '600', 'ALMOLOYA DEL RÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('605', 'ALMOLOYA DE JUÁREZ', '600', 'ALMOLOYA DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('604', 'ALMOLOYA DE ALQUISIRAS', '600', 'ALMOLOYA DE ALQUISIRAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('602', 'ACOLMAN', '600', 'ACOLMAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('603', 'ACULCO', '600', 'ACULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('601', 'ACAMBAY DE RUÍZ CASTAÑEDA', '600', 'ACAMBAY DE RUÍZ CASTAÑEDA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('600', 'ESTADO DE MÉXICO', '0', 'ESTADO DE MÉXICO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('599', 'SAN IGNACIO CERRO GORDO', '474', 'SAN IGNACIO CERRO GORDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('598', 'ZAPOTLANEJO', '474', 'ZAPOTLANEJO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('597', 'ZAPOTLÁN DEL REY', '474', 'ZAPOTLÁN DEL REY', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('596', 'ZAPOTITLÁN DE VADILLO', '474', 'ZAPOTITLÁN DE VADILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('595', 'ZAPOTILTIC', '474', 'ZAPOTILTIC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('594', 'ZAPOPAN', '474', 'ZAPOPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('593', 'ZACOALCO DE TORRES', '474', 'ZACOALCO DE TORRES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('592', 'YAHUALICA DE GONZÁLEZ GALLO', '474', 'YAHUALICA DE GONZÁLEZ GALLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('591', 'CAÑADAS DE OBREGÓN', '474', 'CAÑADAS DE OBREGÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('590', 'VILLA HIDALGO', '474', 'VILLA HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('589', 'VILLA GUERRERO', '474', 'VILLA GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('588', 'VILLA CORONA', '474', 'VILLA CORONA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('587', 'SAN GABRIEL', '474', 'SAN GABRIEL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('586', 'VALLE DE JUÁREZ', '474', 'VALLE DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('585', 'VALLE DE GUADALUPE', '474', 'VALLE DE GUADALUPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('584', 'UNIÓN DE TULA', '474', 'UNIÓN DE TULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('583', 'UNIÓN DE SAN ANTONIO', '474', 'UNIÓN DE SAN ANTONIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('582', 'TUXPAN', '474', 'TUXPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('581', 'TUXCUECA', '474', 'TUXCUECA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('580', 'TUXCACUESCO', '474', 'TUXCACUESCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('579', 'TOTOTLÁN', '474', 'TOTOTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('578', 'TOTATICHE', '474', 'TOTATICHE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('577', 'TONILA', '474', 'TONILA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('576', 'TONAYA', '474', 'TONAYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('575', 'TONALÁ', '474', 'TONALÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('574', 'TOMATLÁN', '474', 'TOMATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('573', 'TOLIMÁN', '474', 'TOLIMÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('572', 'SAN PEDRO TLAQUEPAQUE', '474', 'SAN PEDRO TLAQUEPAQUE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('571', 'TLAJOMULCO DE ZÚÑIGA', '474', 'TLAJOMULCO DE ZÚÑIGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('570', 'TIZAPÁN EL ALTO', '474', 'TIZAPÁN EL ALTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('569', 'TEUCHITLÁN', '474', 'TEUCHITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('568', 'TEQUILA', '474', 'TEQUILA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('567', 'TEPATITLÁN DE MORELOS', '474', 'TEPATITLÁN DE MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('566', 'TEOCUITATLÁN DE CORONA', '474', 'TEOCUITATLÁN DE CORONA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('565', 'TEOCALTICHE', '474', 'TEOCALTICHE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('564', 'TENAMAXTLÁN', '474', 'TENAMAXTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('563', 'TECHALUTA DE MONTENEGRO', '474', 'TECHALUTA DE MONTENEGRO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('562', 'TECOLOTLÁN', '474', 'TECOLOTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('561', 'TECALITLÁN', '474', 'TECALITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('560', 'TAPALPA', '474', 'TAPALPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('559', 'TAMAZULA DE GORDIANO', '474', 'TAMAZULA DE GORDIANO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('558', 'TALPA DE ALLENDE', '474', 'TALPA DE ALLENDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('557', 'TALA', '474', 'TALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('556', 'SAYULA', '474', 'SAYULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('555', 'SANTA MARÍA DE LOS ÁNGELES', '474', 'SANTA MARÍA DE LOS ÁNGELES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('554', 'SAN SEBASTIÁN DEL OESTE', '474', 'SAN SEBASTIÁN DEL OESTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('553', 'GÓMEZ FARÍAS', '474', 'GÓMEZ FARÍAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('552', 'SAN MIGUEL EL ALTO', '474', 'SAN MIGUEL EL ALTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('551', 'SAN MARTÍN HIDALGO', '474', 'SAN MARTÍN HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('550', 'SAN MARTÍN DE BOLAÑOS', '474', 'SAN MARTÍN DE BOLAÑOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('549', 'SAN MARCOS', '474', 'SAN MARCOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('548', 'SAN JULIÁN', '474', 'SAN JULIÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('547', 'SAN JUAN DE LOS LAGOS', '474', 'SAN JUAN DE LOS LAGOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('546', 'SAN DIEGO DE ALEJANDRÍA', '474', 'SAN DIEGO DE ALEJANDRÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('545', 'SAN CRISTÓBAL DE LA BARRANCA', '474', 'SAN CRISTÓBAL DE LA BARRANCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('544', 'EL SALTO', '474', 'EL SALTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('543', 'QUITUPAN', '474', 'QUITUPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('542', 'VILLA PURIFICACIÓN', '474', 'VILLA PURIFICACIÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('541', 'PUERTO VALLARTA', '474', 'PUERTO VALLARTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('540', 'PONCITLÁN', '474', 'PONCITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('539', 'PIHUAMO', '474', 'PIHUAMO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('538', 'OJUELOS DE JALISCO', '474', 'OJUELOS DE JALISCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('537', 'OCOTLÁN', '474', 'OCOTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('536', 'MIXTLÁN', '474', 'MIXTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('535', 'MEZQUITIC', '474', 'MEZQUITIC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('534', 'MEXTICACÁN', '474', 'MEXTICACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('533', 'MAZAMITLA', '474', 'MAZAMITLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('532', 'MASCOTA', '474', 'MASCOTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('531', 'LA MANZANILLA DE LA PAZ', '474', 'LA MANZANILLA DE LA PAZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('530', 'SANTA MARÍA DEL ORO', '474', 'SANTA MARÍA DEL ORO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('529', 'MAGDALENA', '474', 'MAGDALENA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('528', 'EL LIMÓN', '474', 'EL LIMÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('527', 'LAGOS DE MORENO', '474', 'LAGOS DE MORENO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('526', 'JUCHITLÁN', '474', 'JUCHITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('525', 'JUANACATLÁN', '474', 'JUANACATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('524', 'JOCOTEPEC', '474', 'JOCOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('523', 'JILOTLÁN DE LOS DOLORES', '474', 'JILOTLÁN DE LOS DOLORES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('522', 'JESÚS MARÍA', '474', 'JESÚS MARÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('521', 'JAMAY', '474', 'JAMAY', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('520', 'JALOSTOTITLÁN', '474', 'JALOSTOTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('519', 'IXTLAHUACÁN DEL RÍO', '474', 'IXTLAHUACÁN DEL RÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('518', 'IXTLAHUACÁN DE LO SMEMBRILLOS', '474', 'IXTLAHUACÁN DE LOS MEMBRILLOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('517', 'LAHUERTA', '474', 'LAHUERTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('516', 'HUEJUQUILLA EL ALTO', '474', 'HUEJUQUILLA EL ALTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('515', 'HUEJÚCAR', '474', 'HUEJÚCAR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('514', 'HOSTOTIPAQUILLO', '474', 'HOSTOTIPAQUILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('513', 'GUADALAJARA', '474', 'GUADALAJARA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('512', 'GUACHINANGO', '474', 'GUACHINANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('511', 'EL GRULLO', '474', 'EL GRULLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('510', 'ETZATLÁN', '474', 'ETZATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('509', 'ENCARNACIÓN DE DÍAZ', '474', 'ENCARNACIÓN DE DÍAZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('508', 'EJUTLA', '474', 'EJUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('507', 'DEGOLLADO', '474', 'DEGOLLADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('506', 'CHIQUILISTLÁN', '474', 'CHIQUILISTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('505', 'CHIMALTITÁN', '474', 'CHIMALTITÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('504', 'CHAPALA', '474', 'CHAPALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('503', 'CUQUÍO', '474', 'CUQUÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('502', 'CUAUTLA', '474', 'CUAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('501', 'CUAUTITLÁN DE GARCÍA BARRAGÁN', '474', 'CUAUTITLÁN DE GARCÍA BARRAGÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('500', 'CONCEPCIÓN DE BUENOS AIRES', '474', 'CONCEPCIÓN DE BUENOS AIRES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('499', 'COLOTLÁN', '474', 'COLOTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('498', 'COCULA', '474', 'COCULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('497', 'ZAPOTLÁN EL GRANDE', '474', 'ZAPOTLÁN EL GRANDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('496', 'CIHUATLÁN', '474', 'CIHUATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('495', 'CASIMIRO CASTILLO', '474', 'CASIMIRO CASTILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('494', 'CABO CORRIENTES', '474', 'CABO CORRIENTES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('493', 'BOLAÑOS', '474', 'BOLAÑOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('492', 'LABARCA', '474', 'LABARCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('491', 'AYUTLA', '474', 'AYUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('490', 'AYOTLÁN', '474', 'AYOTLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('489', 'AUTLÁN DE NAVARRO', '474', 'AUTLÁN DE NAVARRO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('488', 'ATOYAC', '474', 'ATOYAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('487', 'ATOTONILCO EL ALTO', '474', 'ATOTONILCO EL ALTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('486', 'ATENGUILLO', '474', 'ATENGUILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('485', 'ATENGO', '474', 'ATENGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('484', 'ATEMAJAC DE BRIZUELA', '474', 'ATEMAJAC DE BRIZUELA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('483', 'EL ARENAL', '474', 'EL ARENAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('482', 'ARANDAS', '474', 'ARANDAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('481', 'SAN JUANITO DE ESCOBEDO', '474', 'SAN JUANITO DE ESCOBEDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('480', 'AMECA', '474', 'AMECA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('479', 'AMATITÁN', '474', 'AMATITÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('478', 'AMACUECA', '474', 'AMACUECA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('477', 'AHUALULCO DE MERCADO', '474', 'AHUALULCO DE MERCADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('476', 'ACATLÁN DE JUÁREZ', '474', 'ACATLÁN DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('475', 'ACATIC', '474', 'ACATIC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('474', 'JALISCO', '0', 'JALISCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('473', 'ZIMAPÁN', '389', 'ZIMAPÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('472', 'ZEMPOALA', '389', 'ZEMPOALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('471', 'ZAPOTLÁN DE JUÁREZ', '389', 'ZAPOTLÁN DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('470', 'ZACUALTIPÁN DE ÁNGELES', '389', 'ZACUALTIPÁN DE ÁNGELES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('469', 'YAHUALICA', '389', 'YAHUALICA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('468', 'XOCHICOATLÁN', '389', 'XOCHICOATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('467', 'XOCHIATIPAN', '389', 'XOCHIATIPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('466', 'TULANCINGO DE BRAVO', '389', 'TULANCINGO DE BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('465', 'TULA DE ALLENDE', '389', 'TULA DE ALLENDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('464', 'TOLCAYUCA', '389', 'TOLCAYUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('463', 'TLAXCOAPAN', '389', 'TLAXCOAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('462', 'TLANCHINOL', '389', 'TLANCHINOL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('460', 'TLAHUILTEPA', '389', 'TLAHUILTEPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('461', 'TLANALAPA', '389', 'TLANALAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('459', 'TLAHUELILPAN', '389', 'TLAHUELILPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('458', 'TIZAYUCA', '389', 'TIZAYUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('457', 'TIANGUISTENGO', '389', 'TIANGUISTENGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('456', 'TEZONTEPEC DE ALDAMA', '389', 'TEZONTEPEC DE ALDAMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('455', 'VILLA DE TEZONTEPEC', '389', 'VILLA DE TEZONTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('454', 'TETEPANGO', '389', 'TETEPANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('453', 'TEPETITLÁN', '389', 'TEPETITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('452', 'TEPEJI DEL RÍO DE OCAMPO', '389', 'TEPEJI DEL RÍO DE OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('451', 'TEPEHUACÁN DE GUERRERO', '389', 'TEPEHUACÁN DE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('450', 'TEPEAPULCO', '389', 'TEPEAPULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('449', 'TENANGO DE DORIA', '389', 'TENANGO DE DORIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('448', 'TECOZAUTLA', '389', 'TECOZAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('447', 'TASQUILLO', '389', 'TASQUILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('446', 'SINGUILUCAN', '389', 'SINGUILUCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('445', 'SANTIAGO TULANTEPEC DE LUGO GUERRERO', '389', 'SANTIAGO TULANTEPEC DE LUGO GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('444', 'SANTIAGO DE ANAYA', '389', 'SANTIAGO DE ANAYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('443', 'SAN SALVADOR', '389', 'SAN SALVADOR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('442', 'SAN BARTOLO TUTOTEPEC', '389', 'SAN BARTOLO TUTOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('441', 'SAN AGUSTÍN TLAXIACA', '389', 'SAN AGUSTÍN TLAXIACA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('440', 'MINERAL DE LA REFORMA', '389', 'MINERAL DE LA REFORMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('439', 'PROGRESO DE OBREGÓN', '389', 'PROGRESO DE OBREGÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('438', 'PISA FLORES', '389', 'PISA FLORES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('437', 'PACHUCA DE SOTO', '389', 'PACHUCA DE SOTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('436', 'PACULA', '389', 'PACULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('435', 'SAN FELIPE ORIZATLÁN', '389', 'SAN FELIPE ORIZATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('434', 'OMITLÁN DE JUÁREZ', '389', 'OMITLÁN DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('433', 'NOPALA DE VILLAGRÁN', '389', 'NOPALA DE VILLAGRÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('432', 'NICOLÁS FLORES', '389', 'NICOLÁS FLORES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('431', 'MOLANGO DE ESCAMILLA', '389', 'MOLANGO DE ESCAMILLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('430', 'MIXQUIAHUALA DE JUÁREZ', '389', 'MIXQUIAHUALA DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('429', 'LA MISIÓN', '389', 'LA MISIÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('428', 'MINERAL DEL MONTE', '389', 'MINERAL DEL MONTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('427', 'MINERAL DEL CHICO', '389', 'MINERAL DEL CHICO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('426', 'METZTITLÁN', '389', 'METZTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('425', 'SAN AGUSTÍN METZQUITITLÁN', '389', 'SAN AGUSTÍN METZQUITITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('424', 'METEPEC', '389', 'METEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('423', 'LOLOTLA', '389', 'LOLOTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('422', 'JUÁREZ HIDALGO', '389', 'JUÁREZ HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('421', 'JALTOCÁN', '389', 'JALTOCÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('420', 'JACALA DE LEDEZMA', '389', 'JACALA DE LEDEZMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('419', 'IXMIQUILPAN', '389', 'IXMIQUILPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('418', 'HUICHAPAN', '389', 'HUICHAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('417', 'HUEJUTLA DE REYES', '389', 'HUEJUTLA DE REYES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('416', 'HUEHUETLA', '389', 'HUEHUETLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('415', 'HUAZALINGO', '389', 'HUAZALINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('414', 'HUAUTLA', '389', 'HUAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('413', 'HUASCA DE OCAMPO', '389', 'HUASCA DE OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('412', 'FRANCISCO I. MADERO', '389', 'FRANCISCO I. MADERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('411', 'EPAZOYUCAN', '389', 'EPAZOYUCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('409', 'ELOXOCHITLÁN', '389', 'ELOXOCHITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('410', 'EMILIANO ZAPATA', '389', 'EMILIANOZAPATA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('407', 'CHAPULHUACÁN', '389', 'CHAPULHUACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('408', 'CHILCUAUTLA', '389', 'CHILCUAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('406', 'CHAPANTONGO', '389', 'CHAPANTONGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('404', 'CARDONAL', '389', 'CARDONAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('405', 'CUAUTEPEC DE HINOJOSA', '389', 'CUAUTEPEC DE HINOJOSA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('403', 'CALNALI', '389', 'CALNALI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('402', 'ATOTONILCO DE TULA', '389', 'ATOTONILCO DE TULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('401', 'ATOTONILCO EL GRANDE', '389', 'ATOTONILCO EL GRANDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('400', 'ATLAPEXCO', '389', 'ATLAPEXCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('399', 'ATITALAQUIA', '389', 'ATITALAQUIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('398', 'EL ARENAL', '389', 'EL ARENAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('397', 'APAN', '389', 'APAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('396', 'ALMOLOYA', '389', 'ALMOLOYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('395', 'ALFAJAYUCAN', '389', 'ALFAJAYUCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('394', 'AJACUBA', '389', 'AJACUBA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('393', 'AGUA BLANCA DE ITURBIDE', '389', 'AGUA BLANCA DE ITURBIDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('392', 'ACTOPAN', '389', 'ACTOPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('391', 'ACAXOCHITLÁN', '389', 'ACAXOCHITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('390', 'ACATLÁN', '389', 'ACATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('388', 'ILIATENCO', '307', 'ILIATENCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('389', 'HIDALGO', '0', 'HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('387', 'JUCHITÁN', '307', 'JUCHITÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('386', 'JOSÉ JOAQUÍN DE HERRERA', '307', 'JOSÉ JOAQUÍN DE HERRERA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('385', 'COCHOAPA EL GRANDE', '307', 'COCHOAPA EL GRANDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('384', 'MARQUELIA', '307', 'MARQUELIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('383', 'ACATEPEC', '307', 'ACATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('382', 'EDUARDO NERI', '307', 'EDUARDO NERI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('381', 'ZITLALA', '307', 'ZITLALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('380', 'ZIRÁNDARO', '307', 'ZIRÁNDARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('379', 'ZAPOTITLÁN TABLAS', '307', 'ZAPOTITLÁN TABLAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('378', 'XOCHISTLAHUACA', '307', 'XOCHISTLAHUACA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('377', 'XOCHIHUEHUETLÁN', '307', 'XOCHIHUEHUETLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('376', 'XALPATLÁHUAC', '307', 'XALPATLÁHUAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('375', 'LA UNIÓN DE ISIDORO MONTESDEOCA', '307', 'LA UNIÓN DE ISIDORO MONTESDEOCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('374', 'TLAPEHUALA', '307', 'TLAPEHUALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('373', 'TLAPA DE COMONFORT', '307', 'TLAPA DE COMONFORT', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('372', 'TLALIXTAQUILLA DE MALDONADO', '307', 'TLALIXTAQUILLA DE MALDONADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('371', 'TLALCHAPA', '307', 'TLALCHAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('370', 'TLACOAPA', '307', 'TLACOAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('369', 'TLACOACHISTLAHUACA', '307', 'TLACOACHISTLAHUACA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('368', 'TIXTLA DE GUERRERO', '307', 'TIXTLA DE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('367', 'TETIPAC', '307', 'TETIPAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('366', 'TEPECOACUILCO DE TRUJANO', '307', 'TEPECOACUILCO DE TRUJANO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('365', 'TELOLOAPAN', '307', 'TELOLOAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('364', 'TÉCPAN DE GALEANA', '307', 'TÉCPAN DE GALEANA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('363', 'TECOANAPA', '307', 'TECOANAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('362', 'TAXCO DE ALARCÓN', '307', 'TAXCO DE ALARCÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('360', 'SAN MARCOS', '307', 'SAN MARCOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('361', 'SAN MIGUEL TOTOLAPAN', '307', 'SAN MIGUEL TOTOLAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('359', 'SAN LUIS ACATLÁN', '307', 'SAN LUIS ACATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('358', 'QUECHULTENANGO', '307', 'QUECHULTENANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('357', 'PUNGARABATO', '307', 'PUNGARABATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('356', 'PILCAYA', '307', 'PILCAYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('355', 'PETATLÁN', '307', 'PETATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('354', 'PEDRO ASCENCIO ALQUISIRAS', '307', 'PEDRO ASCENCIO ALQUISIRAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('353', 'OMETEPEC', '307', 'OMETEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('352', 'OLINALÁ', '307', 'OLINALÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('351', 'MOCHITLÁN', '307', 'MOCHITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('350', 'METLATÓNOC', '307', 'METLATÓNOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('349', 'MÁRTIR DE CUILAPAN', '307', 'MÁRTIR DE CUILAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('348', 'MALINALTEPEC', '307', 'MALINALTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('347', 'LEONARDO BRAVO', '307', 'LEONARDO BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('346', 'JUAN R. ESCUDERO', '307', 'JUAN R. ESCUDERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('345', 'ZIHUATANEJO DE AZUETA', '307', 'ZIHUATANEJO DE AZUETA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('344', 'IXCATEOPAN DE CUAUHTÉMOC', '307', 'IXCATEOPAN DE CUAUHTÉMOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('343', 'IGUALAPA', '307', 'IGUALAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('342', 'IGUALA DE LA INDEPENDENCIA', '307', 'IGUALA DE LA INDEPENDENCIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('341', 'HUITZUCO DE LOS FIGUEROA', '307', 'HUITZUCO DE LOS FIGUEROA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('340', 'HUAMUXTITLÁN', '307', 'HUAMUXTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('339', 'GENERAL HELIODORO CASTILLO', '307', 'GENERAL HELIODORO CASTILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('338', 'GENERAL CANUTO A.NERI', '307', 'GENERALCANUTO A. NERI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('337', 'FLORENCIO VILLARREAL', '307', 'FLORENCIO VILLARREAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('336', 'CHILPANCINGO DE LOS BRAVO', '307', 'CHILPANCINGO DE LOS BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('335', 'CHILAPA DE ÁLVAREZ', '307', 'CHILAPA DE ÁLVAREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('334', 'CUTZAMALA DE PINZÓN', '307', 'CUTZAMALA DE PINZÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('333', 'CUETZALA DEL PROGRESO', '307', 'CUETZALA DEL PROGRESO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('332', 'CUAUTEPEC', '307', 'CUAUTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('331', 'CUALÁC', '307', 'CUALÁC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('330', 'CUAJINICUILAPA', '307', 'CUAJINICUILAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('329', 'COYUCA DE CATALÁN', '307', 'COYUCA DE CATALÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('328', 'COYUCA DE BENÍTEZ', '307', 'COYUCA DE BENÍTEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('327', 'COPANATOYAC', '307', 'COPANATOYAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('326', 'COPALILLO', '307', 'COPALILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('325', 'COPALA', '307', 'COPALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('324', 'COCULA', '307', 'COCULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('323', 'COAHUAYUTLA DE JOSÉ MARÍA IZAZAGA', '307', 'COAHUAYUTLA DE JOSÉ MARÍA IZAZAGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('322', 'BUENA VISTA DE CUÉLLAR', '307', 'BUENA VISTA DE CUÉLLAR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('321', 'BENITO JUÁREZ', '307', 'BENITO JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('320', 'AZOYÚ', '307', 'AZOYÚ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('319', 'AYUTLA DE LOS LIBRES', '307', 'AYUTLA DE LOS LIBRES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('318', 'ATOYAC DE ÁLVAREZ', '307', 'ATOYAC DE ÁLVAREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('317', 'ATLIXTAC', '307', 'ATLIXTAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('316', 'ATLAMAJALCINGO DEL MONTE', '307', 'ATLAMAJALCINGO DEL MONTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('315', 'ATENANGO DEL RÍO', '307', 'ATENANGO DEL RÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('314', 'ARCELIA', '307', 'ARCELIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('313', 'APAXTLA', '307', 'APAXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('312', 'ALPOYECA', '307', 'ALPOYECA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('311', 'ALCOZAUCA DE GUERRERO', '307', 'ALCOZAUCA DE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('310', 'AJUCHITLÁN DEL PROGRESO', '307', 'AJUCHITLÁN DEL PROGRESO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('309', 'AHUACUOTZINGO', '307', 'AHUACUOTZINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('308', 'ACAPULCO DE JUÁREZ', '307', 'ACAPULCO DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('307', 'GUERRERO', '0', 'GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('306', 'YURIRIA', '260', 'YURIRIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('305', 'XICHÚ', '260', 'XICHÚ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('304', 'VILLAGRÁN', '260', 'VILLAGRÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('303', 'VICTORIA', '260', 'VICTORIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('302', 'VALLE DE SANTIAGO', '260', 'VALLE DE SANTIAGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('301', 'URIANGATO', '260', 'URIANGATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('300', 'TIERRA BLANCA', '260', 'TIERRABLANCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('299', 'TARIMORO', '260', 'TARIMORO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('298', 'TARANDACUAO', '260', 'TARANDACUAO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('297', 'SILAO DE LA VICTORIA', '260', 'SILAO DE LA VICTORIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('296', 'SANTIAGO MARAVATÍO', '260', 'SANTIAGO MARAVATÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('295', 'SANTA CRUZ DE JUVENTINO ROSAS', '260', 'SANTA CRUZ DE JUVENTINO ROSAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('294', 'SANTA CATARINA', '260', 'SANTA CATARINA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('293', 'SAN LUIS DE LA PAZ', '260', 'SAN LUIS DE LA PAZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('292', 'SAN JOSÉ ITURBIDE', '260', 'SAN JOSÉ ITURBIDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('291', 'SAN FRANCISCO DEL RINCÓN', '260', 'SAN FRANCISCO DEL RINCÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('290', 'SAN FELIPE', '260', 'SAN FELIPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('289', 'SAN DIEGO DE LA UNIÓN', '260', 'SAN DIEGO DE LA UNIÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('288', 'SALVA TIERRA', '260', 'SALVA TIERRA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('287', 'SALAMANCA', '260', 'SALAMANCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('286', 'ROMITA', '260', 'ROMITA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('285', 'PURÍSIMA DEL RINCÓN', '260', 'PURÍSIMA DEL RINCÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('284', 'PUEBLO NUEVO', '260', 'PUEBLO NUEVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('283', 'PÉNJAMO', '260', 'PÉNJAMO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('282', 'OCAMPO', '260', 'OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('281', 'MOROLEÓN', '260', 'MOROLEÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('280', 'LEÓN', '260', 'LEÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('279', 'JERÉCUARO', '260', 'JERÉCUARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('278', 'JARAL DEL PROGRESO', '260', 'JARAL DEL PROGRESO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('277', 'IRAPUATO', '260', 'IRAPUATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('276', 'HUANÍMARO', '260', 'HUANÍMARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('275', 'GUANAJUATO', '260', 'GUANAJUATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('274', 'DOLORES HIDALGO CUNA DE LA INDEPENDENCIA NACIONAL', '260', 'DOLORES HIDALGO CUNA DE LA INDEPENDENCIA NACIONAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('273', 'DOCTOR MORA', '260', 'DOCTORMORA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('272', 'CUERÁMARO', '260', 'CUERÁMARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('271', 'CORTAZAR', '260', 'CORTAZAR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('270', 'CORONEO', '260', 'CORONEO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('269', 'COMONFORT', '260', 'COMONFORT', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('268', 'MANUEL DOBLADO', '260', 'MANUEL DOBLADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('267', 'CELAYA', '260', 'CELAYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('266', 'ATARJEA', '260', 'ATARJEA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('265', 'APASEO EL GRANDE', '260', 'APASEO EL GRANDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('264', 'APASEO EL ALTO', '260', 'APASEO EL ALTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('263', 'SAN MIGUEL DE ALLENDE', '260', 'SAN MIGUEL DE ALLENDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('262', 'ACÁMBARO', '260', 'ACÁMBARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('261', 'ABASOLO', '260', 'ABASOLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('260', 'GUANAJUATO', '0', 'GUANAJUATO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('259', 'NUEVO IDEAL', '225', 'NUEVO IDEAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('258', 'VICENTE GUERRERO', '225', 'VICENTE GUERRERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('257', 'TOPIA', '225', 'TOPIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('256', 'TLAHUALILO', '225', 'TLAHUALILO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('255', 'TEPEHUANES', '225', 'TEPEHUANES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('254', 'TAMAZULA', '225', 'TAMAZULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('253', 'SÚCHIL', '225', 'SÚCHIL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('252', 'SANTIAGO PAPASQUIARO', '225', 'SANTIAGO PAPASQUIARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('251', 'SANTA CLARA', '225', 'SANTA CLARA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('250', 'SAN PEDRO DEL GALLO', '225', 'SAN PEDRO DEL GALLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('249', 'SAN LUIS DEL CORDERO', '225', 'SAN LUIS DEL CORDERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('248', 'SAN JUAN DEL RÍO', '225', 'SAN JUAN DEL RÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('247', 'SAN JUAN DE GUADALUPE', '225', 'SAN JUAN DE GUADALUPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('246', 'SAN DIMAS', '225', 'SAN DIMAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('245', 'SAN BERNARDO', '225', 'SAN BERNARDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('244', 'RODEO', '225', 'RODEO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('243', 'PUEBLO NUEVO', '225', 'PUEBLO NUEVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('242', 'POANAS', '225', 'POANAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('241', 'PEÑÓN BLANCO', '225', 'PEÑÓN BLANCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('240', 'PÁNUCO DE CORONADO', '225', 'PÁNUCO DE CORONADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('239', 'OTÁEZ', '225', 'OTÁEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('238', 'ELORO', '225', 'ELORO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('237', 'OCAMPO', '225', 'OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('236', 'NOMBRE DE DIOS', '225', 'NOMBRE DE DIOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('235', 'NAZAS', '225', 'NAZAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('234', 'MEZQUITAL', '225', 'MEZQUITAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('233', 'MAPIMÍ', '225', 'MAPIMÍ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('232', 'LERDO', '225', 'LERDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('231', 'INDÉ', '225', 'INDÉ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('230', 'HIDALGO', '225', 'HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('229', 'GUANACEVÍ', '225', 'GUANACEVÍ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('228', 'GUADALUPE VICTORIA', '225', 'GUADALUPE VICTORIA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('227', 'GÓMEZ PALACIO', '225', 'GÓMEZ PALACIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('226', 'GENERAL SIMÓN BOLÍVAR', '225', 'GENERAL SIMÓN BOLÍVAR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('225', 'DURANGO', '0', 'DURANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('224', 'CUENCAMÉ', '209', 'CUENCAMÉ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('223', 'CONETO DE COMONFORT', '209', 'CONETO DE COMONFORT', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('222', 'CANELAS', '209', 'CANELAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('221', 'CANATLÁN', '209', 'CANATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('220', 'DURANGO', '209', 'DURANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('219', 'VILLA DE ÁLVAREZ', '209', 'VILLA DE ÁLVAREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('218', 'TECOMÁN', '209', 'TECOMÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('217', 'MINATITLÁN', '209', 'MINATITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('216', 'MANZANILLO', '209', 'MANZANILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('215', 'IXTLAHUACÁN', '209', 'IXTLAHUACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('213', 'COQUIMATLÁN', '209', 'COQUIMATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('214', 'CUAUHTÉMOC', '209', 'CUAUHTÉMOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('212', 'COMALA', '209', 'COMALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('211', 'COLIMA', '209', 'COLIMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('210', 'ARMERÍA', '209', 'ARMERÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('208', 'ZARAGOZA', '170', 'ZARAGOZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('209', 'COLIMA', '0', 'COLIMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('207', 'VILLA UNIÓN', '170', 'VILLA UNIÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('206', 'VIESCA', '170', 'VIESCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('205', 'TORREÓN', '170', 'TORREÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('204', 'SIERRA MOJADA', '170', 'SIERRA MOJADA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('203', 'SAN PEDRO', '170', 'SAN PEDRO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('202', 'SAN JUAN DE SABINAS', '170', 'SAN JUAN DE SABINAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('201', 'SAN BUENAVENTURA', '170', 'SAN BUENAVENTURA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('200', 'SALTILLO', '170', 'SALTILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('199', 'SACRAMENTO', '170', 'SACRAMENTO', '126', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('198', 'SABINAS', '170', 'SABINAS', '125', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('197', 'RAMOS ARIZPE', '170', 'RAMOS ARIZPE', '124', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('196', 'PROGRESO', '170', 'PROGRESO', '123', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('195', 'PIEDRAS NEGRAS', '170', 'PIEDRAS NEGRAS', '122', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('194', 'PARRAS', '170', 'PARRAS', '121', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('193', 'OCAMPO', '170', 'OCAMPO', '120', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('192', 'NAVA', '170', 'NAVA', '119', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('191', 'NADADORES', '170', 'NADADORES', '118', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('190', 'MÚZQUIZ', '170', 'MÚZQUIZ', '117', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('189', 'MORELOS', '170', 'MORELOS', '116', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('188', 'MONCLOVA', '170', 'MONCLOVA', '115', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('187', 'MATAMOROS', '170', 'MATAMOROS', '114', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('186', 'LAMADRID', '170', 'LAMADRID', '113', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('185', 'JUÁREZ', '170', 'JUÁREZ', '112', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('184', 'JIMÉNEZ', '170', 'JIMÉNEZ', '111', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('183', 'HIDALGO', '170', 'HIDALGO', '110', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('182', 'GUERRERO', '170', 'GUERRERO', '109', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('181', 'GENERAL CEPEDA', '170', 'GENERAL CEPEDA', '108', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('180', 'FRONTERA', '170', 'FRONTERA', '107', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('179', 'FRANCISCO I. MADERO', '170', 'FRANCISCO I. MADERO', '106', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('178', 'ESCOBEDO', '170', 'ESCOBEDO', '105', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('177', 'CUATRO CIÉNEGAS', '170', 'CUATRO CIÉNEGAS', '104', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('176', 'CASTAÑOS', '170', 'CASTAÑOS', '103', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('175', 'CANDELA', '170', 'CANDELA', '102', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('174', 'ARTEAGA', '170', 'ARTEAGA', '101', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('173', 'ALLENDE', '170', 'ALLENDE', '100', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('172', 'ACUÑA', '170', 'ACUÑA', '99', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('171', 'ABASOLO', '170', 'ABASOLO', '98', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('170', 'COAHULIA', '0', 'COAHULIA', '97', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('169', 'VENUSTIANO CARRANZA', '153', 'VENUSTIANO CARRANZA', '96', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('168', 'MIGUEL HIDALGO', '153', 'MIGUEL HIDALGO', '95', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('167', 'CUAUHTÉMOC', '153', 'CUAUHTÉMOC', '94', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('166', 'BENITO JUÁREZ', '153', 'BENITO JUÁREZ', '93', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('165', 'XOCHIMILCO', '153', 'XOCHIMILCO', '92', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('164', 'TLALPAN', '153', 'TLALPAN', '91', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('163', 'TLÁHUAC', '153', 'TLÁHUAC', '90', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('162', 'ÁLVARO OBREGÓN', '153', 'ÁLVARO OBREGÓN', '89', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('161', 'MILPA ALTA', '153', 'MILPA ALTA', '88', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('160', 'LA MAGDALENA CONTRERAS', '153', 'LA MAGDALENA CONTRERAS', '87', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('159', 'IZTAPALAPA', '153', 'IZTAPALAPA', '86', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('158', 'IZTACALCO', '153', 'IZTACALCO', '85', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('157', 'GUSTAVO A. MADERO', '153', 'GUSTAVO A. MADERO', '84', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('156', 'CUAJIMALPA DE MORELOS', '153', 'CUAJIMALPA DE MORELOS', '83', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('155', 'COYOACÁN', '153', 'COYOACÁN', '82', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('154', 'AZCAPOTZALCO', '153', 'AZCAPOTZALCO', '81', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('153', 'CIUDAD DE MÉXICO', '0', 'CIUDAD DE MÉXICO', '80', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('152', 'VILLA AHUMADA', '122', 'VILLA AHUMADA', '79', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('151', 'SAUCILLO', '122', 'SAUCILLO', '78', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('150', 'NUEVO CASAS GRANDES', '122', 'NUEVO CASAS GRANDES', '77', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('149', 'MEOQUI', '122', 'MEOQUI', '76', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('148', 'MADERA', '122', 'MADERA', '75', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('147', 'JIMÉNEZ', '122', 'JIMÉNEZ', '74', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('146', 'GUERRERO', '122', 'GUERRERO', '73', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('145', 'GUACHOCHI', '122', 'GUACHOCHI', '72', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('144', 'GÓMEZ FARÍAS', '122', 'GÓMEZ FARÍAS', '71', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('143', 'DELICIAS', '122', 'DELICIAS', '70', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('142', 'CUAUHTEMOC', '122', 'CUAUHTEMOC', '69', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('141', 'CHIHUAHUA', '122', 'CHIHUAHUA', '68', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('140', 'CÁRDENAS', '122', 'CÁRDENAS', '67', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('139', 'CAMARGO', '122', 'CAMARGO', '66', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('138', 'ASCENSIÓN', '122', 'ASCENSIÓN', '65', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('137', 'SANTA EULALIA', '122', 'SANTA EULALIA', '64', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('136', 'SANTA CRUZ DE ROSALES', '122', 'SANTA CRUZ DE ROSALES', '63', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('135', 'SANTA BÁRBARA', '122', 'SANTA BÁRBARA', '62', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('134', 'SAN JUANITO', '122', 'SAN JUANITO', '61', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('133', 'SAN BUENAVENTURA', '122', 'SAN BUENAVENTURA', '60', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('132', 'OJINAGA', '122', 'OJINAGA', '59', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('131', 'LAJUNTA', '122', 'LAJUNTA', '58', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('130', 'HIDALGO DEL PARRAL', '122', 'HIDALGO DEL PARRAL', '57', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('129', 'GUADALUPE Y CALVO', '122', 'GUADALUPE Y CALVO', '56', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('128', 'EJIDO BENITO JUÁREZ', '122', 'EJIDO BENITO JUÁREZ', '55', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('127', 'CREEL', '122', 'CREEL', '54', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('126', 'COLONIA ANÁHUAC', '122', 'COLONIA ANÁHUAC', '53', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('125', 'CIUDAD JUÁREZ', '122', 'CIUDAD JUÁREZ', '52', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('124', 'CASAS GRANDES', '122', 'CASAS GRANDES', '51', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('123', 'ALDAMA', '122', 'ALDAMA', '50', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('122', 'CHIHUAHUA', '0', 'CHIHUAHUA', '49', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('121', 'VILLAFLORES', '110', 'VILLAFLORES', '48', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('120', 'TUXTLA GUTIERREZ', '110', 'TUXTLA GUTIERREZ', '47', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('119', 'TONALÁ', '110', 'TONALÁ', '46', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('118', 'TAPACHULA', '110', 'TAPACHULA', '45', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('117', 'SAN CRISTOBAL', '110', 'SAN CRISTOBAL', '44', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('116', 'PALENQUE', '110', 'PALENQUE', '43', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('115', 'OCOZOCOAUTLA', '110', 'OCOZOCOAUTLA', '42', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('114', 'OCOSINGO', '110', 'OCOSINGO', '41', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('113', 'COMITÁN', '110', 'COMITÁN', '40', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('112', 'CINTALAPA', '110', 'CINTALAPA', '39', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('110', 'CHIAPAS', '0', 'CHIAPAS', '37', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('111', 'CHIAPA DE CORZO', '110', 'CHIAPA DE CORZO', '38', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('109', 'CANDELARIA', '98', 'CANDELARIA', '36', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('108', 'CALAKMUL', '98', 'CALAKMUL', '35', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('107', 'ESCÁRCEGA', '98', 'ESCÁRCEGA', '34', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('106', 'TENABO', '98', 'TENABO', '33', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('105', 'PALIZADA', '98', 'PALIZADA', '32', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('104', 'HOPELCHÉN', '98', 'HOPELCHÉN', '31', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('103', 'HECELCHAKÁN', '98', 'HECELCHAKÁN', '30', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('102', 'CHAMPOTÓN', '98', 'CHAMPOTÓN', '29', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('101', 'CARMEN', '98', 'CARMEN', '28', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('100', 'CAMPECHE', '98', 'CAMPECHE', '27', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('99', 'CALKINÍ', '98', 'CALKINÍ', '26', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('98', 'CAMPECHE', '0', 'CAMPECHE', '25', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('97', 'LORETO', '92', 'LORETO', '24', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('96', 'LOS CABOS', '92', 'LOS CABOS', '23', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('95', 'LA PAZ', '92', 'LA PAZ', '22', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('94', 'MULEGÉ', '92', 'MULEGÉ', '21', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('93', 'COMONDÚ', '92', 'COMONDÚ', '20', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('92', 'BAJA CALIFORNIA SUR', '0', 'BAJA CALIFORNIA SUR', '19', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('91', 'PLAYAS DE ROSARITO', '86', 'PLAYAS DE ROSARITO', '18', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('90', 'TIJUANA', '86', 'TIJUANA', '17', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('89', 'TECATE', '86', 'TECATE', '16', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('88', 'MEXICALI', '86', 'MEXICALI', '15', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('87', 'ENSENADA', '86', 'ENSENADA', '14', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('86', 'BAJA CALIFORNIA', '0', 'BAJA CALIFORNIA', '13', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('85', 'SAN FRANCISCO DE LOS ROMO', '74', 'SAN FRANCISCO DE LOS ROMO', '12', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('84', 'EL LLANO', '74', 'EL LLANO', '11', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('83', 'TEPEZALÁ', '74', 'TEPEZALÁ', '10', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('82', 'SAN JOSÉ DE GRACIA', '74', 'SAN JOSÉ DE GRACIA', '9', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('81', 'RINCÓN DE ROMOS', '74', 'RINCÓN DE ROMOS', '8', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('80', 'PABELLÓN DE ARTEAGA', '74', 'PABELLÓN DE ARTEAGA', '7', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('79', 'JESÚS MARÍA', '74', 'JESÚS MARÍA', '6', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('78', 'COSÍO', '74', 'COSÍO', '5', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('77', 'CALVILLO', '74', 'CALVILLO', '4', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('76', 'ASIENTOS', '74', 'ASIENTOS', '3', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('75', 'AGUASCALIENTES', '74', 'AGUASCALIENTES', '2', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('74', 'AGUASCALIENTES', '0', 'AGUASCALIENTES', '1', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1518', 'SANTA CRUZ QUILEHTLA', '1415', 'SANTA CRUZ QUILEHTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1519', 'SANTA ISABEL XILOXOXTLA', '1415', 'SANTA ISABEL XILOXOXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1520', 'VERACRUZ', '0', 'VERACRUZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1521', 'ACAJETE', '1520', 'ACAJETE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1522', 'ACATLÁN', '1520', 'ACATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1523', 'ACAYUCAN', '1520', 'ACAYUCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1524', 'ACTOPAN', '1520', 'ACTOPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1525', 'ACULA', '1520', 'ACULA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1526', 'ACULTZINGO', '1520', 'ACULTZINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1527', 'CAMARÓN DE TEJEDA', '1520', 'CAMARÓN DE TEJEDA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1528', 'ALPATLÁHUAC', '1520', 'ALPATLÁHUAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1529', 'ALTO LUCERO DE GUTIÉRREZ BARRIOS', '1520', 'ALTO LUCERO DE GUTIÉRREZ BARRIOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1530', 'ALTOTONGA', '1520', 'ALTOTONGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1531', 'ALVARADO', '1520', 'ALVARADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1532', 'AMATITLÁN', '1520', 'AMATITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1533', 'NARANJOS AMATLÁN', '1520', 'NARANJOS AMATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1534', 'AMATLÁN DE LOS REYES', '1520', 'AMATLÁN DE LOS REYES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1535', 'ANGEL R. CABADA', '1520', 'ANGEL R. CABADA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1536', 'LA ANTIGUA', '1520', 'LA ANTIGUA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1537', 'APAZAPAN', '1520', 'APAZAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1538', 'AQUILA', '1520', 'AQUILA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1539', 'ASTACINGA', '1520', 'ASTACINGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1540', 'ATLAHUILCO', '1520', 'ATLAHUILCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1541', 'ATOYAC', '1520', 'ATOYAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1542', 'ATZACAN', '1520', 'ATZACAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1543', 'ATZALAN', '1520', 'ATZALAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1544', 'TLALTETELA', '1520', 'TLALTETELA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1545', 'AYAHUALULCO', '1520', 'AYAHUALULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1546', 'BANDERILLA', '1520', 'BANDERILLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1547', 'BENITO JUÁREZ', '1520', 'BENITO JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1548', 'BOCADELRÍO', '1520', 'BOCADELRÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1549', 'CALCAHUALCO', '1520', 'CALCAHUALCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1550', 'CAMERINO Z. MENDOZA', '1520', 'CAMERINO Z. MENDOZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1551', 'CARRILLO PUERTO', '1520', 'CARRILLO PUERTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1552', 'CATEMACO', '1520', 'CATEMACO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1553', 'CAZONES DE HERRERA', '1520', 'CAZONES DE HERRERA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1554', 'CERRO AZUL', '1520', 'CERRO AZUL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1555', 'CITLALTÉPETL', '1520', 'CITLALTÉPETL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1556', 'COACOATZINTLA', '1520', 'COACOATZINTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1557', 'COAHUITLÁN', '1520', 'COAHUITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1558', 'COATEPEC', '1520', 'COATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1559', 'COATZACOALCOS', '1520', 'COATZACOALCOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1560', 'COATZINTLA', '1520', 'COATZINTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1561', 'COETZALA', '1520', 'COETZALA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1562', 'COLIPA', '1520', 'COLIPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1563', 'COMAPA', '1520', 'COMAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1564', 'CÓRDOBA', '1520', 'CÓRDOBA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1565', 'COSAMALOAPAN DE CARPIO', '1520', 'COSAMALOAPAN DE CARPIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1566', 'COSAUTLÁN DE CARVAJAL', '1520', 'COSAUTLÁN DE CARVAJAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1567', 'COSCOMATEPEC', '1520', 'COSCOMATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1568', 'COSOLEACAQUE', '1520', 'COSOLEACAQUE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1569', 'COTAXTLA', '1520', 'COTAXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1570', 'COXQUIHUI', '1520', 'COXQUIHUI', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1571', 'COYUTLA', '1520', 'COYUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1572', 'CUICHAPA', '1520', 'CUICHAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1573', 'CUITLÁHUAC', '1520', 'CUITLÁHUAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1574', 'CHACALTIANGUIS', '1520', 'CHACALTIANGUIS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1575', 'CHALMA', '1520', 'CHALMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1576', 'CHICONAMEL', '1520', 'CHICONAMEL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1577', 'CHICONQUIACO', '1520', 'CHICONQUIACO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1578', 'CHICONTEPEC', '1520', 'CHICONTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1579', 'CHINAMECA', '1520', 'CHINAMECA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1580', 'CHINAMPA DE GOROSTIZA', '1520', 'CHINAMPA DE GOROSTIZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1581', 'LAS CHOAPAS', '1520', 'LAS CHOAPAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1582', 'CHOCAMÁN', '1520', 'CHOCAMÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1583', 'CHONTLA', '1520', 'CHONTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1584', 'CHUMATLÁN', '1520', 'CHUMATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1585', 'EMILIANO ZAPATA', '1520', 'EMILIANO ZAPATA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1586', 'ESPINAL', '1520', 'ESPINAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1587', 'FILOMENO MATA', '1520', 'FILOMENO MATA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1588', 'FORTÍN', '1520', 'FORTÍN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1589', 'GUTIÉRREZ ZAMORA', '1520', 'GUTIÉRREZ ZAMORA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1590', 'HIDALGOTITLÁN', '1520', 'HIDALGOTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1591', 'HUATUSCO', '1520', 'HUATUSCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1592', 'HUAYACOCOTLA', '1520', 'HUAYACOCOTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1593', 'HUEYAPAN DE OCAMPO', '1520', 'HUEYAPAN DE OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1594', 'HUILOAPAN DE CUAUHTÉMOC', '1520', 'HUILOAPAN DE CUAUHTÉMOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1595', 'IGNACIO DE LA LLAVE', '1520', 'IGNACIO DE LA LLAVE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1596', 'ILAMATLÁN', '1520', 'ILAMATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1597', 'ISLA', '1520', 'ISLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1598', 'IXCATEPEC', '1520', 'IXCATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1599', 'IXHUACÁN DE LOS REYES', '1520', 'IXHUACÁN DE LOS REYES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1600', 'IXHUATLÁN DEL CAFÉ', '1520', 'IXHUATLÁN DEL CAFÉ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1601', 'IXHUATLANCILLO', '1520', 'IXHUATLANCILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1602', 'IXHUATLÁN DEL SURESTE', '1520', 'IXHUATLÁN DEL SURESTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1603', 'IXHUATLÁN DE MADERO', '1520', 'IXHUATLÁN DE MADERO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1604', 'IXMATLAHUACAN', '1520', 'IXMATLAHUACAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1605', 'IXTACZOQUITLÁN', '1520', 'IXTACZOQUITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1606', 'JALACINGO', '1520', 'JALACINGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1607', 'XALAPA', '1520', 'XALAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1608', 'JALCOMULCO', '1520', 'JALCOMULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1609', 'JÁLTIPAN', '1520', 'JÁLTIPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1610', 'JAMAPA', '1520', 'JAMAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1611', 'JESÚS CARRANZA', '1520', 'JESÚS CARRANZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1612', 'XICO', '1520', 'XICO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1613', 'JILOTEPEC', '1520', 'JILOTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1614', 'JUAN RODRÍGUEZ CLARA', '1520', 'JUAN RODRÍGUEZ CLARA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1615', 'JUCHIQUE DE FERRER', '1520', 'JUCHIQUE DE FERRER', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1616', 'LANDERO Y COSS', '1520', 'LANDERO Y COSS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1617', 'LERDO DE TEJADA', '1520', 'LERDO DE TEJADA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1618', 'MAGDALENA', '1520', 'MAGDALENA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1619', 'MALTRATA', '1520', 'MALTRATA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1620', 'MANLIO FABIO ALTAMIRANO', '1520', 'MANLIO FABIO ALTAMIRANO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1621', 'MARIANO ESCOBEDO', '1520', 'MARIANO ESCOBEDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1622', 'MARTÍNEZ DE LA TORRE', '1520', 'MARTÍNEZ DE LA TORRE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1623', 'MECATLÁN', '1520', 'MECATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1624', 'MECAYAPAN', '1520', 'MECAYAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1625', 'MEDELLÍN DE BRAVO', '1520', 'MEDELLÍN DE BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1626', 'MIAHUATLÁN', '1520', 'MIAHUATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1627', 'LAS MINAS', '1520', 'LAS MINAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1628', 'MINATITLÁN', '1520', 'MINATITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1629', 'MISANTLA', '1520', 'MISANTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1630', 'MIXTLA DE ALTAMIRANO', '1520', 'MIXTLA DE ALTAMIRANO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1631', 'MOLOACÁN', '1520', 'MOLOACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1632', 'NAOLINCO', '1520', 'NAOLINCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1633', 'NARANJAL', '1520', 'NARANJAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1634', 'NAUTLA', '1520', 'NAUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1635', 'NOGALES', '1520', 'NOGALES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1636', 'OLUTA', '1520', 'OLUTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1637', 'OMEALCA', '1520', 'OMEALCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1638', 'ORIZABA', '1520', 'ORIZABA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1639', 'OTATITLÁN', '1520', 'OTATITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1640', 'OTEAPAN', '1520', 'OTEAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1641', 'OZULUAMA DE MASCAREÑAS', '1520', 'OZULUAMA DE MASCAREÑAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1642', 'PAJAPAN', '1520', 'PAJAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1643', 'PÁNUCO', '1520', 'PÁNUCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1644', 'PAPANTLA', '1520', 'PAPANTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1645', 'PASO DEL MACHO', '1520', 'PASO DEL MACHO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1646', 'PASO DE OVEJAS', '1520', 'PASO DE OVEJAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1647', 'LA PERLA', '1520', 'LA PERLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1648', 'PEROTE', '1520', 'PEROTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1649', 'PLATÓN SÁNCHEZ', '1520', 'PLATÓN SÁNCHEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1650', 'PLAYA VICENTE', '1520', 'PLAYA VICENTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1651', 'POZARICA DE HIDALGO', '1520', 'POZARICA DE HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1652', 'LAS VIGAS DE RAMÍREZ', '1520', 'LAS VIGAS DE RAMÍREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1653', 'PUEBLO VIEJO', '1520', 'PUEBLO VIEJO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1654', 'PUENTE NACIONAL', '1520', 'PUENTE NACIONAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1655', 'RAFAEL DELGADO', '1520', 'RAFAEL DELGADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1656', 'RAFAEL LUCIO', '1520', 'RAFAEL LUCIO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1657', 'LOS REYES', '1520', 'LOS REYES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1658', 'RÍO BLANCO', '1520', 'RÍO BLANCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1659', 'SALTABARRANCA', '1520', 'SALTABARRANCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1660', 'SAN ANDRÉS TENEJAPAN', '1520', 'SAN ANDRÉS TENEJAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1661', 'SAN ANDRÉS TUXTLA', '1520', 'SAN ANDRÉS TUXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1662', 'SAN JUAN EVANGELISTA', '1520', 'SAN JUAN EVANGELISTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1663', 'SANTIAGO TUXTLA', '1520', 'SANTIAGO TUXTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1664', 'SAYULA DE ALEMÁN', '1520', 'SAYULA DE ALEMÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1665', 'SOCONUSCO', '1520', 'SOCONUSCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1666', 'SOCHIAPA', '1520', 'SOCHIAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1667', 'SOLEDAD ATZOMPA', '1520', 'SOLEDAD ATZOMPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1668', 'SOLEDAD DE DOBLADO', '1520', 'SOLEDAD DE DOBLADO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1669', 'SOTEAPAN', '1520', 'SOTEAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1670', 'TAMALÍN', '1520', 'TAMALÍN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1671', 'TAMIAHUA', '1520', 'TAMIAHUA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1672', 'TAMPICO ALTO', '1520', 'TAMPICO ALTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1673', 'TANCOCO', '1520', 'TANCOCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1674', 'TANTIMA', '1520', 'TANTIMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1675', 'TANTOYUCA', '1520', 'TANTOYUCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1676', 'TATATILA', '1520', 'TATATILA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1677', 'CASTILLO DE TEAYO', '1520', 'CASTILLO DE TEAYO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1678', 'TECOLUTLA', '1520', 'TECOLUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1679', 'TEHUIPANGO', '1520', 'TEHUIPANGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1680', 'ÁLAMO TEMAPACHE', '1520', 'ÁLAMO TEMAPACHE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1681', 'TEMPOAL', '1520', 'TEMPOAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1682', 'TENAMPA', '1520', 'TENAMPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1683', 'TENOCHTITLÁN', '1520', 'TENOCHTITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1684', 'TEOCELO', '1520', 'TEOCELO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1685', 'TEPATLAXCO', '1520', 'TEPATLAXCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1686', 'TEPETLÁN', '1520', 'TEPETLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1687', 'TEPETZINTLA', '1520', 'TEPETZINTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1688', 'TEQUILA', '1520', 'TEQUILA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1689', 'JOSÉAZUETA', '1520', 'JOSÉAZUETA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1690', 'TEXCATEPEC', '1520', 'TEXCATEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1691', 'TEXHUACÁN', '1520', 'TEXHUACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1692', 'TEXISTEPEC', '1520', 'TEXISTEPEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1693', 'TEZONAPA', '1520', 'TEZONAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1694', 'TIERRA BLANCA', '1520', 'TIERRA BLANCA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1695', 'TIHUATLÁN', '1520', 'TIHUATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1696', 'TLACOJALPAN', '1520', 'TLACOJALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1697', 'TLACOLULAN', '1520', 'TLACOLULAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1698', 'TLACOTALPAN', '1520', 'TLACOTALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1699', 'TLACOTEPEC DE MEJÍA', '1520', 'TLACOTEPEC DE MEJÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1700', 'TLACHICHILCO', '1520', 'TLACHICHILCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1701', 'TLALIXCOYAN', '1520', 'TLALIXCOYAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1702', 'TLALNELHUAYOCAN', '1520', 'TLALNELHUAYOCAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1703', 'TLAPACOYAN', '1520', 'TLAPACOYAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1704', 'TLAQUILPA', '1520', 'TLAQUILPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1705', 'TLILAPAN', '1520', 'TLILAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1706', 'TOMATLÁN', '1520', 'TOMATLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1707', 'TONAYÁN', '1520', 'TONAYÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1708', 'TOTUTLA', '1520', 'TOTUTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1709', 'TUXPAN', '1520', 'TUXPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1710', 'TUXTILLA', '1520', 'TUXTILLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1711', 'URSULO GALVÁN', '1520', 'URSULO GALVÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1712', 'VEGA DE ALATORRE', '1520', 'VEGA DE ALATORRE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1713', 'VERACRUZ', '1520', 'VERACRUZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1714', 'VILLA ALDAMA', '1520', 'VILLA ALDAMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1715', 'XOXOCOTLA', '1520', 'XOXOCOTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1716', 'YANGA', '1520', 'YANGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1717', 'YECUATLA', '1520', 'YECUATLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1718', 'ZACUALPAN', '1520', 'ZACUALPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1719', 'ZARAGOZA', '1520', 'ZARAGOZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1720', 'ZENTLA', '1520', 'ZENTLA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1721', 'ZONGOLICA', '1520', 'ZONGOLICA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1722', 'ZONTECOMATLÁN DE LÓPEZ Y FUENTES', '1520', 'ZONTECOMATLÁN DE LÓPEZ Y FUENTES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1723', 'ZOZOCOLCO DE HIDALGO', '1520', 'ZOZOCOLCO DE HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1724', 'AGUA DULCE', '1520', 'AGUA DULCE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1725', 'EL HIGO', '1520', 'EL HIGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1726', 'NANCHITAL DE LÁZARO CÁRDENAS DEL RÍO', '1520', 'NANCHITAL DE LÁZARO CÁRDENAS DEL RÍO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1727', 'TRES VALLES', '1520', 'TRES VALLES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1728', 'CARLOS A. CARRILLO', '1520', 'CARLOS A. CARRILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1729', 'TATAHUICAPAN DE JUÁREZ', '1520', 'TATAHUICAPAN DE JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1730', 'UXPANAPA', '1520', 'UXPANAPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1731', 'SAN RAFAEL', '1520', 'SAN RAFAEL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1732', 'SANTIAGO SOCHIAPAN', '1520', 'SANTIAGO SOCHIAPAN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1733', 'YUCATÁN', '0', 'YUCATÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1734', 'ABALÁ', '1733', 'ABALÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1735', 'ACANCEH', '1733', 'ACANCEH', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1736', 'AKIL', '1733', 'AKIL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1737', 'BACA', '1733', 'BACA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1738', 'BOKOBÁ', '1733', 'BOKOBÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1739', 'BUCTZOTZ', '1733', 'BUCTZOTZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1740', 'CACALCHÉN', '1733', 'CACALCHÉN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1741', 'CALOTMUL', '1733', 'CALOTMUL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1742', 'CANSAHCAB', '1733', 'CANSAHCAB', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1743', 'CANTAMAYEC', '1733', 'CANTAMAYEC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1744', 'CELESTÚN', '1733', 'CELESTÚN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1745', 'CENOTILLO', '1733', 'CENOTILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1746', 'CONKAL', '1733', 'CONKAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1747', 'CUNCUNUL', '1733', 'CUNCUNUL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1748', 'CUZAMÁ', '1733', 'CUZAMÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1749', 'CHACSINKÍN', '1733', 'CHACSINKÍN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1750', 'CHANKOM', '1733', 'CHANKOM', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1751', 'CHAPAB', '1733', 'CHAPAB', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1752', 'CHEMAX', '1733', 'CHEMAX', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1753', 'CHICXULUB PUEBLO', '1733', 'CHICXULUB PUEBLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1754', 'CHICHIMILÁ', '1733', 'CHICHIMILÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1755', 'CHIKINDZONOT', '1733', 'CHIKINDZONOT', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1756', 'CHOCHOLÁ', '1733', 'CHOCHOLÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1757', 'CHUMAYEL', '1733', 'CHUMAYEL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1758', 'DZÁN', '1733', 'DZÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1759', 'DZEMUL', '1733', 'DZEMUL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1760', 'DZIDZANTÚN', '1733', 'DZIDZANTÚN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1761', 'DZILAM DE BRAVO', '1733', 'DZILAM DE BRAVO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1762', 'DZILAM GONZÁLEZ', '1733', 'DZILAM GONZÁLEZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1763', 'DZITÁS', '1733', 'DZITÁS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1764', 'DZONCAUICH', '1733', 'DZONCAUICH', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1765', 'ESPITA', '1733', 'ESPITA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1766', 'HALACHÓ', '1733', 'HALACHÓ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1767', 'HOCABÁ', '1733', 'HOCABÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1768', 'HOCTÚN', '1733', 'HOCTÚN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1769', 'HOMÚN', '1733', 'HOMÚN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1770', 'HUHÍ', '1733', 'HUHÍ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1771', 'HUNUCMÁ', '1733', 'HUNUCMÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1772', 'IXIL', '1733', 'IXIL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1773', 'IZAMAL', '1733', 'IZAMAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1774', 'KANASÍN', '1733', 'KANASÍN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1775', 'KANTUNIL', '1733', 'KANTUNIL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1776', 'KAUA', '1733', 'KAUA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1777', 'KINCHIL', '1733', 'KINCHIL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1778', 'KOPOMÁ', '1733', 'KOPOMÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1779', 'MAMA', '1733', 'MAMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1780', 'MANÍ', '1733', 'MANÍ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1781', 'MAXCANÚ', '1733', 'MAXCANÚ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1782', 'MAYAPÁN', '1733', 'MAYAPÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1783', 'MÉRIDA', '1733', 'MÉRIDA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1784', 'MOCOCHÁ', '1733', 'MOCOCHÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1785', 'MOTUL', '1733', 'MOTUL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1786', 'MUNA', '1733', 'MUNA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1787', 'MUXUPIP', '1733', 'MUXUPIP', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1788', 'OPICHÉN', '1733', 'OPICHÉN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1789', 'OXKUTZCAB', '1733', 'OXKUTZCAB', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1790', 'PANABÁ', '1733', 'PANABÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1791', 'PETO', '1733', 'PETO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1792', 'PROGRESO', '1733', 'PROGRESO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1793', 'QUINTANA ROO', '1733', 'QUINTANA ROO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1794', 'RÍO LAGARTOS', '1733', 'RÍO LAGARTOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1795', 'SACALUM', '1733', 'SACALUM', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1796', 'SAMAHIL', '1733', 'SAMAHIL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1797', 'SANAHCAT', '1733', 'SANAHCAT', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1798', 'SAN FELIPE', '1733', 'SAN FELIPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1799', 'SANTA ELENA', '1733', 'SANTA ELENA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1800', 'SEYÉ', '1733', 'SEYÉ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1801', 'SINANCHÉ', '1733', 'SINANCHÉ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1802', 'SOTUTA', '1733', 'SOTUTA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1803', 'SUCILÁ', '1733', 'SUCILÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1804', 'SUDZAL', '1733', 'SUDZAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1805', 'SUMA', '1733', 'SUMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1806', 'TAHDZIÚ', '1733', 'TAHDZIÚ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1807', 'TAHMEK', '1733', 'TAHMEK', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1808', 'TEABO', '1733', 'TEABO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1809', 'TECOH', '1733', 'TECOH', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1810', 'TEKAL DE VENEGAS', '1733', 'TEKAL DE VENEGAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1811', 'TEKANTÓ', '1733', 'TEKANTÓ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1812', 'TEKAX', '1733', 'TEKAX', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1813', 'TEKIT', '1733', 'TEKIT', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1814', 'TEKOM', '1733', 'TEKOM', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1815', 'TELCHAC PUEBLO', '1733', 'TELCHAC PUEBLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1816', 'TELCHAC PUERTO', '1733', 'TELCHAC PUERTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1817', 'TEMAX', '1733', 'TEMAX', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1818', 'TEMOZÓN', '1733', 'TEMOZÓN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1819', 'TEPAKÁN', '1733', 'TEPAKÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1820', 'TETIZ', '1733', 'TETIZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1821', 'TEYA', '1733', 'TEYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1822', 'TICUL', '1733', 'TICUL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1823', 'TIMUCUY', '1733', 'TIMUCUY', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1824', 'TINUM', '1733', 'TINUM', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1825', 'TIXCACALCUPUL', '1733', 'TIXCACALCUPUL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1826', 'TIXKOKOB', '1733', 'TIXKOKOB', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1827', 'TIXMEHUAC', '1733', 'TIXMEHUAC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1828', 'TIXPÉHUAL', '1733', 'TIXPÉHUAL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1829', 'TIZIMÍN', '1733', 'TIZIMÍN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1830', 'TUNKÁS', '1733', 'TUNKÁS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1831', 'TZUCACAB', '1733', 'TZUCACAB', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1832', 'UAYMA', '1733', 'UAYMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1833', 'UCÚ', '1733', 'UCÚ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1834', 'UMÁN', '1733', 'UMÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1835', 'VALLADOLID', '1733', 'VALLADOLID', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1836', 'XOCCHEL', '1733', 'XOCCHEL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1837', 'YAXCABÁ', '1733', 'YAXCABÁ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1838', 'YAXKUKUL', '1733', 'YAXKUKUL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1839', 'YOBAÍN', '1733', 'YOBAÍN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1840', 'ZACATECAS', '0', 'ZACATECAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1841', 'APOZOL', '1840', 'APOZOL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1842', 'APULCO', '1840', 'APULCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1843', 'ATOLINGA', '1840', 'ATOLINGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1844', 'BENITO JUÁREZ', '1840', 'BENITO JUÁREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1845', 'CALERA', '1840', 'CALERA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1846', 'CAÑITAS DE FELIPE PESCADOR', '1840', 'CAÑITAS DE FELIPE PESCADOR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1847', 'CONCEPCIÓN DEL ORO', '1840', 'CONCEPCIÓN DEL ORO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1848', 'CUAUHTÉMOC', '1840', 'CUAUHTÉMOC', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1849', 'CHALCHIHUITES', '1840', 'CHALCHIHUITES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1850', 'FRESNILLO', '1840', 'FRESNILLO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1851', 'TRINIDAD GARCÍA DE LA CADENA', '1840', 'TRINIDAD GARCÍA DE LA CADENA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1852', 'GENARO CODINA', '1840', 'GENARO CODINA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1853', 'GENERAL ENRIQUE ESTRADA', '1840', 'GENERAL ENRIQUE ESTRADA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1854', 'GENERAL FRANCISCO R. MURGUÍA', '1840', 'GENERAL FRANCISCO R. MURGUÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1855', 'EL PLATEADO DE JOAQUÍN AMARO', '1840', 'EL PLATEADO DE JOAQUÍN AMARO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1856', 'GENERAL PÁNFILO NATERA', '1840', 'GENERAL PÁNFILO NATERA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1857', 'GUADALUPE', '1840', 'GUADALUPE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1858', 'HUANUSCO', '1840', 'HUANUSCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1859', 'JALPA', '1840', 'JALPA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1860', 'JEREZ', '1840', 'JEREZ', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1861', 'JIMÉNEZ DEL TEUL', '1840', 'JIMÉNEZ DEL TEUL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1862', 'JUAN ALDAMA', '1840', 'JUAN ALDAMA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1863', 'JUCHIPILA', '1840', 'JUCHIPILA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1864', 'LORETO', '1840', 'LORETO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1865', 'LUISMOYA', '1840', 'LUISMOYA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1866', 'MAZAPIL', '1840', 'MAZAPIL', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1867', 'MELCHOR OCAMPO', '1840', 'MELCHOR OCAMPO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1868', 'MEZQUITAL DEL ORO', '1840', 'MEZQUITAL DEL ORO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1869', 'MIGUEL AUZA', '1840', 'MIGUEL AUZA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1870', 'MOMAX', '1840', 'MOMAX', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1871', 'MONTE ESCOBEDO', '1840', 'MONTE ESCOBEDO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1872', 'MORELOS', '1840', 'MORELOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1873', 'MOYAHUA DE ESTRADA', '1840', 'MOYAHUA DE ESTRADA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1874', 'NOCHISTLÁN DE MEJÍA', '1840', 'NOCHISTLÁN DE MEJÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1875', 'NORIA DE ÁNGELES', '1840', 'NORIA DE ÁNGELES', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1876', 'OJOCALIENTE', '1840', 'OJOCALIENTE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1877', 'PÁNUCO', '1840', 'PÁNUCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1878', 'PINOS', '1840', 'PINOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1879', 'RÍO GRANDE', '1840', 'RÍO GRANDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1880', 'SAIN ALTO', '1840', 'SAIN ALTO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1881', 'EL SALVADOR', '1840', 'EL SALVADOR', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1882', 'SOMBRERETE', '1840', 'SOMBRERETE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1883', 'SUSTICACÁN', '1840', 'SUSTICACÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1884', 'TABASCO', '1840', 'TABASCO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1885', 'TEPECHITLÁN', '1840', 'TEPECHITLÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1886', 'TEPETONGO', '1840', 'TEPETONGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1887', 'TEÚL DE GONZÁLEZ ORTEGA', '1840', 'TEÚL DE GONZÁLEZ ORTEGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1888', 'TLALTENANGO DE SÁNCHEZ ROMÁN', '1840', 'TLALTENANGO DE SÁNCHEZ ROMÁN', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1889', 'VALPARAÍSO', '1840', 'VALPARAÍSO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1890', 'VETAGRANDE', '1840', 'VETAGRANDE', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1891', 'VILLADECOS', '1840', 'VILLADECOS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1892', 'VILLAGARCÍA', '1840', 'VILLAGARCÍA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1893', 'VILLA GONZÁLEZ ORTEGA', '1840', 'VILLA GONZÁLEZ ORTEGA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1894', 'VILLA HIDALGO', '1840', 'VILLA HIDALGO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1895', 'VILLA NUEVA', '1840', 'VILLA NUEVA', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1896', 'ZACATECAS', '1840', 'ZACATECAS', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1897', 'TRANCOSO', '1840', 'TRANCOSO', '127', '1');
INSERT INTO `job_locations` (`id`, `name`, `parent_id`, `slug`, `position`, `active`) VALUES ('1898', 'SANTA MARÍA DE LA PAZ', '1840', 'SANTA MARÍA DE LA PAZ', '127', '1');


-- --------------------------------------------------
# -- Table structure for table `job_skills`
-- --------------------------------------------------
DROP TABLE IF EXISTS `job_skills`;
CREATE TABLE `job_skills` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `position` tinyint(2) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `job_skills`
-- --------------------------------------------------

INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('65', '\tAUTOCONFIANZA\t ', '\tAUTOCONFIANZA\t ', '6', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('73', '\tCOMUNICACIÓN\t ', '\tCOMUNICACIÓN\t ', '14', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('72', '\tCOMPROMISO\t ', '\tCOMPROMISO\t ', '13', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('71', '\tCAPACIDAD CRÍTICA\t ', '\tCAPACIDAD CRÍTICA\t ', '12', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('70', '\tAUTONOMÍA\t ', '\tAUTONOMÍA\t ', '11', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('69', '\tAUTOMOTIVACIÓN\t ', '\tAUTOMOTIVACIÓN\t ', '10', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('68', '\tAUTOCRÍTICA\t ', '\tAUTOCRÍTICA\t ', '9', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('67', '\tAUTOCONTROL\t ', '\tAUTOCONTROL\t ', '8', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('66', '\tAUTOCONOCIMIENTO\t ', '\tAUTOCONOCIMIENTO\t ', '7', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('64', '\tASUNCIÓN DE RIESGOS\t ', '\tASUNCIÓN DE RIESGOS\t ', '5', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('63', '\tASERTIVIDAD\t ', '\tASERTIVIDAD\t ', '4', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('62', '\tANÁLISIS DE PROBLEMAS\t ', '\tANÁLISIS DE PROBLEMAS\t ', '3', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('61', '\tAMABILIDAD\t ', '\tAMABILIDAD\t ', '2', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('60', '\tADAPTABILIDAD\t ', '\tADAPTABILIDAD\t ', '1', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('74', '\tCREATIVIDAD\t ', '\tCREATIVIDAD\t ', '15', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('75', '\tDECISIÓN\t ', '\tDECISIÓN\t ', '16', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('76', '\tDELEGACIÓN\t ', '\tDELEGACIÓN\t ', '17', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('77', '\tDISCRECIÓN\t ', '\tDISCRECIÓN\t ', '18', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('78', '\tEMPATIA\t ', '\tEMPATIA\t ', '19', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('79', '\tESCUCHA\t ', '\tESCUCHA\t ', '20', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('80', '\tESPÍRITU COMERCIAL\t ', '\tESPÍRITU COMERCIAL\t ', '21', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('81', '\tFACILIDAD DE APRENDIZAJE\t ', '\tFACILIDAD DE APRENDIZAJE\t ', '22', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('82', '\tFLEXIBILIDAD\t ', '\tFLEXIBILIDAD\t ', '23', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('83', '\tGESTIÓN\t ', '\tGESTIÓN\t ', '24', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('84', '\tGESTIÓN DEL TIEMPO\t ', '\tGESTIÓN DEL TIEMPO\t ', '25', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('85', '\tINICIATIVA\t ', '\tINICIATIVA\t ', '26', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('86', '\tINTEGRIDAD\t ', '\tINTEGRIDAD\t ', '27', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('87', '\tLIDERAZGO\t ', '\tLIDERAZGO\t ', '28', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('88', '\tMETICULOSIDAD\t ', '\tMETICULOSIDAD\t ', '29', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('89', '\tNEGOCIACIÓN\t ', '\tNEGOCIACIÓN\t ', '30', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('90', '\tNETWORKING\t ', '\tNETWORKING\t ', '31', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('91', '\tORGANIZACIÓN\t ', '\tORGANIZACIÓN\t ', '32', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('92', '\tORIENTACIÓN AL CLIENTE\t ', '\tORIENTACIÓN AL CLIENTE\t ', '33', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('93', '\tORIENTACIÓN AL LOGRO\t ', '\tORIENTACIÓN AL LOGRO\t ', '34', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('94', '\tORIENTACIÓN ESTRATÉGICA\t ', '\tORIENTACIÓN ESTRATÉGICA\t ', '35', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('95', '\tPERSUASIÓN\t ', '\tPERSUASIÓN\t ', '36', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('96', '\tPLANIFICACIÓN\t ', '\tPLANIFICACIÓN\t ', '37', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('97', '\tPROACTIVIDAD\t ', '\tPROACTIVIDAD\t ', '38', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('98', '\tRESOLUCIÓN DE CONFLICTOS\t ', '\tRESOLUCIÓN DE CONFLICTOS\t ', '39', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('99', '\tSÍNTESIS\t ', '\tSÍNTESIS\t ', '40', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('100', '\tSOCIABILIDAD\t ', '\tSOCIABILIDAD\t ', '41', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('101', '\tTENACIDAD\t ', '\tTENACIDAD\t ', '42', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('102', '\tTOLERANCIA AL ESTRÉS\t ', '\tTOLERANCIA AL ESTRÉS\t ', '43', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('103', '\tTRABAJO EN EQUIPO\t ', '\tTRABAJO EN EQUIPO\t ', '44', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('105', 'RESPONSABILIDAD', 'responsabilidad', '0', '1');
INSERT INTO `job_skills` (`id`, `name`, `slug`, `position`, `active`) VALUES ('106', 'HONESTIDAD', 'honestidad', '0', '1');


-- --------------------------------------------------
# -- Table structure for table `job_types`
-- --------------------------------------------------
DROP TABLE IF EXISTS `job_types`;
CREATE TABLE `job_types` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(60) NOT NULL,
  `color` varchar(11) NOT NULL,
  `position` tinyint(2) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `job_types`
-- --------------------------------------------------

INSERT INTO `job_types` (`id`, `name`, `slug`, `color`, `position`, `active`) VALUES ('28', 'Medio tiempo', 'medio-tiempo', '#f1630d', '2', '1');
INSERT INTO `job_types` (`id`, `name`, `slug`, `color`, `position`, `active`) VALUES ('32', 'Contrato indefinido', 'contrato-indefinido', '#5AB1EF', '0', '1');
INSERT INTO `job_types` (`id`, `name`, `slug`, `color`, `position`, `active`) VALUES ('30', 'Practicante', 'practicante', '#e1d123', '1', '1');
INSERT INTO `job_types` (`id`, `name`, `slug`, `color`, `position`, `active`) VALUES ('33', 'Contrato temporal', 'contrato-temporal', '#2EC7C9', '0', '1');
INSERT INTO `job_types` (`id`, `name`, `slug`, `color`, `position`, `active`) VALUES ('36', 'Obra', 'obra', 'green', '0', '1');


-- --------------------------------------------------
# -- Table structure for table `jobs`
-- --------------------------------------------------
DROP TABLE IF EXISTS `jobs`;
CREATE TABLE `jobs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` int(2) NOT NULL,
  `location` varchar(255) NOT NULL,
  `categories` varchar(50) NOT NULL,
  `skills` varchar(255) NOT NULL,
  `salary` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `responsibility` text NOT NULL,
  `experience` text NOT NULL,
  `education` text NOT NULL,
  `benefits` text NOT NULL,
  `additional_info` text NOT NULL,
  `apply_url` varchar(255) NOT NULL,
  `filled` int(2) NOT NULL DEFAULT '0',
  `publish_date` datetime NOT NULL,
  `expire_date` datetime NOT NULL,
  `featured` enum('No Destacado','Destacado') NOT NULL DEFAULT 'No Destacado',
  `status` enum('pending','approved','declined') NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------
# Dumping data for table `jobs`
-- --------------------------------------------------



-- --------------------------------------------------
# -- Table structure for table `menus`
-- --------------------------------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `page_id` tinyint(2) NOT NULL DEFAULT '0',
  `parent_id` int(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `content_type` varchar(20) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `target` enum('_self','_blank') NOT NULL DEFAULT '_blank',
  `position` tinyint(2) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_id` (`active`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `menus`
-- --------------------------------------------------

INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('1', '18', '0', 'Inicio', 'page', '', '', '1', '1');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('2', '4', '0', 'Contacto', 'page', '', '', '12', '1');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('3', '2', '13', 'About Us', 'page', '', '', '4', '1');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('4', '3', '13', 'FAQ', 'page', '', '', '6', '1');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('11', '20', '0', 'Ofertas de empleo', 'page', '', '', '10', '0');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('6', '2', '13', 'Browse Resumes', 'web', 'http://localhost/jb/browse-resumes.php', '_self', '7', '0');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('8', '2', '13', 'Browse Categories', 'web', 'http://localhost/jb/browse-categories.php', '_self', '5', '1');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('7', '2', '0', 'Ofertas de empleo', 'web', 'browse-jobs.php', '_self', '2', '1');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('9', '16', '0', 'Categorías', 'page', '', '', '9', '1');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('10', '17', '0', 'Pricing', 'page', '', '', '8', '0');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('12', '21', '0', 'Resumes', 'page', '', '', '11', '0');
INSERT INTO `menus` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('13', '2', '0', 'Pages', 'web', '#', '_self', '3', '0');


-- --------------------------------------------------
# -- Table structure for table `menus_footer`
-- --------------------------------------------------
DROP TABLE IF EXISTS `menus_footer`;
CREATE TABLE `menus_footer` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `page_id` tinyint(2) NOT NULL DEFAULT '0',
  `parent_id` int(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `content_type` varchar(20) NOT NULL,
  `link` varchar(255) DEFAULT NULL,
  `target` enum('_self','_blank') NOT NULL DEFAULT '_blank',
  `position` tinyint(2) NOT NULL DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `content_id` (`active`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `menus_footer`
-- --------------------------------------------------

INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('1', '18', '0', 'Inicio', 'page', '', '', '1', '1');
INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('2', '4', '0', 'Contacto', 'page', '', '', '6', '1');
INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('3', '2', '0', 'Acerca de', 'page', '', '', '2', '1');
INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('4', '3', '0', 'Preguntas Frecuentes', 'page', '', '', '7', '1');
INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('6', '2', '4', 'Browse Resumes', 'web', 'http://localhost/jb/browse-resumes.php', '_self', '9', '0');
INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('8', '2', '0', 'Ofertas por categoría', 'web', 'browse-categories.php', '_self', '4', '1');
INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('7', '2', '0', 'Ofertas de trabajo', 'web', 'browse-jobs.php', '_self', '5', '1');
INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('10', '17', '4', 'Precios', 'page', '', '', '10', '0');
INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('12', '21', '4', 'Resumes', 'page', '', '', '11', '0');
INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('13', '2', '4', 'Paginas', 'web', '#', '_self', '8', '0');
INSERT INTO `menus_footer` (`id`, `page_id`, `parent_id`, `name`, `content_type`, `link`, `target`, `position`, `active`) VALUES ('14', '22', '0', 'Aviso de privacidad', 'page', 'http://www.cmic.org.mx/cmic/privacidad/', '_blank', '3', '1');


-- --------------------------------------------------
# -- Table structure for table `messages`
-- --------------------------------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid1` int(11) NOT NULL DEFAULT '0',
  `uid2` int(11) NOT NULL DEFAULT '0',
  `user1` int(11) NOT NULL DEFAULT '0',
  `user2` int(11) NOT NULL DEFAULT '0',
  `msgsubject` varchar(200) NOT NULL,
  `body` text NOT NULL,
  `attachment` varchar(60) DEFAULT NULL,
  `user1read` varchar(3) DEFAULT NULL,
  `user2read` varchar(3) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `messages`
-- --------------------------------------------------



-- --------------------------------------------------
# -- Table structure for table `news`
-- --------------------------------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` tinyint(2) NOT NULL AUTO_INCREMENT,
  `title` varchar(55) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `body` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `author` varchar(55) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `created` date NOT NULL DEFAULT '0000-00-00',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `news`
-- --------------------------------------------------

INSERT INTO `news` (`id`, `title`, `body`, `author`, `created`, `active`) VALUES ('1', 'Anuncio de noticias', '&lt;div&gt;Estamos felices de liberar Job Board 1.0.&lt;/div&gt;&lt;p&gt;&lt;/p&gt;&lt;div&gt;Viene con una función completa de bolsa de trabajo con un diseño y funcionalidad impresionantes.&lt;/div&gt;&lt;p&gt;&lt;/p&gt;', 'Administrator', '2017-01-19', '0');


-- --------------------------------------------------
# -- Table structure for table `packages`
-- --------------------------------------------------
DROP TABLE IF EXISTS `packages`;
CREATE TABLE `packages` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `slug` varchar(50) NOT NULL,
  `features` text NOT NULL,
  `price` float NOT NULL,
  `limit` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `billing` enum('onetime','weekly','monthly','yearly') NOT NULL,
  `featured` int(1) NOT NULL,
  `position` tinyint(3) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `packages`
-- --------------------------------------------------

INSERT INTO `packages` (`id`, `name`, `slug`, `features`, `price`, `limit`, `duration`, `billing`, `featured`, `position`, `active`, `created`) VALUES ('8', 'Company', 'basic', '&lt;ul&gt;\r\n\t\t\t\t&lt;li&gt;Monthly Fee&lt;/li&gt;\r\n\t\t\t\t&lt;li&gt;This Plan Includes 50 Job&lt;/li&gt;\r\n\t\t\t\t&lt;li&gt;5 Highlighted Post&lt;/li&gt;\r\n\t\t\t\t&lt;li&gt;Posted For 30 Days&lt;/li&gt;\r\n\t\t\t&lt;/ul&gt;', '34.99', '50', '30', 'monthly', '1', '3', '1', '2017-02-13 08:12:07');
INSERT INTO `packages` (`id`, `name`, `slug`, `features`, `price`, `limit`, `duration`, `billing`, `featured`, `position`, `active`, `created`) VALUES ('7', 'Basic', 'basic', '&lt;ul&gt;\r\n\t\t\t\t&lt;li&gt;One Time Fee&lt;/li&gt;\r\n\t\t\t\t&lt;li&gt;This Plan Includes 30 Job&lt;/li&gt;\r\n\t\t\t\t&lt;li&gt;1 Highlighted Post&lt;/li&gt;\r\n\t\t\t\t&lt;li&gt;Posted For 30 Days&lt;/li&gt;\r\n\t\t\t&lt;/ul&gt;', '19.99', '30', '30', 'onetime', '0', '2', '1', '2017-02-13 08:12:07');
INSERT INTO `packages` (`id`, `name`, `slug`, `features`, `price`, `limit`, `duration`, `billing`, `featured`, `position`, `active`, `created`) VALUES ('11', 'Enterprise', 'enterprise', '&lt;ul&gt;\r\n\t\t\t\t&lt;li&gt;&lt;div&gt;Plan anual&lt;/div&gt;&lt;div&gt;Este plan incluye 10,000 trabajos&lt;/div&gt;&lt;div&gt;Publicado durante 30 días&lt;/div&gt;&lt;/li&gt;\r\n\t\t\t&lt;/ul&gt;', '0', '10000', '365', 'yearly', '0', '4', '1', '2017-02-13 08:12:07');
INSERT INTO `packages` (`id`, `name`, `slug`, `features`, `price`, `limit`, `duration`, `billing`, `featured`, `position`, `active`, `created`) VALUES ('12', 'Gold', 'gold', '&lt;p&gt;Lorem ipsu&amp;nbsp; dolor&amp;nbsp;&lt;/p&gt;', '22', '15', '12', 'yearly', '1', '0', '1', '0000-00-00 00:00:00');


-- --------------------------------------------------
# -- Table structure for table `page_templates`
-- --------------------------------------------------
DROP TABLE IF EXISTS `page_templates`;
CREATE TABLE `page_templates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `template` varchar(50) NOT NULL,
  `directory` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- --------------------------------------------------
# Dumping data for table `page_templates`
-- --------------------------------------------------

INSERT INTO `page_templates` (`id`, `name`, `slug`, `filename`, `template`, `directory`, `created`) VALUES ('1', 'FAQ Page', 'faq', 'faq.php', 'faq.tpl.php', 'template', '2017-02-13 00:00:00');
INSERT INTO `page_templates` (`id`, `name`, `slug`, `filename`, `template`, `directory`, `created`) VALUES ('2', 'Contact Page', 'contact', 'contact.php', 'contact.tpl.php', 'template', '2017-02-13 00:00:00');
INSERT INTO `page_templates` (`id`, `name`, `slug`, `filename`, `template`, `directory`, `created`) VALUES ('3', 'Package List Page', 'package', 'packages.php', 'packages.tpl.php', 'template', '2017-02-13 00:00:00');
INSERT INTO `page_templates` (`id`, `name`, `slug`, `filename`, `template`, `directory`, `created`) VALUES ('4', 'Browse Categories', 'browse-categories', 'browse-categories.php', 'browse-categories.tpl.php', 'template', '2017-02-14 17:15:04');
INSERT INTO `page_templates` (`id`, `name`, `slug`, `filename`, `template`, `directory`, `created`) VALUES ('5', 'Browse Jobs', 'browse-jobs', 'browse-jobs-template.php', 'browse-jobs-template.tpl.php', 'template', '0000-00-00 00:00:00');
INSERT INTO `page_templates` (`id`, `name`, `slug`, `filename`, `template`, `directory`, `created`) VALUES ('7', 'Browse Resumes', 'browse-resumes', 'browse-resumes-template.php', 'browse-resumes-template.tpl.php', 'template', '0000-00-00 00:00:00');


-- --------------------------------------------------
# -- Table structure for table `pages`
-- --------------------------------------------------
DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `template` varchar(255) NOT NULL,
  `body` longtext,
  `breadcrumb` int(1) DEFAULT '1',
  `created` datetime NOT NULL,
  `home_page` tinyint(1) NOT NULL DEFAULT '0',
  `active` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `pages`
-- --------------------------------------------------

INSERT INTO `pages` (`id`, `title`, `slug`, `template`, `body`, `breadcrumb`, `created`, `home_page`, `active`) VALUES ('2', 'Sobre Nosotros', 'about-us', '', '&lt;style&gt;\n.iframe-container{\n  position: relative;\n  width: 100%;\n  padding-bottom: 56.25%; \n  height: 0;\n}\n.iframe-container iframe{\n  position: absolute;\n  top:0;\n  left: 0;\n  width: 100%;\n  height: 100%;\n}\n&lt;/style&gt;\n&lt;h3&gt;&lt;strong&gt;Misión:&lt;/strong&gt;&lt;/h3&gt;\n&lt;p style=&quot;margin-left: 20px;&quot;&gt;\n\t“Representar los intereses de los industriales de la construcción, brindando servicios de excelencia, para impulsar una industria altamente competitiva, de vanguardia, con responsabilidad social e innovación tecnológica, contribuyendo así al desarrollo de México”.\n&lt;/p&gt;\n&lt;div class=&quot;iframe-container&quot;&gt;\n&lt;iframe width=&quot;600&quot; height=&quot;350&quot; src=&quot;https://www.youtube.com/embed/cmRX3sdn0UI&quot; frameborder=&quot;0&quot; allow=&quot;accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture&quot; allowfullscreen=&quot;&quot;&gt;\n&lt;/iframe&gt;\n&lt;/div&gt;\n&lt;h3&gt;&lt;strong&gt;Visión:&lt;/strong&gt;&lt;/h3&gt;\n&lt;p style=&quot;margin-left: 20px;&quot;&gt;\n\t“Ser una Cámara de clase mundial, líder y referencia indispensable, así como plataforma para el desarrollo sustentable de la industria de la construcción”.\n&lt;/p&gt;\n&lt;p style=&quot;margin-left: 20px; text-align: justify;&quot;&gt;\n\tAl mismo tiempo, sin embargo, exige de todos y cada uno de los demás integrantes de esta industria -clientes, contratistas y proveedores- y de las autoridades, una visión ética de su actividad que deberá traducirse en acciones concretas, tendientes a la búsqueda permanente del bien común, de la honestidad, de la equidad y de la justicia, asumiendo solidariamente la responsabilidad de vivir estos compromisos, y de antender, promover y difundir estos valores, adhiriéndose así voluntariamente a este magno esfuerzo social, mediante su participación recíproca, corresponsable y solidaria, permitiendo que los principios y normas éticos sean una realidad cotidiana en el desarrollo de nuestra industria.&amp;nbsp;\n&lt;/p&gt;\n&lt;h3&gt;&lt;strong&gt;Valores:&lt;/strong&gt;&lt;/h3&gt;\n&lt;p style=&quot;margin-left: 40px; text-align: justify;&quot;&gt;\n\t&lt;strong&gt;1.- &lt;u&gt;UNIDAD&lt;/u&gt;:&lt;/strong&gt;\n&lt;/p&gt;\n&lt;p style=&quot;text-align: justify; margin-left: 60px;&quot;&gt;\n\tEntendida como la solidaridad que existe entre todos los integrantes de la Cámara y los grupos de interés.&amp;nbsp;\n&lt;/p&gt;\n&lt;p style=&quot;margin-left: 40px; text-align: justify;&quot;&gt;\n\t&lt;strong&gt;2.- &lt;u&gt;INSTITUCIONALIDAD&lt;/u&gt;:&lt;/strong&gt;\n&lt;/p&gt;\n&lt;p style=&quot;text-align: justify; margin-left: 60px;&quot;&gt;\n\t&lt;b style=&quot;color: inherit; font-family: &amp;quot;Open Sans&amp;quot;, arial, sans-serif;&quot;&gt;&lt;/b&gt;La cual se relaciona con la formalidad de todos los individuos y organizaciones de la construcción, al comprometerse con la Cámara y sus Delegaciones para el funcionamiento eficiente de sus Institutos y objetivos estratégicos.\n&lt;/p&gt;\n&lt;p style=&quot;margin-left: 40px; text-align: justify;&quot;&gt;\n\t&lt;strong&gt;3.- &lt;u&gt;EXCELENCIA&lt;/u&gt;:&lt;/strong&gt;\n&lt;/p&gt;\n&lt;p style=&quot;text-align: justify; margin-left: 60px;&quot;&gt;\n\tEntendida como la búsqueda del desarrollo humano y profesional de todos los integrantes de la Cámara para lograr que la industria de la construcción logre estándares de calidad mundial.\n&lt;/p&gt;\n&lt;p style=&quot;margin-left: 40px; text-align: justify;&quot;&gt;\n\t&lt;strong&gt;4.- &lt;u&gt;HONESTIDAD&lt;/u&gt;:&lt;/strong&gt;\n&lt;/p&gt;\n&lt;p style=&quot;text-align: justify; margin-left: 60px;&quot;&gt;\n\t&lt;b style=&quot;color: inherit; font-family: &amp;quot;Open Sans&amp;quot;, arial, sans-serif;&quot;&gt;&lt;/b&gt;La cual se refiere a nuestra capacidad para conducirnos siempre con apego a la verdad y un respeto absoluto a la propiedad económica e intelectual de los demás.\n&lt;/p&gt;\n&lt;p style=&quot;margin-left: 40px; text-align: justify;&quot;&gt;\n\t&lt;strong&gt;5.- &lt;u&gt;JUSTICIA Y EQUIDAD&lt;/u&gt;:&lt;/strong&gt;\n&lt;/p&gt;\n&lt;p style=&quot;text-align: justify; margin-left: 60px;&quot;&gt;\n\tEntendida la justicia como la capacidad de dar a cada quien lo que le corresponde cuando le corresponde y apoyar a los débiles sin abusar de una posición privilegiada, la equidad.\n&lt;/p&gt;\n&lt;p style=&quot;margin-left: 40px; text-align: justify;&quot;&gt;\n\t&lt;strong&gt;6.- &lt;u&gt;RESPONSABILIDAD&lt;/u&gt;:&lt;/strong&gt;\n&lt;/p&gt;\n&lt;p style=&quot;text-align: justify; margin-left: 60px;&quot;&gt;\n\tNuestra capacidad de cumplir siempre con lo ofrecido hacia terceros, corrigiendo errores y reparando los daños que pudiesen causarse\n&lt;/p&gt;\n&lt;p style=&quot;margin-left: 40px; text-align: justify;&quot;&gt;\n\t&lt;strong&gt;7.- &lt;u&gt;SALUD Y SEGURIDAD&lt;/u&gt;:&lt;/strong&gt;\n&lt;/p&gt;\n&lt;p style=&quot;text-align: justify; margin-left: 60px;&quot;&gt;\n\t&lt;b style=&quot;color: inherit; font-family: &amp;quot;Open Sans&amp;quot;, arial, sans-serif;&quot;&gt;&lt;/b&gt;&lt;b style=&quot;color: inherit; font-family: &amp;quot;Open Sans&amp;quot;, arial, sans-serif;&quot;&gt;&lt;/b&gt;Entendida como el cuidado por encima de cualquier otro interés la salud y seguridad de las personas, así como de las instalaciones encomendadas a nuestra gestión, desarrollando acciones para cuidar y evitar poner en riesgo mi salud, mi seguridad y la de mis compañeros y compañeras.\n&lt;/p&gt;\n&lt;p style=&quot;margin-left: 40px; text-align: justify;&quot;&gt;\n\t&lt;strong&gt;8.- &lt;u&gt;INNOVACIÓN Y BUENAS PRÁCTICAS&lt;/u&gt;:&lt;/strong&gt;\n&lt;/p&gt;\n&lt;p style=&quot;text-align: justify; margin-left: 60px;&quot;&gt;\n\t&lt;b style=&quot;color: inherit; font-family: &amp;quot;Open Sans&amp;quot;, arial, sans-serif;&quot;&gt;&lt;/b&gt;&lt;b style=&quot;color: inherit; font-family: &amp;quot;Open Sans&amp;quot;, arial, sans-serif;&quot;&gt;&lt;/b&gt;Mantener un sentido permanente de búsqueda de las mejores prácticas en la construcción y el abatimiento de los costos y precios de los proyectos y compromisos.\n&lt;/p&gt;\n&lt;p style=&quot;margin-left: 40px; text-align: justify;&quot;&gt;\n\t&lt;strong&gt;9.- &lt;u&gt;TRANSPARENCIA Y PRÁCTICAS ANTICORRUPCIÓN&lt;/u&gt;:&lt;/strong&gt;\n&lt;/p&gt;\n&lt;p style=&quot;text-align: justify; margin-left: 60px;&quot;&gt;\n\t&lt;b style=&quot;color: inherit; font-family: &amp;quot;Open Sans&amp;quot;, arial, sans-serif;&quot;&gt;&lt;/b&gt;&lt;b style=&quot;color: inherit; font-family: &amp;quot;Open Sans&amp;quot;, arial, sans-serif;&quot;&gt;&lt;/b&gt;Llevar a cabo prácticas y métodos, sin tener nada que ocultar y abuso del poder o de posiciones personales para un beneficio personal.\n&lt;/p&gt;', '1', '2014-09-02 00:00:00', '0', '1');
INSERT INTO `pages` (`id`, `title`, `slug`, `template`, `body`, `breadcrumb`, `created`, `home_page`, `active`) VALUES ('3', 'Preguntas frecuentes', 'faq', 'faq', '', '1', '2014-08-06 00:00:00', '0', '1');
INSERT INTO `pages` (`id`, `title`, `slug`, `template`, `body`, `breadcrumb`, `created`, `home_page`, `active`) VALUES ('4', 'Contacto', 'contacto', 'contact', '&lt;h1 style=&quot;margin-bottom:25px&quot;&gt;&lt;strong&gt;Contáctanos!&lt;/strong&gt;&lt;/h1&gt;\n&lt;p style=&quot;margin-left:15px; &quot;&gt;&lt;strong&gt;\n\t¿Necesitas mas información? ¿Tienes alguna duda, queja u opinión? &amp;nbsp;\n\t&lt;br&gt;\n\tNos encantaría saber de ti, mándanos un mensaje.\n&lt;/strong&gt;&lt;/p&gt;', '1', '2014-09-01 00:00:00', '0', '1');
INSERT INTO `pages` (`id`, `title`, `slug`, `template`, `body`, `breadcrumb`, `created`, `home_page`, `active`) VALUES ('22', 'Aviso de privacidad', 'aviso-de-privacidad', '', '&lt;p style=&quot;text-align: right;&quot;&gt;\n    &lt;strong&gt;Nueva revisión: 28 de marzo de 2017&lt;/strong&gt;\n&lt;/p&gt;\n&lt;h3&gt;\n    &lt;strong&gt;Aviso de privacidad:&lt;/strong&gt;\n&lt;/h3&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    En la Cámara Mexicana de la Industria de la Construcción, con domicilio en Periférico Sur # 4839, Colonia Parques\n    del Pedregal,\n    C.P. 14010, Delegación Tlalpan, México, Distrito Federal, estamos conscientes que nuestros afiliados, asociados o\n    colaboradores\n    tienen derecho a conocer qué información recabamos de ellos y nuestras prácticas en relación con dicha información.\n    Las condiciones\n    contenidas en el presente son aplicables a toda la información que se recaba a nombre de y por la Cámara Mexicana de\n    la Industria\n    de la Construcción en adelante La CMIC, a través de sus 43 Delegaciones en todo el país.\n&lt;/p&gt;\n&lt;h5&gt;\n    &lt;strong&gt;Datos Personales que se recaban:&lt;/strong&gt;\n&lt;/h5&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    La CMIC puede tratar los Datos Personales de sus afiliados, asociados o colaboradores, es decir, información que los\n    identifica o\n    los hace identificables. Algunos de estos datos personales son, por ejemplo: nombre, género, correo electrónico,\n    domicilio fiscal,\n    número de teléfono, fecha de nacimiento, datos de pago (tales como, número de tarjeta de crédito, vigencia), datos\n    de facturación.\n    Estos datos personales son proporcionados voluntariamente al afiliarse, asociarse o al integrarse a la CMIC o al\n    decidir participar\n    en los diversos productos y servicios que se ofrecen como Congresos, Reuniones Nacionales, boletines, foros,\n    concursos o al registrarse\n    para participar en otras actividades que la CMIC ofrezca.\n&lt;/p&gt;\n&lt;p&gt;\n    La CMIC podrá interrelacionar la información proporcionada por sus afiliados, asociados o colaboradores, con otra\n    información que\n    voluntariamente le sea proporcionada por terceros. Lo anterior, con el fin de adaptar nuestros productos, servicios\n    y promociones\n    de acuerdo a las necesidades y preferencias de cada uno.\n&lt;/p&gt;\n&lt;h5&gt;\n    &lt;strong&gt;Usos y finalidades de sus datos personales:&lt;/strong&gt;\n&lt;/h5&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;Los Datos Personales que reciba la CMIC serán tratados, además de cumplir con las\n    obligaciones derivadas de la relación creada con sus afiliados o asociados, con las siguientes finalidades:\n&lt;/p&gt;\n&lt;div style=&quot;margin-left: 70px; text-align: justify;&quot;&gt;\n    &lt;ol&gt;\n        &lt;li&gt;Proporcionar información sobre productos, servicios, promociones u otras actividades o propuestas de interés\n            de la CMIC.\n        &lt;/li&gt;\n        &lt;li&gt;Solicitar información para efectos de investigación de mercado.&lt;/li&gt;\n        &lt;li&gt;Brindar información para que los afiliados o asociados conozcan y participen en concursos, certámenes u\n            otras actividades de la\n            CMIC, así como para contactarlos con tales fines.\n        &lt;/li&gt;\n        &lt;li&gt;Solicitar que se respondan algunas encuestas de satisfacción respecto a nuestros productos y servicios.&lt;/li&gt;\n        &lt;li&gt;Contactarlos en relación a los resultados de las evaluaciones realizadas de los productos y servicios de la\n            CMIC o sobre algún otro tema.\n        &lt;/li&gt;\n        &lt;li&gt;Dar seguimiento a algún problema que haya surgido respecto a los servicios y productos brindados.&lt;/li&gt;\n        &lt;li&gt;En general, enviar información de la CMIC y sus Instituciones Conexas, con fines publicitarios y de\n            promoción.\n        &lt;/li&gt;\n    &lt;/ol&gt;\n\n&lt;/div&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    En el caso de los colaboradores, los datos personales que reciba la CMIC serán tratados exclusivamente para dar\n    cumplimiento a las obligaciones\n    derivadas de la relación contractual generada con ellos y no podrá ser compartida con terceras personas.\n&lt;/p&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    En caso de que los datos personales pretendan ser utilizados para una finalidad distinta a las anteriores, la CMIC\n    notificará por correo\n    electrónico o por teléfono, a fin de obtener su consentimiento para el tratamiento de sus datos personales de\n    acuerdo a las nuevas finalidades.\n    De no recibir su consentimiento, para los casos en que es necesario, la CMIC no utilizará sus datos personales.\n&lt;/p&gt;\n&lt;h5&gt;\n    &lt;strong&gt;Transmisión y transferencia de Datos Personales:&lt;/strong&gt;\n&lt;/h5&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    La CMIC no compartirá, sin el consentimiento previo, los datos personales de nuestros afiliados, asociados o\n    colaboradores con terceros, nacionales o extranjeros, tales como: Empresas Afiliadas, Instituciones de Crédito,\n    Instituciones de Seguros y Fianzas, Organismos Empresariales Nacionales o Extranjeros. Dicha transferencia deberá\n    estar prevista en la Ley Federal de Protección de Datos Personales en Posesión de los Particulares. Se requerirá a\n    terceros utilizar la información transferida únicamente para el fin de llevar a cabo el fin específico.\n&lt;/p&gt;\n&lt;h5&gt;\n    &lt;strong&gt;Seguridad:&lt;/strong&gt;\n&lt;/h5&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    La CMIC ha adoptado las medidas de seguridad, administrativas, técnicas y físicas, necesarias para proteger\n    los datos personales de sus afiliados, asociados y colaboradores contra daño, pérdida, alteración, destrucción o el\n    uso, acceso o tratamiento no autorizado.\n&lt;/p&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    Sin embargo, la CMIC no puede y por ende no garantiza que no existirá un acceso no autorizado a sus datos personales\n    o que las comunicaciones estén libres de toda interrupción, error o intercepción por terceros no autorizados.\n&lt;/p&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    El acceso a los datos personales, en poder de la CMIC, se limitará a las personas que necesiten tener acceso a dicha\n    información, con el propósito de llevar a cabo las finalidades identificadas.\n&lt;/p&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    Todos los afiliados, asociados o colaboradores que proporcionen sus Datos Personales a la CMIC y les sea asignado un\n    nombre de usuario y contraseña, serán responsables de que dicha información se mantenga confidencial. Por lo que la\n    CMIC sugiere no compartir esta información con nadie más.\n&lt;/p&gt;\n&lt;h5&gt;\n    &lt;strong&gt;Derechos que le corresponden:&lt;/strong&gt;\n&lt;/h5&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    El titular de datos personales podrá solicitar ante el Área de Protección de Datos Personales de la CMIC el\n    acceso, rectificación, cancelación u oposición (derechos ARCO), respecto de sus datos personales. Asimismo, podrá,\n    en todo momento, revocar el consentimiento que haya otorgado, y que fuere necesario, para el tratamiento de sus\n    datos personales. Para ello, deberá enviar a Periférico Sur # 4839, Colonia Parques del Pedregal, C.P. 14010,\n    Delegación Tlalpan, México, Distrito Federal o al correo electrónico: &lt;a href=&quot;mailto:avisodeprivacidad@cmic.org&quot;&gt;&lt;strong style=&quot;color:red;&quot;&gt;avisodeprivacidad@cmic.org&lt;/strong&gt;&lt;/a&gt;\n    su solicitud respectiva en el Formato que para tales efectos se proporcione en la página web. &lt;a href=&quot;http://www.cmic.org&quot;&gt; &lt;strong style=&quot;color:red;&quot;&gt;www.cmic.org&lt;/strong&gt;&lt;/a&gt;\n&lt;/p&gt;\n&lt;br\n&gt;\n&lt;p&gt;\n    Para fácil referencia, les comunicamos que todos nuestros afiliados, asociados y colaboradores están en su derecho\n    a:\n&lt;/p&gt;\n&lt;br\n&gt;\n&lt;div style=&quot;margin-left: 30px; text-align: justify;&quot;&gt;\n    &lt;ul&gt;\n        &lt;li&gt;&lt;p&gt;\n            &lt;strong&gt;Acceso&lt;/strong&gt; .- que se les informe cuáles de sus datos personales están contenidos en las bases\n            de\n            datos de la CMIC y para qué se utilizan, así como el origen de dichos datos y las comunicaciones que se\n            hayan\n            realizado con los mismos.\n        &lt;/p&gt;&lt;/li&gt;\n        &lt;li&gt;&lt;p&gt;\n            &lt;strong&gt;Rectificación&lt;/strong&gt; .- que se corrijan o actualicen sus datos personales en caso de que sean\n            inexactos o\n            incompletos. Para lo cual deberán informar a la CMIC de los cambios que se deban hacer a sus datos\n            personales,\n            cuando dichos cambios solo sean de su conocimiento.\n        &lt;/p&gt;&lt;/li&gt;\n        &lt;li&gt;&lt;p&gt;\n            &lt;strong&gt;Cancelación&lt;/strong&gt; .- que sus datos personales sean dados de baja total o parcialmente de las\n            bases de datos de la CMIC. Esta solicitud dará lugar a un periodo de bloqueo tras el cual procederá la\n            supresión de los datos. Es importante señalar que, existen casos en los que la cancelación no será posible\n            en términos de la Ley Federal de Protección de Datos Personales en Posesión de los Particulares y otras\n            disposiciones legales aplicables.\n        &lt;/p&gt;&lt;/li&gt;\n        &lt;li&gt;&lt;p&gt;\n            &lt;strong&gt;Oposición&lt;/strong&gt; .- oponerse por causa legítima al tratamiento de sus datos personales por parte\n            de la CMIC o limitar su uso o divulgación.En los casos que la oposición verse sobre la recepción de ciertos\n            comunicados, en dichos comunicados se incluirá la opción para salir de la lista de envío y dejar de\n            recibirlos.\n        &lt;/p&gt;&lt;/li&gt;\n    &lt;/ul&gt;\n&lt;/div&gt;\n&lt;h5&gt;\n    &lt;strong&gt;Cambios al Aviso de Privacidad: &lt;/strong&gt;\n&lt;/h5&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify;&quot;&gt;\n    La CMIC se reserva el derecho de modificar o enmendar, total o parcialmente, el presente Aviso de Privacidad. El\n    Aviso de Privacidad modificado se le hará llegar al correo electrónico que nos proporcionó o se publicará en la\n    página web:&lt;a href=&quot;http://www.cmic.org.&quot;&gt; &lt;strong style=&quot;color:red;&quot;&gt;www.cmic.org&lt;/strong&gt;&lt;/a&gt; .La falta de oposición al nuevo Aviso de Privacidad\n    dentro de los 5 días hábiles siguientes a su recepción se entenderá como su aceptación a los términos que en él se\n    establecen.\n&lt;/p&gt;\n&lt;h5&gt;\n    &lt;strong&gt;Consentimiento:&lt;/strong&gt;\n&lt;/h5&gt;\n&lt;br\n&gt;\n&lt;p&gt;Al proporcionar cualquier tipo de información, incluyendo sus datos personales, usted expresamente:&lt;/p&gt;\n&lt;div style=&quot;margin-left: 30px; text-align: justify;&quot;&gt;\n    &lt;ul&gt;\n        &lt;li&gt; Acepta las condiciones contenidas en el presente Aviso de Privacidad;&lt;/li&gt;\n        &lt;li&gt; Está de acuerdo en que la información proporcionada por usted pueda almacenarse, usarse y, en general,\n            tratarse para los fines que se señalan, incluso para fines comerciales y de promoción;\n        &lt;/li&gt;\n        &lt;li&gt; Otorga a la CMIC autorización para obtener, compilar, almacenar, compartir,\n            comunicar, transmitir y usar tal información de cualquier manera o forma, de conformidad con las condiciones\n            establecidas en el presente y las leyes aplicables.\n        &lt;/li&gt;\n    &lt;/ul&gt;\n&lt;/div&gt;\n&lt;h5&gt;\n    &lt;strong&gt;Contacto:&lt;/strong&gt;\n&lt;/h5&gt;\n&lt;br\n&gt;\n&lt;p style=&quot;text-align: justify; margin-left: 100px;&quot;&gt;\n    Preguntas, comentarios, ejercicio de derechos al:&lt;br&gt;\n    Área de Protección de Datos Personales de la CMIC&lt;br&gt;\n    Correo electrónico:&lt;a href=&quot;mailto:avisodeprivacidad@cmic.org&quot;&gt; &lt;strong style=&quot;color:red;&quot;&gt;avisodeprivacidad@cmic.org&lt;/strong&gt;&lt;/a&gt;&lt;br&gt;\n    Domicilio: Periférico Sur # 4839, Colonia Parques del Pedregal, C.P. 14010,&lt;br&gt;\n    Delegación Tlalpan, México, Distrito Federal&lt;br&gt;\n    Atención: C.P. Liliana Guadalupe Alfaro Santos.\n&lt;/p&gt;', '1', '2019-09-02 00:00:00', '0', '1');
INSERT INTO `pages` (`id`, `title`, `slug`, `template`, `body`, `breadcrumb`, `created`, `home_page`, `active`) VALUES ('17', 'Pricing', 'pricing', 'package', '&lt;p&gt;&lt;/p&gt;', '1', '2017-02-15 15:38:11', '0', '1');
INSERT INTO `pages` (`id`, `title`, `slug`, `template`, `body`, `breadcrumb`, `created`, `home_page`, `active`) VALUES ('16', 'Browse Categories', 'browse-categories', 'browse-categories', '', '0', '2017-02-14 00:00:00', '0', '1');
INSERT INTO `pages` (`id`, `title`, `slug`, `template`, `body`, `breadcrumb`, `created`, `home_page`, `active`) VALUES ('18', 'Home', 'home', 'home', '  &lt;div id=&quot;banner&quot; style=&quot;background: url(/themes/default/images/banner-home-01.jpg)&quot;&gt;\n  \t&lt;div class=&quot;container&quot;&gt;\n  \t\t&lt;div class=&quot;sixteen columns&quot;&gt;\n\n  \t\t\t&lt;div class=&quot;search-container&quot;&gt;\n\n  \t\t\t\t&lt;!-- Form --&gt;\n  \t\t\t\t&lt;h2&gt;Find job&lt;/h2&gt;\n  \t\t\t\t&lt;input type=&quot;text&quot; class=&quot;ico-01&quot; placeholder=&quot;job title, keywords or company name&quot; value=&quot;&quot;/&gt;\n  \t\t\t\t&lt;input type=&quot;text&quot; class=&quot;ico-02&quot; placeholder=&quot;city, province or region&quot; value=&quot;&quot;/&gt;\n  \t\t\t\t&lt;button&gt;&lt;i class=&quot;fa fa-search&quot;&gt;&lt;/i&gt;&lt;/button&gt;\n\n  \t\t\t\t&lt;!-- Browse Jobs --&gt;\n  \t\t\t\t&lt;div class=&quot;browse-jobs&quot;&gt;\n  \t\t\t\t\tBrowse job offers by &lt;a href=&quot;browse-categories.html&quot;&gt; category&lt;/a&gt; or &lt;a href=&quot;index.html#&quot;&gt;location&lt;/a&gt;\n  \t\t\t\t&lt;/div&gt;\n\n  \t\t\t\t&lt;!-- Announce --&gt;\n  \t\t\t\t&lt;div class=&quot;announce&quot;&gt;\n  \t\t\t\t\tWe&#039;ve over &lt;strong&gt;15 000&lt;/strong&gt; job offers for you!\n  \t\t\t\t&lt;/div&gt;\n\n  \t\t\t&lt;/div&gt;\n\n  \t\t&lt;/div&gt;\n  \t&lt;/div&gt;\n  &lt;/div&gt;', '1', '2018-03-15 00:00:00', '1', '1');
INSERT INTO `pages` (`id`, `title`, `slug`, `template`, `body`, `breadcrumb`, `created`, `home_page`, `active`) VALUES ('20', 'Browse Jobs', 'browse-jobs', 'browse-jobs', '&lt;p&gt;hola&lt;/p&gt;', '1', '2018-04-02 00:00:00', '0', '1');
INSERT INTO `pages` (`id`, `title`, `slug`, `template`, `body`, `breadcrumb`, `created`, `home_page`, `active`) VALUES ('21', 'Browse Resumes', 'browse-resumes', 'browse-resumes', '', '1', '2018-04-09 00:00:00', '0', '1');


-- --------------------------------------------------
# -- Table structure for table `resumes`
-- --------------------------------------------------
DROP TABLE IF EXISTS `resumes`;
CREATE TABLE `resumes` (
  `uid` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `objective` text NOT NULL,
  `hourly_rate` varchar(10) NOT NULL,
  `education` text NOT NULL,
  `skills` varchar(255) NOT NULL,
  `experience` text NOT NULL,
  `portfolio` text NOT NULL,
  `present_address` text NOT NULL,
  `permanent_address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `website` varchar(255) NOT NULL,
  `references` text NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `gplus` varchar(255) NOT NULL,
  `featured` enum('notfeatured','featured') NOT NULL DEFAULT 'notfeatured',
  `banned` int(1) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------
# Dumping data for table `resumes`
-- --------------------------------------------------



-- --------------------------------------------------
# -- Table structure for table `settings`
-- --------------------------------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings` (
  `site_name` varchar(100) NOT NULL,
  `company` varchar(100) NOT NULL,
  `site_url` varchar(150) NOT NULL,
  `site_dir` varchar(60) DEFAULT NULL,
  `site_email` varchar(50) NOT NULL,
  `seo` tinyint(1) NOT NULL DEFAULT '0',
  `perpage` tinyint(4) NOT NULL DEFAULT '10',
  `backup` varchar(25) NOT NULL,
  `thumb_w` varchar(3) NOT NULL,
  `thumb_h` varchar(3) NOT NULL,
  `img_w` varchar(5) NOT NULL,
  `img_h` varchar(5) NOT NULL,
  `show_home` tinyint(1) NOT NULL DEFAULT '1',
  `show_slider` tinyint(1) NOT NULL DEFAULT '1',
  `file_dir` varchar(200) DEFAULT NULL,
  `short_date` varchar(50) NOT NULL,
  `long_date` varchar(50) NOT NULL,
  `time_format` varchar(20) DEFAULT NULL,
  `dtz` varchar(120) DEFAULT NULL,
  `locale` varchar(120) DEFAULT NULL,
  `latest_jobs` tinyint(1) NOT NULL DEFAULT '6',
  `featured_jobs` tinyint(1) NOT NULL DEFAULT '5',
  `featured_resumes` tinyint(1) NOT NULL DEFAULT '5',
  `featured` tinyint(2) NOT NULL DEFAULT '10',
  `popular` tinyint(2) NOT NULL DEFAULT '6',
  `hlayout` tinyint(4) NOT NULL DEFAULT '0',
  `homelist` tinyint(1) NOT NULL DEFAULT '4',
  `free_allowed` tinyint(1) NOT NULL DEFAULT '0',
  `logo` varchar(100) DEFAULT NULL,
  `theme` varchar(30) DEFAULT NULL,
  `tax` tinyint(1) NOT NULL DEFAULT '0',
  `psize` varchar(10) DEFAULT NULL,
  `inv_note` text,
  `inv_info` text,
  `lang` varchar(10) DEFAULT NULL,
  `currency` varchar(8) DEFAULT NULL,
  `cur_symbol` varchar(6) DEFAULT NULL,
  `reg_verify` tinyint(1) NOT NULL DEFAULT '1',
  `auto_verify` tinyint(1) NOT NULL DEFAULT '1',
  `reg_allowed` tinyint(1) NOT NULL DEFAULT '1',
  `user_limit` int(6) NOT NULL DEFAULT '0',
  `notify_admin` tinyint(1) NOT NULL DEFAULT '0',
  `offline` tinyint(1) NOT NULL DEFAULT '0',
  `offline_msg` text,
  `offline_d` date DEFAULT NULL,
  `offline_t` time DEFAULT NULL,
  `facebook_url` varchar(100) NOT NULL,
  `twitter_url` varchar(100) NOT NULL,
  `google_plus_url` varchar(100) NOT NULL,
  `linkedin_url` varchar(100) NOT NULL,
  `copyright` varchar(200) NOT NULL,
  `metakeys` text,
  `metadesc` text,
  `analytics` text,
  `mailer` enum('PHP','SMTP','SMAIL') DEFAULT NULL,
  `smtp_host` varchar(150) DEFAULT NULL,
  `smtp_user` varchar(50) DEFAULT NULL,
  `smtp_pass` varchar(50) DEFAULT NULL,
  `smtp_port` smallint(3) DEFAULT NULL,
  `is_ssl` tinyint(1) NOT NULL DEFAULT '0',
  `sendmail` varchar(100) DEFAULT NULL,
  `version` varchar(10) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `settings`
-- --------------------------------------------------

INSERT INTO `settings` (`site_name`, `company`, `site_url`, `site_dir`, `site_email`, `seo`, `perpage`, `backup`, `thumb_w`, `thumb_h`, `img_w`, `img_h`, `show_home`, `show_slider`, `file_dir`, `short_date`, `long_date`, `time_format`, `dtz`, `locale`, `latest_jobs`, `featured_jobs`, `featured_resumes`, `featured`, `popular`, `hlayout`, `homelist`, `free_allowed`, `logo`, `theme`, `tax`, `psize`, `inv_note`, `inv_info`, `lang`, `currency`, `cur_symbol`, `reg_verify`, `auto_verify`, `reg_allowed`, `user_limit`, `notify_admin`, `offline`, `offline_msg`, `offline_d`, `offline_t`, `facebook_url`, `twitter_url`, `google_plus_url`, `linkedin_url`, `copyright`, `metakeys`, `metadesc`, `analytics`, `mailer`, `smtp_host`, `smtp_user`, `smtp_pass`, `smtp_port`, `is_ssl`, `sendmail`, `version`, `id`) VALUES ('Bolsa de Trabajo CMIC', 'CMIC Sonora', 'trabajos.cmicsonora.org', '', 'socios@cmicsonora.org', '0', '10', '03-Oct-2019_12-41-23.sql', '300', '350', '0', '0', '1', '1', 'jb', '%m-%d-%Y', '%B %d, %Y %I:%M %p', '%I:%M %p', 'America/Hermosillo', 'es_utf8,Spanish (International),es_ES.UTF-8,Spanish_Spain.1252,WINDOWS-1252', '7', '5', '5', '0', '0', '1', '0', '0', 'LOGO.png', 'default', '1', 'LETTER', '&lt;p&gt;&lt;/p&gt;&lt;p&gt;TERMS &amp;amp; CONDITIONS&lt;br&gt;1. Interest may be levied on overdue accounts. &lt;br&gt;2. Goods sold are not returnable or refundable\n&lt;/p&gt;', '&lt;p&gt;&lt;b&gt;CMIC DELEGACIÓN SONORA | BOLSA DE TRABAJO&lt;/b&gt;&lt;br&gt;Periférico Poniente 104, Raquet Club 83200 Hillo, Son. &lt;br&gt;Tel. 662 218 9457 &amp;amp; 662 2189 562 , Email : servicioalsocio@cmicsonora.org &lt;/p&gt;', 'en', 'MXN', '$', '1', '1', '1', '0', '1', '0', '&lt;p&gt;&lt;/p&gt;', '0000-00-00', '00:00:00', 'https://www.facebook.com/cmicdelegacionsonora/', 'https://twitter.com/cmicsonora', 'https://www.youtube.com/user/CMICTVMexico', 'https://www.instagram.com/cmicson', 'Powered by quantumbit.mx', 'metakeys, separated,by coma', 'Your website description goes here', '', 'SMTP', 'in-v3.mailjet.com', '637a695e35c650c8273b854039b02c87', 'ad61928ea2a3431bae6a83372bd700fb', '587', '0', '', '1.00', '1');


-- --------------------------------------------------
# -- Table structure for table `slider`
-- --------------------------------------------------
DROP TABLE IF EXISTS `slider`;
CREATE TABLE `slider` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `thumb` varchar(60) DEFAULT NULL,
  `caption` varchar(100) DEFAULT NULL,
  `body` text,
  `alignment` varchar(10) NOT NULL,
  `button_text` varchar(20) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `page_id` smallint(6) NOT NULL DEFAULT '0',
  `urltype` enum('int','ext','nourl') DEFAULT 'nourl',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sorting` int(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `slider`
-- --------------------------------------------------

INSERT INTO `slider` (`id`, `thumb`, `caption`, `body`, `alignment`, `button_text`, `url`, `page_id`, `urltype`, `created`, `sorting`) VALUES ('2', 'IMG_AFE6BB-05620F-4A63B9-5A74AE-D517FD-030C4D.jpg', 'Bolsa de trabajo CMIC Sonora', 'Somos la bolsa de trabajo especializada en el nicho de la construcción. Un beneficio más para nuestros socios.', 'center', 'PUBLICAR OFERTA', 'add-job', '2', 'int', '0000-00-00 00:00:00', '2');
INSERT INTO `slider` (`id`, `thumb`, `caption`, `body`, `alignment`, `button_text`, `url`, `page_id`, `urltype`, `created`, `sorting`) VALUES ('3', 'IMG_D4EDD0-289335-A467CE-99C6C7-704F7E-BADF14.jpg', '¿Estás en busca de un nuevo empleo?', 'Regístrate en nuestra bolsa de trabajo y postula a la oferta de empleo acorde a tu curriculum.', 'left', 'REGISTRARSE', 'register', '16', 'int', '0000-00-00 00:00:00', '1');
INSERT INTO `slider` (`id`, `thumb`, `caption`, `body`, `alignment`, `button_text`, `url`, `page_id`, `urltype`, `created`, `sorting`) VALUES ('6', 'IMG_C731D8-18BED2-E889EE-3F5231-F2FFD4-F4EDEF.jpg', 'Descubre Nuevas Oportunidades', 'Encuentra ofertas de trabajo por categoría o ubicación. ¡Tenemos más de 15 000 ofertas de trabajo para ti!', 'right', 'VER OFERTAS', 'browse-jobs', '4', 'int', '0000-00-00 00:00:00', '3');


-- --------------------------------------------------
# -- Table structure for table `slider_config`
-- --------------------------------------------------
DROP TABLE IF EXISTS `slider_config`;
CREATE TABLE `slider_config` (
  `showArrows` tinyint(1) NOT NULL DEFAULT '1',
  `showCaptions` tinyint(1) NOT NULL DEFAULT '1',
  `showDots` tinyint(1) NOT NULL DEFAULT '1',
  `showFilmstrip` tinyint(1) NOT NULL DEFAULT '0',
  `showPause` tinyint(1) NOT NULL DEFAULT '1',
  `showTimer` tinyint(1) NOT NULL DEFAULT '1',
  `simultaneousCaptions` tinyint(1) NOT NULL DEFAULT '0',
  `slideImageScaleMode` varchar(20) NOT NULL DEFAULT 'fill',
  `slideReverse` tinyint(1) NOT NULL DEFAULT '0',
  `slideShuffle` tinyint(1) NOT NULL DEFAULT '0',
  `slideTransition` varchar(20) NOT NULL DEFAULT 'slide',
  `slideTransitionDelay` smallint(6) NOT NULL DEFAULT '5000',
  `slideTransitionDirection` varchar(10) NOT NULL DEFAULT 'left',
  `slideTransitionEasing` varchar(20) NOT NULL DEFAULT 'swing',
  `slideTransitionSpeed` smallint(6) NOT NULL DEFAULT '1000',
  `sliderAutoPlay` tinyint(1) NOT NULL DEFAULT '1',
  `sliderHeight` smallint(6) NOT NULL DEFAULT '500',
  `sliderHeightAdaptable` tinyint(1) NOT NULL DEFAULT '1',
  `waitForLoad` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `slider_config`
-- --------------------------------------------------

INSERT INTO `slider_config` (`showArrows`, `showCaptions`, `showDots`, `showFilmstrip`, `showPause`, `showTimer`, `simultaneousCaptions`, `slideImageScaleMode`, `slideReverse`, `slideShuffle`, `slideTransition`, `slideTransitionDelay`, `slideTransitionDirection`, `slideTransitionEasing`, `slideTransitionSpeed`, `sliderAutoPlay`, `sliderHeight`, `sliderHeightAdaptable`, `waitForLoad`) VALUES ('1', '1', '1', '0', '1', '1', '0', 'fill', '0', '0', 'slide', '8000', 'left', 'swing', '1000', '1', '500', '1', '1');


-- --------------------------------------------------
# -- Table structure for table `subscriptions`
-- --------------------------------------------------
DROP TABLE IF EXISTS `subscriptions`;
CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `pid` int(11) NOT NULL,
  `txn_id` varchar(50) NOT NULL,
  `limit` int(11) NOT NULL,
  `usage` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=232 DEFAULT CHARSET=latin1;

-- --------------------------------------------------
# Dumping data for table `subscriptions`
-- --------------------------------------------------

INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('36', '4', '11', 'MAN_1568926618', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('37', '5', '11', '\tMAN_1568926619\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('38', '6', '11', '\tMAN_1568926620\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('39', '7', '11', '\tMAN_1568926621\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('40', '8', '11', '\tMAN_1568926622\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('41', '9', '11', '\tMAN_1568926623\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('42', '10', '11', '\tMAN_1568926624\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('43', '11', '11', '\tMAN_1568926625\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('44', '12', '11', '\tMAN_1568926626\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('45', '13', '11', '\tMAN_1568926627\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('46', '14', '11', '\tMAN_1568926628\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('47', '15', '11', '\tMAN_1568926629\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('48', '16', '11', '\tMAN_1568926630\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('49', '17', '11', '\tMAN_1568926631\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('50', '18', '11', '\tMAN_1568926632\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('51', '19', '11', '\tMAN_1568926633\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('52', '20', '11', '\tMAN_1568926634\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('53', '21', '11', '\tMAN_1568926635\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('54', '22', '11', '\tMAN_1568926636\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('55', '23', '11', '\tMAN_1568926637\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('56', '24', '11', '\tMAN_1568926638\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('57', '25', '11', '\tMAN_1568926639\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('58', '26', '11', '\tMAN_1568926640\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('59', '27', '11', '\tMAN_1568926641\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('60', '28', '11', '\tMAN_1568926642\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('61', '29', '11', '\tMAN_1568926643\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('62', '30', '11', '\tMAN_1568926644\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('63', '31', '11', '\tMAN_1568926645\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('64', '32', '11', '\tMAN_1568926646\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('65', '33', '11', '\tMAN_1568926647\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('66', '34', '11', '\tMAN_1568926648\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('67', '35', '11', '\tMAN_1568926649\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('68', '36', '11', '\tMAN_1568926650\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('69', '37', '11', '\tMAN_1568926651\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('70', '38', '11', '\tMAN_1568926652\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('71', '39', '11', '\tMAN_1568926653\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('72', '40', '11', '\tMAN_1568926654\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('73', '41', '11', '\tMAN_1568926655\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('74', '42', '11', '\tMAN_1568926656\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('75', '43', '11', '\tMAN_1568926657\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('76', '44', '11', '\tMAN_1568926658\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('77', '45', '11', '\tMAN_1568926659\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('78', '46', '11', '\tMAN_1568926660\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('79', '47', '11', '\tMAN_1568926661\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('80', '48', '11', '\tMAN_1568926662\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('81', '49', '11', '\tMAN_1568926663\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('82', '50', '11', '\tMAN_1568926664\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('83', '51', '11', '\tMAN_1568926665\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('84', '52', '11', '\tMAN_1568926666\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('85', '53', '11', '\tMAN_1568926667\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('86', '54', '11', '\tMAN_1568926668\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('87', '55', '11', '\tMAN_1568926669\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('88', '56', '11', '\tMAN_1568926670\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('89', '57', '11', '\tMAN_1568926671\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('90', '58', '11', '\tMAN_1568926672\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('91', '59', '11', '\tMAN_1568926673\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('92', '60', '11', '\tMAN_1568926674\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('93', '61', '11', '\tMAN_1568926675\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('94', '62', '11', '\tMAN_1568926676\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('95', '63', '11', '\tMAN_1568926677\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('96', '64', '11', '\tMAN_1568926678\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('97', '65', '11', '\tMAN_1568926679\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('98', '66', '11', '\tMAN_1568926680\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('99', '67', '11', '\tMAN_1568926681\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('100', '68', '11', '\tMAN_1568926682\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('101', '69', '11', '\tMAN_1568926683\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('102', '70', '11', '\tMAN_1568926684\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('103', '71', '11', '\tMAN_1568926685\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('104', '72', '11', '\tMAN_1568926686\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('105', '73', '11', '\tMAN_1568926687\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('106', '74', '11', '\tMAN_1568926688\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('107', '75', '11', '\tMAN_1568926689\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('108', '76', '11', '\tMAN_1568926690\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('109', '77', '11', '\tMAN_1568926691\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('110', '78', '11', '\tMAN_1568926692\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('111', '79', '11', '\tMAN_1568926693\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('112', '80', '11', '\tMAN_1568926694\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('113', '81', '11', '\tMAN_1568926695\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('114', '82', '11', '\tMAN_1568926696\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('115', '83', '11', '\tMAN_1568926697\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('116', '84', '11', '\tMAN_1568926698\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('117', '85', '11', '\tMAN_1568926699\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('118', '86', '11', '\tMAN_1568926700\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('119', '87', '11', '\tMAN_1568926701\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('120', '88', '11', '\tMAN_1568926702\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('121', '89', '11', '\tMAN_1568926703\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('122', '90', '11', '\tMAN_1568926704\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('123', '91', '11', '\tMAN_1568926705\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('124', '92', '11', '\tMAN_1568926706\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('125', '93', '11', '\tMAN_1568926707\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('126', '94', '11', '\tMAN_1568926708\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('127', '95', '11', '\tMAN_1568926709\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('128', '96', '11', '\tMAN_1568926710\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('129', '97', '11', '\tMAN_1568926711\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('130', '98', '11', '\tMAN_1568926712\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('131', '99', '11', '\tMAN_1568926713\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('132', '100', '11', '\tMAN_1568926714\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('133', '101', '11', '\tMAN_1568926715\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('134', '102', '11', '\tMAN_1568926716\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('135', '103', '11', '\tMAN_1568926717\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('136', '104', '11', '\tMAN_1568926718\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('137', '105', '11', '\tMAN_1568926719\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('138', '106', '11', '\tMAN_1568926720\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('139', '107', '11', '\tMAN_1568926721\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('140', '108', '11', '\tMAN_1568926722\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('141', '109', '11', '\tMAN_1568926723\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('142', '110', '11', '\tMAN_1568926724\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('143', '111', '11', '\tMAN_1568926725\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('144', '112', '11', '\tMAN_1568926726\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('145', '113', '11', '\tMAN_1568926727\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('146', '114', '11', '\tMAN_1568926728\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('147', '115', '11', '\tMAN_1568926729\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('148', '116', '11', '\tMAN_1568926730\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('149', '117', '11', '\tMAN_1568926731\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('150', '118', '11', '\tMAN_1568926732\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('151', '119', '11', '\tMAN_1568926733\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('152', '120', '11', '\tMAN_1568926734\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('153', '121', '11', '\tMAN_1568926735\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('154', '122', '11', '\tMAN_1568926736\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('155', '123', '11', '\tMAN_1568926737\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('156', '124', '11', '\tMAN_1568926738\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('157', '125', '11', '\tMAN_1568926739\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('158', '126', '11', '\tMAN_1568926740\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('159', '127', '11', '\tMAN_1568926741\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('160', '128', '11', '\tMAN_1568926742\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('161', '129', '11', '\tMAN_1568926743\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('162', '130', '11', '\tMAN_1568926744\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('163', '131', '11', '\tMAN_1568926745\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('164', '132', '11', '\tMAN_1568926746\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('165', '133', '11', '\tMAN_1568926747\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('166', '134', '11', '\tMAN_1568926748\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('167', '135', '11', '\tMAN_1568926749\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('168', '136', '11', '\tMAN_1568926750\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('169', '137', '11', '\tMAN_1568926751\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('170', '138', '11', '\tMAN_1568926752\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('171', '139', '11', '\tMAN_1568926753\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('172', '140', '11', '\tMAN_1568926754\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('173', '141', '11', '\tMAN_1568926755\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('174', '142', '11', '\tMAN_1568926756\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('175', '143', '11', '\tMAN_1568926757\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('176', '144', '11', '\tMAN_1568926758\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('177', '145', '11', '\tMAN_1568926759\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('178', '146', '11', '\tMAN_1568926760\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('179', '147', '11', '\tMAN_1568926761\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('180', '148', '11', '\tMAN_1568926762\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('181', '149', '11', '\tMAN_1568926763\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('182', '150', '11', '\tMAN_1568926764\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('183', '151', '11', '\tMAN_1568926765\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('184', '152', '11', '\tMAN_1568926766\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('185', '153', '11', '\tMAN_1568926767\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('186', '154', '11', '\tMAN_1568926768\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('187', '155', '11', '\tMAN_1568926769\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('188', '156', '11', '\tMAN_1568926770\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('189', '157', '11', '\tMAN_1568926771\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('190', '158', '11', '\tMAN_1568926772\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('191', '159', '11', '\tMAN_1568926773\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('192', '160', '11', '\tMAN_1568926774\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('193', '161', '11', '\tMAN_1568926775\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('194', '162', '11', '\tMAN_1568926776\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('195', '163', '11', '\tMAN_1568926777\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('196', '164', '11', '\tMAN_1568926778\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('197', '165', '11', '\tMAN_1568926779\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('198', '166', '11', '\tMAN_1568926780\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('199', '167', '11', '\tMAN_1568926781\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('200', '168', '11', '\tMAN_1568926782\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('201', '169', '11', '\tMAN_1568926783\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('202', '170', '11', '\tMAN_1568926784\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('203', '171', '11', '\tMAN_1568926785\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('204', '172', '11', '\tMAN_1568926786\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('205', '173', '11', '\tMAN_1568926787\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('206', '174', '11', '\tMAN_1568926788\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('207', '175', '11', '\tMAN_1568926789\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('208', '176', '11', '\tMAN_1568926790\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('209', '177', '11', '\tMAN_1568926791\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('210', '178', '11', '\tMAN_1568926792\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('211', '179', '11', '\tMAN_1568926793\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('212', '180', '11', '\tMAN_1568926794\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('213', '181', '11', '\tMAN_1568926795\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('214', '182', '11', '\tMAN_1568926796\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('215', '183', '11', '\tMAN_1568926797\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('216', '184', '11', '\tMAN_1568926798\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('217', '185', '11', '\tMAN_1568926799\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('218', '186', '11', '\tMAN_1568926800\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('219', '187', '11', '\tMAN_1568926801\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('220', '188', '11', '\tMAN_1568926802\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('221', '189', '11', '\tMAN_1568926803\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('222', '190', '11', '\tMAN_1568926804\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('223', '191', '11', '\tMAN_1568926805\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('224', '192', '11', '\tMAN_1568926806\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('225', '193', '11', '\tMAN_1568926807\t ', '10000', '0', '2019-09-19 00:00:00', '2020-09-18 00:00:00', '2019-09-19 15:56:58');
INSERT INTO `subscriptions` (`id`, `uid`, `pid`, `txn_id`, `limit`, `usage`, `start_date`, `end_date`, `created`) VALUES ('231', '225', '11', 'MAN_1569961180', '10000', '1', '2019-10-01 00:00:00', '2020-09-30 00:00:00', '2019-10-01 15:19:40');


-- --------------------------------------------------
# -- Table structure for table `testimonials`
-- --------------------------------------------------
DROP TABLE IF EXISTS `testimonials`;
CREATE TABLE `testimonials` (
  `id` tinyint(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) DEFAULT NULL,
  `company` varchar(150) NOT NULL,
  `content` text,
  `position` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `testimonials`
-- --------------------------------------------------

INSERT INTO `testimonials` (`id`, `name`, `company`, `content`, `position`) VALUES ('1', 'Jose Perez Ruiz', '', 'Está a sido una de las mejores experiencias que he tenido al buscar empleo. \nEspero que, como yo, tú también encuentres el empleo que buscas.\n', '1');
INSERT INTO `testimonials` (`id`, `name`, `company`, `content`, `position`) VALUES ('2', 'María Guadalupe Martínez Sanchez', '', 'Me registre, llene los datos de mi currículo, me postule a una oferta de empleo y ahora ya tengo trabajo. ¿Qué esperas tú?', '2');
INSERT INTO `testimonials` (`id`, `name`, `company`, `content`, `position`) VALUES ('4', 'Javier López Hernandez', '', 'Cuando me quede sin empleo no sabía qué hacer, ni dónde empezar a buscar empleo. Pero navegando un día en internet encontré esta página y desde ese momento mi problema dejo de existir. Hoy trabajo en una empresa de clase mundial y estoy muy feliz. \n', '3');


-- --------------------------------------------------
# -- Table structure for table `transactions`
-- --------------------------------------------------
DROP TABLE IF EXISTS `transactions`;
CREATE TABLE `transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `txn_id` varchar(50) NOT NULL,
  `pid` int(11) DEFAULT NULL,
  `uid` int(11) unsigned NOT NULL DEFAULT '0',
  `downloads` int(11) unsigned DEFAULT '0',
  `file_date` int(11) unsigned DEFAULT NULL,
  `ip` text,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `payer_email` varchar(75) DEFAULT NULL,
  `payer_status` varchar(50) DEFAULT NULL,
  `item_qty` int(11) NOT NULL DEFAULT '1',
  `price` decimal(9,2) NOT NULL DEFAULT '0.00',
  `mc_fee` decimal(9,2) NOT NULL DEFAULT '0.00',
  `coupon` decimal(9,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(9,2) NOT NULL DEFAULT '0.00',
  `currency` char(3) DEFAULT NULL,
  `pp` varchar(40) DEFAULT NULL,
  `memo` text,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=208 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `transactions`
-- --------------------------------------------------

INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('19', '\tMAN_1568926625\t ', '11', '11', '0', '1568926625', '', '2019-09-19 15:56:58', '\tgrupo8a@prodigy.net.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('12', 'MAN_1568926618', '11', '4', '0', '1568926618', '', '2019-09-19 13:56:58', 'administracion@pcvirgo.com', 'verified', '1', '0.00', '0.00', '0.00', '0.00', 'MXN', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('13', '\tMAN_1568926619\t ', '11', '5', '0', '1568926619', '', '2019-09-19 15:56:58', '\tmmdessens@yahoo.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('14', '\tMAN_1568926620\t ', '11', '6', '0', '1568926620', '', '2019-09-19 15:56:58', '\tconsein@conseinedificaciones.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('15', '\tMAN_1568926621\t ', '11', '7', '0', '1568926621', '', '2019-09-19 15:56:58', '\tvvalencia@canoras.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('16', '\tMAN_1568926622\t ', '11', '8', '0', '1568926622', '', '2019-09-19 15:56:58', '\tinfo@gluyasconstrucciones.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('17', '\tMAN_1568926623\t ', '11', '9', '0', '1568926623', '', '2019-09-19 15:56:58', '\tventas@construplan.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('18', '\tMAN_1568926624\t ', '11', '10', '0', '1568926624', '', '2019-09-19 15:56:58', '\tazteca860203@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('20', '\tMAN_1568926626\t ', '11', '12', '0', '1568926626', '', '2019-09-19 15:56:58', '\tacabrera@luconsa.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('21', '\tMAN_1568926627\t ', '11', '13', '0', '1568926627', '', '2019-09-19 15:56:58', '\tROSORIO@OPOSON.COM.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('22', '\tMAN_1568926628\t ', '11', '14', '0', '1568926628', '', '2019-09-19 15:56:58', '\tgamezosio@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('23', '\tMAN_1568926629\t ', '11', '15', '0', '1568926629', '', '2019-09-19 15:56:58', '\tale.majerus@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('24', '\tMAN_1568926630\t ', '11', '16', '0', '1568926630', '', '2019-09-19 15:56:58', '\tCONSTRUDISENOSDESONORA@PRODIGY.NET.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('25', '\tMAN_1568926631\t ', '11', '17', '0', '1568926631', '', '2019-09-19 15:56:58', '\tACAMOU@CWMETAL.COM.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('26', '\tMAN_1568926632\t ', '11', '18', '0', '1568926632', '', '2019-09-19 15:56:58', '\taharispuru@icsa.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('27', '\tMAN_1568926633\t ', '11', '19', '0', '1568926633', '', '2019-09-19 15:56:58', '\tjharispuru@icsa.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('28', '\tMAN_1568926634\t ', '11', '20', '0', '1568926634', '', '2019-09-19 15:56:58', '\tcfd@larivdesarrollos.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('29', '\tMAN_1568926635\t ', '11', '21', '0', '1568926635', '', '2019-09-19 15:56:58', '\tcontacto@puente391.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('30', '\tMAN_1568926636\t ', '11', '22', '0', '1568926636', '', '2019-09-19 15:56:58', '\tpatyl@spazicorp.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('31', '\tMAN_1568926637\t ', '11', '23', '0', '1568926637', '', '2019-09-19 15:56:58', '\tmitzi.ibarra@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('32', '\tMAN_1568926638\t ', '11', '24', '0', '1568926638', '', '2019-09-19 15:56:58', '\tOBRAS@INMEX.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('33', '\tMAN_1568926639\t ', '11', '25', '0', '1568926639', '', '2019-09-19 15:56:58', '\tkarina@construpima.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('34', '\tMAN_1568926640\t ', '11', '26', '0', '1568926640', '', '2019-09-19 15:56:58', '\tedificacionesboza@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('35', '\tMAN_1568926641\t ', '11', '27', '0', '1568926641', '', '2019-09-19 15:56:58', '\tcvdario@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('36', '\tMAN_1568926642\t ', '11', '28', '0', '1568926642', '', '2019-09-19 15:56:58', '\tlcproyectos@yahoo.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('37', '\tMAN_1568926643\t ', '11', '29', '0', '1568926643', '', '2019-09-19 15:56:58', '\tcontacto@nennca.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('38', '\tMAN_1568926644\t ', '11', '30', '0', '1568926644', '', '2019-09-19 15:56:58', '\tcontacto@viasmvp.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('39', '\tMAN_1568926645\t ', '11', '31', '0', '1568926645', '', '2019-09-19 15:56:58', '\tinfo@cgarale.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('40', '\tMAN_1568926646\t ', '11', '32', '0', '1568926646', '', '2019-09-19 15:56:58', '\tacclaboratorio@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('41', '\tMAN_1568926647\t ', '11', '33', '0', '1568926647', '', '2019-09-19 15:56:58', '\tlauracmicnavojoa@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('42', '\tMAN_1568926648\t ', '11', '34', '0', '1568926648', '', '2019-09-19 15:56:58', '\tminargc@prodigy.net.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('43', '\tMAN_1568926649\t ', '11', '35', '0', '1568926649', '', '2019-09-19 15:56:58', '\tdso.construcciones@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('44', '\tMAN_1568926650\t ', '11', '36', '0', '1568926650', '', '2019-09-19 15:56:58', '\tgrupototalinbas@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('45', '\tMAN_1568926651\t ', '11', '37', '0', '1568926651', '', '2019-09-19 15:56:58', '\tinecaconstrucciones@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('46', '\tMAN_1568926652\t ', '11', '38', '0', '1568926652', '', '2019-09-19 15:56:58', '\tienriquez@manser.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('47', '\tMAN_1568926653\t ', '11', '39', '0', '1568926653', '', '2019-09-19 15:56:58', '\tcontacto@maqgala.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('48', '\tMAN_1568926654\t ', '11', '40', '0', '1568926654', '', '2019-09-19 15:56:58', '\tALEXIBARRA@PRODIGY.NET.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('49', '\tMAN_1568926655\t ', '11', '41', '0', '1568926655', '', '2019-09-19 15:56:58', '\tbrauliofontane@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('50', '\tMAN_1568926656\t ', '11', '42', '0', '1568926656', '', '2019-09-19 15:56:58', '\tINGELECTRICADELCOBRE@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('51', '\tMAN_1568926657\t ', '11', '43', '0', '1568926657', '', '2019-09-19 15:56:58', '\tCONSTRUCCIONESR.R@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('52', '\tMAN_1568926658\t ', '11', '44', '0', '1568926658', '', '2019-09-19 15:56:58', '\tcontacto@constructoravelis.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('53', '\tMAN_1568926659\t ', '11', '45', '0', '1568926659', '', '2019-09-19 15:56:58', '\tfacturacion@edinosa.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('54', '\tMAN_1568926660\t ', '11', '46', '0', '1568926660', '', '2019-09-19 15:56:58', '\tdapci.construccion@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('55', '\tMAN_1568926661\t ', '11', '47', '0', '1568926661', '', '2019-09-19 15:56:58', '\tbenitodosamantes@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('56', '\tMAN_1568926662\t ', '11', '48', '0', '1568926662', '', '2019-09-19 15:56:58', '\tgrupomesis@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('57', '\tMAN_1568926663\t ', '11', '49', '0', '1568926663', '', '2019-09-19 15:56:58', '\tgys_constructores@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('58', '\tMAN_1568926664\t ', '11', '50', '0', '1568926664', '', '2019-09-19 15:56:58', '\tanaelcamacho@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('59', '\tMAN_1568926665\t ', '11', '51', '0', '1568926665', '', '2019-09-19 15:56:58', '\tjlpopocasotelo@yahoo.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('60', '\tMAN_1568926666\t ', '11', '52', '0', '1568926666', '', '2019-09-19 15:56:58', '\timeconstrucciones@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('61', '\tMAN_1568926667\t ', '11', '53', '0', '1568926667', '', '2019-09-19 15:56:58', '\tconstrucciones.felgar@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('62', '\tMAN_1568926668\t ', '11', '54', '0', '1568926668', '', '2019-09-19 15:56:58', '\tPROCONS.08@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('63', '\tMAN_1568926669\t ', '11', '55', '0', '1568926669', '', '2019-09-19 15:56:58', '\toestec@oestec.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('64', '\tMAN_1568926670\t ', '11', '56', '0', '1568926670', '', '2019-09-19 15:56:58', '\tHMARTINEZ@PIXO.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('65', '\tMAN_1568926671\t ', '11', '57', '0', '1568926671', '', '2019-09-19 15:56:58', '\tpycmagus@yahoo.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('66', '\tMAN_1568926672\t ', '11', '58', '0', '1568926672', '', '2019-09-19 15:56:58', '\tmaricela.ramirez@servechermosillo.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('67', '\tMAN_1568926673\t ', '11', '59', '0', '1568926673', '', '2019-09-19 15:56:58', '\trplatt@conde.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('68', '\tMAN_1568926674\t ', '11', '60', '0', '1568926674', '', '2019-09-19 15:56:58', '\tinfo@casnor.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('69', '\tMAN_1568926675\t ', '11', '61', '0', '1568926675', '', '2019-09-19 15:56:58', '\tcoanamark@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('70', '\tMAN_1568926676\t ', '11', '62', '0', '1568926676', '', '2019-09-19 15:56:58', '\tcoconosa@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('71', '\tMAN_1568926677\t ', '11', '63', '0', '1568926677', '', '2019-09-19 15:56:58', '\talejandropra@me.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('72', '\tMAN_1568926678\t ', '11', '64', '0', '1568926678', '', '2019-09-19 15:56:58', '\tyolanda@grupoimc.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('73', '\tMAN_1568926679\t ', '11', '65', '0', '1568926679', '', '2019-09-19 15:56:58', '\tadministracion@electroinsumos.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('74', '\tMAN_1568926680\t ', '11', '66', '0', '1568926680', '', '2019-09-19 15:56:58', '\tbig_samoano@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('75', '\tMAN_1568926681\t ', '11', '67', '0', '1568926681', '', '2019-09-19 15:56:58', '\tluiscano@certusgerencia.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('76', '\tMAN_1568926682\t ', '11', '68', '0', '1568926682', '', '2019-09-19 15:56:58', '\tADMIN@ESCOBO.COM.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('77', '\tMAN_1568926683\t ', '11', '69', '0', '1568926683', '', '2019-09-19 15:56:58', '\tverificacion2000@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('78', '\tMAN_1568926684\t ', '11', '70', '0', '1568926684', '', '2019-09-19 15:56:58', '\tFORTEIOP@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('79', '\tMAN_1568926685\t ', '11', '71', '0', '1568926685', '', '2019-09-19 15:56:58', '\tcontratistarodo@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('80', '\tMAN_1568926686\t ', '11', '72', '0', '1568926686', '', '2019-09-19 15:56:58', '\tcontacto@princeconstrucciones.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('81', '\tMAN_1568926687\t ', '11', '73', '0', '1568926687', '', '2019-09-19 15:56:58', '\tgmb-64@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('82', '\tMAN_1568926688\t ', '11', '74', '0', '1568926688', '', '2019-09-19 15:56:58', '\tfernando.avila@industrialavila.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('83', '\tMAN_1568926689\t ', '11', '75', '0', '1568926689', '', '2019-09-19 15:56:58', '\thugolfnavarro@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('84', '\tMAN_1568926690\t ', '11', '76', '0', '1568926690', '', '2019-09-19 15:56:58', '\tinfogerpro@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('85', '\tMAN_1568926691\t ', '11', '77', '0', '1568926691', '', '2019-09-19 15:56:58', '\tiblopconstrucciones@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('86', '\tMAN_1568926692\t ', '11', '78', '0', '1568926692', '', '2019-09-19 15:56:58', '\tinvino2014@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('87', '\tMAN_1568926693\t ', '11', '79', '0', '1568926693', '', '2019-09-19 15:56:58', '\tcontabilidad_icssa@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('88', '\tMAN_1568926694\t ', '11', '80', '0', '1568926694', '', '2019-09-19 15:56:58', '\tingenieria_paay@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('89', '\tMAN_1568926695\t ', '11', '81', '0', '1568926695', '', '2019-09-19 15:56:58', '\tandres_pacheco@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('90', '\tMAN_1568926696\t ', '11', '82', '0', '1568926696', '', '2019-09-19 15:56:58', '\tsitten_trax@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('91', '\tMAN_1568926697\t ', '11', '83', '0', '1568926697', '', '2019-09-19 15:56:58', '\tjorlo2001@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('92', '\tMAN_1568926698\t ', '11', '84', '0', '1568926698', '', '2019-09-19 15:56:58', '\tlespiproyectos@yahoo.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('93', '\tMAN_1568926699\t ', '11', '85', '0', '1568926699', '', '2019-09-19 15:56:58', '\tproteko@prodigy.net.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('94', '\tMAN_1568926700\t ', '11', '86', '0', '1568926700', '', '2019-09-19 15:56:58', '\toceanussyp@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('95', '\tMAN_1568926701\t ', '11', '87', '0', '1568926701', '', '2019-09-19 15:56:58', '\tscjconstruye@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('96', '\tMAN_1568926702\t ', '11', '88', '0', '1568926702', '', '2019-09-19 15:56:58', '\tfelipefimbresr@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('97', '\tMAN_1568926703\t ', '11', '89', '0', '1568926703', '', '2019-09-19 15:56:58', '\tOPOSURADO@YAHOO.COM.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('98', '\tMAN_1568926704\t ', '11', '90', '0', '1568926704', '', '2019-09-19 15:56:58', '\tadministracion@petrotransmodal.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('99', '\tMAN_1568926705\t ', '11', '91', '0', '1568926705', '', '2019-09-19 15:56:58', '\tcalliandraconstructora@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('100', '\tMAN_1568926706\t ', '11', '92', '0', '1568926706', '', '2019-09-19 15:56:58', '\ttocaing@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('101', '\tMAN_1568926707\t ', '11', '93', '0', '1568926707', '', '2019-09-19 15:56:58', '\tinformes@icsa.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('102', '\tMAN_1568926708\t ', '11', '94', '0', '1568926708', '', '2019-09-19 15:56:58', '\ting_bojorquez9@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('103', '\tMAN_1568926709\t ', '11', '95', '0', '1568926709', '', '2019-09-19 15:56:58', '\tadministracion@verticasc.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('104', '\tMAN_1568926710\t ', '11', '96', '0', '1568926710', '', '2019-09-19 15:56:58', '\tconstruccionesrenav@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('105', '\tMAN_1568926711\t ', '11', '97', '0', '1568926711', '', '2019-09-19 15:56:58', '\tacsaconstructores@outlook.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('106', '\tMAN_1568926712\t ', '11', '98', '0', '1568926712', '', '2019-09-19 15:56:58', '\tcyurgsa@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('107', '\tMAN_1568926713\t ', '11', '99', '0', '1568926713', '', '2019-09-19 15:56:58', '\tGSAMANIEGO@ALFALEANCONSTRUCTION.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('108', '\tMAN_1568926714\t ', '11', '100', '0', '1568926714', '', '2019-09-19 15:56:58', '\tecapusa.2016@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('109', '\tMAN_1568926715\t ', '11', '101', '0', '1568926715', '', '2019-09-19 15:56:58', '\tpenelope@arqcoenlinea.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('110', '\tMAN_1568926716\t ', '11', '102', '0', '1568926716', '', '2019-09-19 15:56:58', '\thector.cuellar@panelrex.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('111', '\tMAN_1568926717\t ', '11', '103', '0', '1568926717', '', '2019-09-19 15:56:58', '\tgrupomercla@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('112', '\tMAN_1568926718\t ', '11', '104', '0', '1568926718', '', '2019-09-19 15:56:58', '\tadministracion@penaporchas.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('113', '\tMAN_1568926719\t ', '11', '105', '0', '1568926719', '', '2019-09-19 15:56:58', '\thugollano.2393@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('114', '\tMAN_1568926720\t ', '11', '106', '0', '1568926720', '', '2019-09-19 15:56:58', '\tmarthagd@derex.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('115', '\tMAN_1568926721\t ', '11', '107', '0', '1568926721', '', '2019-09-19 15:56:58', '\td.rubio@ryrce.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('116', '\tMAN_1568926722\t ', '11', '108', '0', '1568926722', '', '2019-09-19 15:56:58', '\tjr2group@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('117', '\tMAN_1568926723\t ', '11', '109', '0', '1568926723', '', '2019-09-19 15:56:58', '\tjr_maldonadog@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('118', '\tMAN_1568926724\t ', '11', '110', '0', '1568926724', '', '2019-09-19 15:56:58', '\tinfo@maquinaria.biz\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('119', '\tMAN_1568926725\t ', '11', '111', '0', '1568926725', '', '2019-09-19 15:56:58', '\tjopada.edificaciones@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('120', '\tMAN_1568926726\t ', '11', '112', '0', '1568926726', '', '2019-09-19 15:56:58', '\tISLAS_BELTRANJUAN@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('121', '\tMAN_1568926727\t ', '11', '113', '0', '1568926727', '', '2019-09-19 15:56:58', '\tgamezosio@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('122', '\tMAN_1568926728\t ', '11', '114', '0', '1568926728', '', '2019-09-19 15:56:58', '\td.rubio@ryrce.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('123', '\tMAN_1568926729\t ', '11', '115', '0', '1568926729', '', '2019-09-19 15:56:58', '\tsasaem.construccion@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('124', '\tMAN_1568926730\t ', '11', '116', '0', '1568926730', '', '2019-09-19 15:56:58', '\tcarlos.lopez@equiplan.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('125', '\tMAN_1568926731\t ', '11', '117', '0', '1568926731', '', '2019-09-19 15:56:58', '\tsithermosillo@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('126', '\tMAN_1568926732\t ', '11', '118', '0', '1568926732', '', '2019-09-19 15:56:58', '\tmeridamillan@prodigy.net.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('127', '\tMAN_1568926733\t ', '11', '119', '0', '1568926733', '', '2019-09-19 15:56:58', '\tJOSE.AGUIRRE210457@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('128', '\tMAN_1568926734\t ', '11', '120', '0', '1568926734', '', '2019-09-19 15:56:58', '\tINMCCDE@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('129', '\tMAN_1568926735\t ', '11', '121', '0', '1568926735', '', '2019-09-19 15:56:58', '\tCETYACONSTRUCCIONES@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('130', '\tMAN_1568926736\t ', '11', '122', '0', '1568926736', '', '2019-09-19 15:56:58', '\tRNS@CLIMA3.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('131', '\tMAN_1568926737\t ', '11', '123', '0', '1568926737', '', '2019-09-19 15:56:58', '\tIMT_CONSTRUCCION@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('132', '\tMAN_1568926738\t ', '11', '124', '0', '1568926738', '', '2019-09-19 15:56:58', '\tmaxfregoso@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('133', '\tMAN_1568926739\t ', '11', '125', '0', '1568926739', '', '2019-09-19 15:56:58', '\tMACONTR02@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('134', '\tMAN_1568926740\t ', '11', '126', '0', '1568926740', '', '2019-09-19 15:56:58', '\tINDICO.HMO@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('135', '\tMAN_1568926741\t ', '11', '127', '0', '1568926741', '', '2019-09-19 15:56:58', '\tGERMANING1223@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('136', '\tMAN_1568926742\t ', '11', '128', '0', '1568926742', '', '2019-09-19 15:56:58', '\tCONSTRUCTORA_MUSARO@YAHOO.COM.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('137', '\tMAN_1568926743\t ', '11', '129', '0', '1568926743', '', '2019-09-19 15:56:58', '\talberto_sanchez_encinas@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('138', '\tMAN_1568926744\t ', '11', '130', '0', '1568926744', '', '2019-09-19 15:56:58', '\tDIRECCION.NORIGA@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('139', '\tMAN_1568926745\t ', '11', '131', '0', '1568926745', '', '2019-09-19 15:56:58', '\thector.cuellar@panelrex.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('140', '\tMAN_1568926746\t ', '11', '132', '0', '1568926746', '', '2019-09-19 15:56:58', '\tTONAHUACGT@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('141', '\tMAN_1568926747\t ', '11', '133', '0', '1568926747', '', '2019-09-19 15:56:58', '\tEDGARDO_ANGUIS@ESPACIODU.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('142', '\tMAN_1568926748\t ', '11', '134', '0', '1568926748', '', '2019-09-19 15:56:58', '\talfaraenaconstrucciones@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('143', '\tMAN_1568926749\t ', '11', '135', '0', '1568926749', '', '2019-09-19 15:56:58', '\tarquinfra@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('144', '\tMAN_1568926750\t ', '11', '136', '0', '1568926750', '', '2019-09-19 15:56:58', '\tfranciscavazqueztrillas@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('145', '\tMAN_1568926751\t ', '11', '137', '0', '1568926751', '', '2019-09-19 15:56:58', '\tjaimeparramo@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('146', '\tMAN_1568926752\t ', '11', '138', '0', '1568926752', '', '2019-09-19 15:56:58', '\tJUANMENDOZA1@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('147', '\tMAN_1568926753\t ', '11', '139', '0', '1568926753', '', '2019-09-19 15:56:58', '\tSALMARVI@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('148', '\tMAN_1568926754\t ', '11', '140', '0', '1568926754', '', '2019-09-19 15:56:58', '\tMARAGUACONSTRUCCIONES@YAHOO.COM.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('149', '\tMAN_1568926755\t ', '11', '141', '0', '1568926755', '', '2019-09-19 15:56:58', '\tEPMOCELICK@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('150', '\tMAN_1568926756\t ', '11', '142', '0', '1568926756', '', '2019-09-19 15:56:58', '\trecepcion@dtnmex.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('151', '\tMAN_1568926757\t ', '11', '143', '0', '1568926757', '', '2019-09-19 15:56:58', '\tCRLIMON@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('152', '\tMAN_1568926758\t ', '11', '144', '0', '1568926758', '', '2019-09-19 15:56:58', '\tjavier_toledoruiz@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('153', '\tMAN_1568926759\t ', '11', '145', '0', '1568926759', '', '2019-09-19 15:56:58', '\tacclaboratorio@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('154', '\tMAN_1568926760\t ', '11', '146', '0', '1568926760', '', '2019-09-19 15:56:58', '\tmarielit@msn.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('155', '\tMAN_1568926761\t ', '11', '147', '0', '1568926761', '', '2019-09-19 15:56:58', '\tspencervlza@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('156', '\tMAN_1568926762\t ', '11', '148', '0', '1568926762', '', '2019-09-19 15:56:58', '\tprocodesa.construcciones@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('157', '\tMAN_1568926763\t ', '11', '149', '0', '1568926763', '', '2019-09-19 15:56:58', '\tINM.SOCE@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('158', '\tMAN_1568926764\t ', '11', '150', '0', '1568926764', '', '2019-09-19 15:56:58', '\tSCULLGROUP@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('159', '\tMAN_1568926765\t ', '11', '151', '0', '1568926765', '', '2019-09-19 15:56:58', '\tJAVALENZUELA@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('160', '\tMAN_1568926766\t ', '11', '152', '0', '1568926766', '', '2019-09-19 15:56:58', '\tTERPLAN_OBREGON@YAHOO.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('161', '\tMAN_1568926767\t ', '11', '153', '0', '1568926767', '', '2019-09-19 15:56:58', '\tarsagregados@yahoo.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('162', '\tMAN_1568926768\t ', '11', '154', '0', '1568926768', '', '2019-09-19 15:56:58', '\tconst.sayg@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('163', '\tMAN_1568926769\t ', '11', '155', '0', '1568926769', '', '2019-09-19 15:56:58', '\tadministracion@bapco.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('164', '\tMAN_1568926770\t ', '11', '156', '0', '1568926770', '', '2019-09-19 15:56:58', '\tcpitic@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('165', '\tMAN_1568926771\t ', '11', '157', '0', '1568926771', '', '2019-09-19 15:56:58', '\tconstructoragaraco84@yahoo.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('166', '\tMAN_1568926772\t ', '11', '158', '0', '1568926772', '', '2019-09-19 15:56:58', '\trussell_57mx@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('167', '\tMAN_1568926773\t ', '11', '159', '0', '1568926773', '', '2019-09-19 15:56:58', '\tcoaxiro@yahoo.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('168', '\tMAN_1568926774\t ', '11', '160', '0', '1568926774', '', '2019-09-19 15:56:58', '\tcosco.obregon@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('169', '\tMAN_1568926775\t ', '11', '161', '0', '1568926775', '', '2019-09-19 15:56:58', '\tadministracion@electroinsumos.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('170', '\tMAN_1568926776\t ', '11', '162', '0', '1568926776', '', '2019-09-19 15:56:58', '\tadmon.drk@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('171', '\tMAN_1568926777\t ', '11', '163', '0', '1568926777', '', '2019-09-19 15:56:58', '\tgarza012013@outlook.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('172', '\tMAN_1568926778\t ', '11', '164', '0', '1568926778', '', '2019-09-19 15:56:58', '\tGALINDO_FIERRO@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('173', '\tMAN_1568926779\t ', '11', '165', '0', '1568926779', '', '2019-09-19 15:56:58', '\tREZ@GYGCONSTRUCTORES.COM.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('174', '\tMAN_1568926780\t ', '11', '166', '0', '1568926780', '', '2019-09-19 15:56:58', '\tRNS@CLIMA3.MX\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('175', '\tMAN_1568926781\t ', '11', '167', '0', '1568926781', '', '2019-09-19 15:56:58', '\tgujaconstructores@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('176', '\tMAN_1568926782\t ', '11', '168', '0', '1568926782', '', '2019-09-19 15:56:58', '\tcmiramarsadecv@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('177', '\tMAN_1568926783\t ', '11', '169', '0', '1568926783', '', '2019-09-19 15:56:58', '\tmiguelruelas@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('178', '\tMAN_1568926784\t ', '11', '170', '0', '1568926784', '', '2019-09-19 15:56:58', '\tmiguelruelas@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('179', '\tMAN_1568926785\t ', '11', '171', '0', '1568926785', '', '2019-09-19 15:56:58', '\tjosearmenta70@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('180', '\tMAN_1568926786\t ', '11', '172', '0', '1568926786', '', '2019-09-19 15:56:58', '\tINGQUINTANA@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('181', '\tMAN_1568926787\t ', '11', '173', '0', '1568926787', '', '2019-09-19 15:56:58', '\tcontacto@lacosi.com.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('182', '\tMAN_1568926788\t ', '11', '174', '0', '1568926788', '', '2019-09-19 15:56:58', '\tLCAMARGOROJO@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('183', '\tMAN_1568926789\t ', '11', '175', '0', '1568926789', '', '2019-09-19 15:56:58', '\tjimm.ledezma@lecta.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('184', '\tMAN_1568926790\t ', '11', '176', '0', '1568926790', '', '2019-09-19 15:56:58', '\tROGER_G14@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('185', '\tMAN_1568926791\t ', '11', '177', '0', '1568926791', '', '2019-09-19 15:56:58', '\tmtp1@outlook.es\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('186', '\tMAN_1568926792\t ', '11', '178', '0', '1568926792', '', '2019-09-19 15:56:58', '\tosmanindustrial@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('187', '\tMAN_1568926793\t ', '11', '179', '0', '1568926793', '', '2019-09-19 15:56:58', '\telgudo@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('188', '\tMAN_1568926794\t ', '11', '180', '0', '1568926794', '', '2019-09-19 15:56:58', '\teypsancarlos@yahoo.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('189', '\tMAN_1568926795\t ', '11', '181', '0', '1568926795', '', '2019-09-19 15:56:58', '\tpueblaarquitectos@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('190', '\tMAN_1568926796\t ', '11', '182', '0', '1568926796', '', '2019-09-19 15:56:58', '\tRMSISTEMASCONSTRUCTIVOS@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('191', '\tMAN_1568926797\t ', '11', '183', '0', '1568926797', '', '2019-09-19 15:56:58', '\trocio-robles84@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('192', '\tMAN_1568926798\t ', '11', '184', '0', '1568926798', '', '2019-09-19 15:56:58', '\tcortinas.cmh@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('193', '\tMAN_1568926799\t ', '11', '185', '0', '1568926799', '', '2019-09-19 15:56:58', '\tconradosanchezd@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('194', '\tMAN_1568926800\t ', '11', '186', '0', '1568926800', '', '2019-09-19 15:56:58', '\tsomedesarrollos@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('195', '\tMAN_1568926801\t ', '11', '187', '0', '1568926801', '', '2019-09-19 15:56:58', '\ttemarqsa@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('196', '\tMAN_1568926802\t ', '11', '188', '0', '1568926802', '', '2019-09-19 15:56:58', '\tadministracion@grupovalpo.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('197', '\tMAN_1568926803\t ', '11', '189', '0', '1568926803', '', '2019-09-19 15:56:58', '\trmartinez@xxiconstrucciones.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('198', '\tMAN_1568926804\t ', '11', '190', '0', '1568926804', '', '2019-09-19 15:56:58', '\telectrosupplier@prodigy.net.mx\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('199', '\tMAN_1568926805\t ', '11', '191', '0', '1568926805', '', '2019-09-19 15:56:58', '\ting_javitre@hotmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('200', '\tMAN_1568926806\t ', '11', '192', '0', '1568926806', '', '2019-09-19 15:56:58', '\tRUBIO_SIEMS@HOTMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('201', '\tMAN_1568926807\t ', '11', '193', '0', '1568926807', '', '2019-09-19 15:56:58', '\tFCOJCORONADOCASTRO@GMAIL.COM\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('202', '\tMAN_1568926808\t ', '11', '2', '0', '1568926808', '', '2019-09-19 15:56:58', '\tk.wilson.a@gmail.com\t ', '\tverified\t ', '1', '0.00', '0.00', '0.00', '0.00', '\tMX', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('203', 'MAN_1569364492', '11', '211', '0', '1569364492', '', '2019-09-24 15:34:52', 'luisfernandobarrazamartinez@gmail.com', 'verified', '1', '0.00', '0.00', '0.00', '0.00', 'MXN', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('204', 'MAN_1569371580', '11', '2', '0', '1569371580', '', '2019-09-24 17:33:00', 'k.wilson.a@gmail.com', 'verified', '1', '0.00', '0.00', '0.00', '0.00', 'MXN', '6', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('206', 'MAN_1569614217', '8', '219', '0', '1569614217', '', '2019-09-27 12:56:57', 'k.wilson.a@gmail.com123', 'verified', '1', '34.99', '0.00', '0.00', '0.00', 'MXN', '5', '', '1', '1');
INSERT INTO `transactions` (`id`, `txn_id`, `pid`, `uid`, `downloads`, `file_date`, `ip`, `created`, `payer_email`, `payer_status`, `item_qty`, `price`, `mc_fee`, `coupon`, `tax`, `currency`, `pp`, `memo`, `status`, `active`) VALUES ('207', 'MAN_1569961180', '11', '225', '0', '1569961180', '', '2019-10-01 13:19:40', 'k.wilson.a1@gmail.com', 'verified', '1', '0.00', '0.00', '0.00', '0.00', 'MXN', '6', '', '1', '1');


-- --------------------------------------------------
# -- Table structure for table `users`
-- --------------------------------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(80) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `newsletter` tinyint(1) NOT NULL DEFAULT '0',
  `cookie_id` varchar(50) NOT NULL DEFAULT '0',
  `token` varchar(50) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT '0000-00-00 00:00:00',
  `avatar` varchar(50) DEFAULT NULL,
  `address` varchar(150) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `zip` varchar(20) DEFAULT NULL,
  `country` varchar(4) DEFAULT NULL,
  `notes` text,
  `info` text,
  `lastlogin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastip` varchar(16) DEFAULT NULL,
  `userlevel` tinyint(1) NOT NULL DEFAULT '1',
  `active` enum('y','n','t','b') NOT NULL DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=228 DEFAULT CHARSET=utf8;

-- --------------------------------------------------
# Dumping data for table `users`
-- --------------------------------------------------

INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('62', '29800', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CESAR ARTURO', 'SILVA GOMEZ', 'coconosa@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DUBLIN 17, RAQUET CLUB II', 'HERMOSILLO', 'SONORA', '83200', 'MX', '', '', '2019-09-24 15:41:04', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('227', 'aspirante', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'aspirante', 'Seeker', 'k.wilson.a@gmail.com', '0', '0', '0', '2019-10-02 14:41:30', '', '', '', '', '', '', '', '', '2019-10-02 19:03:16', '187.189.34.154', '1', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('4', '00073501', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JORGE ALBERTO', 'AGUIRRE ROBLES', 'administracion@pcvirgo.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'BLVD. SOLIDARIDAD 1002 1,LA MOSCA', 'HERMOSILLO', 'SONORA', '83270', 'MX', '', '', '2019-09-23 18:46:07', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('5', '00086324', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MARIO', 'MENDEZ DESSENS', 'mmdessens@yahoo.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'YAÑEZ 34, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '2019-09-23 18:50:40', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('6', '00116999', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FRANCISCO', 'BELTRAN VILLANUEVA', 'consein@conseinedificaciones.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'MELCHOR OCAMPO 1, MELCHOR OCAMPO', 'NACOZARI', 'SONORA', '84340', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('7', '00021659', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ALEJANDRO', 'MORENO LAUTERIO', 'vvalencia@canoras.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'PASEO DE LOS ALAMOS S/N, VILLA DEL SOL', 'HERMOSILLO', 'SONORA', '83240', 'MX', '', '', '2019-09-23 18:57:35', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('8', '00048497', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MARCOS FRANCISCO', 'GLUYAS SOLORZANO', 'info@gluyasconstrucciones.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'TARASCA 16 SUR NUEVA ESTACION', 'HERMOSILLO', 'SONORA', '83160', 'MX', '', '', '2019-09-23 18:59:03', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('9', '00020560', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ALFONSO', 'REINA VILLEGAS', 'ventas@construplan.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'PERIFERICO PONIENTE 770 EMILIANO ZAPATA', 'HERMOSILLO', 'SONORA', '83280', 'MX', '', '', '2019-09-23 19:03:17', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('10', '00039098', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE MARIA', 'GALLEGOS LARIOS', 'azteca860203@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('11', '00100627', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ROBERTO', 'OCHOA PADILLA', 'grupo8a@prodigy.net.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA DOCTOR PALIZA PADILLA 92 CENTENARIO', 'HERMOSILLO', 'SONORA', '83260', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('12', '00102368', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'PEDRO', 'LUNA DE LA ROSA', 'acabrera@luconsa.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'TAMAULIPAS 551 NTE. ZONA NORTE', 'CAJEME', 'SONORA', '85040', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('13', '00101260', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RICARDO', 'OSORIO MORALES', 'ROSORIO@OPOSON.COM.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVE. TEHUANTEPEC 111 CENTENARIO', 'HERMOSILLO', 'SONORA', '83260', 'MX', '', '', '2019-09-24 11:53:57', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('14', '00085021', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JUAN CARLOS', 'GAMEZ OSIO', 'gamezosio@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'CAMPECHE 462 PIMENTEL', 'HERMOSILLO', 'SONORA', '83188\t', 'MX', '', '', '2019-09-24 11:56:14', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('15', '00117847', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'LUIS FELIPE', 'CAMOU LEON', 'ale.majerus@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'HERMENEGILDO RANGEL LUGO 167 5 DE MAYO', 'HERMOSILLO', 'SONORA', '83010', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('16', '00080712', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JORGE ARTURO', 'ZEPEDA ARRIAGA', 'CONSTRUDISENOSDESONORA@PRODIGY.NET.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'ANGEL GARCIA ABURTO 221 JESUS GARCIA', 'HERMOSILLO', 'SONORA', '83140', 'MX', '', '', '2019-09-24 12:16:03', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('17', '00116896C0', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ADRIAN', 'CAMOU LOERA', 'ACAMOU@CWMETAL.COM.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'corregidores 50 villa satelite', 'HERMOSILLO\t', 'SONORA', '83200', 'MX', '', '', '2019-09-24 12:17:56', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('18', '00001283', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ADOLFO JORGE', 'HARISPURU BORQUEZ', 'aharispuru@icsa.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'CARRETERA FEDERAL MEXICO 015 NTE. 3101 1, ZONA URBANA', 'ESPERANZA', 'SONORA', '85210', 'MX', '', '', '2019-09-24 12:19:43', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('19', '00118127', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ADOLFO JORGE', 'HARISPURU BORQUEZ', 'jharispuru@icsa.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'CARRETERA FEDERAL MEXICO 015 NTE. 3101, ZONA URBANA', 'ESPERANZA', 'SONORA', '85210', 'MX', '', '', '2019-09-24 12:21:56', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('20', '00107708', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'EDUARDO', 'LARA RIVERA', 'cfd@larivdesarrollos.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DEL ORO 130, PARQUE INDUSTRIAL', 'HERMOSILLO', 'SONORA', '83299', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('225', 'empleador', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'empleador', 'Company', 'k.wilson.a1@gmail.com', '0', '0', '0', '2019-10-01 15:16:36', 'AVT_6961D5-7CBFEA-AF99AC-1C1271-1D0F2F-CBA13B.png', '', '', '', '', '', '', '', '2019-10-02 15:54:52', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('61', '00073946', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MARCOS', 'DUARTE VIDAL', 'coanamark@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVE. MICHOACAN0 221, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('60', '00124341', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'HORACIO', 'PADILLA QUIRARTE', 'info@casnor.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'PINO SUAREZ 104, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '2019-09-24 15:21:37', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('59', '00100911', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RICARDO', 'PLATT GARCIA', 'rplatt@conde.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'BLVD. NAVARRETE 97 25', 'HERMOSILLO', 'SONORA', '83206', 'MX', '', '', '2019-09-24 15:19:07', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('58', '00124449', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'WALTER ROBERTO', 'VILLEGAS LARES', 'maricela.ramirez@servechermosillo.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'Camelia 619, Libertad', 'HERMOSILLO\t', 'SONORA', '83130', 'MX', '', '', '2019-09-24 15:17:11', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('57', '00092345', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'GUSTAVO', 'ANAYA SAÑUDO', 'pycmagus@yahoo.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'NAVOJOA', '\tSONORA', '85860', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('56', '00117254', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'HECTOR MANUEL', 'MARTINEZ FIERRO', 'HMARTINEZ@PIXO.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-24 15:12:34', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('55', '00069697', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JORGE', 'HERNANDEZ FLORES', 'oestec@oestec.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-24 15:11:26', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('54', '00126586', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MARTIN', 'HERNANDEZ RODRIGUEZ', 'PROCONS.08@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('53', '00114291', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JAVIER ENRIQUE', 'GARCIA ALONSO', 'construcciones.felgar@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85040', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('52', '00055491', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'IVAN', 'MLADOSICH ESTRADA', 'imeconstrucciones@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'NAVOJOA', 'SONORA', '85860', 'MX', '', '', '2019-09-24 15:07:17', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('51', '00106740', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE CARLOS', 'AGUILAR RIVERA', 'jlpopocasotelo@yahoo.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85040', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('50', '00104444', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'HECTOR', 'ISLAS CASTRO', 'anaelcamacho@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('49', '00047271', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE GUILLERMO', 'GAMBOA BALLESTEROS', 'gys_constructores@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('48', '00074360', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MARIO', 'LOPEZ CUEVAS', 'grupomesis@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'NAVOJOA\t', 'SONORA', '85860', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('47', '00105121', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'BENITO JESUS', 'DOSAMANTES GALLEGOS', 'benitodosamantes@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('45', '00108901', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'GILBERTO', 'BRITO MARTINEZ', 'facturacion@edinosa.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85040', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('46', '00117118', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ROBERTO', 'GAMBOA MORALES', 'dapci.construccion@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85040', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('1', 'admin', '216d076dc7bd213ca69682321b9f7d027ec5a85f', 'Web1', 'Master', 'eduardohs@gmail.com', '0', '0', '0', '2019-08-23 03:04:44', '', '', '', '', '', '', '', '', '2019-10-03 14:41:12', '187.189.34.154', '9', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('63', '00088576', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE ALEJANDRO', 'PEREZ RUBIO ARTEE', 'alejandropra@me.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'JUAN JOSE AGUIRRE 295, BALDERRAMA', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('44', '00027862', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CARLOS ALBERTO\t', 'VELIS CAMPOY', 'contacto@constructoravelis.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85040', 'MX', '', '', '2019-09-24 13:52:03', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('26', '00073006', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ANGEL ANTONIO', 'BOURS ZARAGOZA', 'edificacionesboza@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'GARCIA MORALES 601 A JUAREZ', 'NAVOJOA', 'SONORA', '85870', 'MX', '', '', '2019-09-24 12:44:54', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('27', '00096572', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE DARIO', 'CABRAL VALDEZ', 'cvdario@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'BENITO JÚAREZ', 'SONORA', '85290', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('28', '00095142', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FRANCISCO', 'LOPEZ ANAYA', 'lcproyectos@yahoo.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'NAVOJOA', 'SONORA', '85870', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('29', '00124071', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ANDRES', 'RAMIREZ NENNINGER', 'contacto@nennca.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-24 12:56:03', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('30', '00102581', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'SANTOS ROGELIO', 'MEDINA CORONA', 'contacto@viasmvp.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'Blvd. Luis Donaldo Colosio 294-F', 'HERMOSILLO', 'SONORA', '83260', 'MX', '', '', '2019-09-24 12:56:29', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('43', '00122707', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE RAUL', 'VAZQUEZ RASCON', 'CONSTRUCCIONESR.R@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA NIÑOS HEROES 115 A CENTRO', 'HERMOSILLO', 'SONORA', '83100', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('42', '00115573', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RUBEN FRANCISCO', 'BALLESTEROS VILLEGAS', 'INGELECTRICADELCOBRE@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'TLAXCALA 77, SAN BENITO', 'HERMOSILLO\t', 'SONORA', '83190', 'MX', '', '', '2019-09-24 17:17:43', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('41', '00055323', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'BRAULIO', 'CORONEL SANDOVAL', 'brauliofontane@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'EVERARDO MONROY 118 10, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '2019-09-24 17:16:54', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('40', '00081370', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ALEJANDRO', 'IBARRA MC LAURIN', 'ALEXIBARRA@PRODIGY.NET.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'CALLE DALIAS 9, PALO VERDE', 'HERMOSILLO', 'SONORA', '83280', 'MX', '', '', '2019-09-24 17:15:40', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('39', '00117896', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JAVEIR', 'GALLEGOS LARIOS', 'contacto@maqgala.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'REPUBLICA DE JAMAICA 12, EL SAHUARO', 'HERMOSILLO', 'SONORA', '83170', 'MX', '', '', '2019-09-24 17:19:13', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('38', '00123943', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'KARLA FABIOLA', 'GARCIA PERALTA', 'ienriquez@manser.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'FRANCISCO MONTEVERDE 38, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '2019-09-24 17:19:48', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('37', '00111467', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'DAVID CARLOS', 'LLANO ORONA', 'inecaconstrucciones@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'QUINTA LAS CANORAS 59 las quintas', 'HERMOSILLO', 'SONORA', '83240', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('36', '00114608', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'DANIEL ENRIQUE', 'HINOJOS BASTIDAS', 'grupototalinbas@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA SIETE 225, APOLO', 'HERMOSILLO', 'SONORA', '83100', 'MX', '', '', '2019-09-24 13:11:26', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('35', '00121404', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MANUEL', 'ALBELAIS HOPKINS', 'dso.construcciones@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'PASEO DE LA COLINA 66 VALLE VERDE', 'HERMOSILLO', 'SONORA', '83200', 'MX', '', '', '2019-09-24 13:06:07', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('34', '00093508', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'TERESITA DE JESUS', 'RUIZ ACOSTA', 'minargc@prodigy.net.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'CUAUHTEMOC 123 D ALTOS CONSTITUCION', 'NAVOJOA', 'SONORA', '85820', 'MX', '', '', '2019-09-24 13:04:16', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('33', '00090575', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE ALFREDO', 'GAXIOLA VILLALBA', 'lauracmicnavojoa@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'GUELATAO 3606 FRACC BUGAMBILIAS', 'NAVOJOA', 'SONORA', '85860', 'MX', '', '', '2019-09-24 13:01:11', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('32', '00110867', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FRANCISCO JAVIER', 'CORDOVA LOPEZ', 'acclaboratorio@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'CUERNAVACA 51 SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('31', '00101070', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ARNOLDO', 'GARCIA PEREZ', 'info@cgarale.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'GUADALUPE VICTORIA 15 26, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('24', '00126496', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ISIDRO', 'JAUREGUI INZUNZA', 'OBRAS@INMEX.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-24 12:41:27', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('25', '00089481', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ANA KARINA', 'MALDONADO ANDREWS', 'karina@construpima.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-24 12:42:45', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('21', '00118181', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ANA SOFIA', 'HERNANDEZ LEON', 'contacto@puente391.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA 7 20 6, JESUS GARCIA', 'HERMOSILLO', 'SONORA', '83140', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('22', '00099345', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FRANCISCA PATRICIA', 'LEON SALAS', 'patyl@spazicorp.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'QUINTA DE LA FLOR 13 D, LAS QUINTAS', 'HERMOSILLO', 'SONORA', '83240', 'MX', '', '', '2019-09-24 12:32:19', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('23', '00126413', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'LIZETH GPE', 'BORBON', 'mitzi.ibarra@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA DE LOS YAQUIS 1187, NUEVA ESPAÑA', 'HERMOSILLO', 'SONORA', '\t83126', 'MX', '', '', '2019-09-24 12:37:23', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('64', '00121119', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JORGE MARTIN', 'PEÑA CALIXTRO', 'yolanda@grupoimc.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'TEPIC 16, PALO VERDE', 'HERMOSILLO', 'SONORA', '83280', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('65', '00118600', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ANDRES', 'PACHECO GONZALEZ', 'administracion@electroinsumos.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'TLAXCALA 334, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '2019-09-24 16:03:18', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('66', '00116910', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ENRIQUE', 'SAMOANO VALENZUELA', 'big_samoano@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'TAMAULIPAS 712, DEPORTIVA', 'NAVOJOA', 'SONORA', '85860', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('67', '00104242', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'LUIS ENRIQUE', 'CANO GAXIOLA', 'luiscano@certusgerencia.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'MONTERREY 131, CENTRO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('68', '00124592', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FERNANDO', 'ESCOBOZA DURON', 'ADMIN@ESCOBO.COM.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA NUEVE 150 PRADOS DEL SOL', 'HERMOSILLO', 'SONORA', '83100', 'MX', '', '', '2019-09-24 17:09:21', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('69', '00097696', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FILIBERTO', 'LUGO RODRIGUEZ', 'verificacion2000@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'CALLEJON AGUA ZARCA 5 OLIVARES', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('70', '00124440', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE EMMANUEL', 'OROZCO AISPURO', 'FORTEIOP@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'OLGA CAMOU 17 SAN JUAN', 'HERMOSILLO', 'SONORA', '83070', 'MX', '', '', '2019-09-24 17:24:06', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('71', '00086323', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JORGE MARIO', 'ROCHIN RUIZ', 'contratistarodo@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'LEOCADIO SALCEDO 136, JESUS GARCIA', 'HERMOSILLO', 'SONORA\t', '83140', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('72', '00123190', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CARLOS ENRIQUE', 'PRINCE REYNA', 'contacto@princeconstrucciones.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'CALLE JULIA TRUJILLO 1019, HOGAR Y PATRIMONIO\t', 'CAJEME', 'SONORA', '85210', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('73', '00077883', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'GUSTAVO', 'MACHADO BELTRAN', 'gmb-64@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DONATO GUERRA 1252 SOCHILOA', 'OBREGON', 'SONORA', '85050', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('74', '00125585', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FERNANDO ENRIQUE', 'AVILA GAMBOA', 'fernando.avila@industrialavila.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'AV. MANUEL OJEDA RIVERA, VILLA DE SERIS', 'HERMOSILLO', 'SONORA', '83280', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('75', '00123882', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'HUGO CESAR', 'NAVARRO BELTRÁN', 'hugolfnavarro@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA ENRIQUE GARCÍA SÁNCHEZ 100 C CENTRO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('76', '00125414', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'IRVING', 'ESCOBOZA QUINTERO', 'infogerpro@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'QUINTA ALEGRE 15, LAS QUINTAS', 'HERMOSILLO', 'SONORA', '83240', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('77', '00102785', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'GLORIA CECILIA', 'IBARRA LOPEZ', 'iblopconstrucciones@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'CIRCUITO VENECIA 96 VILLA DORADA', 'NAVOJOA', 'SONORA', '85874', 'MX', '', '', '2019-09-24 17:36:59', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('78', '00125485', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'SERGIO', 'PEÑA RIVAS', 'invino2014@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOCTOR PESQUEIRA 70 A, CENTENARIO', 'HERMOSILLO', 'SONORA', '83260', 'MX', '', '', '2019-09-24 17:38:58', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('79', '00087720', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JAVIER', 'BARCELO DURAZO', 'contabilidad_icssa@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'ANGEL ARREOLA 119 VALLE DORADO', 'HERMOSILLO', 'SONORA', '83175', 'MX', '', '', '2019-09-24 17:43:16', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('80', '00117465', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'AARON', 'PABLOS PACHECO', 'ingenieria_paay@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'HIDALGO 619 6 ALTOS, CENTRO', 'OBREGON', 'SONORA', '85000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('81', '00101329', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ANDRES', 'PACHECO GONZALEZ', 'andres_pacheco@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVE TLAXCALA 334 LOCAL 1 OLIVARES', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('82', '00100626', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ROBERTO', 'SITTEN AYALA', 'sitten_trax@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AV. SEGURO SOCIAL 30, MODELO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('83', '00125738', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JORGE ALBERTO', 'LOPEZ LOPEZ', 'jorlo2001@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'PRIMERA DE BUGAMBILIAS 85, BUGAMBILIAS', 'HERMOSILLO', 'SONORA', '83140', 'MX', '', '', '2019-09-24 17:52:21', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('84', '00097537', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JUAN CARLOS', 'ROCHA ROMERO', 'lespiproyectos@yahoo.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'CALLE ALDAMA 106 SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '2019-09-24 17:57:46', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('85', '00093926', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOAQUIN ALBERTO', 'PARRA PEÑA', 'proteko@prodigy.net.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'CALIFORNIA 326 - 5 NORTE, CENTRO', 'OBREGON', 'SONORA', '85000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('86', '00098866', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JUAN ADOLFO', 'DWORAK ROBINSON', 'oceanussyp@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA 1 61 SAN VICENTE', 'GUAYMAS', 'SONORA', '85465', 'MX', '', '', '2019-09-25 15:32:36', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('87', '00125740', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ANTONIO', 'CAMOU JORDAN', 'scjconstruye@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DE LAS FERIAS 11723 1, PUEBLO BONITO', 'TIJUANA', 'BAJA CALIFORNIA NORTE', '22034', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('88', '00125555', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ALAN HUMBERTO', 'CELAYA URIBE', 'felipefimbresr@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'VILLA SIENA 2, VILLAS DEL MEDITERRANEO', 'HERMOSILLO', 'SONORA', '83220', 'MX', '', '', '2019-09-25 15:46:12', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('89', '00080714', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS', 'VALENZUELA VASQUEZ', 'OPOSURADO@YAHOO.COM.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVE. TENOCHTITLAN 180 VALLE DEL MARQUEZ', 'HERMOSILLO', 'SONORA', '83294', 'MX', '', '', '0000-00-00 00:00:00', '', '2', '');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('90', '00114346', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ANDRES', 'PACHECO GONZALEZ', 'administracion@petrotransmodal.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVE PLAN DE AGUA PRIETA 254 LEY 57', 'HERMOSILLO', 'SONORA', '83100', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('91', '0012604', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'AGUSTIN ENRIQUE', 'BARRAGAN ARAQUE', 'calliandraconstructora@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'LEON GUZMAN 21, CONSTITUCION', 'HERMOSILLO', 'SONORA', '83150\t', 'MX', '', '', '2019-09-25 17:36:28', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('92', '00091480', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CLAUDIO', 'MILLAN IBARRA', 'tocaing@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'ARIZONA 333 A, BALDERRAMA', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('93', '00113267', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ADOLFO JORGE', 'HARISPURU BORQUEZ', 'informes@icsa.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'CARRETERA FEDERAL MEXICO 015 NTE. 3101 2, ZONA URBANA', 'ESPERANZA', 'SONORA', '85210', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('94', '00101334', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MANUEL', 'BOJORQUEZ LUGO', 'ing_bojorquez9@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'CHIAPAS 807 JUAREZ', 'NAVOJOA', 'SONORA', '85870', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('95', '00121372', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ANDRES', 'PACHECO GONZALEZ', 'administracion@verticasc.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'BLVD. LAZARO CARDENAS 10 K AMPLIACION LADRILLERAS', 'HERMOSILLO', 'SONORA', '83179', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('96', '00125750', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'GERARDO ERNESTO', 'NAVARRO BURRUEL', 'construccionesrenav@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'BABILOMO 6 12, VALLEVERDE', 'HERMOSILLO', 'SONORA', '83200', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('97', '00080248', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JUAN JOSE', 'AGUIRRE', 'acsaconstructores@outlook.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'JUAN JOSE AGUIRRE 19 BALDERRAMA', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('98', '00066781', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ROBERTO', 'GASTELUM FELIX', 'cyurgsa@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'TERRENATE 20, RESIDENCIAL DE ANZA', 'HERMOSILLO', 'SONORA', '83248', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('99', '00122088', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'GERARDO', 'SAMANIEGO RIVAS', 'GSAMANIEGO@ALFALEANCONSTRUCTION.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'PRIVADA MORELIA 56 CENTRO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-25 16:02:09', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('100', '00126037', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CHRISTIAN IVAN', 'GONZALEZ SALGADO', 'ecapusa.2016@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'PARROQUIA 13, VILLA SATELITE', 'HERMOSILLO', 'SONORA', '83200', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('101', '00103921', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'OCTAVIO', 'ROSAS DUARTE', 'penelope@arqcoenlinea.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'BLVD. MORELOS 389 LOCAL 20 BACHOCO', 'HERMOSILLO', 'SONORA', '83148', 'MX', '', '', '2019-09-25 17:31:27', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('102', '00121728', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'HECTOR', 'CUELLAR', 'hector.cuellar@panelrex.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'GUADALUPE VICTORIA 121 SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('103', '00119520', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CLAUDIO', 'MILLAN IBARRA', 'grupomercla@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'ARIZONA 333 ALTOS, BALDERRAMA', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('104', '00125874', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RODRIGO ALBERTO', 'PEÑA PORCHAS', 'administracion@penaporchas.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'PASEO DE LAS FUENTES 33, VALLE VERDE', 'HERMOSILLO', 'SONORA', '83200', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('105', '00078386', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'HUGO', 'LLANO MONTAÑO', 'hugollano.2393@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'ALFREDO EGUIARTE 268 10 jesus garcia', 'HERMOSILLO', 'SONORA', '83140', 'MX', '', '', '2019-09-25 18:48:18', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('106', '00123529', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MARTHA', 'GARCÍA DE LEÓN PEÑÚÑURI', 'marthagd@derex.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'BLVD HIDALGO 70 centenario', 'HERMOSILLO', 'SONORA', '83260', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('107', '00111748', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE LUIS', 'RUBIO PINO', 'd.rubio@ryrce.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'BOULEVARD GARCIA MORALES KM 6.5 el llano', 'HERMOSILLO', 'SONORA', '83210', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('108', '00125826', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JONATAN EULALIO', 'RIVERA MACOTELA', 'jr2group@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'INDEPENDENCIA 336, 2 DE ABRIL', 'XALAPA', 'VERACRUZ', '91030', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('109', '00119543', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ROBERTO', 'MALDONADO GASTELUM', 'jr_maldonadog@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'miguel hidalgo 354 palmar del sol', 'HERMOSILLO', 'SONORA', '83250', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('110', '00116649', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'SANTOS ROGELIO', 'MEDINA CORONA', 'info@maquinaria.biz', '0', '0', '0', '2019-09-18 03:00:00', '', 'LUIS DONALDO COLOSIO 294 F, PRADOS DE CENTENARIO', 'HERMOSILLO', 'SONORA', '83260', 'MX', '', '', '2019-09-25 18:25:21', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('111', '00126224', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE ERNESTO', 'RASCON ENRIQUEZ', 'jopada.edificaciones@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'ARDILLAS 115 casa linda', 'HERMOSILLO', 'SONORA', '83284', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('112', '00091317', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JUAN', 'SLAS BELTRAN', 'ISLAS_BELTRANJUAN@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'CALLES 17 Y 18 AV. 42 S/N NUEVO PROGRESO', 'AGUA PRIETA', 'SONORA', '84279', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('113', '00123643', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JUAN CARLOS', 'GAMEZ OSIO', 'gamezosio@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'REAL DEL ARCO 26, VILLA SATELITE', 'HERMOSILLO', 'SONORA', '83200', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('114', '00107378', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE LUIS', 'RUBIO PINO', 'd.rubio@ryrce.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'BLVD. GARCIA MORALES KM. 6.5 S/N EL LLANO', 'HERMOSILLO', 'SONORA', '83210', 'MX', '', '', '2019-09-25 18:40:38', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('115', '00125957', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'BERENICE', 'AREVALO GALVAN', 'sasaem.construccion@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'VALLE DE ALGODON 1545, VILLA FONTANA', 'OBREGON', 'SONORA', '85096', 'MX', '', '', '0000-00-00 00:00:00', '', '2', '');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('116', '00076007', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CARLOS ARNULFO', 'LOPEZ LOPEZ', 'carlos.lopez@equiplan.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'PASEO VALLE GRANDE 134 INT.3, VALLE GRANDE', 'HERMOSILLO', 'SONORA', '83205', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('117', '00088573', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'GERARDO', 'KOSSIO PARTIDA', 'sithermosillo@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AV. DEL PASEO 65, PASEO DEL SOL', 'HERMOSILLO', 'SONORA', '83246', 'MX', '', '', '0000-00-00 00:00:00', '', '2', '');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('118', '00122842', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CLAUDIO', 'MILLAN IBARRA', 'meridamillan@prodigy.net.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'CALLEJON BENJAMIN HILL 60, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('119', '00088465', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE ANGEL', 'AGUIRRE VAZQUEZ', 'JOSE.AGUIRRE210457@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'GUILLERMO ARREOLA 243, OLIVARES', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('120', '00123462', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ENRIQUE', 'ESQUEDA DE DOMINICS', 'INMCCDE@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'CALLE DE LA REFORMA 237, CENTENARIO', 'HERMOSILLO', 'SONORA', '83260', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('121', '00117935', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CARLOS RAMON', 'CORONADO AMEZCUA', 'CETYACONSTRUCCIONES@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA MARTIN DE VALENCIA 647, PROGRESISTA', 'HERMOSILLO', 'SONORA', '83120', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('122', '00126306', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ROBERTO', 'NORIEGA SOTO', 'RNS@CLIMA3.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVE. ALFREDO EGUIARTE 90, JESUS GARCIA', 'HERMOSILLO', 'SONORA', '83140', 'MX', '', '', '2019-09-26 12:29:10', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('123', '00126440', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'IDELFONSO', 'MANCILLAS TREJO', 'IMT_CONSTRUCCION@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'PAPALOTE 47, HACIENDAS', 'HERMOSILLO', 'SONORA', '83105', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('124', '00121014', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MAXIMILIANO', 'FRAGOSO GUERRERO', 'maxfregoso@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AGUA FRIA 22, SANTA FE', 'HERMOSILLO', 'SONORA', '83249', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('125', '00093742', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FRANCISCO JAVIER', 'BUSTILLOS', 'MACONTR02@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', '\tJOSE CARMELO 141, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('126', '00126308', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'LUIS', 'MIRANDA ROMERO\t', 'INDICO.HMO@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'TEPIC 44, PALO VERDE', 'HERMOSILLO', 'SONORA', '83280', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('127', '00117335', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'GERMAN', 'MARTINEZ GOMEZ', 'GERMANING1223@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'IXTLAXIHUATL 75, Y GRIEGA', 'HERMOSILLO', 'SONORA', '83290', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('128', '00090070', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FRANCISCO JAVIER', 'SALAZAR TRUJILLO', 'CONSTRUCTORA_MUSARO@YAHOO.COM.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'CALLEJON JOSE MARIA MENDOZA 961, EL SAHUARO', 'HERMOSILLO', 'SONORA', '83170', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('129', '00074180', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ALBERTO', 'SANCHEZ ENCINAS', 'alberto_sanchez_encinas@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'SAN FELIPE 123, SAN ANGEL', 'HERMOSILLO', 'SONORA', '83287', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('130', '00126449', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'NORIDA', 'OIVAS PEREZ', 'DIRECCION.NORIGA@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'JUAN ESCUTIA 29, LET ECHEVERRIA', 'VILLA JUAREZ', 'SONORA', '85290', 'MX', '', '', '2019-09-19 15:00:39', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('131', '00123167', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'HECTOR', 'CUELLAR', 'hector.cuellar@panelrex.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'GUADALUPE VICTORIA 121, SAN BENITO', 'HERMOSILLO', '\tSONORA', '83190', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('132', '00118482', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'GERMAN CERVANTES', 'JOSE JUAN', 'TONAHUACGT@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'JOHN F KENNEDY 61 D2, SAN JUAN', 'HERMOSILLO', 'SONORA', '83070', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('133', '00122798', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE EDGARDO', 'ANGUIS CARRILLO', '\tEDGARDO_ANGUIS@ESPACIODU.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'GUADALUPE VICTORIA 10 INT.3, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('134', '00126169', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'AHMED ALI', 'ABDELGHANY', 'alfaraenaconstrucciones@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'PODER LEGISLATIVO 140, MISION DEL REAL', 'HERMOSILLO', 'SONORA', '83100', 'MX', '', '', '2019-09-26 13:14:40', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('135', '00126490', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'NORMA', 'DIAZ MORALES', 'arquinfra@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'CIRCUITO LOMA ALTA 89, PRIVADA LAS LOMAS', 'HERMOSILLO', 'SONORA', '83293', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('136', '00126811', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FRANCISCA', 'VAZQUEZ TRILLAS', 'franciscavazqueztrillas@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'FRACC SIERRA BLANCA 78, FRACC SIERRA BLANCA', 'HERMOSILLO', 'SONORA', '83148', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('137', '00086554', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JAIME WENCESLAO', 'PARRA MOROYOQUI', 'jaimeparramo@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'BVLD. LOS ALAMOS 402, SIERRA VISTA', 'NOGALES', 'SONORA', '84085', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('138', '00120288', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JUAN', 'MENDOZA CASTRO', 'JUANMENDOZA1@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'QUINTANA ROO 727 NTE. noroeste', 'CAJEME', 'SONORA', '85100', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('139', '00038683', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'SALVADOR', 'MARTINEZ VILLARDAGA', 'SALMARVI@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'COAHUILA 816 SUR, CENTRO', 'CD. OBREGON', 'SONORA', '85000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('140', '00084620', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ALEXIS', 'MARES AGUAYO', 'MARAGUACONSTRUCCIONES@YAHOO.COM.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'sosa chavez 409 deportiva', 'NAVOJOA', 'SONORA', '85860', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('141', '00117817', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RAFAEL', 'VALENZUELA SALAS', 'EPMOCELICK@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'ARIZONA 333 BALDERRAMA', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('142', '00072434', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JUAN PEDRO', 'TOBIN VALENZUELA', 'recepcion@dtnmex.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'VILLA MARIA 10, VILLA SOL', 'HERMOSILLO', 'SONORA', '83240', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('143', '00126610', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CARLOS RODOLFO', 'LIMON PEREZ', 'CRLIMON@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'GUADALUPE VICTORIA 143 C SAN BENITO', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '2019-09-26 13:37:06', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('144', '00055103', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FRANCISCO JAVIER', 'TOLEDO RUIZ', 'javier_toledoruiz@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'MADERO 184, CENTRO', 'CAJEME', 'SONORA', '85210', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('145', '00126852', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RAFAEL', 'QUINTERO LOPEZ', 'acclaboratorio@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA LIBERTAD Y CALLE 13 1210 RESIDENCIAS', 'SAN LUIS RIO COLORADO', 'SONORA', '83448', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('146', '00118132', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ROSA MARIEL', 'VAZQUEZ LEON', 'marielit@msn.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'PASEO SANTA FE 57, SANTA FE', 'HERMOSILLO', 'SONORA', '83249', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('147', '00126860', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JUDITH', 'SPENCER VALENZUELA', 'spencervlza@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'PUERTO VALLARTA 4, CAFE COMBATE', 'HERMOSILLO', 'SONORA', '83165', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('148', '00126882', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RODRIGO', 'APONTE ESPINOSA', 'procodesa.construcciones@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'PASEO DEL PRADO 39 VALLE GRANDE', 'HERMOSILLO', 'SONORA', '83205', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('149', '00117820', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JUAN CARLOS', 'ENCINAS SOTO', 'INM.SOCE@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'DR. PALIZA 78, CENTENARIO', 'HERMOSILLO', 'SONORA', '83260', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('150', '00100624', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FLORENCIO', 'FELIX PEÑA', 'SCULLGROUP@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'LUIS ORCI 472 CENTRO', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('151', '00126759', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ALFONSO', 'VALENZUELA FIGUEROA', 'JAVALENZUELA@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'MISIÓN DE OQUITOA 19, FRACCIONAMIENTO AMANECER DE KINO', 'MAGDALENA DE KINO', 'SONORA', '84160', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('152', '00101677', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'HECTOR JAVIER', 'HERRERA BOURS', 'TERPLAN_OBREGON@YAHOO.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'VERACRUZ 467 LOCAL 3 ZONA NORTE', 'OBREGON', 'SONORA', '85010', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('153', '00117115', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'NORMA ROSALVA', 'ANAYA ROJAS', 'arsagregados@yahoo.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('154', '00090774', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'GPE YALIA', 'SALIDO IBARRA', 'const.sayg@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'NAVOJOA', 'SONORA', '85860', 'MX', '', '', '2019-09-26 14:11:13', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('155', '00114958', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'TADEO', 'BARREDA PALACIOS', 'administracion@bapco.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-26 14:36:37', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('156', '00067461', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CARLOS RAMON', 'CORNADO AMEZCUA', 'cpitic@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'GUADALUPE VICTORIA 143 C SAN BENITO', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('157', '00034642', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RAMON ROLANDO', 'GARCIA SALAZAR', 'constructoragaraco84@yahoo.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-26 15:06:41', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('158', '00101256', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RUSSELL FEDERICO', 'CORELLA GRENFELL', 'russell_57mx@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('159', '00117041', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'REYES', 'SOTO BELTRAN', 'coaxiro@yahoo.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85210', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('160', '00120826', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'DAVID FRANCISCO', 'HUICOSA ISLAS', 'cosco.obregon@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85210', 'MX', '', '', '2019-09-26 15:17:35', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('161', '00114347', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'YESSICA MARIA', 'TORRES COTA', 'administracion@electroinsumos.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('162', '00117753', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'DAGOBERTO', 'RODRIGUEZ KIRKBRIDE', 'admon.drk@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85210', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('163', '00125114', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RICARDO', 'GARZA CON', 'garza012013@outlook.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83001', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('164', '00126414', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE ROBERTO', 'GALINDO FIERRO', 'GALINDO_FIERRO@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('165', '00126461', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MARTIN', 'GUTIERREZ VAZQUEZ', 'REZ@GYGCONSTRUCTORES.COM.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '8300', 'MX', '', '', '2019-09-26 15:28:07', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('166', '00095466', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ROBETO', 'NORIEGA SOTO', 'RNS@CLIMA3.MX', '0', '0', '0', '2019-09-18 03:00:00', '', 'AVENIDA ALFREDO EGUIARTE COL. JESUS GARCÍA', 'HERMOSILLO', 'SONORA', '83140', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('167', '00125435', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ALFONSO ARTURO', 'LABORIN MARQUEZ', 'gujaconstructores@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-26 15:39:32', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('168', '00101151', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FERNANDO', 'GUTIERREZ COHEN', 'cmiramarsadecv@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('169', '00125112', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MIGUEL ANGEL', 'RUELAS VEGA', 'miguelruelas@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85210', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('170', '00126669', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ISRAEL', 'TOLANO CHACON', 'miguelruelas@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85210', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('171', '00098409', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE SEPCHEM', 'ARMENTA NAVA', 'josearmenta70@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('172', '00118567', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE MARIA', 'QUINTANA ALVAREZ', 'INGQUINTANA@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('173', '00112477', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'IBAN', 'GARCIA MADRID', 'contacto@lacosi.com.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-26 15:54:16', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('174', '00126643', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'LAURA MARIA', 'CAMARGO ROJO', 'LCAMARGOROJO@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('175', '00126937', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JIMM', 'LEDEZMA ANDRADE', 'jimm.ledezma@lecta.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('176', '00087402', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MANUEL ROGELIO', 'GONZALEZ FERRA MARTINEZ', 'ROGER_G14@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('177', '00091150', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JESUS ENRIQUE', 'AROS OLAJE', 'mtp1@outlook.es', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('178', '00123162', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CLAUDIA ELIZABETH', 'VALDEZ MARTINEZ', 'osmanindustrial@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85210', 'MX', '', '', '2019-09-26 17:02:56', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('179', '00119381', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JORGE ALBERTO', 'MEDINA CAMPILLO', 'elgudo@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('180', '00073309', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'SALVADOR', 'CASTELLANOS', 'eypsancarlos@yahoo.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'GUAYMAS', 'SONORA', '85465', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('181', '00104797', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ALEJANDRO', 'PUEBLA GUTIERREZ', 'pueblaarquitectos@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-26 17:15:56', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('182', '00118303', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE RAMON', 'MENDIVIL QUIJADA', 'RMSISTEMASCONSTRUCTIVOS@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HUATABAMPO', 'SONORA', '85900', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('183', '00104550', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ROCIO PEREZ', 'RUBIO GUTIERREZ', 'rocio-robles84@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-26 17:25:51', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('184', '00124882', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'MARCO ANTONIO', 'CALDERON GAMEZ', 'cortinas.cmh@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('185', '00115755', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'CONRADO', 'SANCHEZ DUARTE', 'conradosanchezd@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', '\tSONORA', '85210', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('186', '00127070', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'LYDIA SOFIA', 'MENESES MERCADO', 'somedesarrollos@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('187', '00115814', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'LUIS ARTURO', 'GUAJARDO DE LA CRUZ', 'temarqsa@gmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'CAJEME', 'SONORA', '85210', 'MX', '', '', '2019-09-26 17:31:24', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('188', '00094925', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RODRIGO ALBERTO', 'PEÑA PORCHAS', 'administracion@grupovalpo.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'HERMOSILLO', 'SONORA', '83000', 'MX', '', '', '2019-09-26 17:35:47', '187.189.34.154', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('189', '00064774', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RICARDO', 'MARTINEZ TERRAZAS', 'rmartinez@xxiconstrucciones.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'DOMICILIO CONOCIDO', 'NAVOJOA', 'SONORA', '85860', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('190', '00123527', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RAUL MANUEL', 'TERAN SALCIDO', 'electrosupplier@prodigy.net.mx', '0', '0', '0', '2019-09-18 03:00:00', '', 'OTHON ALMADA 230, BALDERRAMA', 'HERMOSILLO', 'SONORA', '83180', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('191', '00088709', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'RAUL RENE', 'TREJO CHAVIRA', 'ing_javitre@hotmail.com', '0', '0', '0', '2019-09-18 03:00:00', '', 'AGUASCALIENTES 158 1, SAN BENITO', 'HERMOSILLO', 'SONORA', '83190', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('192', '00126539', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'JOSE DE JESUS', 'RUBIO MORALES', 'RUBIO_SIEMS@HOTMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'NAVOJOA 1144, CAMINO REAL', 'HERMOSILLO', 'SONORA', '83178', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('193', '00126460', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'FRANCISCO JAVIER', 'CORONADO CASTRO', 'FCOJCORONADOCASTRO@GMAIL.COM', '0', '0', '0', '2019-09-18 03:00:00', '', 'CAÑADA 2213, LAS MISIONES', 'CAJEME', 'SONORA', '85099', 'MX', '', '', '0000-00-00 00:00:00', '', '2', 'y');
INSERT INTO `users` (`id`, `username`, `password`, `fname`, `lname`, `email`, `newsletter`, `cookie_id`, `token`, `created`, `avatar`, `address`, `city`, `state`, `zip`, `country`, `notes`, `info`, `lastlogin`, `lastip`, `userlevel`, `active`) VALUES ('226', 'ra_saralegui', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'Roberto', 'Saralegui', 'rasaralegui@example.com', '0', '0', '0', '2019-10-02 14:24:57', '', '', '', '', '', '', '', '', '2019-10-02 14:25:26', '187.189.34.154', '9', 'y');


