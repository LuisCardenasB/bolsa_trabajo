<?php
  if (!defined("_VALID_PHP"))
      die('Direct access to this location is not allowed.');
?>
<?php
  $sname = $_SERVER["SCRIPT_NAME"];
  $pages = array(
      '404',
      'account',
      'activate',
      'add-job',
      'add-resume',
      'bookmarks',
      'browse-categories',
      'browse-jobs',
      'browse-resumes',
      'checkout',
      'company-settings',
      'company',
      'content',
      'invoices',
      'job',
      'login',
      'logout',
      'maintenance',
      'manage-applications',
      'manage-jobs',
      'messages',
      'profile',
      'register',
      'resume',
      'search',
      'summary',
      'tags',
      'view',
      );
  $regexp = '#' . implode('|', $pages) . '#';
  $pages = preg_match($regexp, $sname, $matches) ? $matches[0] : '';
  $html = '';

  switch ($pages) {

      case "404":
          $html =  'Not Found';
          break;

      case "account":
          $html =  Lang::$word->_UA_MYACC;
          break;

      case "activate":
          print Lang::$word->_UA_TITLE3;
          break;

      case "add-job":
          print "Publicar empleo";
          break;

      case "add-resume":
          print "Administrar / Actualizar currículum";
          break;

      case "bookmarks":
          print "Administrar marcadores";
          break;

      case "browse-categories":
          print "Categorías de trabajo";
          break;

      case "browse-jobs":
          print "Buscar empleos";
          break;

      case "browse-resumes":
          print "Buscar currículums";
          break;

      case "checkout":
          $html = Lang::$word->CKO_TITLE;
          break;

      case "company-settings":
          $html = "Configuraciones de la compañía";
          break;

      case "company":
          $html = "Detalles de la compañía";
          break;

      case "content":
          $html = ($row) ? $row->title : "";
          break;

      case "invoices":
          $html = 'Facturas';
          break;

      case "job":
          $html = 'Detalles del trabajo';
          break;

      case "login":
          $html = Lang::$word->_UA_TITLE;
          break;

      case "logout":
          $html = 'Cerrar sesión';
          break;

      case "maintenance":
          $html = 'Modo de mantenimiento';
          break;

      case "manage-applications":
          $html = 'Administrar aplicaciones';
          break;

      case "manage-jobs":
          $html = 'Administrar puestos de trabajo';
          break;

      case "messages":
          $html = 'Mensajes instantáneos';
          break;

      case "profile":
          $html = Lang::$word->_UA_TITLE4;
          break;

      case "register":
          $html = Lang::$word->_UA_TITLE2;
          break;

      case "resume":
          $html = "Detalles del currículum";
          break;

      case "search":
          $html = Lang::$word->FSRC_TITLE;
          break;

      case "summary":
          $html = Lang::$word->SMY_TITLE;
          break;

      default:
		  $html = '';
          break;

  }

  //print '<div class="active section">' . $html . '</div>';
  return $html;
?>
