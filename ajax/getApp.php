<?php
  define("_VALID_PHP", true);
  require_once("../init.php");
  require_once("../lib/class_db.php");
 
?>
<?php
	  $q = "SELECT * FROM jobs as j WHERE j.expire_date >= CURDATE() AND j.status='approved' AND j.active=1";
      $qr = $db->query($q);
      $counter = $db->numrows($qr);

      $pager = Paginator::instance();
	  $pager->items_total = $counter;
	  $pager->default_ipp = Registry::get("Core")->perpage;
	  $pager->paginate();
      
      $sql = "SELECT j.id,j.title,l.name,t.name,s.name,j.description,j.salary,c.name,c.avatar FROM jobs as j 
      INNER JOIN companies as c ON j.company = c.uid
      INNER JOIN job_locations as l ON j.location = l.id
      INNER JOIN job_types as t ON j.type = t.id
      INNER JOIN job_skills as s ON j.skills = s.id
      WHERE j.status='approved'  AND j.active=1 " . $pager->limit;
      $row = $db->fetch_all($sql);
      
      $row[0]->description = addslashes(htmlspecialchars_decode($row[0]->description));
      $row[0]->description = strip_tags($row[0]->description);
      
      $data = json_encode($row);

      print $data;
?>